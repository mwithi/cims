# -*- coding: utf-8 -*-
{
	'name': 'ISF Monthly Key Data Report',
	'version': '0.1',
	'category': 'Tools',
	'description': 
"""
ISF Monthly Key Data Report 
===========================

* Produces a new report with Incomes and Expenses re-classification (to be configured)
* Configuration section in Accounting \ Configuration \ Financial Reports \ IS Monthly Key Data

The module allows to define:

- Incomes section
- Expenditure section
- Depreciation section

both with accounts and sub-section as well. It then calculates for each period of the choosen Fiscal Year: 

- Monthly summaries 
- Differences with forecasting totals (to be inputed)

The report also warns the user if some accounts are used in more than one section.
""",
	'author': 'Alessandro Domanico (alessandro.domanico@informaticisenzafrontiere.org)',
	'website': 'www.informaticisenzafrontiere.org',
	'license': 'AGPL-3',
	'depends': ['base_headers_webkit','account_accountant','oe_excel_report'],
	'data': [
		'isf_monthly_key_data_report.xml',
        'wizard/isf_monthly_key_data_report_view.xml',
        'security/ir.model.access.csv',
    ],
	'demo': [],
	'installable' : True,
}

