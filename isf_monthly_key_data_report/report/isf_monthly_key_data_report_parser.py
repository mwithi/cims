# -*- coding: utf-8 -*-
import time
import pprint
from datetime import datetime

from openerp.report import report_sxw
from collections import OrderedDict
import logging
import locale
import platform
import os
import pycountry
from decimal import Decimal

_logger = logging.getLogger(__name__)
_debug = False
_debug_lang = False
pp = pprint.PrettyPrinter(indent=4)

class isf_monthly_key_data_report_parser(report_sxw.rml_parse):
    _name = 'report.isf.monthly.key.data.webkit'
    
    def __init__(self, cr, uid, name, context=None):
        if _debug:
            _logger.debug('==> in init: %s', __name__)
            
        super(isf_monthly_key_data_report_parser, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
            'pp': pp,
            'lines': self.lines,
            'datelines' : self.datelines,
            'months_names' : self.months_names,
            'excluded' : self.excluded,
            'duplicated' : self.duplicated,
        })
        self.context = context
        self.result_date = []
        self.result_months = []
        self.result_values = []
        self.result_excluded = []
        self.result_duplicated = []
        
    def duplicated(self, ids=None, done=None):
        return self.result_duplicated
        
    def excluded(self, ids=None, done=None):
        return self.result_excluded
        
    def datelines(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.monthly.key.data.report.view')
        report_fiscal_year = obj_report.read(self.cr, self.uid, ctx['active_id'], ['fiscalyear_id'])
        report_divisor = obj_report.read(self.cr, self.uid, ctx['active_id'], ['divisor'])
        report_target_move = obj_report.read(self.cr, self.uid, ctx['active_id'], ['target_move'])
        report_detail = obj_report.read(self.cr, self.uid, ctx['active_id'], ['detail'])
        users = self.pool.get('res.users').browse(self.cr, self.uid, self.uid, context=None)
        company_id = users.company_id.id
        currency = users.company_id.currency_id
        
        fiscal_year = report_fiscal_year['fiscalyear_id']
        previous_label = 'Previous'
        previous_fiscal_year_id = self.pool.get('account.fiscalyear').search(self.cr, self.uid, [('id','<',fiscal_year[0])], order='id desc', limit=1)
        
        try:
            previous_fiscal_year = self.pool.get('account.fiscalyear').browse(self.cr, self.uid, previous_fiscal_year_id)[0]
            if _debug:
                _logger.debug('fiscal_year: %s', fiscal_year)
                _logger.debug('previous_fiscal_year: %s', previous_fiscal_year)
            previous_label = previous_label + ' ' + previous_fiscal_year.name
        except Exception, e:
            _logger.debug('previous_fiscal_year: %s', 'no previous fiscal year')
            
        divisor = report_divisor['divisor']
        target_move = report_target_move['target_move']
        detail = report_detail['detail'] == 'detail'
        
        if _debug:
            _logger.debug('fiscal_year: %s', fiscal_year)
            _logger.debug('divisor: %s', divisor)
            _logger.debug('target_move: %s', target_move)
            _logger.debug('detail: %s', detail)
        
        res = {
            'fiscal_year' : fiscal_year[1],
            'previous_fy' : previous_label,
            'divisor' : divisor,
            'currency' : currency.name,
            'warning' : False,
            'target' : target_move,
            'detail' : detail,
        }
        
        self.result_date.append(res)
        
        return self.result_date
    
    def months_names(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_period = self.pool.get('account.period')
        obj_report = self.pool.get('isf.monthly.key.data.report.view')
        report_fiscal_year = obj_report.read(self.cr, self.uid, ctx['active_id'], ['fiscalyear_id'])
        fiscal_year = report_fiscal_year['fiscalyear_id']
        
        period_ids = obj_period.search(self.cr, self.uid, [('fiscalyear_id','=',fiscal_year[0]),('special','=',False)])
        periods = obj_period.browse(self.cr, self.uid, period_ids)
        
        def date_format_encoding():
            return locale.getlocale(locale.LC_TIME)[1] or locale.getpreferredencoding()
        
        try:
            current_user = self.pool.get('res.users').browse(self.cr, self.uid, self.uid)
            user_lang = current_user.lang
            lang_id = self.pool.get('res.lang').search(self.cr, self.uid, [('code','=',user_lang)])[0]
            lang = self.pool.get('res.lang').browse(self.cr, self.uid, [lang_id])[0]
            code = lang.iso_code or lang.code or user_lang
            language = pycountry.languages.lookup(str(code[:2])) #needs pycountry=18.5.26
            if _debug_lang:
                _logger.debug('user_lang : %s', user_lang)
                _logger.debug('lang : %s', lang.code)
                _logger.debug('lang (ISO) : %s', lang.iso_code)
                _logger.debug('setting... => %s', language.alpha_2)
                _logger.debug('LC_TIME or default: %s', date_format_encoding())
                
            if os.name == "posix":
                locale.setlocale(locale.LC_ALL, str(lang.iso_code))
            else:
                locale.setlocale(locale.LC_ALL, str(language.name))
        except Exception, e:
            if _debug_lang:
                _logger.debug('Exception : %s', e)
            _logger.debug('Please set the ISO Code properly in Languages!')
        
        # Get Months names for .mako report 
        for period in periods:
            date = datetime.strptime(period.date_start, '%Y-%m-%d')
            month = date.strftime("%b").decode(date_format_encoding()).encode('utf-8')
            self.result_months.append(month)
            if _debug:
                _logger.debug('period_obj : %s', period)
                _logger.debug('period_id %s : %s', period.id, month)
                
        return self.result_months
        
        
    def lines(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_move = self.pool.get('account.move')
        obj_move_line = self.pool.get('account.move.line')
        obj_account = self.pool.get('account.account')
        obj_report = self.pool.get('isf.monthly.key.data.report.view')
        obj_period = self.pool.get('account.period')
        obj_fiscal_year = self.pool.get('account.fiscalyear')
        report_fiscal_year = obj_report.read(self.cr, self.uid, ctx['active_id'], ['fiscalyear_id'])
        report_divisor = obj_report.read(self.cr, self.uid, ctx['active_id'], ['divisor'])
        report_target_move = obj_report.read(self.cr, self.uid, ctx['active_id'], ['target_move'])
        
        fiscal_year = report_fiscal_year['fiscalyear_id']
        previous_fiscal_year_ids = self.pool.get('account.fiscalyear').search(self.cr, self.uid, [('id','<',fiscal_year[0])], order='id desc', limit=1)
        previous_fiscal_year_id = 0
        try:
            previous_fiscal_year = self.pool.get('account.fiscalyear').browse(self.cr, self.uid, previous_fiscal_year_ids)[0]
            previous_fiscal_year_id = previous_fiscal_year.id
            if _debug:
                _logger.debug('fiscal_year: %s', fiscal_year)
                _logger.debug('previous_fiscal_year: %s', previous_fiscal_year)
        except Exception, e:
            _logger.debug('previous_fiscal_year: %s', 'no previous fiscal year')
        
        divisor = Decimal(str(report_divisor['divisor']))
        target_move = report_target_move['target_move']
        
        #report mapping
        map = {}
        
        #cash and bank accounts
        cash_and_bank_accounts_ids = self.pool.get('account.account').search(self.cr, self.uid, [('type','=','liquidity')], context=None)
        cash_and_bank_accounts = self.pool.get('account.account').browse(self.cr, self.uid, cash_and_bank_accounts_ids, context=None)
        
        #cashin accounts
        cashin_account_ids = []
        cashin_report_ids = self.pool.get('isf.monthly.key.data.report').search(self.cr, self.uid, [('type','=','cashin')])
        for report in self.pool.get('isf.monthly.key.data.report').browse(self.cr, self.uid, cashin_report_ids, context=None):
            for account in report.account_ids:
                cashin_account_ids.append(account.id)
                #If account.id is already mapped, it will raise a Warning
                if map.has_key(account.id):
                    note = "\"" + map[account.id] + "\" and \"" + report.name_total + "\""
                    self.result_duplicated.append({'account_code' : account.code, 'ref' : account.name, 'note' : note})
                    self.result_date[0].update({ 'warning' : True })
                map[account.id] = report.name_total
                    
        #cashout accounts
        cashout_account_ids = []
        cashout_report_ids = self.pool.get('isf.monthly.key.data.report').search(self.cr, self.uid, [('type','=','cashout')])
        for report in self.pool.get('isf.monthly.key.data.report').browse(self.cr, self.uid, cashout_report_ids, context=None):
            for account in report.account_ids:
                cashout_account_ids.append(account.id)
                #If account.id is already mapped, it will raise a Warning
                if map.has_key(account.id):
                    note = "\"" + map[account.id] + "\" and \"" + report.name_total + "\""
                    self.result_duplicated.append({'account_code' : account.code, 'ref' : account.name, 'note' : note})
                    self.result_date[0].update({ 'warning' : True })
                map[account.id] = report.name_total
                
        #depreciations accounts
        depreciations_account_ids = []
        depreciations_account_ids = self.pool.get('isf.monthly.key.data.report').search(self.cr, self.uid, [('type','=','depreciations')])
        for report in self.pool.get('isf.monthly.key.data.report').browse(self.cr, self.uid, depreciations_account_ids, context=None):
            for account in report.account_ids:
                depreciations_account_ids.append(account.id)
                #If account.id is already mapped, it will raise a Warning
                if map.has_key(account.id):
                    note = "\"" + map[account.id] + "\" and \"" + report.name_total + "\""
                    self.result_duplicated.append({'account_code' : account.code, 'ref' : account.name, 'note' : note})
                    self.result_date[0].update({ 'warning' : True })
                map[account.id] = report.name_total
        
        if _debug:    
            _logger.debug('cash_and_bank_accounts_ids : %s', cash_and_bank_accounts_ids)
            _logger.debug('cashin_account_ids : %s', cashin_account_ids)
            _logger.debug('cashout_account_ids : %s', cashout_account_ids)
            _logger.debug('depreciations_account_ids : %s', depreciations_account_ids)
            
        opening_period_id = obj_period.search(self.cr, self.uid, [('fiscalyear_id','=',fiscal_year[0]),('special','=',True)])
        if _debug:
            _logger.debug('Opening Period id : %s', opening_period_id)
        
        period_ids = obj_period.search(self.cr, self.uid, [('fiscalyear_id','=',fiscal_year[0]),('special','=',False)])
        periods = obj_period.browse(self.cr, self.uid, period_ids)
        if _debug:
            _logger.debug('period_ids : %s', period_ids)
        
        period_fy = obj_fiscal_year.browse(self.cr, self.uid, fiscal_year[0])
        period_previous_fiscal_year = self._get_period_object('0')
        period_fiscal_year = self._get_period_object('13')
        
        previous_period_ids = obj_period.search(self.cr, self.uid, [('fiscalyear_id','=',previous_fiscal_year_id),('special','=',False)])
        if previous_period_ids:
            cashin_flow = self.pool.get('isf.monthly.key.data.report').search(self.cr, self.uid, [('type','=','cashin'),('parent_id','=',False)])
            for report in self.pool.get('isf.monthly.key.data.report').browse(self.cr, self.uid, cashin_flow, context=None):
                account_ids = [account.id for account in report.account_ids]
                accounts_query = obj_account.read(
                        self.cr,
                        self.uid,
                        account_ids,
                        ['type', 'code', 'name', 'debit', 'credit', 'balance', 'parent_id', 'level', 'child_id'],
                        context={'all_fiscalyear': True,
                         'periods': previous_period_ids})
                for account in accounts_query:
                    period_previous_fiscal_year[str(account['code'])]['value'] = report.sign * Decimal(str(account['balance']))
                    period_previous_fiscal_year['total_' + report.name_total]['value'] += report.sign * Decimal(str(account['balance']))
                    period_previous_fiscal_year['total_cash_in_flows']['value'] += report.sign * Decimal(str(account['balance']))
            
            cashout_flow = self.pool.get('isf.monthly.key.data.report').search(self.cr, self.uid, [('type','=','cashout'),('parent_id','=',False)])
            for report in self.pool.get('isf.monthly.key.data.report').browse(self.cr, self.uid, cashout_flow, context=None):
                account_ids = [account.id for account in report.account_ids]
                accounts_query = obj_account.read(
                        self.cr,
                        self.uid,
                        account_ids,
                        ['type', 'code', 'name', 'debit', 'credit', 'balance', 'parent_id', 'level', 'child_id'],
                        context={'all_fiscalyear': True,
                         'periods': previous_period_ids})
                for account in accounts_query:
                    period_previous_fiscal_year[str(account['code'])]['value'] = report.sign * Decimal(str(account['balance']))
                    period_previous_fiscal_year['total_' + report.name_total]['value'] += report.sign * Decimal(str(account['balance']))
                    period_previous_fiscal_year['total_cash_out_flows']['value'] += report.sign * Decimal(str(account['balance']))
            
            depreciation_flow = self.pool.get('isf.monthly.key.data.report').search(self.cr, self.uid, [('type','=','depreciations'),('parent_id','=',False)])
            for report in self.pool.get('isf.monthly.key.data.report').browse(self.cr, self.uid, depreciation_flow, context=None):
                account_ids = [account.id for account in report.account_ids]
                accounts_query = obj_account.read(
                        self.cr,
                        self.uid,
                        account_ids,
                        ['type', 'code', 'name', 'debit', 'credit', 'balance', 'parent_id', 'level', 'child_id'],
                        context={'all_fiscalyear': True,
                         'periods': previous_period_ids})
                for account in accounts_query:
                    period_previous_fiscal_year[str(account['code'])]['value'] = report.sign * Decimal(str(account['balance']))
                    period_previous_fiscal_year['total_' + report.name_total]['value'] += report.sign * Decimal(str(account['balance']))
                    period_previous_fiscal_year['total_depreciations']['value'] += report.sign * Decimal(str(account['balance']))
        
            
        if _debug:
            _logger.debug('==> period_previous_fiscal_year : %s', period_previous_fiscal_year)
        result_periods = []
        result_periods.append(period_previous_fiscal_year)
        
        previous_period = {}
        for p in periods:
            # save previous period, if any
            if len(result_periods) > 0:
                previous_period = result_periods[-1]
            
            # start new period
            period = self._get_period_object(p.id)
            
            # get only period move lines
            all_account_ids = []
            all_account_ids.append(cashin_account_ids)
            all_account_ids.append(cashout_account_ids)
            all_account_ids.append(depreciations_account_ids)
            move_lines_ids = obj_move_line.search(self.cr, self.uid, [('period_id','=',p.id),('account_id','in',cashin_account_ids+cashout_account_ids+depreciations_account_ids)])
            move_lines = obj_move_line.browse(self.cr, self.uid, move_lines_ids)
            if not isinstance(move_lines, list):
                move_lines = [move_lines]
            move_ids = []
            for move_line in move_lines:
                move_id = move_line.move_id.id
                state = move_line.move_id.state
                if target_move == 'all':
                    if move_id not in move_ids:
                        move_ids.append(move_id)
                else:
                    if state == 'posted' and move_id not in move_ids:
                        move_ids.append(move_id)
            move_lines_ids = obj_move_line.search(self.cr, self.uid, [('move_id','in',move_ids)])
            if _debug:
                _logger.debug('selected move_ids [period %s]: %s', p.id, move_ids)
                _logger.debug('selected move_lines_ids [period %s]: %s', p.id, move_lines_ids)
                
            move_lines = obj_move_line.browse(self.cr, self.uid, move_lines_ids)
            for line in move_lines:
                account_id = line.account_id.id
                account_name = line.account_id.name
                account_code = line.account_id.code
                state = line.move_id.state
                credit_value = Decimal(str(line.credit)) - Decimal(str(line.debit))
                debit_value = Decimal(str(line.debit)) - Decimal(str(line.credit))
                period_id = line.period_id.id
                
                try:
                    
                    #Test mapping:
                    #If account_code not mapped, it will raise an Exception
                    period[str(account_code)]
                    
                    #posted or not posted
                    if target_move == 'all' and state == 'draft':
                        period[str(account_code)]['posted'] = False
                    
                    # Cash in flows / receipts
                    if account_id in cashin_account_ids:
                        period[str(account_code)]['value'] += Decimal(str(credit_value))
                        period_fiscal_year[str(account_code)]['value'] += Decimal(str(credit_value))
                        report_name = map[account_id]
                        period['total_' +  report_name]['value'] += Decimal(str(credit_value))
                        period['total_' +  report_name]['posted'] &= period[str(account_code)]['posted']
                        period_fiscal_year['total_' + report_name]['value'] += Decimal(str(credit_value))
                        period_fiscal_year['total_' + report_name]['posted'] &= period[str(account_code)]['posted']
                    
                    # Cash out flows / disbursement
                    elif account_id in cashout_account_ids:
                        period[str(account_code)]['value'] += Decimal(str(debit_value))
                        period_fiscal_year[str(account_code)]['value'] += Decimal(str(debit_value))
                        report_name = map[account_id]
                        period['total_' +  report_name]['value'] += Decimal(str(debit_value))
                        period['total_' +  report_name]['posted'] &= period[str(account_code)]['posted']
                        period_fiscal_year['total_' + report_name]['value'] += Decimal(str(debit_value))
                        period_fiscal_year['total_' + report_name]['posted'] &= period[str(account_code)]['posted']
                        
                    # Depreciations
                    elif account_id in depreciations_account_ids:
                        period[str(account_code)]['value'] += Decimal(str(debit_value))
                        period_fiscal_year[str(account_code)]['value'] += Decimal(str(debit_value))
                        report_name = map[account_id]
                        period['total_' +  report_name]['value'] += Decimal(str(debit_value))
                        period['total_' +  report_name]['posted'] &= period[str(account_code)]['posted']
                        period_fiscal_year['total_' + report_name]['value'] += Decimal(str(debit_value))
                        period_fiscal_year['total_' + report_name]['posted'] &= period[str(account_code)]['posted']
                
                except Exception, e:
                    if _debug:
                        #logging.exception(e) # or pass an error message, see comment
                        _logger.debug('Exception : %s', e)
                        _logger.debug('Not mapped account : %s', account_code)
                    if account_code not in [d['account_code'] for d in self.result_excluded]:
                        self.result_excluded.append({'account_code' : account_code, 'ref' : line.move_id.name, 'note' : line.move_id.ref})
                    self.result_date[0].update({ 'warning' : True })
            
            #Total Cash in flows
            period['total_cash_in_flows']['value'] = Decimal(str(0))
            period_fiscal_year['total_cash_in_flows']['value'] = Decimal(str(0))
            #period_previous_fiscal_year['total_cash_in_flows']['value'] = Decimal(str(0))
            cashin_flow = self.pool.get('isf.monthly.key.data.report').search(self.cr, self.uid, [('type','=','cashin'),('parent_id','=',False)])
            for report in self.pool.get('isf.monthly.key.data.report').browse(self.cr, self.uid, cashin_flow, context=None):
                #Since the first sections have been already summarized from related accounts, we recalculate only the sections with subsections (children)
                children = self.pool.get('isf.monthly.key.data.report')._get_children_by_order(self.cr, self.uid, [report.id], context=None)
                children = children[1:] #skip the parent itself
                if len(children) > 0:
                    period['total_' + report.name_total]['value'] += Decimal(str(self._get_report_total_recursive(p.id, ids, report, period, context=None)))
                    period_fiscal_year['total_' + report.name_total]['value'] += Decimal(str(self._get_report_total_recursive(p.id, ids, report, period, context=None)))
                #Summarized all first sections
                period['total_cash_in_flows']['value'] += Decimal(str(period['total_' + report.name_total]['value']))
                period['total_cash_in_flows']['posted'] &= period['total_' + report.name_total]['posted']
                period_fiscal_year['total_cash_in_flows']['value'] += Decimal(str(period_fiscal_year['total_' + report.name_total]['value']))
                #period_previous_fiscal_year['total_cash_in_flows']['value'] += Decimal(str(period_fiscal_year['total_' + report.name_total]['value']))
                
            #Total Cash out flows
            period['total_cash_out_flows']['value'] = Decimal(str(0))
            period_fiscal_year['total_cash_out_flows']['value'] = Decimal(str(0))
            #period_previous_fiscal_year['total_cash_out_flows']['value'] = Decimal(str(0))
            cashout_flow = self.pool.get('isf.monthly.key.data.report').search(self.cr, self.uid, [('type','=','cashout'),('parent_id','=',False)])
            for report in self.pool.get('isf.monthly.key.data.report').browse(self.cr, self.uid, cashout_flow, context=None):
                #Since the first sections have been already summarized from related accounts, we recalculate only the sections with subsections (children)
                children = self.pool.get('isf.monthly.key.data.report')._get_children_by_order(self.cr, self.uid, [report.id], context=None)
                children = children[1:] #skip the parent itself
                if len(children) > 0:
                    period['total_' + report.name_total]['value'] += Decimal(str(self._get_report_total_recursive(p.id, ids, report, period, context=None)))
                    period_fiscal_year['total_' + report.name_total]['value'] += Decimal(str(self._get_report_total_recursive(p.id, ids, report, period, context=None)))
                #Summarized all first sections
                period['total_cash_out_flows']['value'] += Decimal(str(period['total_' + report.name_total]['value']))
                period['total_cash_out_flows']['posted'] &= period['total_' + report.name_total]['posted']
                period_fiscal_year['total_cash_out_flows']['value'] += Decimal(str(period_fiscal_year['total_' + report.name_total]['value']))
                #period_previous_fiscal_year['total_cash_out_flows']['value'] += Decimal(str(period_fiscal_year['total_' + report.name_total]['value']))
            
            # Difference Cash In - Cash Out (before depreciations)
            period['difference_cashin_cashout_before']['value'] = Decimal(str(period['total_cash_in_flows']['value'])) - Decimal(str(period['total_cash_out_flows']['value']))
            period['difference_cashin_cashout_before']['posted'] = period['total_cash_in_flows']['posted'] & period['total_cash_out_flows']['posted']
            period_fiscal_year['difference_cashin_cashout_before']['value'] = Decimal(str(period_fiscal_year['total_cash_in_flows']['value'])) - Decimal(str(period_fiscal_year['total_cash_out_flows']['value']))
            period_previous_fiscal_year['difference_cashin_cashout_before']['value'] = Decimal(str(period_previous_fiscal_year['total_cash_in_flows']['value'])) - Decimal(str(period_previous_fiscal_year['total_cash_out_flows']['value']))
            
            #Total Depreciations
            period['total_depreciations']['value'] = Decimal(str(0))
            period_fiscal_year['total_depreciations']['value'] = Decimal(str(0))
            #period_previous_fiscal_year['total_depreciations']['value'] = Decimal(str(0))
            depreciations_flow = self.pool.get('isf.monthly.key.data.report').search(self.cr, self.uid, [('type','=','depreciations'),('parent_id','=',False)])
            for report in self.pool.get('isf.monthly.key.data.report').browse(self.cr, self.uid, depreciations_flow, context=None):
                #Since the first sections have been already summarized from related accounts, we recalculate only the sections with subsections (children)
                children = self.pool.get('isf.monthly.key.data.report')._get_children_by_order(self.cr, self.uid, [report.id], context=None)
                children = children[1:] #skip the parent itself
                if len(children) > 0:
                    period['total_' + report.name_total]['value'] += Decimal(str(self._get_report_total_recursive(p.id, ids, report, period, context=None)))
                    period_fiscal_year['total_' + report.name_total]['value'] += Decimal(str(self._get_report_total_recursive(p.id, ids, report, period, context=None)))
                #Summarized all first sections
                period['total_depreciations']['value'] += Decimal(str(period['total_' + report.name_total]['value']))
                period['total_depreciations']['posted'] &= period['total_' + report.name_total]['posted']
                period_fiscal_year['total_depreciations']['value'] += Decimal(str(period_fiscal_year['total_' + report.name_total]['value']))
                #period_previous_fiscal_year['total_depreciations']['value'] += Decimal(str(period_fiscal_year['total_' + report.name_total]['value']))
            
            # Difference Cash In - Cash Out (after depreciations)
            period['difference_cashin_cashout_after']['value'] = Decimal(str(period['difference_cashin_cashout_before']['value'])) - Decimal(str(period['total_depreciations']['value']))
            period['difference_cashin_cashout_after']['posted'] = period['difference_cashin_cashout_before']['posted'] & period['total_depreciations']['posted']
            period_fiscal_year['difference_cashin_cashout_after']['value'] = Decimal(str(period_fiscal_year['difference_cashin_cashout_before']['value'])) - Decimal(str(period_fiscal_year['total_depreciations']['value']))
            period_previous_fiscal_year['difference_cashin_cashout_after']['value'] = Decimal(str(period_previous_fiscal_year['difference_cashin_cashout_before']['value'])) - Decimal(str(period_previous_fiscal_year['total_depreciations']['value']))
            
            result_periods.append(period)

        result_periods.append(period_fiscal_year)
        
        ### percentage calculation
        period_percentage = self._get_period_object('%')
        period_keys = period_percentage.keys()
        reports_ids = self.pool.get('isf.monthly.key.data.report').search(self.cr, self.uid, [])
        for report in self.pool.get('isf.monthly.key.data.report').browse(self.cr, self.uid, reports_ids):
            period_percentage['total_' + report.name_total]['value'] = 100
            for account in report.account_ids:
                try:
                    period_percentage[account.code]['value'] = abs(period_fiscal_year[account.code]['value'] / period_fiscal_year['total_' + report.name_total]['value'] * 100)
                except Exception, e:
                    if _debug:
                        _logger.debug('Exception : %s', e)
                        
        #### CONVERT TO REPORT DICTIONARY ####
        for key in period_keys:
            temp = {}
            period_id = 0 #override period id for report
            title = ''
            fiscal_year = 0
            level = 0
            for period in result_periods:

                period_value = str(period_id)
                value_posted = str(period_id) + '_posted'
                
                temp['position'] = period[key]['position']
                temp['title'] = period[key]['title']
                temp['type'] = period[key]['type']
                temp['level'] = period[key]['level']
                temp[period_value] = 0
                temp[value_posted] = True 
                
                temp[value_posted] = temp[value_posted] and period[key]['posted']
                
                value = period[key]['value']
                
                if value:
                    if period[key]['period'] == '%':
                        temp[period_value] = value
                    else:
                        temp[period_value] = value / divisor
                        
                period_id += 1
                
            self.result_values.append(temp)
            
        if _debug:
            _logger.debug('result_values : %s', self.result_values)
        return self.result_values
    
    
    def _get_report_total_recursive(self, pid, ids=None, report=None, period=None, context=None):
        total = Decimal(str(0))
        children = self.pool.get('isf.monthly.key.data.report')._get_children_by_order(self.cr, self.uid, [report.id], context=context)
        children = children[1:] #skip the parent itself
        if len(children) > 0:
            for child in self.pool.get('isf.monthly.key.data.report').browse(self.cr, self.uid, children, context=context):
                total += Decimal(str(self._get_report_total_recursive(pid, ids, child, period, context)))
        else:
            total = Decimal(str(period['total_' + report.name_total]['value']))
        return total
    
    def _get_period_object_recursive(self, pid, ids=None, report=None, context=None):
        period = OrderedDict()
        period['title_' + report.name] = {'level' : report.level, 'type' : 'title', 'posted' : True, 'position' : '', 'period' : pid, 'title' : report.name, 'value' : None}
        for account in report.account_ids:
            period[account.code] = {'level' : report.level, 'type' : 'value', 'posted' : True, 'position' : account.code, 'period' : pid, 'title' : '', 'value' : Decimal(str(0))}
        children = self.pool.get('isf.monthly.key.data.report')._get_children_by_order(self.cr, self.uid, [report.id], context=context)
        children = children[1:] #skip the parent itself
        if len(children) > 0:
            for child in self.pool.get('isf.monthly.key.data.report').browse(self.cr, self.uid, children, context=context):
                period.update(self._get_period_object_recursive(pid, ids, child, context))
        period['total_' + report.name_total] = {'level' : report.level, 'type' : 'total', 'posted' : True, 'position' : '', 'period' : pid, 'title' : report.name_total, 'value' : Decimal(str(0))}
        return period
        
    def _get_period_object(self, pid, ids=None, context=None):
        period = OrderedDict()
        
        ## BEGINNING BALANCE
#         period['balance_at_beginning_of_period'] = {'level' : 0, 'type' : 'calc', 'posted' : True, 'position' : '', 'period' : pid, 'title' : 'BALANCE AT BEGINNING OF PERIOD', 'value' : Decimal(str(0))}
        
        ## CASHIN FLOWS
        period['title_cash_in_flows'] = {'level' : 0, 'type' : 'section', 'posted' : True, 'position' : '', 'period' : pid, 'title' : 'INCOMES', 'value' : None}
        cashin_flow = self.pool.get('isf.monthly.key.data.report').search(self.cr, self.uid, [('type','=','cashin'),('parent_id','=',False)])
        for report in self.pool.get('isf.monthly.key.data.report').browse(self.cr, self.uid, cashin_flow, context=context):
            period.update(self._get_period_object_recursive(pid, ids, report, context))
        period['total_cash_in_flows'] = {'level' : 0, 'type' : 'section_total', 'posted' : True, 'position' : '', 'period' : pid, 'title' : 'TOTAL INCOMES', 'value' : Decimal(str(0))}
        
        ## CASHOUT FLOWS
        period['title_cash_out_flows'] = {'level' : 0, 'type' : 'section', 'posted' : True, 'position' : '', 'period' : pid, 'title' : 'EXPENDITURES', 'value' : None}
        cashout_flow = self.pool.get('isf.monthly.key.data.report').search(self.cr, self.uid, [('type','=','cashout'),('parent_id','=',False)])
        for report in self.pool.get('isf.monthly.key.data.report').browse(self.cr, self.uid, cashout_flow, context=context):
            period.update(self._get_period_object_recursive(pid, ids, report, context))
        period['total_cash_out_flows'] = {'level' : 0, 'type' : 'section_total', 'posted' : True, 'position' : '', 'period' : pid, 'title' : 'TOTAL EXPENDITURES', 'value' : Decimal(str(0))}

        ## DIFF CASHIN - CASHOUT before depreciations
        period['difference_cashin_cashout_before'] = {'level' : 0, 'type' : 'calc', 'posted' : True, 'position' : '', 'period' : pid, 'title' : 'RESULT before depreciation', 'value' : Decimal(str(0))}
        
        ## DEPRECIATIONS
        period['title_depreciations'] = {'level' : 0, 'type' : 'section', 'posted' : True, 'position' : '', 'period' : pid, 'title' : 'DEPRECIATIONS', 'value' : None}
        depreciation_flow = self.pool.get('isf.monthly.key.data.report').search(self.cr, self.uid, [('type','=','depreciations'),('parent_id','=',False)])
        for report in self.pool.get('isf.monthly.key.data.report').browse(self.cr, self.uid, depreciation_flow, context=context):
            period.update(self._get_period_object_recursive(pid, ids, report, context))
        period['total_depreciations'] = {'level' : 0, 'type' : 'section_total', 'posted' : True, 'position' : '', 'period' : pid, 'title' : 'TOTAL DEPRECIATIONS', 'value' : Decimal(str(0))}
        
        ## DIFF CASHIN - CASHOUT after depreciations
        period['difference_cashin_cashout_after'] = {'level' : 0, 'type' : 'calc', 'posted' : True, 'position' : '', 'period' : pid, 'title' : 'RESULT after depreciation ', 'value' : Decimal(str(0))}
        
        period_keys = period.keys();
            
        # get accounts names (first 50 characters)
        obj_account = self.pool.get('account.account')
        accounts_ids = obj_account.search(self.cr, self.uid, [('code', 'in', period_keys)])
        accounts = obj_account.browse(self.cr, self.uid, accounts_ids)
        for account in accounts:
            code = account.code
            period[code]['title'] = account.name[:50]
            
        if _debug:
            _logger.debug('==> period object:')
        #pp.pprint(period)
            
        return period;
    
report_sxw.report_sxw('report.isf.monthly.key.data.webkit', 
                      'isf.monthly.key.data.report.view', 
                      'addons/isf_monthly_key_data_report/report/monthly_key_data_report.mako', 
                      parser=isf_monthly_key_data_report_parser)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
