<html>
<head>
<title>Monthly Key Data Report</title>
    <style>
		table {
		    border-spacing: 0px;
		    page-break-inside: auto
		}
		
		table tr {
                page-break-inside: avoid; 
                page-break-after: auto;
  		}
		
		table, td, th {
		    border: 1px dotted black;
		}
		
		.excel {
			background-color: yellow;
		}
		
		.none {
			border: 0px;
		}
		
		.section {
			border: 0px;
			font-size: large;
		}
		
		.title {
			border: 0px;
			font-size: medium;
		}
		
		.title_children {
			border: 0px;
			font-size: small;
		}
		
		.subtitle {
			border: 0px;
			font-size: small;
		}
		
		.total {
			border: 2px solid black;
		}
		
		.empty {
			border: 0px;
			border-left: 1px dotted black;
			border-right: 1px dotted black;
		}
		
		.emptyfy {
			border: 0px;
			border-left: 2px solid black;
			border-right: 2px solid black;
		}
		
		.endfy {
			border: 2px solid black;
			border-left: 2px solid black;
			border-right: 2px solid black;
			border-bottom: 2px solid black;
		}
		
		.fy {
			border-left: 2px solid black;
			border-right: 2px solid black;
		}
		
		.posted {
			color: black;
		}
		
		.unposted {
			color: blue;
		}
		
    </style>
</head>
<body>
    <%page expression_filter="entity"/>
    <h1><b><i>Monthly Key Data Report</i></b></h1>
    <%
    """
    print "IN REPORT!"
    print objects, dir(objects)
    print "*************** LOCALS:"
    pp(locals())
    print date_lines()
    print lines()
    """
    %>
    
    <%
    	row = 6
    %>
    
    <%def name="indent(level, title)">
	    ${"-" * level + " " * level * 4}${title}
	</%def>
	
	<%def name="td_normal(line, x)">
		%if line[x+'_posted']:
			<td width="5%" align="right" class="posted">${formatLang(line[x])}</td>
		%else:
			<td width="5%" align="right" class="unposted">${formatLang(line[x])}</td>
		%endif
	</%def>
	
	<%def name="td_strong(line, x)">
		%if line[x+'_posted']:
			<td width="5%" align="right" class="posted"><strong>${formatLang(line[x])}</strong></td>
		%else:
			<td width="5%" align="right" class="unposted"><strong>${formatLang(line[x])}</strong></td>
		%endif
	</%def>
    
    <table style="width: 100%" border="0">
        <tr class="table_header">
            <td colspan="3" height="30px">
                %for date in datelines():
                    Fiscal Year: ${date['fiscal_year']} (Values: ${date['divisor']} ${date['currency']})
                    %if date['target'] == 'all':
                    	<br />
                    	Unposted in blue
                    %else:
                    	<br />
                    	Posted only
                    %endif
                %endfor
            </td>
        </tr>
    </table>

    <table style="width: 100%; font-size: 12px; border: 1px dotted;">
        <tr>
        	%if date['detail']:
	            <th> Ledger Position </th>
	            <th> Account </th>
	        %else:
	        	<th colspan="2"> Account </th>
	        %endif
	        
	        	<th> Actual ${date['previous_fy']} </th>
	            <th> Budget ${date['fiscal_year']} </th>
            
            %for month in months_names():
            	<th> ${month.decode('utf-8', errors='replace')} </th>
            %endfor
            
	            <th class="total"> Total </th>
	            <th class="excel"> Previous VS Budget </th>
	            <th class="excel"> Budget VS Total </th>
        </tr>
        %for line in lines():
        
        	%if date['detail']:
        
	        	%if line['type'] == 'section':
				    <tr class="section">
				    
	        	%elif line['type'] == 'title':
	        		<tr class="title">  
	        		
	        	%elif line['type'] == 'total':
					<tr class="total">
					
				%elif line['type'] == 'section_total':
					<tr class="total">    
				
				%elif line['type'] == 'calc':
					<tr>
					
	    		%else:
	   				<tr>
	
	        	%endif
		        	%if line['type'] == 'section':
		        		<td colspan="16" class="section" height="50px"><strong>${line['title']}</strong></td>
		        		<td class="emptyfy"></td>
		        		
		        	%elif line['type'] == 'title':
		        		%if line['level'] == 0:
		        			<td colspan="16" class="title" height="40px"><strong>${indent(line['level'],line['title'])}</strong></td>
		        		%else:
		        			<td colspan="16" class="title_children" height="30px">${indent(line['level'],line['title'])}</td>
		        		%endif
		        		<td class="emptyfy"></td>
		        	
		        	%elif line['type'] == 'subtitle':
		        		<td class="empty"></td>
		        		<td colspan="13" class="subtitle"><strong>${line['title']}</strong></td>
		        		<td class="emptyfy"></td>
		        		
		        	%elif line['type'] == 'total':
		        		<td class="none" width="5%"></td>
		        		<td width="25%"><strong>${line['title']}</strong></td>
		        		
		        	%elif line['type'] == 'section_total':
		        		<td class="none" width="5%"></td>
		        		<td width="25%" aling="right"><strong>${line['title']}</strong></td>
		        	
		        	%elif line['type'] == 'calc':
			        		<td colspan="16" class="section" height="20px"></td>
			        		<td class="emptyfy"></tr>
			        	</tr>
			        	<% row = row + 1 %>
			        	<tr>
			        		<td colspan="2" width="20%"><strong>${line['title']}</strong></td>
		        		
		        	%else:
		        		<td width="5%">${indent(line['level'],line['position'])}</td>
		        		<td width="25%">${line['title']}</td>
		        		
		        	%endif
		        	
	        		%if line['type'] == 'value':
		        		${td_normal(line, '0')}
		        		<td></td>
	        			${td_normal(line, '1')}
			        	${td_normal(line, '2')}
			        	${td_normal(line, '3')}
			        	${td_normal(line, '4')}
			        	${td_normal(line, '5')}
			        	${td_normal(line, '6')}
			        	${td_normal(line, '7')}
			        	${td_normal(line, '8')}
			        	${td_normal(line, '9')}
			        	${td_normal(line, '10')}
			        	${td_normal(line, '11')}
			        	${td_normal(line, '12')}
		        		<td width="5%" align="right" class="fy">${formatLang(line['13'])}</td>
		        		<td class="excel" width="5%">=D${row}-C${row}</td>
		        		<td class="excel" width="5%">=D${row}-Q${row}</td>
		        		
		        	%elif line['type'] == 'total' or line['type'] == 'section_total':
			        	${td_strong(line, '0')}
			        	<td></td>
			        	${td_strong(line, '1')}
			        	${td_strong(line, '2')}
			        	${td_strong(line, '3')}
			        	${td_strong(line, '4')}
			        	${td_strong(line, '5')}
			        	${td_strong(line, '6')}
			        	${td_strong(line, '7')}
			        	${td_strong(line, '8')}
			        	${td_strong(line, '9')}
			        	${td_strong(line, '10')}
			        	${td_strong(line, '11')}
			        	${td_strong(line, '12')}
		        		<td width="6%" align="right" class="fy"><strong>${formatLang(line['13'])}</strong></td>
		        		<td class="excel" width="5%"><strong>=D${row}-C${row}</strong></td>
		        		<td class="excel" width="5%"><strong>=D${row}-Q${row}</strong></td>
		        		
		        	%elif line['type'] == 'calc':
			        	${td_strong(line, '0')}
			        	<td></td>
			        	${td_strong(line, '1')}
			        	${td_strong(line, '2')}
			        	${td_strong(line, '3')}
			        	${td_strong(line, '4')}
			        	${td_strong(line, '5')}
			        	${td_strong(line, '6')}
			        	${td_strong(line, '7')}
			        	${td_strong(line, '8')}
			        	${td_strong(line, '9')}
			        	${td_strong(line, '10')}
			        	${td_strong(line, '11')}
			        	${td_strong(line, '12')}
		        		<td width="6%" align="right" class="endfy"><strong>${formatLang(line['13'])}</strong></td>
		        		<td class="excel" width="5%"><strong>=D${row}-C${row}</strong></td>
		        		<td class="excel" width="5%"><strong>=D${row}-Q${row}</strong></td>
		        		
		        	%else:
		        		<!-- td colspan="16"></td -->
		        	%endif
	        	</tr>
	        	<% row = row + 1 %>
	        %else:
	        
	        	%if line['type'] == 'section':
				    <tr class="section">
				    	<td colspan="16" class="section" height="50px"><strong>${line['title']}</strong></td>
		        		<td class="emptyfy"></td>
				    </tr>
				    <% row = row + 1 %>
				%elif line['type'] == 'total':
					<tr class="total">
						%if line['type'] == 'total':
		        			<td colspan="2" width="20%">${line['title']}</td>
		        		%else:
		        			<td colspan="2" width="20%" align="right">${line['title']}</strong></td>
		        		%endif
		        		${td_normal(line, '0')}
		        		<td></td>
	        			${td_normal(line, '1')}
			        	${td_normal(line, '2')}
			        	${td_normal(line, '3')}
			        	${td_normal(line, '4')}
			        	${td_normal(line, '5')}
			        	${td_normal(line, '6')}
			        	${td_normal(line, '7')}
			        	${td_normal(line, '8')}
			        	${td_normal(line, '9')}
			        	${td_normal(line, '10')}
			        	${td_normal(line, '11')}
			        	${td_normal(line, '12')}
		        		<td width="6%" align="right" class="fy">${formatLang(line['13'])}</td>
		        		<td class="excel" width="5%">=D${row}-C${row}</td>
		        		<td class="excel" width="5%">=D${row}-Q${row}</td>
		        	</tr>
		        	<% row = row + 1 %>	
				%elif line['type'] == 'section_total':
					<tr class="total">
						%if line['type'] == 'total':
		        			<td colspan="2" width="20%">${line['title']}</td>
		        		%else:
		        			<td colspan="2" width="20%" align="right"><strong>${line['title']}</strong></td>
		        		%endif
		        		${td_strong(line, '0')}
			        	<td></td>
			        	${td_strong(line, '1')}
			        	${td_strong(line, '2')}
			        	${td_strong(line, '3')}
			        	${td_strong(line, '4')}
			        	${td_strong(line, '5')}
			        	${td_strong(line, '6')}
			        	${td_strong(line, '7')}
			        	${td_strong(line, '8')}
			        	${td_strong(line, '9')}
			        	${td_strong(line, '10')}
			        	${td_strong(line, '11')}
			        	${td_strong(line, '12')}
		        		<td width="6%" align="right" class="fy"><strong>${formatLang(line['13'])}</strong></td>
		        		<td class="excel" width="5%"><strong>=D${row}-C${row}</strong></td>
		        		<td class="excel" width="5%"><strong>=D${row}-Q${row}</strong></td>
		        	</tr>
		        	<% row = row + 1 %>	
		        %elif line['type'] == 'calc':
		        	<tr>
		        		<td colspan="16" class="section" height="20px"></td>
		        		<td class="emptyfy"></td>
		        		<td width="5%"></td>
		        		<td width="5%"></td>
		        	</tr>
		        	<% row = row + 1 %>
		        	<tr>
		        		<td colspan="2" width="20%"><strong>${line['title']}</strong></td>
		        		${td_strong(line, '0')}
			        	<td></td>
			        	${td_strong(line, '1')}
			        	${td_strong(line, '2')}
			        	${td_strong(line, '3')}
			        	${td_strong(line, '4')}
			        	${td_strong(line, '5')}
			        	${td_strong(line, '6')}
			        	${td_strong(line, '7')}
			        	${td_strong(line, '8')}
			        	${td_strong(line, '9')}
			        	${td_strong(line, '10')}
			        	${td_strong(line, '11')}
			        	${td_strong(line, '12')}
		        		<td width="6%" align="right" class="endfy"><strong>${formatLang(line['13'])}</strong></td>
		        		<td class="excel" width="5%"><strong>=D${row}-C${row}</strong></td>
		        		<td class="excel" width="5%"><strong>=D${row}-Q${row}</strong></td>
		        	</tr>
		        	<% row = row + 1 %>
				%endif
	        %endif
        %endfor
    </table>
    %for date in datelines():
        %if date['warning']:
        <!--      
        	<br />
        	%if len(excluded()) > 0:
	        <table width="50%">
		            <tr class="total">
		        		<td>WARNING!!! Some Ledger Positions are not included, please inform System Administrator</td>
		            </tr> 
		            %for account in excluded():
		            <tr>
		            	<td>${account['account_code']} - ${account['ref']} found in ${account['note']}</td>
		            </tr>
		            %endfor
		    </table>
		-->    
		    %endif
		    <br />
		    %if len(duplicated()) > 0:
		    <table width="50%">
		        
			        <tr class="total">
		        		<td>WARNING!!! Some Ledger Positions are duplicated, please inform System Administrator</td>
		            </tr> 
		            %for account in duplicated():
		            <tr>
		            	<td>${account['account_code']} - ${account['ref']} found in ${account['note']}</td>
		            </tr>
		            %endfor
	        </table>
	        %endif
    	%endif
   	%endfor 
</body>
</html>
