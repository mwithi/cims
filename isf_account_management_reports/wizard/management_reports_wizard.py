# -*- encoding: utf-8 -*-

from openerp.osv import fields, orm

import logging
_logger = logging.getLogger(__name__)
_debug = False

class ManagementReportsWizard(orm.TransientModel):
    """Will launch the Management Reports wizard and pass required args"""

    _inherit = "account.common.balance.report"
    _name = "management.reports.webkit"
    _description = "Management Reports"
    
    _columns = {
        
        'management_report_type': fields.selection([('balance_sheet','Balance Sheet YTD'),
                                                    ('income_statement','Income Statement YTD'),
                                                    ('income_statement_monthly','Income Statement Monthly')],
                                                    "Report Type",
                                                    required=True,
                                                    help='Report Type:\n '
                                                        '- Balance Sheet YTD only assets and liabilities\n '
                                                        '- Income Statement YTD only income and expenses\n '
                                                        '- Income Statement Monthly only monthly values'),
    }

    def _print_report(self, cursor, uid, ids, data, context=None):
        _logger.debug('==> Called _print_report')
        context = context or {}
        # we update form with display account value
        data = self.pre_print_report(cursor, uid, ids, data, context=context)
        
        fields_to_read = ['management_report_type']
        vals = self.read(cursor, uid, ids, fields_to_read, context=context)[0]
        data['form'].update(vals)
        _logger.debug('==> vals : %s', vals)

        return {'type': 'ir.actions.report.xml',
                'report_name': 'account.isf_management_reports_webkit',
                'datas': data}

ManagementReportsWizard()
