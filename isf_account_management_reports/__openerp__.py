# -*- encoding: utf-8 -*-
##############################################################################
#
#    Authors: Alessandro Domanico
#    Copyright Informatici Senza Frontiere ONLUS
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    'name': 'ISF Management Reports',
    'description': """
ISF Management Reports (Webkit)
===============================

This module adds the following management reports:
 - Management Balance Sheet
 - Management Income Statement
 - Management Income Statement Monthly

Main characteristics:
---------------------

The Management Balance Sheet

* Produces a standard Balance Sheet report with progressive values per month
* Summarize at the bottom of the report report categories Assets and Liabilities

The Management Income Statement

* Produces a standard Income Statement report with progressive values per month
* Summarize at the bottom of the report report categories Incomes and Expenses

The Management Income Statement Monthly

* Produces a standard Income Statement report with monthly values
* Summarize at the bottom of the report report categories Incomes and Expenses per month

Limitations:
------------

In order to run properly this module makes sure you have installed the
library `wkhtmltopdf` for the pdf rendering (the library path must be
set in a System Parameter `webkit_path`).

Initial balances in these reports are based either on opening entry
posted in the opening period or computed on the fly. So make sure
that your past accounting opening entries are in an opening period.
Initials balances are not computed when using the Date filter (since a
date can be outside its logical period and the initial balance could
be different when computed by data or by initial balance for the
period). The opening period is assumed to be the Jan. 1st of the year
with an opening flag and the first period of the year must start also
on Jan 1st.

Totals for amounts in currencies are effective if the partner belongs to
an account with a secondary currency.

HTML headers and footers are deactivated for these reports because of
an issue in wkhtmltopdf
(http://code.google.com/p/wkhtmltopdf/issues/detail?id=656) Instead,
the header and footer are created as text with arguments passed to
wkhtmltopdf. The texts are defined inside the report classes.
""",
    'version': '0.0.1',
    'author': 'Alessandro Domanico <alessandro.domanico@informaticisenzafrontiere.org>',
    'license': 'AGPL-3',
    'category': 'Finance',
    'website': 'http://www.informaticisenzafrontiere.org',
    'images': [
        'static/src/img/icon.png',],
    'depends': ['account_financial_report_webkit'],
    'update_xml':['report/report.xml',
                  'wizard/management_reports_wizard_view.xml',
                  'report_menus.xml'],
    'demo_xml' : [],
    'active': False,
    'installable': True,
    'application': True,
}
