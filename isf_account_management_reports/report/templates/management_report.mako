## -*- coding: utf-8 -*-
<!DOCTYPE html SYSTEM "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <style type="text/css">
            .account_level_1 {
                text-transform: uppercase;
                font-size: 15px;
                background-color:#F0F0F0;
            }

            .account_level_2 {
                font-size: 12px;
                background-color:#F0F0F0;
            }

            .regular_account_type {
                font-weight: normal;
            }

            .view_account_type {
                font-weight: bold;
            }

            .account_level_consol {
                font-weight: normal;
            	font-style: italic;
            }

            ${css}

            .list_table .act_as_row {
                margin-top: 10px;
                margin-bottom: 10px;
                font-size:10px;
            }
        </style>
    </head>
    <body>
        <%!
        def amount(text):
            return text.replace('-', '&#8209;')  # replace by a non-breaking hyphen (it will not word-wrap between hyphen and numbers)
        %>

        <%setLang(user.lang)%>

        <%
        initial_balance_text = {'initial_balance': _('Computed'), 'opening_balance': _('Opening Entries'), False: _('No')}
        %>
        
        <div class="act_as_table data_table" style="width: 100%;">
            <div class="act_as_row labels">
                <div class="act_as_cell">${_('Chart of Account')}</div>
                <div class="act_as_cell">${_('Fiscal Year')}</div>
                <div class="act_as_cell">
                    %if filter_form(data) == 'filter_date':
                        ${_('Dates Filter')}
                    %else:
                        ${_('Periods Filter')}
                    %endif
                </div>
                <div class="act_as_cell">${_('Accounts Filter')}</div>
                <div class="act_as_cell">${_('Target Moves')}</div>
                <div class="act_as_cell">${_('Initial Balance')}</div>
            </div>
            <div class="act_as_row">
                <div class="act_as_cell">${ chart_account.name }</div>
                <div class="act_as_cell">${ fiscalyear.name if fiscalyear else '-' }</div>
                <div class="act_as_cell">
                    ${_('From:')}
                    %if filter_form(data) == 'filter_date':
                        ${formatLang(start_date, date=True) if start_date else u'' }
                    %else:
                        ${start_period.name if start_period else u''}
                    %endif
                    ${_('To:')}
                    %if filter_form(data) == 'filter_date':
                        ${ formatLang(stop_date, date=True) if stop_date else u'' }
                    %else:
                        ${stop_period.name if stop_period else u'' }
                    %endif
                </div>
                <div class="act_as_cell">
                    %if accounts(data):
                        ${', '.join([account.code for account in accounts(data)])}
                    %else:
                        ${_('All')}
                    %endif
                </div>
                <div class="act_as_cell">${ display_target_move(data) }</div>
                <div class="act_as_cell">${ initial_balance_text[initial_balance_mode] }</div>
            </div>
        </div>

        <div class="act_as_table list_table" style="width: 100%; margin-top: 1%;">

            <div class="act_as_thead">
                <div class="act_as_row labels">
                    ## code
                    <div class="act_as_cell first_column" style="width: 4%;">${_('Code')}</div>
                    ## account name
                    <div class="act_as_cell" style="width: 16%;">${_('Account')}</div>
                    %if initial_balance_mode:
                        ## initial balance
                        <div class="act_as_cell amount" style="width: 2%;">${_('Initial Balance')}</div>
                    %endif
                    <div class="act_as_cell amount" style="width: 2%;">${_('Month 1')}</div>
                    <div class="act_as_cell amount" style="width: 2%;">${_('Month 2')}</div>
                    <div class="act_as_cell amount" style="width: 2%;">${_('Month 3')}</div>
                    <div class="act_as_cell amount" style="width: 2%;">${_('Month 4')}</div>
                    <div class="act_as_cell amount" style="width: 2%;">${_('Month 5')}</div>
                    <div class="act_as_cell amount" style="width: 2%;">${_('Month 6')}</div>
                    <div class="act_as_cell amount" style="width: 2%;">${_('Month 7')}</div>
                    <div class="act_as_cell amount" style="width: 2%;">${_('Month 8')}</div>
                    <div class="act_as_cell amount" style="width: 2%;">${_('Month 9')}</div>
                    <div class="act_as_cell amount" style="width: 2%;">${_('Month 10')}</div>
                    <div class="act_as_cell amount" style="width: 2%;">${_('Month 11')}</div>
                    <div class="act_as_cell amount" style="width: 2%;">${_('Month 12')}</div>
                    %if progressive_total_value:
                    	<div class="act_as_cell amount" style="font-weight: bold; width: 2%;">${_('Totals')}</div>
                    %endif
                </div>
            </div>

            <div class="act_as_tbody">
                <%
                last_child_consol_ids = []
                last_level = False
                %>
                %for current_account in objects:
                    <%
                    if not current_account.to_display:
                        continue

                    if current_account.id in last_child_consol_ids:
                        # current account is a consolidation child of the last account: use the level of last account
                        level = last_level
                        level_class = "account_level_consol"
                    else:
                        # current account is a not a consolidation child: use its own level
                        level = current_account.level or 0
                        level_class = "account_level_%s" % (level,)
                        last_child_consol_ids = [child_consol_id.id for child_consol_id in current_account.child_consol_ids]
                        last_level = current_account.level
                    %>
                    <div class="act_as_row lines ${level_class} ${"%s_account_type" % (current_account.type,)}">
                        ## code
                        <div class="act_as_cell first_column">${current_account.code}</div>
                        ## account name
                        <div class="act_as_cell" style="padding-left: ${level * 5}px;">${current_account.name}</div>
                        %if initial_balance_mode:
                            ## opening balance
                            <div class="act_as_cell amount">${formatLang(current_account.init_balance, digits=0) | amount}</div>
                        %endif
                        ## balance
                        <div class="act_as_cell amount">${formatLang(values_by_period[current_account.id][1], digits=0)}</div>
                        <div class="act_as_cell amount">${formatLang(values_by_period[current_account.id][2], digits=0)}</div>
                        <div class="act_as_cell amount">${formatLang(values_by_period[current_account.id][3], digits=0)}</div>
                        <div class="act_as_cell amount">${formatLang(values_by_period[current_account.id][4], digits=0)}</div>
                        <div class="act_as_cell amount">${formatLang(values_by_period[current_account.id][5], digits=0)}</div>
                        <div class="act_as_cell amount">${formatLang(values_by_period[current_account.id][6], digits=0)}</div>
                        <div class="act_as_cell amount">${formatLang(values_by_period[current_account.id][7], digits=0)}</div>
                        <div class="act_as_cell amount">${formatLang(values_by_period[current_account.id][8], digits=0)}</div>
                        <div class="act_as_cell amount">${formatLang(values_by_period[current_account.id][9], digits=0)}</div>
                        <div class="act_as_cell amount">${formatLang(values_by_period[current_account.id][10], digits=0)}</div>
                        <div class="act_as_cell amount">${formatLang(values_by_period[current_account.id][11], digits=0)}</div>
                        <div class="act_as_cell amount">${formatLang(values_by_period[current_account.id][12], digits=0)}</div>
                        %if progressive_total_value:
	                    	<div class="act_as_cell amount" style="font-weight: bold; width: 2%;">${formatLang(progressive_total_value[current_account.id], digits=0)}</div>
	                    %endif
                    </div>
                %endfor
            </div>
        </div>
        
        %if summary:
	        <div class="act_as_table list_table" style="width: 100%; margin-top: 1%;">
	        	<div class="act_as_tbody">
	                <div class="act_as_row lines">
	                    ## code
	                    <div class="act_as_cell" style="width: 4%;"></div>
	                    ## account name
	                    <div class="act_as_cell" style="width: 16%;">${summary['titleA']}</div>
	                    %if initial_balance_mode:
	                        ## initial balance
	                        <div class="act_as_cell amount" style="width: 2%;">${formatLang(summary['valuesA'][0], digits=0)}</div>
	                    %endif
	                    <div class="act_as_cell amount" style="width: 2%;">${formatLang(summary['valuesA'][1], digits=0)}</div>
	                    <div class="act_as_cell amount" style="width: 2%;">${formatLang(summary['valuesA'][2], digits=0)}</div>
	                    <div class="act_as_cell amount" style="width: 2%;">${formatLang(summary['valuesA'][3], digits=0)}</div>
	                    <div class="act_as_cell amount" style="width: 2%;">${formatLang(summary['valuesA'][4], digits=0)}</div>
	                    <div class="act_as_cell amount" style="width: 2%;">${formatLang(summary['valuesA'][5], digits=0)}</div>
	                    <div class="act_as_cell amount" style="width: 2%;">${formatLang(summary['valuesA'][6], digits=0)}</div>
	                    <div class="act_as_cell amount" style="width: 2%;">${formatLang(summary['valuesA'][7], digits=0)}</div>
	                    <div class="act_as_cell amount" style="width: 2%;">${formatLang(summary['valuesA'][8], digits=0)}</div>
	                    <div class="act_as_cell amount" style="width: 2%;">${formatLang(summary['valuesA'][9], digits=0)}</div>
	                    <div class="act_as_cell amount" style="width: 2%;">${formatLang(summary['valuesA'][10], digits=0)}</div>
	                    <div class="act_as_cell amount" style="width: 2%;">${formatLang(summary['valuesA'][11], digits=0)}</div>
	                    <div class="act_as_cell amount" style="width: 2%;">${formatLang(summary['valuesA'][12], digits=0)}</div>
	                    %if progressive_total_value:
	                    	<div class="act_as_cell amount" style="font-weight: bold; width: 2%;">${formatLang(progressive_total_value['totalA'], digits=0)}</div>
	                    %endif
	                </div>
	                <div class="act_as_row lines">
	                    ## code
	                    <div class="act_as_cell first_column" style="width: 4%;"></div>
	                    ## account name
	                    <div class="act_as_cell" style="width: 16%;">${summary['titleB']}</div>
	                    %if initial_balance_mode:
	                        ## initial balance
	                        <div class="act_as_cell amount" style="width: 2%;">${formatLang(summary['valuesB'][0], digits=0)}</div>
	                    %endif
	                    <div class="act_as_cell amount" style="width: 2%;">${formatLang(summary['valuesB'][1], digits=0)}</div>
	                    <div class="act_as_cell amount" style="width: 2%;">${formatLang(summary['valuesB'][2], digits=0)}</div>
	                    <div class="act_as_cell amount" style="width: 2%;">${formatLang(summary['valuesB'][3], digits=0)}</div>
	                    <div class="act_as_cell amount" style="width: 2%;">${formatLang(summary['valuesB'][4], digits=0)}</div>
	                    <div class="act_as_cell amount" style="width: 2%;">${formatLang(summary['valuesB'][5], digits=0)}</div>
	                    <div class="act_as_cell amount" style="width: 2%;">${formatLang(summary['valuesB'][6], digits=0)}</div>
	                    <div class="act_as_cell amount" style="width: 2%;">${formatLang(summary['valuesB'][7], digits=0)}</div>
	                    <div class="act_as_cell amount" style="width: 2%;">${formatLang(summary['valuesB'][8], digits=0)}</div>
	                    <div class="act_as_cell amount" style="width: 2%;">${formatLang(summary['valuesB'][9], digits=0)}</div>
	                    <div class="act_as_cell amount" style="width: 2%;">${formatLang(summary['valuesB'][10], digits=0)}</div>
	                    <div class="act_as_cell amount" style="width: 2%;">${formatLang(summary['valuesB'][11], digits=0)}</div>
	                    <div class="act_as_cell amount" style="width: 2%;">${formatLang(summary['valuesB'][12], digits=0)}</div>
	                    %if progressive_total_value:
	                    	<div class="act_as_cell amount" style="font-weight: bold; width: 2%;">${formatLang(progressive_total_value['totalB'], digits=0)}</div>
	                    %endif
	                </div>
	                <div class="act_as_row lines">
	                    ## code
	                    <div class="act_as_cell first_column" style="width: 4%;"></div>
	                    ## account name
	                    <div class="act_as_cell" style="width: 16%;">${summary['titleC']}</div>
	                    %if initial_balance_mode:
	                        ## initial balance
	                        <div class="act_as_cell amount" style="width: 2%;">${formatLang(summary['valuesC'][0], digits=0)}</div>
	                    %endif
	                    <div class="act_as_cell amount" style="width: 2%;">${formatLang(summary['valuesC'][1], digits=0)}</div>
	                    <div class="act_as_cell amount" style="width: 2%;">${formatLang(summary['valuesC'][2], digits=0)}</div>
	                    <div class="act_as_cell amount" style="width: 2%;">${formatLang(summary['valuesC'][3], digits=0)}</div>
	                    <div class="act_as_cell amount" style="width: 2%;">${formatLang(summary['valuesC'][4], digits=0)}</div>
	                    <div class="act_as_cell amount" style="width: 2%;">${formatLang(summary['valuesC'][5], digits=0)}</div>
	                    <div class="act_as_cell amount" style="width: 2%;">${formatLang(summary['valuesC'][6], digits=0)}</div>
	                    <div class="act_as_cell amount" style="width: 2%;">${formatLang(summary['valuesC'][7], digits=0)}</div>
	                    <div class="act_as_cell amount" style="width: 2%;">${formatLang(summary['valuesC'][8], digits=0)}</div>
	                    <div class="act_as_cell amount" style="width: 2%;">${formatLang(summary['valuesC'][9], digits=0)}</div>
	                    <div class="act_as_cell amount" style="width: 2%;">${formatLang(summary['valuesC'][10], digits=0)}</div>
	                    <div class="act_as_cell amount" style="width: 2%;">${formatLang(summary['valuesC'][11], digits=0)}</div>
	                    <div class="act_as_cell amount" style="width: 2%;">${formatLang(summary['valuesC'][12], digits=0)}</div>
	                    %if progressive_total_value:
	                    	<div class="act_as_cell amount" style="font-weight: bold; width: 2%;">${formatLang(progressive_total_value['totalC'], digits=0)}</div>
	                    %endif
	                </div>
	            </div>
	        </div>
	    %endif    
    </body>
</html>
