# -*- encoding: utf-8 -*-
##############################################################################
#
#    Author: Guewen Baconnier
#    Copyright Camptocamp SA 2011
#    SQL inspired from OpenERP original code
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import logging

from operator import add
from openerp.tools.translate import _

from openerp.addons.account_financial_report_webkit.report.common_reports import CommonReportHeaderWebkit
from mailcap import show

_logger = logging.getLogger(__name__)
_debug = False

class CommonManagementReportHeaderWebkit(CommonReportHeaderWebkit):
    """Define common helper for balance (trial balance, P&L, BS oriented financial report"""
    
    def _get_report_name(self, data):
        val = self._get_form_param('management_report_type', data)
        if val == 'income_statement':
            return _('Management Income Statement')
        elif val == 'income_statement_monthly':
            return _('Management Income Statement Monthly')
        else:
            return _('Management Balance Sheet')
        
    def _get_from_management_report_type(self, management_report_type):
        if management_report_type == 'balance_sheet':
            return ['asset','liability']
        else:
            return ['income','expense']

    def _get_numbers_display(self, data):
        return self._get_form_param('numbers_display', data)

    @staticmethod
    def find_key_by_value_in_list(dic, value):
        return [key for key, val in dic.iteritems() if value in val][0]

    def _get_diff(self, balance, previous_balance):
        """
        @param balance: current balance
        @param previous_balance: last balance
        @return: dict of form {'diff': difference, 'percent_diff': diff in percentage}
        """
        diff = balance - previous_balance

        obj_precision = self.pool.get('decimal.precision')
        precision = obj_precision.precision_get(self.cursor, self.uid, 'Account')
        # round previous balance with account precision to avoid big numbers if previous
        # balance is 0.0000001 or a any very small number
        if round(previous_balance, precision) == 0:
            percent_diff = False
        else:
            percent_diff = round(diff / previous_balance * 100, precision)

        return {'diff': diff, 'percent_diff': percent_diff}

    def _get_start_stop_for_filter(self, main_filter, fiscalyear, start_date, stop_date, start_period, stop_period):
        if main_filter in ('filter_no', 'filter_year'):
            start_period = self.get_first_fiscalyear_period(fiscalyear)
            stop_period = self.get_last_fiscalyear_period(fiscalyear)
        elif main_filter == 'filter_opening':
            opening_period = self._get_st_fiscalyear_period(fiscalyear, special=True)
            start_period = stop_period = opening_period
        if main_filter == 'filter_date':
            start = start_date
            stop = stop_date
        else:
            start = start_period
            stop = stop_period

        return start_period, stop_period, start, stop
    
    def _get_period_from_date(self, date):
        period_obj = self.pool.get('account.period')
        period_ids = period_obj.search(self.cursor, self.uid, [('date_start','<=',date),('date_stop','>=',date)])
        periods = period_obj.browse(self.cursor, self.uid, period_ids)

        return periods[0]
    
    def _get_account_details(self, account_ids, target_move, fiscalyear, main_filter, start, stop, initial_balance_mode, context=None):
        """
        Get details of accounts to display on the report
        @param account_ids: ids of accounts to get details
        @param target_move: selection filter for moves (all or posted)
        @param fiscalyear: browse of the fiscalyear
        @param main_filter: selection filter period / date or none
        @param start: start date or start period browse instance
        @param stop: stop date or stop period browse instance
        @param initial_balance_mode: False: no calculation, 'opening_balance': from the opening period, 'initial_balance': computed from previous year / periods
        @return: dict of list containing accounts details, keys are the account ids
        """
        if context is None:
            context = {}

        account_obj = self.pool.get('account.account')
        period_obj = self.pool.get('account.period')
        use_period_ids = main_filter in ('filter_no', 'filter_period', 'filter_opening')

        if use_period_ids:
            if main_filter == 'filter_opening':
                period_ids = [start.id]
            else:
                period_ids = period_obj.build_ctx_periods(self.cursor, self.uid, start.id, stop.id)
                # never include the opening in the debit / credit amounts
                period_ids = self.exclude_opening_periods(period_ids)

        init_balance = False
        if initial_balance_mode == 'opening_balance':
            init_balance = self._read_opening_balance(account_ids, start)
        elif initial_balance_mode:
            init_balance = self._compute_initial_balances(account_ids, start, fiscalyear)

        ctx = context.copy()
        ctx.update({'state': target_move,
                    'all_fiscalyear': True})

        if use_period_ids:
            ctx.update({'periods': period_ids})
        elif main_filter == 'filter_date':
            ctx.update({'date_from': start,
                        'date_to': stop})

        accounts = account_obj.read(
                self.cursor,
                self.uid,
                account_ids,
                ['type', 'code', 'name', 'debit', 'credit',  'balance', 'parent_id', 'level', 'child_id'],
                ctx)

        accounts_by_id = {}
        for account in accounts:
            if init_balance:
                # sum for top level views accounts
                child_ids = account_obj._get_children_and_consol(self.cursor, self.uid, account['id'], ctx)
                if child_ids:
                    child_init_balances = [
                            init_bal['init_balance']
                            for acnt_id, init_bal in init_balance.iteritems()
                            if acnt_id in child_ids]
                    top_init_balance = reduce(add, child_init_balances)
                    account['init_balance'] = top_init_balance
                else:
                    account.update(init_balance[account['id']])
                account['balance'] = account['init_balance'] + account['debit'] - account['credit']
            accounts_by_id[account['id']] = account
        return accounts_by_id

    def compute_balance_data(self, data, filter_report_type=None):
        _logger.debug('==> Called compute_balance_data')
        new_ids = data['form']['account_ids'] or data['form']['chart_account_id']
        max_comparison = self._get_form_param('max_comparison', data, default=0)
        main_filter = self._get_form_param('filter', data, default='filter_no')
        with_movements = data['form']['with_movements']
        management_report_type = data['form']['management_report_type']
        
        filter_report_type = self._get_from_management_report_type(management_report_type)

        #comp_filters, nb_comparisons, comparison_mode = self._comp_filters(data, max_comparison)

        fiscalyear = self.get_fiscalyear_br(data)

        start_period = self.get_start_period_br(data)
        stop_period = self.get_end_period_br(data)

        target_move = self._get_form_param('target_move', data, default='all')
        start_date = self._get_form_param('date_from', data)
        stop_date = self._get_form_param('date_to', data)
        chart_account = self._get_chart_account_id_br(data)

        start_period, stop_period, start, stop = \
            self._get_start_stop_for_filter(main_filter, fiscalyear, start_date, stop_date, start_period, stop_period)

        init_balance = self.is_initial_balance_enabled(main_filter)
        initial_balance_mode = init_balance and self._get_initial_balance_mode(start) or False

        # Retrieving accounts
        account_ids = self.get_all_accounts(new_ids, filter_report_type=filter_report_type)

        # get details for each accounts, total of debit / credit / balance
        accounts_by_ids = self._get_account_details(account_ids, target_move, fiscalyear, main_filter, start, stop, initial_balance_mode)
        
        _logger.debug('==> init_balance : %s', init_balance)
        _logger.debug('==> initial_balance_mode : %s', initial_balance_mode)
        _logger.debug('==> accounts_by_ids : %s', accounts_by_ids)
        
        ######## MAJOR CHANGES ########
        ###############################
        period_obj = self.pool.get('account.period')
        period_ids = period_obj.search(self.cursor, self.uid, [('fiscalyear_id','=',fiscalyear.id),('special','=',False)])
        periods = period_obj.browse(self.cursor, self.uid, period_ids)
        accounts_by_period = {}
        for period in periods:
            main_filter = 'filter_date'
            start_period, stop_period, start, stop = \
                self._get_start_stop_for_filter(main_filter, fiscalyear, period.date_start, period.date_stop, start_period, stop_period)
            period_init_balance = self.is_initial_balance_enabled(main_filter)
            period_initial_balance_mode = period_init_balance and self._get_initial_balance_mode(start) or False
            #_logger.debug('==> period : %s (%s -> %s)', period, period.date_start, period.date_stop)
            accounts_by_period[period.id] = self._get_account_details(account_ids, target_move, fiscalyear, main_filter, start, stop, period_initial_balance_mode).values()
            if period.id == 34:
                _logger.debug('==> period_init_balance : %s', period_init_balance)
                _logger.debug('==> period_initial_balance_mode : %s', period_initial_balance_mode)
                _logger.debug('==> period : %s (%s -> %s)', period, period.date_start, period.date_stop)
                _logger.debug('==> accounts_by_period[%s] : %s ', period.id, accounts_by_period[period.id])

        values_by_period = dict([(aid, [0] * (len(periods)+1)) for aid in account_ids])
        progressive_total_value = dict([(aid, 0) for aid in account_ids])
        summary = {'titleA': [0] * (len(periods)+1),
                   'valuesA': [0] * (len(periods)+1),
                   'titleB': [0] * (len(periods)+1),
                   'valuesB': [0] * (len(periods)+1),
                   'titleC': [0] * (len(periods)+1),
                   'valuesC': [0] * (len(periods)+1),
        }
        
        values_assets_by_period = [0] * (len(periods)+1)
        values_liability_by_period = [0] * (len(periods)+1)
        values_income_by_period = [0] * (len(periods)+1)
        values_expense_by_period = [0] * (len(periods)+1)
        values_net_by_period = [0] * (len(periods)+1)
        for account in self.pool.get('account.account').browse(self.cursor, self.uid, account_ids):
            if account.type != 'view':
                if account.user_type.report_type == 'asset':
                    values_assets_by_period[0] += accounts_by_ids[account.id].get('init_balance', 0.0)
                elif account.user_type.report_type == 'liability':
                    values_liability_by_period[0] += accounts_by_ids[account.id].get('init_balance', 0.0)
                elif account.user_type.report_type == 'income':
                    values_income_by_period[0] += accounts_by_ids[account.id].get('init_balance', 0.0)
                elif account.user_type.report_type == 'expense':
                    values_expense_by_period[0] += accounts_by_ids[account.id].get('init_balance', 0.0)
                    
        if management_report_type in ['balance_sheet']:
            values_net_by_period[0] = values_assets_by_period[0] + values_liability_by_period[0]
        else:
            values_net_by_period[0] = values_income_by_period[0] + values_expense_by_period[0]
            
        #_logger.debug('==> values_by_period : %s', values_by_period)
        for account in self.pool.get('account.account').browse(self.cursor, self.uid, account_ids):
            
            period_id = 1 #override period id for report
            if initial_balance_mode == 'opening_balance':
                previous_balance = 0.0
            else:
                previous_balance = accounts_by_ids[account.id].get('init_balance', 0.0) #starting balance
                values_by_period[account.id][0] = accounts_by_ids[account.id].get('init_balance', 0.0)
            
            for period in periods:
                
                for this_account in accounts_by_period[period.id]:
                    
                    if this_account['code'] == account.code:
                        values_by_period[account.id][period_id] = this_account['balance'] + previous_balance
                        
                        if account.type != 'view':
                            if account.user_type.report_type == 'asset':
                                values_assets_by_period[period_id] += values_by_period[account.id][period_id]
                            elif account.user_type.report_type == 'liability':
                                values_liability_by_period[period_id] += values_by_period[account.id][period_id]
                            elif account.user_type.report_type == 'income':
                                values_income_by_period[period_id] += values_by_period[account.id][period_id]
                            elif account.user_type.report_type == 'expense':
                                values_expense_by_period[period_id] += values_by_period[account.id][period_id]
                        
                        if management_report_type in ['balance_sheet','income_statement']:
                            previous_balance = previous_balance + this_account['balance']
                        else:
                            progressive_total_value[account.id] += values_by_period[account.id][period_id]
                            
                        break
                    
                if management_report_type in ['balance_sheet']:
                    values_net_by_period[period_id] = values_assets_by_period[period_id] + values_liability_by_period[period_id]
                else:
                    values_net_by_period[period_id] = values_income_by_period[period_id] + values_expense_by_period[period_id]
                
                period_id += 1
                
                
        if management_report_type in ['balance_sheet']:
            summary.update({
                'titleA': _('Total Assets'),
                'valuesA': values_assets_by_period,
                'titleB': _('Total Liabilities and Equities'),
                'valuesB': values_liability_by_period,
                'titleC': _('Progressive Net Result'),
                'valuesC': values_net_by_period,
            })
            progressive_total_value = None
        elif management_report_type in ['income_statement']:
            summary.update({
                'titleA': _('Total Revenues'),
                'valuesA': values_income_by_period,
                'titleB': _('Total Expenses'),
                'valuesB': values_expense_by_period,
                'titleC': _('Operating Result'),
                'valuesC': values_net_by_period,
            })
            progressive_total_value = None
            values_income_by_period[0] = 0
            values_expense_by_period[0] = 0
            values_net_by_period[0] = 0
        else:
            summary.update({
                'titleA': _('Total Revenues'),
                'valuesA': values_income_by_period,
                'titleB': _('Total Expenses'),
                'valuesB': values_expense_by_period,
                'titleC': _('Monthly Net Result'),
                'valuesC': values_net_by_period,
            })
            values_income_by_period[0] = 0
            values_expense_by_period[0] = 0
            values_net_by_period[0] = 0
            progressive_total_value.update({'totalA': reduce(add, values_income_by_period)})
            progressive_total_value.update({'totalB': reduce(add, values_expense_by_period)})
            progressive_total_value.update({'totalC': reduce(add, values_net_by_period)})
                
        #_logger.debug('==> values_by_period : %s', values_by_period) 
        ###############################
        ######## MAJOR CHANGES ########
        
        to_display = dict.fromkeys(account_ids, True)
        
        objects = []         
        for account in self.pool.get('account.account').browse(self.cursor, self.uid, account_ids):
            if not account.parent_id:  # hide top level account
                continue
            if account.type == 'consolidation':
                to_display.update(dict([(a.id, False) for a in account.child_consol_ids]))
            elif account.type == 'view':
                to_display.update(dict([(a.id, True) for a in account.child_id]))
            account.balance = accounts_by_ids[account.id]['balance']
            account.init_balance = accounts_by_ids[account.id].get('init_balance', 0.0)
 
            display_account = False
            # we have to display the account if we have an amount in the main column
            # we set it as a property to let the data in the report if someone want to use it in a custom report
            if with_movements:
                display_account = display_account or any((account.debit, account.credit, account.balance, account.init_balance))
                to_display.update({account.id: display_account and to_display[account.id]})
            objects.append(account)

        for account in objects:
            account.to_display = to_display[account.id]
            
        context_report_values = {
            'fiscalyear': fiscalyear,
            'start_date': start_date,
            'stop_date': stop_date,
            'start_period': start_period,
            'stop_period': stop_period,
            'chart_account': chart_account,
            'initial_balance': init_balance,
            'initial_balance_mode': initial_balance_mode,
            'with_movements': with_movements,
            'report_name': self._get_report_name(data),
            'values_by_period': values_by_period,
            'progressive_total_value': progressive_total_value,
            'summary': summary,
        }
        #_logger.debug('==> objects : %s', objects)
        return objects, new_ids, context_report_values
