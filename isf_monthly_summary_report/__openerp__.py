# -*- coding: utf-8 -*-
{
	'name': 'ISF Monthly Summary Report',
	'version': '0.1',
	'category': 'Tools',
	'description': 
"""
ISF Monthly Summary Report 
===========================

* Produces a new report with Incomes and Expenses re-classification (to be configured)
* Configuration section in Accounting \ Configuration \ Financial Reports \ Monthly Summary Report

The module allows to define:

- Incomes section
- Expenditure section
- Stock Changes section
- Depreciation section

both with accounts and sub-section as well. It then calculates for each period of the choosen Fiscal Year: 

- Monthly summaries 
- Differences with forecasting totals (to be inputed)
- Horizontal breakdown (2 columns)

The report also warns the user if some accounts are used in more than one section.
""",
	'author': 'Alessandro Domanico (alessandro.domanico@informaticisenzafrontiere.org)',
	'website': 'www.informaticisenzafrontiere.org',
	'license': 'AGPL-3',
	'depends': ['base_headers_webkit','account_accountant','oe_excel_report'],
	'data': [
		'isf_monthly_summary_report.xml',
        'wizard/isf_monthly_summary_report_view.xml',
        'security/ir.model.access.csv',
    ],
	'demo': [],
	'installable' : True,
}

