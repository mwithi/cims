# -*- coding: utf-8 -*-
import time
import pprint
from datetime import datetime

from openerp.report import report_sxw
from collections import OrderedDict
import logging
import locale
import platform
import os
import pycountry
from decimal import Decimal

_logger = logging.getLogger(__name__)
_debug = False
_debug_lang = False
pp = pprint.PrettyPrinter(indent=4)

class isf_monthly_summary_report_parser(report_sxw.rml_parse):
    _name = 'report.isf.monthly.summary.report.webkit'
    
    def __init__(self, cr, uid, name, context=None):
        if _debug:
            _logger.debug('==> in init: %s', __name__)
            
        super(isf_monthly_summary_report_parser, self).__init__(cr, uid, name, context=context)
        
        obj_report = self.pool.get('isf.monthly.summary.report.view')
        report_fiscal_year = obj_report.read(self.cr, self.uid, context['active_id'], ['fiscalyear_id'])
        fiscal_year = report_fiscal_year['fiscalyear_id']
        previous_fiscal_year_id = self.pool.get('account.fiscalyear').search(self.cr, self.uid, [('id','<',fiscal_year[0])], order='id desc', limit=1)
        previous_fiscal_year = self.pool.get('account.fiscalyear').browse(self.cr, self.uid, previous_fiscal_year_id)[0]
        previous = (previous_fiscal_year.id, previous_fiscal_year.name)
        if _debug:
            _logger.debug('==> fiscal_year : %s', fiscal_year)
            _logger.debug('==> previous_fiscal_year : %s', previous)
        
        self.context = context
        self.warnings = [{'warning':False}]
        self.result_months = []
        self.result_excluded = []
        self.result_duplicated = []
        
        self.localcontext.update({
            'time': time,
            'pp': pp,
            'lines': lambda: self.lines(fiscal_year=fiscal_year),
            'previous_lines': lambda: self.lines(fiscal_year=previous),
            'datelines' : lambda: self.datelines(fiscal_year=fiscal_year),
            'previous_datelines' : lambda: self.datelines(fiscal_year=previous),
            'months_names' : self.months_names,
            'excluded' : self.excluded,
            'duplicated' : self.duplicated,
            'warnings' : self.warnings,
        })
        
    def duplicated(self, ids=None, done=None):
        return self.result_duplicated
        
    def excluded(self, ids=None, done=None):
        return self.result_excluded
        
    def datelines(self, ids=None, done=None, fiscal_year=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.monthly.summary.report.view')
        report_fiscal_year = obj_report.read(self.cr, self.uid, ctx['active_id'], ['fiscalyear_id'])
        report_divisor = obj_report.read(self.cr, self.uid, ctx['active_id'], ['divisor'])
        report_target_move = obj_report.read(self.cr, self.uid, ctx['active_id'], ['target_move'])
        report_detail = obj_report.read(self.cr, self.uid, ctx['active_id'], ['detail'])
        users = self.pool.get('res.users').browse(self.cr, self.uid, self.uid, context=None)
        company_id = users.company_id.id
        currency = users.company_id.currency_id
        
        if not fiscal_year:
            fiscal_year = report_fiscal_year['fiscalyear_id']
        previous_label = ''
        previous_fiscal_year_id = self.pool.get('account.fiscalyear').search(self.cr, self.uid, [('id','<',fiscal_year[0])], order='id desc', limit=1)

        try:
            previous_fiscal_year = self.pool.get('account.fiscalyear').browse(self.cr, self.uid, previous_fiscal_year_id)[0]
            if _debug:
                _logger.debug('fiscal_year: %s', fiscal_year)
                _logger.debug('previous_fiscal_year: %s', previous_fiscal_year)
            previous_label = previous_label + ' ' + previous_fiscal_year.name
        except Exception, e:
            previous_label = 'Previous'
            _logger.debug('previous_fiscal_year: %s', 'no previous fiscal year')
            
        divisor = report_divisor['divisor']
        target_move = report_target_move['target_move']
        detail = report_detail['detail'] == 'detail'
        
        if _debug:
            _logger.debug('fiscal_year: %s', fiscal_year)
            _logger.debug('divisor: %s', divisor)
            _logger.debug('target_move: %s', target_move)
            _logger.debug('detail: %s', detail)
        
        res = {
            'fiscal_year' : fiscal_year[1],
            'previous_fy' : previous_label,
            'divisor' : divisor,
            'currency' : currency.name,
            'target' : target_move,
            'detail' : detail,
        }
        
        return_value = []
        return_value.append(res)
        return return_value
    
    def months_names(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_period = self.pool.get('account.period')
        obj_report = self.pool.get('isf.monthly.summary.report.view')
        report_fiscal_year = obj_report.read(self.cr, self.uid, ctx['active_id'], ['fiscalyear_id'])
        fiscal_year = report_fiscal_year['fiscalyear_id']
        
        period_ids = obj_period.search(self.cr, self.uid, [('fiscalyear_id','=',fiscal_year[0]),('special','=',False)])
        periods = obj_period.browse(self.cr, self.uid, period_ids)
        
        def date_format_encoding():
            return locale.getlocale(locale.LC_TIME)[1] or locale.getpreferredencoding()
        
        try:
            current_user = self.pool.get('res.users').browse(self.cr, self.uid, self.uid)
            user_lang = current_user.lang
            lang_id = self.pool.get('res.lang').search(self.cr, self.uid, [('code','=',user_lang)])[0]
            lang = self.pool.get('res.lang').browse(self.cr, self.uid, [lang_id])[0]
            code = lang.iso_code or lang.code or user_lang
            language = pycountry.languages.lookup(str(code[:2])) #needs pycountry=18.5.26
            if _debug_lang:
                _logger.debug('user_lang : %s', user_lang)
                _logger.debug('lang : %s', lang.code)
                _logger.debug('lang (ISO) : %s', lang.iso_code)
                _logger.debug('setting... => %s', language.alpha_2)
                _logger.debug('LC_TIME or default: %s', date_format_encoding())
                
            if os.name == "posix":
                locale.setlocale(locale.LC_ALL, str(lang.iso_code))
            else:
                locale.setlocale(locale.LC_ALL, str(language.name))
        except Exception, e:
            if _debug_lang:
                _logger.debug('Exception : %s', e)
            _logger.debug('Please set the ISO Code properly in Languages!')
        
        # Get Months names for .mako report 
        for period in periods:
            date = datetime.strptime(period.date_start, '%Y-%m-%d')
            month = date.strftime("%b").decode(date_format_encoding()).encode('utf-8')
            self.result_months.append(month)
            if _debug:
                _logger.debug('period_obj : %s', period)
                _logger.debug('period_id %s : %s', period.id, month)
                
        return self.result_months
        
        
    def lines(self, ids=None, done=None, fiscal_year=None):
        ctx = self.context.copy()
        obj_move = self.pool.get('account.move')
        obj_move_line = self.pool.get('account.move.line')
        obj_account = self.pool.get('account.account')
        obj_report = self.pool.get('isf.monthly.summary.report.view')
        obj_period = self.pool.get('account.period')
        obj_fiscal_year = self.pool.get('account.fiscalyear')
        report_fiscal_year = obj_report.read(self.cr, self.uid, ctx['active_id'], ['fiscalyear_id'])
        report_divisor = obj_report.read(self.cr, self.uid, ctx['active_id'], ['divisor'])
        report_target_move = obj_report.read(self.cr, self.uid, ctx['active_id'], ['target_move'])
        
        if not fiscal_year:
            fiscal_year = report_fiscal_year['fiscalyear_id']
        previous_fiscal_year_ids = self.pool.get('account.fiscalyear').search(self.cr, self.uid, [('id','<',fiscal_year[0])], order='id desc', limit=1)
        previous_fiscal_year_id = 0
        try:
            previous_fiscal_year = self.pool.get('account.fiscalyear').browse(self.cr, self.uid, previous_fiscal_year_ids)[0]
            previous_fiscal_year_id = previous_fiscal_year.id
            if _debug:
                _logger.debug('fiscal_year : %s', fiscal_year[1])
                _logger.debug('previous_fiscal_year_id : %s', previous_fiscal_year_id)
        except Exception, e:
            _logger.debug('previous_fiscal_year: %s', 'no previous fiscal year')
            
        divisor = Decimal(str(report_divisor['divisor']))
        target_move = report_target_move['target_move']
        
        #report mapping
        map = {}
        
        #cashin accounts
        cashin_account_hospital_ids = []
        cashin_account_college_ids = []
        cashin_report_ids = self.pool.get('isf.monthly.summary.report').search(self.cr, self.uid, [('type','=','cashin')])
        for report in self.pool.get('isf.monthly.summary.report').browse(self.cr, self.uid, cashin_report_ids, context=None):
            all = report.account_hospital_ids + report.account_college_ids
            all.sort()
            account_hospital_ids = [account.id for account in report.account_hospital_ids]
            account_college_ids = [account.id for account in report.account_college_ids]
            for account in all:
                if account.id in account_hospital_ids:
                    cashin_account_hospital_ids.append(account.id)
                else:
                    cashin_account_college_ids.append(account.id)
                if map.has_key(account.id):
                    note = "\"" + map[account.id] + "\" and \"" + report.name_total + "\""
                    self.result_duplicated.append({'account_code' : account.code, 'ref' : account.name, 'note' : note})
                    self.warnings[0].update({ 'warning' : True })
                map[account.id] = report.name_total
                    
        #cashout accounts
        cashout_account_hospital_ids = []
        cashout_account_college_ids = []
        cashout_report_ids = self.pool.get('isf.monthly.summary.report').search(self.cr, self.uid, [('type','=','cashout')])
        for report in self.pool.get('isf.monthly.summary.report').browse(self.cr, self.uid, cashout_report_ids, context=None):
            all = report.account_hospital_ids + report.account_college_ids
            all.sort()
            account_hospital_ids = [account.id for account in report.account_hospital_ids]
            account_college_ids = [account.id for account in report.account_college_ids]
            for account in all:
                if account.id in account_hospital_ids:
                    cashout_account_hospital_ids.append(account.id)
                else:
                    cashout_account_college_ids.append(account.id)
                if map.has_key(account.id):
                    note = "\"" + map[account.id] + "\" and \"" + report.name_total + "\""
                    self.result_duplicated.append({'account_code' : account.code, 'ref' : account.name, 'note' : note})
                    self.warnings[0].update({ 'warning' : True })
                map[account.id] = report.name_total
                
        #stock changes accounts
        stockchanges_account_hospital_ids = []
        stockchanges_account_college_ids = []
        stockchanges_account_ids = self.pool.get('isf.monthly.summary.report').search(self.cr, self.uid, [('type','=','stockchanges')])
        for report in self.pool.get('isf.monthly.summary.report').browse(self.cr, self.uid, stockchanges_account_ids, context=None):
            all = report.account_hospital_ids + report.account_college_ids
            all.sort()
            account_hospital_ids = [account.id for account in report.account_hospital_ids]
            account_college_ids = [account.id for account in report.account_college_ids]
            for account in all:
                if account.id in account_hospital_ids:
                    stockchanges_account_hospital_ids.append(account.id)
                else:
                    stockchanges_account_college_ids.append(account.id)
                if map.has_key(account.id):
                    note = "\"" + map[account.id] + "\" and \"" + report.name_total + "\""
                    self.result_duplicated.append({'account_code' : account.code, 'ref' : account.name, 'note' : note})
                    self.warnings[0].update({ 'warning' : True })
                map[account.id] = report.name_total
                
        #depreciations accounts
        depreciations_account_hospital_ids = []
        depreciations_account_college_ids = []
        depreciations_account_ids = self.pool.get('isf.monthly.summary.report').search(self.cr, self.uid, [('type','=','depreciations')])
        for report in self.pool.get('isf.monthly.summary.report').browse(self.cr, self.uid, depreciations_account_ids, context=None):
            all = report.account_hospital_ids + report.account_college_ids
            all.sort()
            account_hospital_ids = [account.id for account in report.account_hospital_ids]
            account_college_ids = [account.id for account in report.account_college_ids]
            for account in all:
                if account.id in account_hospital_ids:
                    depreciations_account_hospital_ids.append(account.id)
                else:
                    depreciations_account_college_ids.append(account.id)
                if map.has_key(account.id):
                    note = "\"" + map[account.id] + "\" and \"" + report.name_total + "\""
                    self.result_duplicated.append({'account_code' : account.code, 'ref' : account.name, 'note' : note})
                    self.warnings[0].update({ 'warning' : True })
                map[account.id] = report.name_total
        
        if _debug:    
            _logger.debug('cashin_account_hospital_ids : %s', cashin_account_hospital_ids)
            _logger.debug('cashin_account_college_ids : %s', cashin_account_college_ids)
            _logger.debug('cashout_account_hospital_ids : %s', cashout_account_hospital_ids)
            _logger.debug('cashout_account_college_ids : %s', cashout_account_college_ids)
            _logger.debug('stockchanges_account_hospital_ids : %s', stockchanges_account_hospital_ids)
            _logger.debug('stockchanges_account_college_ids : %s', stockchanges_account_college_ids)
            _logger.debug('depreciations_account_hospital_ids : %s', depreciations_account_hospital_ids)
            _logger.debug('depreciations_account_college_ids : %s', depreciations_account_college_ids)
        
        opening_period_id = obj_period.search(self.cr, self.uid, [('fiscalyear_id','=',fiscal_year[0]),('special','=',True)])
        if _debug:
            _logger.debug('Opening Period id : %s', opening_period_id)
        
        period_ids = obj_period.search(self.cr, self.uid, [('fiscalyear_id','=',fiscal_year[0]),('special','=',False)])
        periods = obj_period.browse(self.cr, self.uid, period_ids)
        if _debug:
            _logger.debug('period_ids : %s', period_ids)
        
        period_hospital_previous_fiscal_year = self._get_period_object('17')
        period_college_previous_fiscal_year = self._get_period_object('18')
        period_previous_fiscal_year = self._get_period_object('0')
        period_fiscal_year = self._get_period_object('13')
        period_hospital = self._get_period_object('14')
        period_college = self._get_period_object('15')
        period_fiscal_year2 = self._get_period_object('16')
        
        previous_period_ids = obj_period.search(self.cr, self.uid, [('fiscalyear_id','=',previous_fiscal_year_id),('special','=',False)])
        if previous_period_ids:
            all_flows = self.pool.get('isf.monthly.summary.report').search(self.cr, self.uid, [('parent_id','=',False)])
            for report in self.pool.get('isf.monthly.summary.report').browse(self.cr, self.uid, all_flows, context=None):
                account_hospital_ids = [account.id for account in report.account_hospital_ids]
                account_college_ids = [account.id for account in report.account_college_ids]
                accounts_query = obj_account.read(
                        self.cr,
                        self.uid,
                        account_hospital_ids + account_college_ids,
                        ['id', 'type', 'code', 'name', 'debit', 'credit', 'balance', 'parent_id', 'level', 'child_id'],
                        context={'all_fiscalyear': True,
                         'periods': previous_period_ids})
                for account in accounts_query:
                    period_previous_fiscal_year[str(account['code'])]['value'] = report.sign * Decimal(str(account['balance']))
                    period_previous_fiscal_year['total_' + report.name_total]['value'] += report.sign * Decimal(str(account['balance']))
                    period_previous_fiscal_year['total_cash_in_flows']['value'] += report.sign * Decimal(str(account['balance']))
                    if account['id'] in account_hospital_ids:
                        period_hospital_previous_fiscal_year[str(account['code'])]['value'] = report.sign * Decimal(str(account['balance']))
                        period_hospital_previous_fiscal_year['total_' + report.name_total]['value'] += report.sign * Decimal(str(account['balance']))
                        period_hospital_previous_fiscal_year['total_cash_in_flows']['value'] += report.sign * Decimal(str(account['balance']))
                    else:
                        period_college_previous_fiscal_year[str(account['code'])]['value'] = report.sign * Decimal(str(account['balance']))
                        period_college_previous_fiscal_year['total_' + report.name_total]['value'] += report.sign * Decimal(str(account['balance']))
                        period_college_previous_fiscal_year['total_cash_in_flows']['value'] += report.sign * Decimal(str(account['balance']))
            
        if _debug:
            _logger.debug('==> period_previous_fiscal_year : %s', period_previous_fiscal_year)
        result_periods = []
        result_periods.append(period_hospital_previous_fiscal_year)
        result_periods.append(period_college_previous_fiscal_year)
        result_periods.append(period_previous_fiscal_year)
        
        period_ids_order = ['17','18','0']
        index = 1 # month 1
        
        previous_period = {}
        for p in periods:
            # save previous period, if any
            if len(result_periods) > 0:
                previous_period = result_periods[-1]
            
            # start new period
            period = self._get_period_object(p.id)
            period_ids_order.append(str(index))
            index += 1
            
            # get all accounts
            all_account_ids = cashin_account_hospital_ids + cashin_account_college_ids \
                + cashout_account_hospital_ids + cashout_account_college_ids \
                + stockchanges_account_hospital_ids + stockchanges_account_college_ids \
                + depreciations_account_hospital_ids + depreciations_account_college_ids
            
            # get only period move lines
            move_lines_ids = obj_move_line.search(self.cr, self.uid, [('period_id','=',p.id),('account_id','in',all_account_ids)])
            move_lines = obj_move_line.browse(self.cr, self.uid, move_lines_ids)
            if not isinstance(move_lines, list):
                move_lines = [move_lines]
            move_ids = []
            for move_line in move_lines:
                move_id = move_line.move_id.id
                state = move_line.move_id.state
                if target_move == 'all':
                    if move_id not in move_ids:
                        move_ids.append(move_id)
                else:
                    if state == 'posted' and move_id not in move_ids:
                        move_ids.append(move_id)
            move_lines_ids = obj_move_line.search(self.cr, self.uid, [('move_id','in',move_ids)])
            if _debug:
                _logger.debug('selected move_ids [period %s]: %s', p.id, move_ids)
                _logger.debug('selected move_lines_ids [period %s]: %s', p.id, move_lines_ids)
                
            move_lines = obj_move_line.browse(self.cr, self.uid, move_lines_ids)
            for line in move_lines:
                account_id = line.account_id.id
                account_name = line.account_id.name
                account_code = line.account_id.code
                state = line.move_id.state
                credit_value = Decimal(str(line.credit)) - Decimal(str(line.debit))
                debit_value = Decimal(str(line.debit)) - Decimal(str(line.credit))
                period_id = line.period_id.id
                
                try:
                    
                    #Test mapping:
                    #If account_code not mapped, it will raise an Exception
                    period[str(account_code)]
                    
                    #posted or not posted
                    if target_move == 'all' and state == 'draft':
                        period[str(account_code)]['posted'] = False
                    
                    # Cash in flows / receipts
                    if account_id in cashin_account_hospital_ids + cashin_account_college_ids:
                        period[str(account_code)]['value'] += Decimal(str(credit_value))
                        period_fiscal_year[str(account_code)]['value'] += Decimal(str(credit_value))
                        report_name = map[account_id]
                        period['total_' +  report_name]['value'] += Decimal(str(credit_value))
                        period['total_' +  report_name]['posted'] &= period[str(account_code)]['posted']
                        period_fiscal_year['total_' + report_name]['value'] += Decimal(str(credit_value))
                        period_fiscal_year['total_' + report_name]['posted'] &= period[str(account_code)]['posted']
                        if account_id in cashin_account_hospital_ids:
                            period_hospital[str(account_code)]['value'] += Decimal(str(credit_value))
                            period_hospital['total_' +  report_name]['value'] += Decimal(str(credit_value))
                            period_hospital['total_' +  report_name]['posted'] &= period[str(account_code)]['posted']
                        elif account_id in cashin_account_college_ids:
                            period_college[str(account_code)]['value'] += Decimal(str(credit_value))
                            period_college['total_' +  report_name]['value'] += Decimal(str(credit_value))
                            period_college['total_' +  report_name]['posted'] &= period[str(account_code)]['posted']
                        period_fiscal_year2[str(account_code)]['value'] += Decimal(str(credit_value))
                        period_fiscal_year2['total_' + report_name]['value'] += Decimal(str(credit_value))
                        period_fiscal_year2['total_' + report_name]['posted'] &= period[str(account_code)]['posted']
                    
                    # Cash out flows / disbursement
                    elif account_id in cashout_account_hospital_ids + cashout_account_college_ids:
                        period[str(account_code)]['value'] += Decimal(str(debit_value))
                        period_fiscal_year[str(account_code)]['value'] += Decimal(str(debit_value))
                        report_name = map[account_id]
                        period['total_' +  report_name]['value'] += Decimal(str(debit_value))
                        period['total_' +  report_name]['posted'] &= period[str(account_code)]['posted']
                        period_fiscal_year['total_' + report_name]['value'] += Decimal(str(debit_value))
                        period_fiscal_year['total_' + report_name]['posted'] &= period[str(account_code)]['posted']
                        if account_id in cashout_account_hospital_ids:
                            period_hospital[str(account_code)]['value'] += Decimal(str(debit_value))
                            period_hospital['total_' +  report_name]['value'] += Decimal(str(debit_value))
                            period_hospital['total_' +  report_name]['posted'] &= period[str(account_code)]['posted']
                        elif account_id in cashout_account_college_ids:
                            period_college[str(account_code)]['value'] += Decimal(str(debit_value))
                            period_college['total_' +  report_name]['value'] += Decimal(str(debit_value))
                            period_college['total_' +  report_name]['posted'] &= period[str(account_code)]['posted']
                        period_fiscal_year2[str(account_code)]['value'] += Decimal(str(debit_value))
                        period_fiscal_year2['total_' + report_name]['value'] += Decimal(str(debit_value))
                        period_fiscal_year2['total_' + report_name]['posted'] &= period[str(account_code)]['posted']
                        
                    # Stock Changes
                    elif account_id in stockchanges_account_hospital_ids + stockchanges_account_college_ids:
                        period[str(account_code)]['value'] += Decimal(str(debit_value))
                        period_fiscal_year[str(account_code)]['value'] += Decimal(str(debit_value))
                        report_name = map[account_id]
                        period['total_' +  report_name]['value'] += Decimal(str(debit_value))
                        period['total_' +  report_name]['posted'] &= period[str(account_code)]['posted']
                        period_fiscal_year['total_' + report_name]['value'] += Decimal(str(debit_value))
                        period_fiscal_year['total_' + report_name]['posted'] &= period[str(account_code)]['posted']
                        if account_id in stockchanges_account_hospital_ids:
                            period_hospital[str(account_code)]['value'] += Decimal(str(debit_value))
                            period_hospital['total_' +  report_name]['value'] += Decimal(str(debit_value))
                            period_hospital['total_' +  report_name]['posted'] &= period[str(account_code)]['posted']
                        elif account_id in stockchanges_account_college_ids:
                            period_college[str(account_code)]['value'] += Decimal(str(debit_value))
                            period_college['total_' +  report_name]['value'] += Decimal(str(debit_value))
                            period_college['total_' +  report_name]['posted'] &= period[str(account_code)]['posted']
                        period_fiscal_year2[str(account_code)]['value'] += Decimal(str(debit_value))
                        period_fiscal_year2['total_' + report_name]['value'] += Decimal(str(debit_value))
                        period_fiscal_year2['total_' + report_name]['posted'] &= period[str(account_code)]['posted']
                        
                    # Depreciations
                    elif account_id in depreciations_account_hospital_ids + depreciations_account_college_ids:
                        period[str(account_code)]['value'] += Decimal(str(debit_value))
                        period_fiscal_year[str(account_code)]['value'] += Decimal(str(debit_value))
                        report_name = map[account_id]
                        period['total_' +  report_name]['value'] += Decimal(str(debit_value))
                        period['total_' +  report_name]['posted'] &= period[str(account_code)]['posted']
                        period_fiscal_year['total_' + report_name]['value'] += Decimal(str(debit_value))
                        period_fiscal_year['total_' + report_name]['posted'] &= period[str(account_code)]['posted']
                        if account_id in depreciations_account_hospital_ids:
                            period_hospital[str(account_code)]['value'] += Decimal(str(debit_value))
                            period_hospital['total_' +  report_name]['value'] += Decimal(str(debit_value))
                            period_hospital['total_' +  report_name]['posted'] &= period[str(account_code)]['posted']
                        elif account_id in depreciations_account_college_ids:
                            period_college[str(account_code)]['value'] += Decimal(str(debit_value))
                            period_college['total_' +  report_name]['value'] += Decimal(str(debit_value))
                            period_college['total_' +  report_name]['posted'] &= period[str(account_code)]['posted']
                        period_fiscal_year2[str(account_code)]['value'] += Decimal(str(debit_value))
                        period_fiscal_year2['total_' + report_name]['value'] += Decimal(str(debit_value))
                        period_fiscal_year2['total_' + report_name]['posted'] &= period[str(account_code)]['posted']
                
                except Exception, e:
                    if _debug:
                        #logging.exception(e) # or pass an error message, see comment
                        _logger.debug('Exception : %s', e)
                        _logger.debug('Not mapped account : %s', account_code)
                    if account_code not in [d['account_code'] for d in self.result_excluded]:
                        self.result_excluded.append({'account_code' : account_code, 'ref' : line.move_id.name, 'note' : line.move_id.ref})
                    self.warnings[0].update({ 'warning' : True })
            
            #Total Cash in flows
            period['total_cash_in_flows']['value'] = Decimal(str(0))
            period_fiscal_year['total_cash_in_flows']['value'] = Decimal(str(0))
            period_hospital_previous_fiscal_year['total_cash_in_flows']['value'] = Decimal(str(0))
            period_college_previous_fiscal_year['total_cash_in_flows']['value'] = Decimal(str(0))
            period_previous_fiscal_year['total_cash_in_flows']['value'] = Decimal(str(0))
            period_hospital['total_cash_in_flows']['value'] = Decimal(str(0))
            period_college['total_cash_in_flows']['value'] = Decimal(str(0))
            period_fiscal_year2['total_cash_in_flows']['value'] = Decimal(str(0))
            cashin_flow = self.pool.get('isf.monthly.summary.report').search(self.cr, self.uid, [('type','=','cashin'),('parent_id','=',False)])
            for report in self.pool.get('isf.monthly.summary.report').browse(self.cr, self.uid, cashin_flow, context=None):
                #Since the first sections have been already summarized from related accounts, we recalculate only the sections with subsections (children)
                children = self.pool.get('isf.monthly.summary.report')._get_children_by_order(self.cr, self.uid, [report.id], context=None)
                children = children[1:] #skip the parent itself
                if len(children) > 0:
                    period['total_' + report.name_total]['value'] += Decimal(str(self._get_report_total_recursive(p.id, ids, report, period, context=None)))
                    period_fiscal_year['total_' + report.name_total]['value'] += Decimal(str(self._get_report_total_recursive(p.id, ids, report, period, context=None)))
                    period_previous_fiscal_year['total_' + report.name_total]['value'] += Decimal(str(self._get_report_total_recursive(p.id, ids, report, period, context=None)))
                    
                #Summarized all first sections
                period['total_cash_in_flows']['value'] += Decimal(str(period['total_' + report.name_total]['value']))
                period['total_cash_in_flows']['posted'] &= period['total_' + report.name_total]['posted']
                period_fiscal_year['total_cash_in_flows']['value'] += Decimal(str(period_fiscal_year['total_' + report.name_total]['value']))
                period_hospital_previous_fiscal_year['total_cash_in_flows']['value'] += Decimal(str(period_hospital_previous_fiscal_year['total_' + report.name_total]['value']))
                period_college_previous_fiscal_year['total_cash_in_flows']['value'] += Decimal(str(period_college_previous_fiscal_year['total_' + report.name_total]['value']))
                period_previous_fiscal_year['total_cash_in_flows']['value'] += Decimal(str(period_previous_fiscal_year['total_' + report.name_total]['value']))
                period_hospital['total_cash_in_flows']['value'] += Decimal(str(period_hospital['total_' + report.name_total]['value']))
                period_college['total_cash_in_flows']['value'] += Decimal(str(period_college['total_' + report.name_total]['value']))
                period_fiscal_year2['total_cash_in_flows']['value'] += Decimal(str(period_fiscal_year2['total_' + report.name_total]['value']))
                
            #Total Cash out flows
            period['total_cash_out_flows']['value'] = Decimal(str(0))
            period_fiscal_year['total_cash_out_flows']['value'] = Decimal(str(0))
            period_hospital_previous_fiscal_year['total_cash_out_flows']['value'] = Decimal(str(0))
            period_college_previous_fiscal_year['total_cash_out_flows']['value'] = Decimal(str(0))
            period_previous_fiscal_year['total_cash_out_flows']['value'] = Decimal(str(0))
            period_hospital['total_cash_out_flows']['value'] = Decimal(str(0))
            period_college['total_cash_out_flows']['value'] = Decimal(str(0))
            period_fiscal_year2['total_cash_out_flows']['value'] = Decimal(str(0))
            cashout_flow = self.pool.get('isf.monthly.summary.report').search(self.cr, self.uid, [('type','=','cashout'),('parent_id','=',False)])
            for report in self.pool.get('isf.monthly.summary.report').browse(self.cr, self.uid, cashout_flow, context=None):
                #Since the first sections have been already summarized from related accounts, we recalculate only the sections with subsections (children)
                children = self.pool.get('isf.monthly.summary.report')._get_children_by_order(self.cr, self.uid, [report.id], context=None)
                children = children[1:] #skip the parent itself
                if len(children) > 0:
                    period['total_' + report.name_total]['value'] += Decimal(str(self._get_report_total_recursive(p.id, ids, report, period, context=None)))
                    period_fiscal_year['total_' + report.name_total]['value'] += Decimal(str(self._get_report_total_recursive(p.id, ids, report, period, context=None)))
                #Summarized all first sections
                period['total_cash_out_flows']['value'] += Decimal(str(period['total_' + report.name_total]['value']))
                period['total_cash_out_flows']['posted'] &= period['total_' + report.name_total]['posted']
                period_fiscal_year['total_cash_out_flows']['value'] += Decimal(str(period_fiscal_year['total_' + report.name_total]['value']))
                period_hospital_previous_fiscal_year['total_cash_out_flows']['value'] += Decimal(str(period_hospital_previous_fiscal_year['total_' + report.name_total]['value']))
                period_college_previous_fiscal_year['total_cash_out_flows']['value'] += Decimal(str(period_college_previous_fiscal_year['total_' + report.name_total]['value']))
                period_previous_fiscal_year['total_cash_out_flows']['value'] += Decimal(str(period_previous_fiscal_year['total_' + report.name_total]['value']))
                period_hospital['total_cash_out_flows']['value'] += Decimal(str(period_hospital['total_' + report.name_total]['value']))
                period_college['total_cash_out_flows']['value'] += Decimal(str(period_college['total_' + report.name_total]['value']))
                period_fiscal_year2['total_cash_out_flows']['value'] += Decimal(str(period_fiscal_year2['total_' + report.name_total]['value']))
            
            # Difference Cash In - Cash Out (before stock changes)
            period['difference_cashin_cashout_before_stockchanges']['value'] = Decimal(str(period['total_cash_in_flows']['value'])) - Decimal(str(period['total_cash_out_flows']['value']))
            period['difference_cashin_cashout_before_stockchanges']['posted'] = period['total_cash_in_flows']['posted'] & period['total_cash_out_flows']['posted']
            period_fiscal_year['difference_cashin_cashout_before_stockchanges']['value'] = Decimal(str(period_fiscal_year['total_cash_in_flows']['value'])) - Decimal(str(period_fiscal_year['total_cash_out_flows']['value']))
            period_hospital_previous_fiscal_year['difference_cashin_cashout_before_stockchanges']['value'] = Decimal(str(period_hospital_previous_fiscal_year['total_cash_in_flows']['value'])) - Decimal(str(period_hospital_previous_fiscal_year['total_cash_out_flows']['value']))
            period_college_previous_fiscal_year['difference_cashin_cashout_before_stockchanges']['value'] = Decimal(str(period_college_previous_fiscal_year['total_cash_in_flows']['value'])) - Decimal(str(period_college_previous_fiscal_year['total_cash_out_flows']['value']))
            period_previous_fiscal_year['difference_cashin_cashout_before_stockchanges']['value'] = Decimal(str(period_previous_fiscal_year['total_cash_in_flows']['value'])) - Decimal(str(period_previous_fiscal_year['total_cash_out_flows']['value']))
            period_hospital['difference_cashin_cashout_before_stockchanges']['value'] = Decimal(str(period_hospital['total_cash_in_flows']['value'])) - Decimal(str(period_hospital['total_cash_out_flows']['value']))
            period_college['difference_cashin_cashout_before_stockchanges']['value'] = Decimal(str(period_college['total_cash_in_flows']['value'])) - Decimal(str(period_college['total_cash_out_flows']['value']))
            period_fiscal_year2['difference_cashin_cashout_before_stockchanges']['value'] = Decimal(str(period_fiscal_year2['total_cash_in_flows']['value'])) - Decimal(str(period_fiscal_year2['total_cash_out_flows']['value']))
            
            #Total Stock Changes
            period['total_stockchanges']['value'] = Decimal(str(0))
            period_fiscal_year['total_stockchanges']['value'] = Decimal(str(0))
            period_hospital_previous_fiscal_year['total_stockchanges']['value'] = Decimal(str(0))
            period_college_previous_fiscal_year['total_stockchanges']['value'] = Decimal(str(0))
            period_previous_fiscal_year['total_stockchanges']['value'] = Decimal(str(0))
            period_hospital['total_stockchanges']['value'] = Decimal(str(0))
            period_college['total_stockchanges']['value'] = Decimal(str(0))
            period_fiscal_year2['total_stockchanges']['value'] = Decimal(str(0))
            stockchanges_flow = self.pool.get('isf.monthly.summary.report').search(self.cr, self.uid, [('type','=','stockchanges'),('parent_id','=',False)])
            for report in self.pool.get('isf.monthly.summary.report').browse(self.cr, self.uid, stockchanges_flow, context=None):
                #Since the first sections have been already summarized from related accounts, we recalculate only the sections with subsections (children)
                children = self.pool.get('isf.monthly.summary.report')._get_children_by_order(self.cr, self.uid, [report.id], context=None)
                children = children[1:] #skip the parent itself
                if len(children) > 0:
                    period['total_' + report.name_total]['value'] += Decimal(str(self._get_report_total_recursive(p.id, ids, report, period, context=None)))
                    period_fiscal_year['total_' + report.name_total]['value'] += Decimal(str(self._get_report_total_recursive(p.id, ids, report, period, context=None)))
                #Summarized all first sections
                period['total_stockchanges']['value'] += Decimal(str(period['total_' + report.name_total]['value']))
                period['total_stockchanges']['posted'] &= period['total_' + report.name_total]['posted']
                period_fiscal_year['total_stockchanges']['value'] += Decimal(str(period_fiscal_year['total_' + report.name_total]['value']))
                period_hospital_previous_fiscal_year['total_stockchanges']['value'] += Decimal(str(period_hospital_previous_fiscal_year['total_' + report.name_total]['value']))
                period_college_previous_fiscal_year['total_stockchanges']['value'] += Decimal(str(period_college_previous_fiscal_year['total_' + report.name_total]['value']))
                period_previous_fiscal_year['total_stockchanges']['value'] += Decimal(str(period_previous_fiscal_year['total_' + report.name_total]['value']))
                period_hospital['total_stockchanges']['value'] += Decimal(str(period_hospital['total_' + report.name_total]['value']))
                period_college['total_stockchanges']['value'] += Decimal(str(period_college['total_' + report.name_total]['value']))
                period_fiscal_year2['total_stockchanges']['value'] += Decimal(str(period_fiscal_year2['total_' + report.name_total]['value']))
            
            # Difference Cash In - Cash Out (before depreciations)
            period['difference_cashin_cashout_before_depreciations']['value'] = Decimal(str(period['difference_cashin_cashout_before_stockchanges']['value'])) - Decimal(str(period['total_stockchanges']['value']))
            period['difference_cashin_cashout_before_depreciations']['posted'] = period['difference_cashin_cashout_before_stockchanges']['posted'] & period['total_stockchanges']['posted']
            period_fiscal_year['difference_cashin_cashout_before_depreciations']['value'] = Decimal(str(period_fiscal_year['difference_cashin_cashout_before_stockchanges']['value'])) - Decimal(str(period_fiscal_year['total_stockchanges']['value']))
            period_hospital_previous_fiscal_year['difference_cashin_cashout_before_depreciations']['value'] = Decimal(str(period_hospital_previous_fiscal_year['difference_cashin_cashout_before_stockchanges']['value'])) - Decimal(str(period_hospital_previous_fiscal_year['total_stockchanges']['value']))
            period_college_previous_fiscal_year['difference_cashin_cashout_before_depreciations']['value'] = Decimal(str(period_college_previous_fiscal_year['difference_cashin_cashout_before_stockchanges']['value'])) - Decimal(str(period_college_previous_fiscal_year['total_stockchanges']['value']))
            period_previous_fiscal_year['difference_cashin_cashout_before_depreciations']['value'] = Decimal(str(period_previous_fiscal_year['difference_cashin_cashout_before_stockchanges']['value'])) - Decimal(str(period_previous_fiscal_year['total_stockchanges']['value']))
            period_hospital['difference_cashin_cashout_before_depreciations']['value'] = Decimal(str(period_hospital['difference_cashin_cashout_before_stockchanges']['value'])) - Decimal(str(period_hospital['total_stockchanges']['value']))
            period_college['difference_cashin_cashout_before_depreciations']['value'] = Decimal(str(period_college['difference_cashin_cashout_before_stockchanges']['value'])) - Decimal(str(period_college['total_stockchanges']['value']))
            period_fiscal_year2['difference_cashin_cashout_before_depreciations']['value'] = Decimal(str(period_fiscal_year2['difference_cashin_cashout_before_stockchanges']['value'])) - Decimal(str(period_fiscal_year2['total_stockchanges']['value']))
            
            #Total Depreciations
            period['total_depreciations']['value'] = Decimal(str(0))
            period_fiscal_year['total_depreciations']['value'] = Decimal(str(0))
            period_hospital_previous_fiscal_year['total_depreciations']['value'] = Decimal(str(0))
            period_college_previous_fiscal_year['total_depreciations']['value'] = Decimal(str(0))
            period_previous_fiscal_year['total_depreciations']['value'] = Decimal(str(0))
            period_hospital['total_depreciations']['value'] = Decimal(str(0))
            period_college['total_depreciations']['value'] = Decimal(str(0))
            period_fiscal_year2['total_depreciations']['value'] = Decimal(str(0))
            depreciations_flow = self.pool.get('isf.monthly.summary.report').search(self.cr, self.uid, [('type','=','depreciations'),('parent_id','=',False)])
            for report in self.pool.get('isf.monthly.summary.report').browse(self.cr, self.uid, depreciations_flow, context=None):
                #Since the first sections have been already summarized from related accounts, we recalculate only the sections with subsections (children)
                children = self.pool.get('isf.monthly.summary.report')._get_children_by_order(self.cr, self.uid, [report.id], context=None)
                children = children[1:] #skip the parent itself
                if len(children) > 0:
                    period['total_' + report.name_total]['value'] += Decimal(str(self._get_report_total_recursive(p.id, ids, report, period, context=None)))
                    period_fiscal_year['total_' + report.name_total]['value'] += Decimal(str(self._get_report_total_recursive(p.id, ids, report, period, context=None)))
                #Summarized all first sections
                period['total_depreciations']['value'] += Decimal(str(period['total_' + report.name_total]['value']))
                period['total_depreciations']['posted'] &= period['total_' + report.name_total]['posted']
                period_fiscal_year['total_depreciations']['value'] += Decimal(str(period_fiscal_year['total_' + report.name_total]['value']))
                period_hospital_previous_fiscal_year['total_depreciations']['value'] += Decimal(str(period_hospital_previous_fiscal_year['total_' + report.name_total]['value']))
                period_college_previous_fiscal_year['total_depreciations']['value'] += Decimal(str(period_college_previous_fiscal_year['total_' + report.name_total]['value']))
                period_previous_fiscal_year['total_depreciations']['value'] += Decimal(str(period_previous_fiscal_year['total_' + report.name_total]['value']))
                period_hospital['total_depreciations']['value'] += Decimal(str(period_hospital['total_' + report.name_total]['value']))
                period_college['total_depreciations']['value'] += Decimal(str(period_college['total_' + report.name_total]['value']))
                period_fiscal_year2['total_depreciations']['value'] += Decimal(str(period_fiscal_year['total_' + report.name_total]['value']))
            
            # Difference Cash In - Cash Out (after depreciations)
            period['difference_cashin_cashout_after_depreciations']['value'] = Decimal(str(period['difference_cashin_cashout_before_depreciations']['value'])) - Decimal(str(period['total_depreciations']['value']))
            period['difference_cashin_cashout_after_depreciations']['posted'] = period['difference_cashin_cashout_before_depreciations']['posted'] & period['total_depreciations']['posted']
            period_fiscal_year['difference_cashin_cashout_after_depreciations']['value'] = Decimal(str(period_fiscal_year['difference_cashin_cashout_before_depreciations']['value'])) - Decimal(str(period_fiscal_year['total_depreciations']['value']))
            period_hospital_previous_fiscal_year['difference_cashin_cashout_after_depreciations']['value'] = Decimal(str(period_hospital_previous_fiscal_year['difference_cashin_cashout_before_depreciations']['value'])) - Decimal(str(period_hospital_previous_fiscal_year['total_depreciations']['value']))
            period_college_previous_fiscal_year['difference_cashin_cashout_after_depreciations']['value'] = Decimal(str(period_college_previous_fiscal_year['difference_cashin_cashout_before_depreciations']['value'])) - Decimal(str(period_college_previous_fiscal_year['total_depreciations']['value']))
            period_previous_fiscal_year['difference_cashin_cashout_after_depreciations']['value'] = Decimal(str(period_previous_fiscal_year['difference_cashin_cashout_before_depreciations']['value'])) - Decimal(str(period_previous_fiscal_year['total_depreciations']['value']))
            period_hospital['difference_cashin_cashout_after_depreciations']['value'] = Decimal(str(period_hospital['difference_cashin_cashout_before_depreciations']['value'])) - Decimal(str(period_hospital['total_depreciations']['value']))
            period_college['difference_cashin_cashout_after_depreciations']['value'] = Decimal(str(period_college['difference_cashin_cashout_before_depreciations']['value'])) - Decimal(str(period_college['total_depreciations']['value']))
            period_fiscal_year2['difference_cashin_cashout_after_depreciations']['value'] = Decimal(str(period_fiscal_year2['difference_cashin_cashout_before_depreciations']['value'])) - Decimal(str(period_fiscal_year2['total_depreciations']['value']))
            
            result_periods.append(period)

        result_periods.append(period_fiscal_year)
        result_periods.append(period_hospital)
        result_periods.append(period_college)
        result_periods.append(period_fiscal_year2)
        
        period_ids_order = period_ids_order + ['13','14','15','16']
        
        if _debug:
            _logger.debug('==> period_ids_order : %s', period_ids_order)  
        
        ### percentage calculation
        period_percentage = self._get_period_object('%')
        period_keys = period_percentage.keys()
        reports_ids = self.pool.get('isf.monthly.summary.report').search(self.cr, self.uid, [])
        for report in self.pool.get('isf.monthly.summary.report').browse(self.cr, self.uid, reports_ids):
            period_percentage['total_' + report.name_total]['value'] = 100
            for account in report.account_hospital_ids + report.account_college_ids:
                try:
                    period_percentage[account.code]['value'] = abs(period_fiscal_year[account.code]['value'] / period_fiscal_year['total_' + report.name_total]['value'] * 100)
                except Exception, e:
                    if _debug:
                        _logger.debug('Exception : %s', e)
         
        result_values = []               
        #### CONVERT TO REPORT DICTIONARY ####
        for key in period_keys:
            temp = {}
            index = 0
            period_id = period_ids_order[index]
            for period in result_periods:
                
                period_id = period_ids_order[index]

                period_value = str(period_id)
                value_posted = str(period_id) + '_posted'
                
                temp['flow'] = period[key]['flow']
                temp['position'] = period[key]['position']
                temp['title'] = period[key]['title']
                temp['type'] = period[key]['type']
                temp['level'] = period[key]['level']
                temp[period_value] = 0
                temp[value_posted] = True 
                
                temp[value_posted] = temp[value_posted] and period[key]['posted']
                
                value = period[key]['value']
                
                if value:
                    if period[key]['period'] == '%':
                        temp[period_value] = value
                    else:
                        temp[period_value] = value / divisor
                 
                index += 1       
                
            result_values.append(temp)
            
        if _debug:
            _logger.debug('result_values : %s', result_values)
        return result_values
    
    
    def _get_report_total_recursive(self, pid, ids=None, report=None, period=None, context=None):
        total = Decimal(str(0))
        children = self.pool.get('isf.monthly.summary.report')._get_children_by_order(self.cr, self.uid, [report.id], context=context)
        children = children[1:] #skip the parent itself
        if len(children) > 0:
            for child in self.pool.get('isf.monthly.summary.report').browse(self.cr, self.uid, children, context=context):
                total += Decimal(str(self._get_report_total_recursive(pid, ids, child, period, context)))
        else:
            total = Decimal(str(period['total_' + report.name_total]['value']))
        return total
    
    def _get_period_object_recursive(self, pid, ids=None, report=None, context=None):
        period = OrderedDict()
        period['title_' + report.name] = {'flow' : report.type, 'level' : report.level, 'type' : 'title', 'posted' : True, 'position' : '', 'period' : pid, 'title' : report.name, 'value' : None}
        sorted_accounts = sorted(report.account_hospital_ids + report.account_college_ids, key=lambda d: d['code']) 
        for account in sorted_accounts:
            period[account.code] = {'flow' : report.type, 'level' : report.level, 'type' : 'value', 'posted' : True, 'position' : account.code, 'period' : pid, 'title' : '', 'value' : Decimal(str(0))}
        children = self.pool.get('isf.monthly.summary.report')._get_children_by_order(self.cr, self.uid, [report.id], context=context)
        children = children[1:] #skip the parent itself
        if len(children) > 0:
            for child in self.pool.get('isf.monthly.summary.report').browse(self.cr, self.uid, children, context=context):
                period.update(self._get_period_object_recursive(pid, ids, child, context))
        period['total_' + report.name_total] = {'flow' : report.type, 'level' : report.level, 'type' : 'total', 'posted' : True, 'position' : '', 'period' : pid, 'title' : report.name_total, 'value' : Decimal(str(0))}
        return period
        
    def _get_period_object(self, pid, ids=None, context=None):
        period = OrderedDict()
        
        ## CASHIN FLOWS
        period['title_cash_in_flows'] = {'flow' : 'cashin', 'level' : 0, 'type' : 'section', 'posted' : True, 'position' : '', 'period' : pid, 'title' : 'INCOMES', 'value' : None}
        cashin_flow = self.pool.get('isf.monthly.summary.report').search(self.cr, self.uid, [('type','=','cashin'),('parent_id','=',False)])
        for report in self.pool.get('isf.monthly.summary.report').browse(self.cr, self.uid, cashin_flow, context=context):
            period.update(self._get_period_object_recursive(pid, ids, report, context))
        period['total_cash_in_flows'] = {'flow' : 'cashin', 'level' : 0, 'type' : 'section_total', 'posted' : True, 'position' : '', 'period' : pid, 'title' : 'TOTAL INCOMES', 'value' : Decimal(str(0))}
        
        ## CASHOUT FLOWS
        period['title_cash_out_flows'] = {'flow' : 'cashout', 'level' : 0, 'type' : 'section', 'posted' : True, 'position' : '', 'period' : pid, 'title' : 'EXPENDITURES', 'value' : None}
        cashout_flow = self.pool.get('isf.monthly.summary.report').search(self.cr, self.uid, [('type','=','cashout'),('parent_id','=',False)])
        for report in self.pool.get('isf.monthly.summary.report').browse(self.cr, self.uid, cashout_flow, context=context):
            period.update(self._get_period_object_recursive(pid, ids, report, context))
        period['total_cash_out_flows'] = {'flow' : 'cashout', 'level' : 0, 'type' : 'section_total', 'posted' : True, 'position' : '', 'period' : pid, 'title' : 'TOTAL EXPENDITURES', 'value' : Decimal(str(0))}
        
        ## DIFF CASHIN - CASHOUT before stock changes
        period['difference_cashin_cashout_before_stockchanges'] = {'flow' : '', 'level' : 0, 'type' : 'calc', 'posted' : True, 'position' : '', 'period' : pid, 'title' : 'RESULT before stock changes', 'value' : Decimal(str(0))}

        ## STOCK CHANGES
        period['title_stockchanges'] = {'flow' : 'stockchanges', 'level' : 0, 'type' : 'section', 'posted' : True, 'position' : '', 'period' : pid, 'title' : 'STOCK CHANGES', 'value' : None}
        stockchanges_flow = self.pool.get('isf.monthly.summary.report').search(self.cr, self.uid, [('type','=','stockchanges'),('parent_id','=',False)])
        for report in self.pool.get('isf.monthly.summary.report').browse(self.cr, self.uid, stockchanges_flow, context=context):
            period.update(self._get_period_object_recursive(pid, ids, report, context))
        period['total_stockchanges'] = {'flow' : 'stockchanges', 'level' : 0, 'type' : 'section_total', 'posted' : True, 'position' : '', 'period' : pid, 'title' : 'TOTAL STOCK CHANGES', 'value' : Decimal(str(0))}
        
        ## DIFF CASHIN - CASHOUT before depreciations
        period['difference_cashin_cashout_before_depreciations'] = {'flow' : '', 'level' : 0, 'type' : 'calc', 'posted' : True, 'position' : '', 'period' : pid, 'title' : 'RESULT before depreciation', 'value' : Decimal(str(0))}
        
        ## DEPRECIATIONS
        period['title_depreciations'] = {'flow' : 'depreciations', 'level' : 0, 'type' : 'section', 'posted' : True, 'position' : '', 'period' : pid, 'title' : 'DEPRECIATIONS', 'value' : None}
        depreciation_flow = self.pool.get('isf.monthly.summary.report').search(self.cr, self.uid, [('type','=','depreciations'),('parent_id','=',False)])
        for report in self.pool.get('isf.monthly.summary.report').browse(self.cr, self.uid, depreciation_flow, context=context):
            period.update(self._get_period_object_recursive(pid, ids, report, context))
        period['total_depreciations'] = {'flow' : 'depreciations', 'level' : 0, 'type' : 'section_total', 'posted' : True, 'position' : '', 'period' : pid, 'title' : 'TOTAL DEPRECIATIONS', 'value' : Decimal(str(0))}
        
        ## DIFF CASHIN - CASHOUT after depreciations
        period['difference_cashin_cashout_after_depreciations'] = {'flow' : '', 'level' : 0, 'type' : 'calc', 'posted' : True, 'position' : '', 'period' : pid, 'title' : 'RESULT after depreciation ', 'value' : Decimal(str(0))}
        
        period_keys = period.keys();
            
        # get accounts names (first 50 characters)
        obj_account = self.pool.get('account.account')
        accounts_ids = obj_account.search(self.cr, self.uid, [('code', 'in', period_keys)])
        accounts = obj_account.browse(self.cr, self.uid, accounts_ids)
        for account in accounts:
            code = account.code
            period[code]['title'] = account.name #[:50]
            
        if _debug:
            _logger.debug('==> period object:')
        #pp.pprint(period)
            
        return period;
    
report_sxw.report_sxw('report.isf.monthly.summary.report.webkit', 
                      'isf.monthly.summary.report.view', 
                      'addons/isf_monthly_summary_report/report/isf_monthly_summary_report.mako', 
                      parser=isf_monthly_summary_report_parser)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
