<html>
<head>
<title>Monthly Summary Report</title>
    <style>
		table {
		    border-spacing: 0px;
		    page-break-inside: auto
		}
		
		table tr {
            page-break-inside: avoid; 
            page-break-after: auto;
  		}
		
		table, td, th {
		    border-bottom: 1px dotted black;
		}
		
		.excel {
			background-color: yellow;
		}
		
		.none {
			border: 0px;
		}
		
		.section {
			font-size: large;
		}
		
		.section_total {
			border-bottom: 2px solid black;
		}
		
		.title {
			border: 0px;
			font-size: medium;
		}
		
		.title_children {
			border: 0px;
			font-size: small;
		}
		
		.subtitle {
			border: 0px;
			font-size: small;
		}
		
		.total {
			border: 2px solid black;
		}
		
		.startfy {
			border-left: 2px solid black;
			border-right: 2px solid black;
			border-top: 2px solid black;
		}
		
		.empty {
			border-left: 1px dotted black;
			border-right: 1px dotted black;
		}
		
		.emptyfy {
			border-left: 2px solid black;
			border-right: 2px solid black;
		}
		
		.endfy {
			border: 2px solid black;
			border-left: 2px solid black;
			border-right: 2px solid black;
			border-bottom: 2px solid black;
		}
		
		.fy {
			border-left: 2px solid black;
			border-right: 2px solid black;
		}
		
		.posted {
			color: black;
		}
		
		.unposted {
			color: blue;
		}
		
    </style>
</head>
<body>
    <%page expression_filter="entity"/>
    <h1><b><i>Monthly Summary Report</i></b></h1>
    <%
    """
    print "IN REPORT!"
    print objects, dir(objects)
    print "*************** LOCALS:"
    pp(locals())
    print date_lines()
    print lines()
    """
    %>
    
    <%
    	months = months_names()
    	cashin_rows = []
    	cashout_rows = []
    	stock_rows = []
    	depreciation_rows = []
    	section_rows = []
    	row_start = 6
    	row = row_start
    %>
    
    <%def name="indent(level, title)">
	    ${"-" * level + " " * level * 4}${title}
	</%def>
	
	<%def name="td_normal(line, x)">
		%if line[x+'_posted']:
			<td width="5%" align="right" class="posted">${formatLang(line[x])}</td>
		%else:
			<td width="5%" align="right" class="unposted">${formatLang(line[x])}</td>
		%endif
	</%def>
	
	<%def name="td_strong(line, x)">
		%if line[x+'_posted']:
			<td width="5%" align="right" class="posted"><strong>${formatLang(line[x])}</strong></td>
		%else:
			<td width="5%" align="right" class="unposted"><strong>${formatLang(line[x])}</strong></td>
		%endif
	</%def>
	
	<%def name="td_strong_sum(line, x, start_row, end_row, column)">
		%if line[x+'_posted']:
			<td width="5%" align="right" class="posted excel"><strong>=SUM(${column}${start_row}:${column}${end_row})</strong></td>
		%else:
			<td width="5%" align="right" class="unposted excel"><strong>=SUM(${column}${start_row}:${column}${end_row})</strong></td>
		%endif
	</%def>
	
	<%def name="td_strong_calc(line, x, column)">
		<% 
			formula = '='
		%>
		%if len(cashin_rows) == 0:
			<% formula = '= 0' %>
		%endif
		%for y in cashin_rows:
			<% formula = formula + column + str(y) %>
		%endfor
		%for y in cashout_rows:
			<% formula = formula + " - " + column + str(y) %>
		%endfor
		%for y in stock_rows:
			<% formula = formula + " - " + column + str(y) %>
		%endfor
		%for y in depreciation_rows:
			<% formula = formula + " - " + column + str(y) %>
		%endfor
		%if line[x+'_posted']:
			<td width="5%" align="right" class="posted excel"><strong>${formula}</strong></td>
		%else:
			<td width="5%" align="right" class="unposted excel"><strong>${formula}</strong></td>
		%endif
	</%def>
	
	<%def name="td_string_calc_detailed(line, x, column)">
		<% 
			formula = '='
		%>
		%if len(section_rows) == 0:
			<% formula = '= 0' %>
		%endif
		%for y in section_rows:
			<% formula = formula + " + " + column + str(y) %>
		%endfor
		%if line[x+'_posted']:
			<td width="5%" align="right" class="posted excel"><strong>${formula}</strong></td>
		%else:
			<td width="5%" align="right" class="unposted excel"><strong>${formula}</strong></td>
		%endif
	</%def>
	
	<%def name="td_empty(x)">
		%for x in ['one'] * x:
			<td></td>
		%endfor
	</%def>
    
    <table style="width: 100%" border="0">
        <tr class="table_header">
            <td colspan="3" height="30px">
                %for date in datelines():
                    Fiscal Year: ${date['fiscal_year']} (Values: ${date['divisor']} ${date['currency']})
                    %if date['target'] == 'all':
                    	<br />
                    	Unposted in blue
                    %else:
                    	<br />
                    	Posted only
                    %endif
                %endfor
            </td>
        </tr>
    </table>

    <table style="width: 100%; font-size: 12px; border: 1px dotted;">
        <tr>
        	%if date['detail']:
	            <th> Ledger Position </th>
	            <th> Account </th>
	        %else:
	        	<th colspan="2"> Account </th>
	        %endif
	        
	        	<th> </th>
	            <th> Hospital ${date['previous_fy']} </th>
	            <th> College ${date['previous_fy']} </th>
	        	<th class="startfy"> Actual ${date['previous_fy']} </th>
	        	<th> </th>
	            <th> Budget ${date['fiscal_year']} </th>
	            
	            %for month in months:
	            	<th> ${month.decode('utf-8', errors='replace')} </th>
	            %endfor
            
	            <th class="startfy"> Actual ${date['fiscal_year']} </th>
	            <th> </th>
	            <th> Hospital ${date['fiscal_year']} </th>
	            <th> College ${date['fiscal_year']} </th>
	            <th class="startfy"> Actual ${date['fiscal_year']} </th>
	            <th> </th>
	            <th class="excel"> Previous VS Budget </th>
	            <th class="excel"> Budget VS Total </th>
        </tr>
        %for line in lines():
        
        	%if date['detail']:
        	
	        	%if line['type'] == 'section':
				    <tr class="section">
				    
				    <% row_new_section = row %>	
				    
	        	%elif line['type'] == 'title':
	        		<tr class="title">
	        		
	        		<% row_new_title = row %>  
	        		
	        	%elif line['type'] == 'total':
					<tr class="total">
					
				%elif line['type'] == 'section_total':
					<tr class="section_total">    
				
				%elif line['type'] == 'calc':
					<tr>
					
	    		%else:
	   				<tr>
	
	        	%endif
		        	%if line['type'] == 'section':
		        		<td class="section" height="50px"><strong>${line['title']}</strong></td>
		        		${td_empty(4)}
		        		<td class="emptyfy"></td>
		        		<td class="section" height="50px"></td>
		        		${td_empty(13)}
		        		<td class="emptyfy"></td>
		        		<td class="section" height="50px"></td>
		        		${td_empty(2)}
		        		<td class="emptyfy"></td>
		        		<td class="section" height="50px"></td>
		        		
		        	%elif line['type'] == 'title':
		        		%if line['level'] == 0:
		        			<td class="title" height="40px"><strong>${indent(line['level'],line['title'])}</strong></td>
		        			${td_empty(4)}
		        			<td class="emptyfy"></td>
		        			<td class="title" height="40px"></td>
		        			${td_empty(13)}
		        		%else:
		        			<td class="title_children" height="30px">${indent(line['level'],line['title'])}</td>
		        			${td_empty(4)}
		        			<td class="emptyfy"></td>
		        			<td class="title_children" height="30px"></td>
		        			${td_empty(13)}
		        		%endif
		        		<td class="emptyfy"></td>
		        		<td class="title" height="40px"></td>
		        		${td_empty(2)}
		        		<td class="emptyfy"></td>
		        		<td class="title" height="40px"></td>
		        	
		        	%elif line['type'] == 'subtitle':
		        		<td class="empty"></td>
		        		<td class="subtitle"><strong>${line['title']}</strong></td>
		        		${td_empty(16)}
		        		<td class="emptyfy"></td>
		        		<td class="subtitle"></td>
		        		${td_empty(2)}
		        		<td class="emptyfy"></td>
		        		<td class="subtitle"></td>
		        		
		        	%elif line['type'] == 'total':
		        		<td class="none" width="5%"></td>
		        		<td width="25%"><strong>${line['title']}</strong></td>
		        				        		
		        	%elif line['type'] == 'section_total':
		        		<td width="5%"></td>
		        		<td width="25%" aling="right"><strong>${line['title']}</strong></td>
		        	
		        	%elif line['type'] == 'calc':
			        		<td class="section" height="20px"></td>
			        		${td_empty(4)}
			        		<td class="emptyfy">
			        		${td_empty(14)}
			        		<td class="emptyfy"></td>
			        		${td_empty(3)}
			        		<td class="emptyfy"></td>
			        		${td_empty(3)}
			        	</tr>
			        	<% row = row + 1 %>
			        	<tr>
			        		<td width="20%"><strong>${line['title']}</strong></td>
			        		<td></td>
		        		
		        	%else:
		        		<td width="5%">${indent(line['level'],line['position'])}</td>
		        		<td width="25%">${line['title']}</td>
		        		
		        	%endif
		        	
	        		%if line['type'] == 'value':
	        			<td></td>
	        			${td_normal(line, '17')}
	        			${td_normal(line, '18')}
		        		<td width="5%" align="right" class="fy">${formatLang(line['0'])}</td>
		        		<td></td>
		        		<td></td>
	        			${td_normal(line, '1')}
			        	${td_normal(line, '2')}
			        	${td_normal(line, '3')}
			        	${td_normal(line, '4')}
			        	${td_normal(line, '5')}
			        	${td_normal(line, '6')}
			        	${td_normal(line, '7')}
			        	${td_normal(line, '8')}
			        	${td_normal(line, '9')}
			        	${td_normal(line, '10')}
			        	${td_normal(line, '11')}
			        	${td_normal(line, '12')}
		        		<td width="5%" align="right" class="fy excel">=SUM(I${row}:T${row})</td>
		        		<td></td>
		        		${td_normal(line, '14')}
		        		${td_normal(line, '15')}
		        		<td width="5%" align="right" class="fy excel">=SUM(W${row}:X${row})</td>
		        		<td></td>
		        		<td class="excel" width="5%">=H${row}-F${row}</td>
		        		<td class="excel" width="5%">=H${row}-U${row}</td>
		        		
		        	%elif line['type'] == 'total':
		        		<td></td>
		        		${td_strong(line, '17')}
		        		${td_strong(line, '18')}
			        	<td width="6%" align="right" class="fy"><strong>${formatLang(line['0'])}</strong></td>
			        	<td></td>
			        	%for column in ['H','I','J','K','L','M','N','O','P','Q','R','S','T']:
							${td_strong_sum(line, str(loop.index + 1), row_new_title + 1, row - 1, column)}
						%endfor
		        		<td width="6%" align="right" class="fy excel"><strong>=SUM(I${row}:T${row})</strong></td>
		        		<td></td>
		        		${td_normal(line, '14')}
		        		${td_normal(line, '15')}
		        		<td width="6%" align="right" class="fy excel"><strong>=SUM(W${row}:X${row})</strong></td>
		        		<td></td>
		        		<td class="excel" width="5%"><strong>=H${row}-F${row}</strong></td>
		        		<td class="excel" width="5%"><strong>=H${row}-U${row}</strong></td>
		        		<% section_rows.append(row) %>
		        	
		        	
		      		%elif line['type'] == 'section_total':
		        		<td></td>
		        		${td_strong(line, '17')}
		        		${td_strong(line, '18')}
			        	<td width="6%" align="right" class="fy"><strong>${formatLang(line['0'])}</strong></td>
			        	<td></td>
			        	%for column in ['H','I','J','K','L','M','N','O','P','Q','R','S','T']:
							${td_string_calc_detailed(line, str(loop.index + 1), column)}
						%endfor
		        		<td width="6%" align="right" class="fy excel"><strong>=SUM(I${row}:T${row})</strong></td>
		        		<td></td>
		        		${td_normal(line, '14')}
		        		${td_normal(line, '15')}
		        		<td width="6%" align="right" class="fy excel"><strong>=SUM(W${row}:X${row})</strong></td>
		        		<td></td>
		        		<td class="excel" width="5%"><strong>=H${row}-F${row}</strong></td>
		        		<td class="excel" width="5%"><strong>=H${row}-U${row}</strong></td>
		        		%if line['flow'] == 'cashin':
							<% cashin_rows.append(row) %>
						
						%elif line['flow'] == 'cashout':
							<% cashout_rows.append(row) %>
							
						%elif line['flow'] == 'stockchanges':
							<% stock_rows.append(row) %>
							
						%elif line['flow'] == 'depreciations':
							<% depreciation_rows.append(row) %>
						
						%endif
						<% section_rows = [] %>
						
								        		
		        	%elif line['type'] == 'calc':
			        	<td></td>
		        		${td_strong(line, '17')}
		        		${td_strong(line, '18')}
			        	<td width="6%" align="right" class="endfy"><strong>${formatLang(line['0'])}</strong></td>
			        	<td></td>
			        	%for column in ['H','I','J','K','L','M','N','O','P','Q','R','S','T']:
							${td_strong_calc(line, str(loop.index + 1), column)}
						%endfor
		        		<td width="6%" align="right" class="endfy excel"><strong>=SUM(I${row}:T${row})</strong></td>
		        		<td></td>
		        		${td_normal(line, '14')}
		        		${td_normal(line, '15')}
		        		<td width="6%" align="right" class="endfy excel"><strong>=SUM(W${row}:X${row})</strong></td>
		        		<td></td>
		        		<td class="excel" width="5%"><strong>=H${row}-F${row}</strong></td>
		        		<td class="excel" width="5%"><strong>=H${row}-U${row}</strong></td>
		        	%endif
	        	</tr>
	        	<% row = row + 1 %>
	        %else:
	        
	        	%if line['type'] == 'section':
				    <tr class="section">
				    	<td class="section" height="50px"><strong>${line['title']}</strong></td>
				    	${td_empty(4)}
				    	<td class="emptyfy"></td>
				    	${td_empty(14)}
		        		<td class="emptyfy"></td>
		        		${td_empty(3)}
		        		<td class="emptyfy"></td>
		        		<td class="section" height="50px"></td>
				    </tr>
				    <% row_new_section = row %>
				    <% row = row + 1 %>
				%elif line['type'] == 'total':
					<tr class="total">
	        			<td colspan="2" width="20%">${line['title']}</td>
						<td></td>
		        		${td_normal(line, '17')}
		        		${td_normal(line, '18')}
		        		<td width="6%" align="right" class="fy">${formatLang(line['0'])}</td>
		        		<td></td>
		        		<td></td>
	        			${td_normal(line, '1')}
			        	${td_normal(line, '2')}
			        	${td_normal(line, '3')}
			        	${td_normal(line, '4')}
			        	${td_normal(line, '5')}
			        	${td_normal(line, '6')}
			        	${td_normal(line, '7')}
			        	${td_normal(line, '8')}
			        	${td_normal(line, '9')}
			        	${td_normal(line, '10')}
			        	${td_normal(line, '11')}
			        	${td_normal(line, '12')}
		        		<td width="6%" align="right" class="fy excel">=SUM(I${row}:T${row})</td>
		        		<td></td>
		        		${td_normal(line, '14')}
		        		${td_normal(line, '15')}
		        		<td width="6%" align="right" class="fy excel">=SUM(W${row}:X${row})</td>
		        		<td></td>
		        		<td class="excel" width="5%">=H${row}-F${row}</td>
		        		<td class="excel" width="5%">=H${row}-U${row}</td>
		        	</tr>
		        	<% row = row + 1 %>	
				%elif line['type'] == 'section_total':
					<tr class="total">
	        			<td colspan="2" width="20%" align="right"><strong>${line['title']}</strong></td>
		        		<td></td>
		        		${td_strong(line, '17')}
		        		${td_strong(line, '18')}
			        	<td width="6%" align="right" class="fy"><strong>${formatLang(line['0'])}</strong></td>
			        	<td></td>
			        	%for column in ['H','I','J','K','L','M','N','O','P','Q','R','S','T']:
							${td_strong_sum(line, str(loop.index + 1), row_new_section + 1, row - 1, column)}
						%endfor
		        		<td width="6%" align="right" class="fy excel"><strong>=SUM(I${row}:T${row})</strong></td>
						<td></td>
						${td_normal(line, '14')}
		        		${td_normal(line, '15')}
		        		<td width="6%" align="right" class="fy excel"><strong>=SUM(W${row}:X${row})</strong></td>
		        		<td></td>
		        		<td class="excel" width="5%"><strong>=H${row}-F${row}</strong></td>
		        		<td class="excel" width="5%"><strong>=H${row}-U${row}</strong></td>
		        	</tr>
					%if line['flow'] == 'cashin':
						<% cashin_rows.append(row) %>
					
					%elif line['flow'] == 'cashout':
						<% cashout_rows.append(row) %>
						
					%elif line['flow'] == 'stockchanges':
						<% stock_rows.append(row) %>
						
					%elif line['flow'] == 'depreciations':
						<% depreciation_rows.append(row) %>
					
					%endif     	
		        	<% row = row + 1 %>
		        %elif line['type'] == 'calc':
		        	<tr>
		        		${td_empty(5)}
		        		<td class="emptyfy"></td>
		        		${td_empty(14)}
		        		<td class="emptyfy"></td>
		        		${td_empty(2)}
		        		<td class="emptyfy"></td>
		        		<td></td>
		        	</tr>
		        	<% row = row + 1 %>
		        	<tr>
		        		<td colspan="2" width="20%"><strong>${line['title']}</strong></td>
		        		<td></td>
		        		${td_strong(line, '17')}
		        		${td_strong(line, '18')}
			        	<td width="6%" align="right" class="endfy"><strong>${formatLang(line['0'])}</strong></td>
			        	<td></td>
			        	%for column in ['H','I','J','K','L','M','N','O','P','Q','R','S','T']:
							${td_strong_calc(line, str(loop.index + 1), column)}
						%endfor
		        		<td width="6%" align="right" class="endfy excel"><strong>=SUM(I${row}:T${row})</strong></td>
		        		<td></td>
		        		${td_normal(line, '14')}
		        		${td_normal(line, '15')}
		        		<td width="6%" align="right" class="endfy excel"><strong>=SUM(W${row}:X${row})</strong></td>
		        		<td></td>
		        		<td class="excel" width="5%"><strong>=H${row}-F${row}</strong></td>
		        		<td class="excel" width="5%"><strong>=H${row}-U${row}</strong></td>
		        	</tr>
		        	<% row = row + 1 %>
				%endif
	        %endif
        %endfor
    </table>
    
    <!--- previous year --->
    <br/>
    <% 
    	row = row + 3
    	distance = row - row_start
    	date = previous_datelines()[0]
    %>
    
    <table style="width: 100%" border="0">
        <tr class="table_header">
            <td colspan="3" height="30px">
                <h1><b><i>Previous Year</i></b></h1>
            </td>
        </tr>
    </table>
    
    <table style="width: 100%; font-size: 12px; border: 1px dotted;">
        <tr>
        	%if date['detail']:
	            <th> Previous Year ${date['fiscal_year']} </th>
	            <th> Account </th>
	        %else:
	        	<th colspan="2"> Account </th>
	        %endif
	        
	        	<th> </th>
	            <th> Hospital ${date['previous_fy']} </th>
	            <th> College ${date['previous_fy']} </th>
	        	<th class="startfy"> Actual ${date['previous_fy']} </th>
	        	<th> </th>
	            <th> Budget ${date['fiscal_year']} </th>
	            
	            %for month in months:
	            	<th> ${month.decode('utf-8', errors='replace')} </th>
	            %endfor
            
	            <th class="startfy"> Actual ${date['fiscal_year']} </th>
	            <th> </th>
	            <th> Hospital ${date['fiscal_year']} </th>
	            <th> College ${date['fiscal_year']} </th>
	            <th class="startfy"> Actual ${date['fiscal_year']} </th>
	            <th> </th>
	            <th class="excel"> Previous VS Budget </th>
	            <th class="excel"> Budget VS Total </th>
        </tr>
        %for line in previous_lines():
        
        	%if date['detail']:
        	
	        	%if line['type'] == 'section':
				    <tr class="section">
				    
				    <% row_new_section = row %>	
				    
	        	%elif line['type'] == 'title':
	        		<tr class="title">
	        		
	        		<% row_new_title = row %>  
	        		
	        	%elif line['type'] == 'total':
					<tr class="total">
					
				%elif line['type'] == 'section_total':
					<tr class="section_total">    
				
				%elif line['type'] == 'calc':
					<tr>
					
	    		%else:
	   				<tr>
	
	        	%endif
		        	%if line['type'] == 'section':
		        		<td class="section" height="50px"><strong>${line['title']}</strong></td>
		        		${td_empty(4)}
		        		<td class="emptyfy"></td>
		        		<td class="section" height="50px"></td>
		        		${td_empty(13)}
		        		<td class="emptyfy"></td>
		        		<td class="section" height="50px"></td>
		        		${td_empty(2)}
		        		<td class="emptyfy"></td>
		        		<td class="section" height="50px"></td>
		        		
		        	%elif line['type'] == 'title':
		        		%if line['level'] == 0:
		        			<td class="title" height="40px"><strong>${indent(line['level'],line['title'])}</strong></td>
		        			${td_empty(4)}
		        			<td class="emptyfy"></td>
		        			<td class="title" height="40px"></td>
		        			${td_empty(13)}
		        		%else:
		        			<td class="title_children" height="30px">${indent(line['level'],line['title'])}</td>
		        			${td_empty(4)}
		        			<td class="emptyfy"></td>
		        			<td class="title_children" height="30px"></td>
		        			${td_empty(13)}
		        		%endif
		        		<td class="emptyfy"></td>
		        		<td class="title" height="40px"></td>
		        		${td_empty(2)}
		        		<td class="emptyfy"></td>
		        		<td class="title" height="40px"></td>
		        	
		        	%elif line['type'] == 'subtitle':
		        		<td class="empty"></td>
		        		<td class="subtitle"><strong>${line['title']}</strong></td>
		        		${td_empty(16)}
		        		<td class="emptyfy"></td>
		        		<td class="subtitle"></td>
		        		${td_empty(2)}
		        		<td class="emptyfy"></td>
		        		<td class="subtitle"></td>
		        		
		        	%elif line['type'] == 'total':
		        		<td class="none" width="5%"></td>
		        		<td width="25%"><strong>${line['title']}</strong></td>
		        				        		
		        	%elif line['type'] == 'section_total':
		        		<td width="5%"></td>
		        		<td width="25%" aling="right"><strong>${line['title']}</strong></td>
		        	
		        	%elif line['type'] == 'calc':
			        		<td class="section" height="20px"></td>
			        		${td_empty(4)}
			        		<td class="emptyfy">
			        		${td_empty(14)}
			        		<td class="emptyfy"></td>
			        		${td_empty(3)}
			        		<td class="emptyfy"></td>
			        		${td_empty(3)}
			        	</tr>
			        	<% row = row + 1 %>
			        	<tr>
			        		<td width="20%"><strong>${line['title']}</strong></td>
			        		<td></td>
		        		
		        	%else:
		        		<td width="5%">${indent(line['level'],line['position'])}</td>
		        		<td width="25%">${line['title']}</td>
		        		
		        	%endif
		        	
	        		%if line['type'] == 'value':
	        			<td></td>
	        			${td_normal(line, '17')}
	        			${td_normal(line, '18')}
		        		<td width="5%" align="right" class="fy">${formatLang(line['0'])}</td>
		        		<td></td>
		        		<td></td>
	        			${td_normal(line, '1')}
			        	${td_normal(line, '2')}
			        	${td_normal(line, '3')}
			        	${td_normal(line, '4')}
			        	${td_normal(line, '5')}
			        	${td_normal(line, '6')}
			        	${td_normal(line, '7')}
			        	${td_normal(line, '8')}
			        	${td_normal(line, '9')}
			        	${td_normal(line, '10')}
			        	${td_normal(line, '11')}
			        	${td_normal(line, '12')}
		        		<td width="5%" align="right" class="fy excel">=SUM(I${row}:T${row})</td>
		        		<td></td>
		        		${td_normal(line, '14')}
		        		${td_normal(line, '15')}
		        		<td width="5%" align="right" class="fy excel">=SUM(W${row}:X${row})</td>
		        		<td></td>
		        		<td class="excel" width="5%">=H${row}-F${row}</td>
		        		<td class="excel" width="5%">=H${row}-U${row}</td>
		        		
		        	%elif line['type'] == 'total':
		        		<td></td>
		        		${td_strong(line, '17')}
		        		${td_strong(line, '18')}
			        	<td width="6%" align="right" class="fy"><strong>${formatLang(line['0'])}</strong></td>
			        	<td></td>
			        	<td></td>
			        	${td_normal(line, '1')}
			        	${td_normal(line, '2')}
			        	${td_normal(line, '3')}
			        	${td_normal(line, '4')}
			        	${td_normal(line, '5')}
			        	${td_normal(line, '6')}
			        	${td_normal(line, '7')}
			        	${td_normal(line, '8')}
			        	${td_normal(line, '9')}
			        	${td_normal(line, '10')}
			        	${td_normal(line, '11')}
			        	${td_normal(line, '12')}
		        		<td width="6%" align="right" class="fy excel"><strong>=SUM(I${row}:T${row})</strong></td>
		        		<td></td>
		        		${td_normal(line, '14')}
		        		${td_normal(line, '15')}
		        		<td width="6%" align="right" class="fy excel"><strong>=SUM(W${row}:X${row})</strong></td>
		        		<td></td>
		        		<td class="excel" width="5%"><strong>=H${row}-F${row}</strong></td>
		        		<td class="excel" width="5%"><strong>=H${row}-U${row}</strong></td>
		        		<% section_rows.append(row) %>
		        	
		        	
		      		%elif line['type'] == 'section_total':
		        		<td></td>
		        		${td_strong(line, '17')}
		        		${td_strong(line, '18')}
			        	<td width="6%" align="right" class="fy"><strong>${formatLang(line['0'])}</strong></td>
			        	<td></td>
			        	<td></td>
			        	${td_normal(line, '1')}
			        	${td_normal(line, '2')}
			        	${td_normal(line, '3')}
			        	${td_normal(line, '4')}
			        	${td_normal(line, '5')}
			        	${td_normal(line, '6')}
			        	${td_normal(line, '7')}
			        	${td_normal(line, '8')}
			        	${td_normal(line, '9')}
			        	${td_normal(line, '10')}
			        	${td_normal(line, '11')}
			        	${td_normal(line, '12')}
		        		<td width="6%" align="right" class="fy excel"><strong>=SUM(I${row}:T${row})</strong></td>
		        		<td></td>
		        		${td_normal(line, '14')}
		        		${td_normal(line, '15')}
		        		<td width="6%" align="right" class="fy excel"><strong>=SUM(W${row}:X${row})</strong></td>
		        		<td></td>
		        		<td class="excel" width="5%"><strong>=H${row}-F${row}</strong></td>
		        		<td class="excel" width="5%"><strong>=H${row}-U${row}</strong></td>
		        		%if line['flow'] == 'cashin':
							<% cashin_rows.append(row) %>
						
						%elif line['flow'] == 'cashout':
							<% cashout_rows.append(row) %>
							
						%elif line['flow'] == 'stockchanges':
							<% stock_rows.append(row) %>
							
						%elif line['flow'] == 'depreciations':
							<% depreciation_rows.append(row) %>
						
						%endif
						<% section_rows = [] %>
						
								        		
		        	%elif line['type'] == 'calc':
			        	<td></td>
		        		${td_strong(line, '17')}
		        		${td_strong(line, '18')}
			        	<td width="6%" align="right" class="endfy"><strong>${formatLang(line['0'])}</strong></td>
			        	<td></td>
			        	<td></td>
			        	${td_normal(line, '1')}
			        	${td_normal(line, '2')}
			        	${td_normal(line, '3')}
			        	${td_normal(line, '4')}
			        	${td_normal(line, '5')}
			        	${td_normal(line, '6')}
			        	${td_normal(line, '7')}
			        	${td_normal(line, '8')}
			        	${td_normal(line, '9')}
			        	${td_normal(line, '10')}
			        	${td_normal(line, '11')}
			        	${td_normal(line, '12')}
		        		<td width="6%" align="right" class="endfy excel"><strong>=SUM(I${row}:T${row})</strong></td>
		        		<td></td>
		        		${td_normal(line, '14')}
		        		${td_normal(line, '15')}
		        		<td width="6%" align="right" class="endfy excel"><strong>=SUM(W${row}:X${row})</strong></td>
		        		<td></td>
		        		<td class="excel" width="5%"><strong>=H${row}-F${row}</strong></td>
		        		<td class="excel" width="5%"><strong>=H${row}-U${row}</strong></td>
		        	%endif
	        	</tr>
	        	<% row = row + 1 %>
	        %else:
	        
	        	%if line['type'] == 'section':
				    <tr class="section">
				    	<td class="section" height="50px"><strong>${line['title']}</strong></td>
				    	${td_empty(4)}
				    	<td class="emptyfy"></td>
				    	${td_empty(14)}
		        		<td class="emptyfy"></td>
		        		${td_empty(3)}
		        		<td class="emptyfy"></td>
		        		<td class="section" height="50px"></td>
				    </tr>
				    <% row_new_section = row %>
				    <% row = row + 1 %>
				%elif line['type'] == 'total':
					<tr class="total">
	        			<td colspan="2" width="20%">${line['title']}</td>
						<td></td>
		        		${td_normal(line, '17')}
		        		${td_normal(line, '18')}
		        		<td width="6%" align="right" class="fy">${formatLang(line['0'])}</td>
		        		<td></td>
		        		<td></td>
	        			${td_normal(line, '1')}
			        	${td_normal(line, '2')}
			        	${td_normal(line, '3')}
			        	${td_normal(line, '4')}
			        	${td_normal(line, '5')}
			        	${td_normal(line, '6')}
			        	${td_normal(line, '7')}
			        	${td_normal(line, '8')}
			        	${td_normal(line, '9')}
			        	${td_normal(line, '10')}
			        	${td_normal(line, '11')}
			        	${td_normal(line, '12')}
		        		<td width="6%" align="right" class="fy excel">=SUM(I${row}:T${row})</td>
		        		<td></td>
		        		${td_normal(line, '14')}
		        		${td_normal(line, '15')}
		        		<td width="6%" align="right" class="fy excel">=SUM(W${row}:X${row})</td>
		        		<td></td>
		        		<td class="excel" width="5%">=H${row}-F${row}</td>
		        		<td class="excel" width="5%">=H${row}-U${row}</td>
		        	</tr>
		        	<% row = row + 1 %>	
				%elif line['type'] == 'section_total':
					<tr class="total">
	        			<td colspan="2" width="20%" align="right"><strong>${line['title']}</strong></td>
		        		<td></td>
		        		${td_strong(line, '17')}
		        		${td_strong(line, '18')}
			        	<td width="6%" align="right" class="fy"><strong>${formatLang(line['0'])}</strong></td>
			        	<td></td>
			        	<td></td>
			        	${td_normal(line, '1')}
			        	${td_normal(line, '2')}
			        	${td_normal(line, '3')}
			        	${td_normal(line, '4')}
			        	${td_normal(line, '5')}
			        	${td_normal(line, '6')}
			        	${td_normal(line, '7')}
			        	${td_normal(line, '8')}
			        	${td_normal(line, '9')}
			        	${td_normal(line, '10')}
			        	${td_normal(line, '11')}
			        	${td_normal(line, '12')}
		        		<td width="6%" align="right" class="fy excel"><strong>=SUM(I${row}:T${row})</strong></td>
						<td></td>
						${td_normal(line, '14')}
		        		${td_normal(line, '15')}
		        		<td width="6%" align="right" class="fy excel"><strong>=SUM(W${row}:X${row})</strong></td>
		        		<td></td>
		        		<td class="excel" width="5%"><strong>=H${row}-F${row}</strong></td>
		        		<td class="excel" width="5%"><strong>=H${row}-U${row}</strong></td>
		        	</tr>
					%if line['flow'] == 'cashin':
						<% cashin_rows.append(row) %>
					
					%elif line['flow'] == 'cashout':
						<% cashout_rows.append(row) %>
						
					%elif line['flow'] == 'stockchanges':
						<% stock_rows.append(row) %>
						
					%elif line['flow'] == 'depreciations':
						<% depreciation_rows.append(row) %>
					
					%endif     	
		        	<% row = row + 1 %>
		        %elif line['type'] == 'calc':
		        	<tr>
		        		${td_empty(5)}
		        		<td class="emptyfy"></td>
		        		${td_empty(14)}
		        		<td class="emptyfy"></td>
		        		${td_empty(2)}
		        		<td class="emptyfy"></td>
		        		<td></td>
		        	</tr>
		        	<% row = row + 1 %>
		        	<tr>
		        		<td colspan="2" width="20%"><strong>${line['title']}</strong></td>
		        		<td></td>
		        		${td_strong(line, '17')}
		        		${td_strong(line, '18')}
			        	<td width="6%" align="right" class="endfy"><strong>${formatLang(line['0'])}</strong></td>
			        	<td></td>
			        	<td></td>
			        	${td_normal(line, '1')}
			        	${td_normal(line, '2')}
			        	${td_normal(line, '3')}
			        	${td_normal(line, '4')}
			        	${td_normal(line, '5')}
			        	${td_normal(line, '6')}
			        	${td_normal(line, '7')}
			        	${td_normal(line, '8')}
			        	${td_normal(line, '9')}
			        	${td_normal(line, '10')}
			        	${td_normal(line, '11')}
			        	${td_normal(line, '12')}
		        		<td width="6%" align="right" class="endfy excel"><strong>=SUM(I${row}:T${row})</strong></td>
		        		<td></td>
		        		${td_normal(line, '14')}
		        		${td_normal(line, '15')}
		        		<td width="6%" align="right" class="endfy excel"><strong>=SUM(W${row}:X${row})</strong></td>
		        		<td></td>
		        		<td class="excel" width="5%"><strong>=H${row}-F${row}</strong></td>
		        		<td class="excel" width="5%"><strong>=H${row}-U${row}</strong></td>
		        	</tr>
		        	<% row = row + 1 %>
				%endif
	        %endif
        %endfor
    </table>
    
    <!-- comparison table -->
    <br/>
    <% row = row + 3 %>
    
    <table style="width: 100%" border="0">
        <tr class="section">
            <td colspan="3" height="30px" class="section">
            	<h1><b><i>Variances (+/-) Current Year / Previous Year</i></b></h1>
            </td>
        </tr>
    </table>
    
    <table style="width: 100%; font-size: 12px; border: 1px dotted;">
        <tr>
        	%if date['detail']:
	            <th> Ledger Position </th>
	            <th> Account </th>
	        %else:
	        	<th colspan="2"> Account </th>
	        %endif
        
        	<th> </th>
            <th> Hospital ${date['previous_fy']} </th>
            <th> College ${date['previous_fy']} </th>
        	<th class="startfy"> Actual ${date['previous_fy']} </th>
        	<th> </th>
            <th> Budget ${date['fiscal_year']} </th>
            
            %for month in months:
            	<th> ${month.decode('utf-8', errors='replace')} </th>
            %endfor
        
            <th class="startfy"> Actual ${date['fiscal_year']} </th>
            <th> </th>
            <th> Hospital ${date['fiscal_year']} </th>
            <th> College ${date['fiscal_year']} </th>
            <th class="startfy"> Actual ${date['fiscal_year']} </th>
            <th> </th>
            <th class="excel"> Previous VS Budget </th>
            <th class="excel"> Budget VS Total </th>
        </tr>
        %for line in previous_lines():
        
        	%if date['detail']:
        	
	        	%if line['type'] == 'section':
				    <tr class="section">
				    
				    <% row_new_section = row %>	
				    
	        	%elif line['type'] == 'title':
	        		<tr class="title">
	        		
	        		<% row_new_title = row %>  
	        		
	        	%elif line['type'] == 'total':
					<tr class="total">
					
				%elif line['type'] == 'section_total':
					<tr class="section_total">    
				
				%elif line['type'] == 'calc':
					<tr>
					
	    		%else:
	   				<tr>
	
	        	%endif
		        	%if line['type'] == 'section':
		        		<td class="section" height="50px"><strong>${line['title']}</strong></td>
		        		${td_empty(4)}
		        		<td class="emptyfy"></td>
		        		<td class="section" height="50px"></td>
		        		${td_empty(13)}
		        		<td class="emptyfy"></td>
		        		<td class="section" height="50px"></td>
		        		${td_empty(2)}
		        		<td class="emptyfy"></td>
		        		<td class="section" height="50px"></td>
		        		
		        	%elif line['type'] == 'title':
		        		%if line['level'] == 0:
		        			<td class="title" height="40px"><strong>${indent(line['level'],line['title'])}</strong></td>
		        			${td_empty(4)}
		        			<td class="emptyfy"></td>
		        			<td class="title" height="40px"></td>
		        			${td_empty(13)}
		        		%else:
		        			<td class="title_children" height="30px">${indent(line['level'],line['title'])}</td>
		        			${td_empty(4)}
		        			<td class="emptyfy"></td>
		        			<td class="title_children" height="30px"></td>
		        			${td_empty(13)}
		        		%endif
		        		<td class="emptyfy"></td>
		        		<td class="title" height="40px"></td>
		        		${td_empty(2)}
		        		<td class="emptyfy"></td>
		        		<td class="title" height="40px"></td>
		        	
		        	%elif line['type'] == 'subtitle':
		        		<td class="empty"></td>
		        		<td class="subtitle"><strong>${line['title']}</strong></td>
		        		${td_empty(16)}
		        		<td class="emptyfy"></td>
		        		<td class="subtitle"></td>
		        		${td_empty(2)}
		        		<td class="emptyfy"></td>
		        		<td class="subtitle"></td>
		        		
		        	%elif line['type'] == 'total':
		        		<td class="none" width="5%"></td>
		        		<td width="25%"><strong>${line['title']}</strong></td>
		        				        		
		        	%elif line['type'] == 'section_total':
		        		<td width="5%"></td>
		        		<td width="25%" aling="right"><strong>${line['title']}</strong></td>
		        	
		        	%elif line['type'] == 'calc':
			        		<td class="section" height="20px"></td>
			        		${td_empty(4)}
			        		<td class="emptyfy">
			        		${td_empty(14)}
			        		<td class="emptyfy"></td>
			        		${td_empty(3)}
			        		<td class="emptyfy"></td>
			        		${td_empty(3)}
			        	</tr>
			        	<% row = row + 1 %>
			        	<tr>
			        		<td width="20%"><strong>${line['title']}</strong></td>
			        		<td></td>
		        		
		        	%else:
		        		<td width="5%">${indent(line['level'],line['position'])}</td>
		        		<td width="25%">${line['title']}</td>
		        		
		        	%endif
		        	
	        		%if line['type'] == 'value':
	        			<td></td>
	        			%for column in ['D','E']:
							<td class="excel">=${column}${row - distance * 2} - ${column}${row - distance}</td>
						%endfor
		        		<td width="5%" align="right" class="fy excel">=${column}${row - distance * 2} - ${column}${row - distance}</td>
		        		<td></td>
		        		<td></td>
		        		%for column in ['I','J','K','L','M','N','O','P','Q','R','S','T']:
							<td class="excel">=${column}${row - distance * 2} - ${column}${row - distance}</td>
						%endfor
		        		<td width="5%" align="right" class="fy excel">=SUM(I${row}:T${row})</td>
		        		<td></td>
		        		%for column in ['W','X']:
							<td class="excel">=${column}${row - distance * 2} - ${column}${row - distance}</td>
						%endfor
		        		<td width="5%" align="right" class="fy excel">=SUM(W${row}:X${row})</td>
		        		<td></td>
		        		<td class="excel" width="5%">=H${row}-F${row}</td>
		        		<td class="excel" width="5%">=H${row}-U${row}</td>
		        		
		        	%elif line['type'] == 'total':
		        		<td></td>
		        		%for column in ['D','E']:
							<td class="excel">=${column}${row - distance * 2} - ${column}${row - distance}</td>
						%endfor
			        	<td width="6%" align="right" class="fy excel"><strong>=${column}${row - distance * 2} - ${column}${row - distance}</strong></td>
			        	<td></td>
			        	<td></td>
			        	%for column in ['I','J','K','L','M','N','O','P','Q','R','S','T']:
							<td class="excel">=${column}${row - distance * 2} - ${column}${row - distance}</td>
						%endfor
		        		<td width="6%" align="right" class="fy excel"><strong>=SUM(I${row}:T${row})</strong></td>
		        		<td></td>
		        		%for column in ['W','X']:
							<td class="excel">=${column}${row - distance * 2} - ${column}${row - distance}</td>
						%endfor
		        		<td width="6%" align="right" class="fy excel"><strong>=SUM(W${row}:X${row})</strong></td>
		        		<td></td>
		        		<td class="excel" width="5%"><strong>=H${row}-F${row}</strong></td>
		        		<td class="excel" width="5%"><strong>=H${row}-U${row}</strong></td>
		        		<% section_rows.append(row) %>
		        	
		        	
		      		%elif line['type'] == 'section_total':
		        		<td></td>
		        		%for column in ['D','E']:
							<td class="excel">=${column}${row - distance * 2} - ${column}${row - distance}</td>
						%endfor
			        	<td width="6%" align="right" class="fy excel"><strong>=${column}${row - distance * 2} - ${column}${row - distance}</strong></td>
			        	<td></td>
			        	<td></td>
			        	%for column in ['I','J','K','L','M','N','O','P','Q','R','S','T']:
							<td class="excel">=${column}${row - distance * 2} - ${column}${row - distance}</td>
						%endfor
		        		<td width="6%" align="right" class="fy excel"><strong>=SUM(I${row}:T${row})</strong></td>
		        		<td></td>
		        		%for column in ['W','X']:
							<td class="excel">=${column}${row - distance * 2} - ${column}${row - distance}</td>
						%endfor
		        		<td width="6%" align="right" class="fy excel"><strong>=SUM(W${row}:X${row})</strong></td>
		        		<td></td>
		        		<td class="excel" width="5%"><strong>=H${row}-F${row}</strong></td>
		        		<td class="excel" width="5%"><strong>=H${row}-U${row}</strong></td>
		        		%if line['flow'] == 'cashin':
							<% cashin_rows.append(row) %>
						
						%elif line['flow'] == 'cashout':
							<% cashout_rows.append(row) %>
							
						%elif line['flow'] == 'stockchanges':
							<% stock_rows.append(row) %>
							
						%elif line['flow'] == 'depreciations':
							<% depreciation_rows.append(row) %>
						
						%endif
						<% section_rows = [] %>
						
								        		
		        	%elif line['type'] == 'calc':
			        	<td></td>
		        		%for column in ['D','E']:
							<td class="excel">=${column}${row - distance * 2} - ${column}${row - distance}</td>
						%endfor
			        	<td width="6%" align="right" class="endfy excel"><strong>=${column}${row - distance * 2} - ${column}${row - distance}</strong></td>
			        	<td></td>
			        	<td></td>
			        	%for column in ['I','J','K','L','M','N','O','P','Q','R','S','T']:
							<td class="excel">=${column}${row - distance * 2} - ${column}${row - distance}</td>
						%endfor
		        		<td width="6%" align="right" class="endfy excel"><strong>=SUM(I${row}:T${row})</strong></td>
		        		<td></td>
		        		%for column in ['W','X']:
							<td class="excel">=${column}${row - distance * 2} - ${column}${row - distance}</td>
						%endfor
		        		<td width="6%" align="right" class="endfy excel"><strong>=SUM(W${row}:X${row})</strong></td>
		        		<td></td>
		        		<td class="excel" width="5%"><strong>=H${row}-F${row}</strong></td>
		        		<td class="excel" width="5%"><strong>=H${row}-U${row}</strong></td>
		        	%endif
	        	</tr>
	        	<% row = row + 1 %>
	        %else:
	        
	        	%if line['type'] == 'section':
				    <tr class="section">
				    	<td class="section" height="50px"><strong>${line['title']}</strong></td>
				    	${td_empty(4)}
				    	<td class="emptyfy"></td>
				    	${td_empty(14)}
		        		<td class="emptyfy"></td>
		        		${td_empty(3)}
		        		<td class="emptyfy"></td>
		        		<td class="section" height="50px"></td>
				    </tr>
				    <% row_new_section = row %>
				    <% row = row + 1 %>
				%elif line['type'] == 'total':
					<tr class="total">
	        			<td colspan="2" width="20%">${line['title']}</td>
						<td></td>
		        		%for column in ['D','E']:
							<td class="excel">=${column}${row - distance * 2} - ${column}${row - distance}</td>
						%endfor
		        		<td width="6%" align="right" class="fy excel">=${column}${row - distance * 2} - ${column}${row - distance}</td>
		        		<td></td>
		        		<td></td>
	        			%for column in ['I','J','K','L','M','N','O','P','Q','R','S','T']:
							<td class="excel">=${column}${row - distance * 2} - ${column}${row - distance}</td>
						%endfor
		        		<td width="6%" align="right" class="fy excel">=SUM(I${row}:T${row})</td>
		        		<td></td>
		        		%for column in ['W','X']:
							<td class="excel">=${column}${row - distance * 2} - ${column}${row - distance}</td>
						%endfor
		        		<td width="6%" align="right" class="fy excel">=SUM(W${row}:X${row})</td>
		        		<td></td>
		        		<td class="excel" width="5%">=H${row}-F${row}</td>
		        		<td class="excel" width="5%">=H${row}-U${row}</td>
		        	</tr>
		        	<% row = row + 1 %>	
				%elif line['type'] == 'section_total':
					<tr class="total">
	        			<td colspan="2" width="20%" align="right"><strong>${line['title']}</strong></td>
		        		<td></td>
		        		%for column in ['D','E']:
							<td class="excel">=${column}${row - distance * 2} - ${column}${row - distance}</td>
						%endfor
			        	<td width="6%" align="right" class="fy excel"><strong>=${column}${row - distance * 2} - ${column}${row - distance}</strong></td>
			        	<td></td>
			        	<td></td>
			        	%for column in ['I','J','K','L','M','N','O','P','Q','R','S','T']:
							<td class="excel">=${column}${row - distance * 2} - ${column}${row - distance}</td>
						%endfor
		        		<td width="6%" align="right" class="fy excel"><strong>=SUM(I${row}:T${row})</strong></td>
						<td></td>
						%for column in ['W','X']:
							<td class="excel">=${column}${row - distance * 2} - ${column}${row - distance}</td>
						%endfor
		        		<td width="6%" align="right" class="fy excel"><strong>=SUM(W${row}:X${row})</strong></td>
		        		<td></td>
		        		<td class="excel" width="5%"><strong>=H${row}-F${row}</strong></td>
		        		<td class="excel" width="5%"><strong>=H${row}-U${row}</strong></td>
		        	</tr>
					%if line['flow'] == 'cashin':
						<% cashin_rows.append(row) %>
					
					%elif line['flow'] == 'cashout':
						<% cashout_rows.append(row) %>
						
					%elif line['flow'] == 'stockchanges':
						<% stock_rows.append(row) %>
						
					%elif line['flow'] == 'depreciations':
						<% depreciation_rows.append(row) %>
					
					%endif     	
		        	<% row = row + 1 %>
		        %elif line['type'] == 'calc':
		        	<tr>
		        		${td_empty(5)}
		        		<td class="emptyfy"></td>
		        		${td_empty(14)}
		        		<td class="emptyfy"></td>
		        		${td_empty(2)}
		        		<td class="emptyfy"></td>
		        		<td></td>
		        	</tr>
		        	<% row = row + 1 %>
		        	<tr>
		        		<td colspan="2" width="20%"><strong>${line['title']}</strong></td>
		        		<td></td>
		        		%for column in ['D','E']:
							<td class="excel">=${column}${row - distance * 2} - ${column}${row - distance}</td>
						%endfor
			        	<td width="6%" align="right" class="endfy excel"><strong>=${column}${row - distance * 2} - ${column}${row - distance}</strong></td>
			        	<td></td>
			        	<td></td>
			        	%for column in ['I','J','K','L','M','N','O','P','Q','R','S','T']:
							<td class="excel">=${column}${row - distance * 2} - ${column}${row - distance}</td>
						%endfor
		        		<td width="6%" align="right" class="endfy excel"><strong>=SUM(I${row}:T${row})</strong></td>
		        		<td></td>
		        		%for column in ['W','X']:
							<td class="excel">=${column}${row - distance * 2} - ${column}${row - distance}</td>
						%endfor
		        		<td width="6%" align="right" class="endfy excel"><strong>=SUM(W${row}:X${row})</strong></td>
		        		<td></td>
		        		<td class="excel" width="5%"><strong>=H${row}-F${row}</strong></td>
		        		<td class="excel" width="5%"><strong>=H${row}-U${row}</strong></td>
		        	</tr>
		        	<% row = row + 1 %>
				%endif
	        %endif
        %endfor
    </table>
    
    <!-- warnings table -->
    
    %for date in warnings:
        %if date['warning']:
        <!--      
        	<br />
        	%if len(excluded()) > 0:
	        <table width="50%">
		            <tr class="total">
		        		<td>WARNING!!! Some Ledger Positions are not included, please inform System Administrator</td>
		            </tr> 
		            %for account in excluded():
		            <tr>
		            	<td>${account['account_code']} - ${account['ref']} found in ${account['note']}</td>
		            </tr>
		            %endfor
		    </table>
		-->    
		    %endif
		    <br />
		    %if len(duplicated()) > 0:
		    <table width="50%">
		        
			        <tr class="total">
		        		<td>WARNING!!! Some Ledger Positions are duplicated, please inform System Administrator</td>
		            </tr> 
		            %for account in duplicated():
		            <tr>
		            	<td>${account['account_code']} - ${account['ref']} found in ${account['note']}</td>
		            </tr>
		            %endfor
	        </table>
	        %endif
    	%endif
   	%endfor
   	
</body>
</html>
