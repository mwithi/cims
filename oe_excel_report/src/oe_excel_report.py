# -*- coding: utf-8 -*-
import subprocess
import os
import sys
from openerp import report
import tempfile
import time
from functools import partial

from mako.template import Template
from mako.lookup import TemplateLookup
from mako import exceptions

from openerp import netsvc
from openerp import pooler
from openerp.addons.report_webkit.report_helper import WebKitHelper
from openerp.report.report_sxw import *
from openerp import addons
from openerp import tools
from openerp.tools.translate import _
from openerp.osv.osv import except_osv

import logging
from openerp.addons.report_webkit.webkit_report import WebKitParser, mako_template

_logger = logging.getLogger(__name__)
_debug = False

def _generate_pdf(self, comm_path, report_xml, header, footer, html_list, webkit_header=False):
    if _debug:
        _logger.debug("==> Running _generate_pdf from local method")
    return self.generate_pdf_orig(comm_path, report_xml, header, footer, html_list, webkit_header)

def get_lib(self, cursor, uid):
    """Return the libreoffice path"""
    proxy = self.pool.get('ir.config_parameter')
    libreoffice_path = proxy.get_param(cursor, uid, 'libreoffice_path')

    if not libreoffice_path:
        try:
            defpath = os.environ.get('PATH', os.defpath).split(os.pathsep)
            if hasattr(sys, 'frozen'):
                defpath.append(os.getcwd())
                if tools.config['root_path']:
                    defpath.append(os.path.dirname(tools.config['root_path']))

            for librebin in ['soffice', 'soffice.exe', 'libreoffice', 'libreoffice.exe']:
                libreoffice_path = tools.which(librebin, path=os.pathsep.join(defpath))
                if libreoffice_path:
                    break
        except IOError:
            libreoffice_path = None

    if libreoffice_path:
        return libreoffice_path

    raise except_osv(
             _('Libreoffice library path is not set'),
             _('Please install executable on your system' \
             ' and set the path in the ir.config_parameter with the libreoffice_path key.' \
             ' Minimal version is 5.4.1')
            )

def _generate_xls(self, comm_path, report_xml, header, footer, html_list, webkit_header=False):
    # libreoffice --headless --calc --convert-to xls:"MS Excel 97" 1516627184.080.body.html
    if not webkit_header:
        webkit_header = report_xml.webkit_header
    tmp_dir = tempfile.gettempdir()
    file_to_del = []
    if comm_path:
        command = [comm_path]
    else:
        command = ['soffice']

    command.extend(['--headless', '--calc', '--convert-to', 'xlsx', '--outdir', tmp_dir])

    if header:
        pass
    if footer:
        pass

    count = 0
    if len(html_list):
        html_file = file(os.path.join(tmp_dir, str(time.time()) + str(count) +'.body.html'), 'w')
        xls_file = html_file.name.replace('html', 'xlsx')
        for html in html_list :
            html_file.write(self._sanitize_html(html))
        html_file.close()
        file_to_del.append(html_file.name)
        file_to_del.append(xls_file)
        command.append(html_file.name)

    if _debug:
        _logger.debug("==> command : %s", " ".join(command))
    stderr_fd, stderr_path = tempfile.mkstemp(text=True)
    file_to_del.append(stderr_path)
    try:
        if os.name == 'nt':
            status = subprocess.call(" ".join(command), shell=False, stderr=stderr_fd)
        else:
            status = subprocess.call(" ".join(command), shell=True, stderr=stderr_fd)
        os.close(stderr_fd) # ensure flush before reading
        stderr_fd = None # avoid closing again in finally block
        fobj = open(stderr_path, 'r')
        error_message = fobj.read()
        fobj.close()
        if not error_message:
            error_message = _('No diagnosis message was provided, os.name : ') + os.name
        else:
            error_message = _('The following diagnosis message was provided:\n') + error_message
        if status :
            raise except_osv(_('Webkit error' ),
                             _("The command 'libreoffice' failed with error code = %s. Message: %s") % (status, error_message))
        xls_file = open(xls_file, 'rb')
        xls = xls_file.read()
        xls_file.close()
    finally:
        if stderr_fd is not None:
            os.close(stderr_fd)
        for f_to_del in file_to_del:
            try:
                os.unlink(f_to_del)
            except (OSError, IOError), exc:
                _logger.error('cannot remove file %s: %s', f_to_del, exc)
    return xls

def _create_single_pdf(self, cursor, uid, ids, data, report_xml, context=None):
    if _debug:
        _logger.debug("==> Running _create_single_pdf from local method: %s" % context)
    #return self.create_single_pdf_orig(cursor, uid, ids, data, report_xml, context)
    # override needed to keep the attachments storing procedure
    if context is None:
        context={}
    if ('xls_output' not in context or context['xls_output'] != True) and ('html_output' not in context or context['html_output'] != True):
        return self.create_single_pdf_orig(cursor, uid, ids, data, report_xml, context=context)
    htmls = []

    self.parser_instance = self.parser(cursor,
                                       uid,
                                       self.name2,
                                       context=context)

    self.pool = pooler.get_pool(cursor.dbname)
    objs = self.getObjects(cursor, uid, ids, context)
    self.parser_instance.set_context(objs, data, ids, report_xml.report_type)

    template =  False

    if report_xml.report_file :
        # backward-compatible if path in Windows format
        report_path = report_xml.report_file.replace("\\", "/")
        path = addons.get_module_resource(*report_path.split('/'))
        if path and os.path.exists(path) :
            template = file(path).read()
    if not template and report_xml.report_webkit_data :
        template =  report_xml.report_webkit_data
    if not template :
        raise except_osv(_('Error!'), _('Webkit report template not found!'))

    header = report_xml.webkit_header.html
    footer = report_xml.webkit_header.footer_html
    if not header and report_xml.header:
        raise except_osv(
              _('No header defined for this Webkit report!'),
              _('Please set a header in company settings.')
          )
    if not report_xml.header :
        header = ''
        default_head = addons.get_module_resource('report_webkit', 'default_header.html')
        with open(default_head,'r') as f:
            header = f.read()
    css = report_xml.webkit_header.css
    if not css :
        css = ''
    
    translate_call = partial(self.translate_call, self.parser_instance)
    #default_filters=['unicode', 'entity'] can be used to set global filter
    body_mako_tpl = mako_template(template)
    helper = WebKitHelper(cursor, uid, report_xml.id, context)
    if report_xml.precise_mode:
        for obj in objs:
            self.parser_instance.localcontext['objects'] = [obj]
            try :
                html = body_mako_tpl.render(helper=helper,
                                            css=css,
                                            _=self.translate_call,
                                            **self.parser_instance.localcontext)
                htmls.append(html)
            except Exception:
                msg = exceptions.text_error_template().render()
                _logger.error(msg)
                raise except_osv(_('Webkit render!'), msg)
    else:
        try :
            html = body_mako_tpl.render(helper=helper,
                                        css=css,
                                        _=self.translate_call,
                                        **self.parser_instance.localcontext)
            htmls.append(html)
        except Exception:
            msg = exceptions.text_error_template().render()
            _logger.error(msg)
            raise except_osv(_('Webkit render!'), msg)
    head_mako_tpl = mako_template(header)
    try :
        head = head_mako_tpl.render(helper=helper,
                                    css=css,
                                    _=translate_call,
                                    _debug=False,
                                    **self.parser_instance.localcontext)
    except Exception:
        raise except_osv(_('Webkit render!'),
            exceptions.text_error_template().render())
    foot = False
    if footer :
        foot_mako_tpl = mako_template(footer)
        try :
            foot = foot_mako_tpl.render(helper=helper,
                                        css=css,
                                        _=translate_call,
                                        **self.parser_instance.localcontext)
        except:
            msg = exceptions.text_error_template().render()
            _logger.error(msg)
            raise except_osv(_('Webkit render!'), msg)
    if report_xml.webkit_debug or ('html_output' in context and context['html_output'] == True):
        try :
            deb = head_mako_tpl.render(helper=helper,
                                       css=css,
                                       _debug=tools.ustr("\n".join(htmls)),
                                       _=self.translate_call,
                                       **self.parser_instance.localcontext)
        except Exception:
            msg = exceptions.text_error_template().render()
            _logger.error(msg)
            raise except_osv(_('Webkit render!'), msg)
        return (deb, 'html')
    comm_path = get_lib(self, cursor, uid)
    xls = _generate_xls(self, comm_path, report_xml, False, False, htmls)
    return (xls, 'xlsx')

tmp = WebKitParser.generate_pdf
WebKitParser.generate_pdf = _generate_pdf
WebKitParser.generate_pdf_orig = tmp

tmp = WebKitParser.create_single_pdf
WebKitParser.create_single_pdf = _create_single_pdf
WebKitParser.create_single_pdf_orig = tmp
