# -*- coding: utf-8 -*-
{
	'name': 'ISF OpenHospital Integration',
	'version': '0.1',
	'category': 'Tools',
	'description': 
"""
ISF OpenHospital Integration 
============================

This integration tool will import daily collections from OpenHospital software
(version Wolisso_core1_7_0_release9 or greater) 
""",
	'author': 'Alessandro Domanico (alessandro.domanico@informaticisenzafrontiere.org)',
	'website': 'www.informaticisenzafrontiere.org',
	'license': 'AGPL-3',
	'depends': ['account_accountant'],
	'data': [
		'views/res_config_view.xml',
		'wizard/import_data.xml',
        'security/ir.model.access.csv',
    ],
	'demo': [],
	'installable' : True,
}

