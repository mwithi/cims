# -*- coding: utf-8 -*-
import time
#import MySQLdb
#import mysql.connector

import sys
if 'linux' in sys.platform:
    import mysql.connector as mysql_connector
else:
    import MySQLdb as mysql_connector

from openerp.osv import fields, osv, orm
from openerp.tools.translate import _

import logging

_logger = logging.getLogger(__name__)
_debug=False

class imported_data(osv.Model):
    _name = 'isf.openhospital.integration.imported.data'
    
    _columns = {
        'date' : fields.date('Generate Entries Before', required=True),
        'status' : fields.selection([('ok','Ok'), ('invalid','Invalid'), ('running','Running')],'Status',required=True,readonly=True),
        'move_id' : fields.many2one('account.move', 'Journal Entry', required=True, ondelete='cascade'),
    }
    
class import_data_and_generate_entries(osv.osv_memory):

    _name = "isf.openhospital.integration.import.data"
    _description = "Generate Entries from Open Hospital data"
    _columns = {
        'help': fields.text('Help', size=512),
        'date': fields.date('Generate Entries Before', required=True),
    }
    _defaults = {
        'help': 'helper text',
        'date': lambda *a: time.strftime('%Y-%m-%d'),
    }
    
    def check_integration_table(self, cr, uid, date):
        _logger.debug('Searching entry for date: %s', date)
        integration_obj = self.pool.get('isf.openhospital.integration.imported.data')
        integration_id = integration_obj.search(cr, uid, [('date','=',date)])
        if len(integration_id) > 0:
            _logger.debug('Found existing entry for date: %s', date)
        return integration_id
    
    def action_generate(self, cr, uid, ids, context=None):
        _logger.debug('Check integration table entries...')
        if _debug:
            _logger.debug('Context: %s ', context)
        
        for data in self.read(cr, uid, ids, context=context):
            date = data['date']
        
        integration_id = self.check_integration_table(cr, uid, date)
        if len(integration_id) > 0:
            integration_obj = self.pool.get('isf.openhospital.integration.imported.data')
            integration_date = integration_obj.browse(cr, uid, integration_id)[0]
            status = integration_date.status
            _logger.debug('==> status : %s', status)
            
            if status == 'ok':
                dummy, view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'isf_openhospital_integration', 'view_isf_openhospital_integration_config_settings_status_ok')
                return {
                    'name': 'Date already processed',
                    'type': 'ir.actions.act_window',
                    'res_model': 'isf.openhospital.integration.config.settings.message',
                    'view_mode': 'form',
                    'view_type': 'form',
                    'view_id': view_id,
                    'target': 'new',
                    'context': {'message': 'Data already imported for date: %s.\n\nDo you want to import it again ?' % date,
                                'date' : date,
                                'move_id' : integration_date.move_id.id}
                }
                
            elif status == 'running':
                dummy, view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'isf_openhospital_integration', 'view_isf_openhospital_integration_config_settings_status_running')
                return {
                    'name': 'Process already running',
                    'type': 'ir.actions.act_window',
                    'res_model': 'isf.openhospital.integration.config.settings.message',
                    'view_mode': 'form',
                    'view_type': 'form',
                    'view_id': view_id,
                    'target': 'new',
                    'context': {'message': 'Process already running for date: %s.\n\nPlease wait a little and try again.' % date}
                }
                
            elif status == 'invalid':
                _logger.debug('Found invalid entry for date: %s\n\nImporting again.', date)
                return self.generate_entries(cr, uid, date)
                
            else:
                _logger.debug('Found unknown status entry for date: %s', date)
                raise orm.except_orm(
                    _('Unknown status entry.'),
                    _("date: %s\nstatus : %s") % (date, integration_date.status))
        else:
            return self.generate_entries(cr, uid, date)
                
    def generate_entries(self, cr, uid, date, context=None):
        _logger.debug('Generate entries for date: %s', date)
        
        openhospital_server = self.pool.get('ir.values').get_default(cr, uid, 'isf.openhospital.integration','openhospital_server')
        openhospital_port = self.pool.get('ir.values').get_default(cr, uid, 'isf.openhospital.integration','openhospital_port')
        openhospital_database = self.pool.get('ir.values').get_default(cr, uid, 'isf.openhospital.integration','openhospital_database')
        openhospital_user = self.pool.get('ir.values').get_default(cr, uid, 'isf.openhospital.integration','openhospital_user')
        openhospital_password = self.pool.get('ir.values').get_default(cr, uid, 'isf.openhospital.integration','openhospital_password')
        
        try:
            # db = MySQLdb.connect(host=openhospital_server, 
            #                      port=int(openhospital_port), 
            #                      db=openhospital_database,
            #                      user=openhospital_user, 
            #                      passwd=openhospital_password)
            db = mysql_connector.connect(
                host=openhospital_server, 
                port=int(openhospital_port), 
                db=openhospital_database,
                user=openhospital_user, 
                passwd=openhospital_password
            )
            
        except Exception, ex:
            raise orm.except_orm(
                _('Connection Error!'),
                _("Exception : %s")
                % (ex))
    
        cursor = db.cursor()
        
        journal_id = 9 # get it from config settings
        
        # execute SQL select statement
        cursor.execute("""
        
            SELECT DATE(BLL_DATE), DATE(BLL_UPDATE), BLL_ID, BLL_STATUS, DATE(BLP_DATE), BLL_AMOUNT, BLL_BALANCE, PME_CODE, SUM(BLP_AMOUNT) AS BLP_AMOUNT, LST_ID, LST_NAME, 
                PME_ACCOUNT_CODE, LST_RECEIVABLE_CODE, LST_PAYABLE_CODE
            
            FROM BILLPAYMENTS
                JOIN BILLPAYMENTMETHOD ON BLP_PME_ID = PME_ID
                LEFT JOIN BILLS ON BLP_ID_BILL = BLL_ID
                LEFT JOIN PRICELISTS ON BLL_ID_LST = LST_ID
            
            WHERE BLL_STATUS NOT IN ('D', 'X')
            AND DATE(BLP_DATE) = %s
            
            GROUP BY BLL_ID
            
        """, (date,))
        
        records = cursor.fetchall()
        if _debug:
            _logger.debug('records found : %s', len(records))
        
        account_pool = self.pool.get('account.account')
        move_line_pool = self.pool.get('account.move.line')        
        move_pool = self.pool.get('account.move')
        
        reference=_('Daily Collections') + ' ' + date
        name=date
        
        move = move_pool.account_move_prepare(cr, uid, journal_id, date, ref=reference, context=context)
        move_id = move_pool.create(cr, uid, move, context=context)
        integration_obj = self.pool.get('isf.openhospital.integration.imported.data')
        integration_obj.create(cr, uid, {
                    'date' : date,
                    'status' : 'running',
                    'move_id' : move_id,
                })
        
        moves_created=[]
        income_lines={}
        expense_lines={}
        cash_and_banks_lines={}
        receivable_lines={}
        payable_lines={}
        exempted_lines={}
        warning_bills=[]
        ignored_bills=[]
        
        for x in records:
            if _debug:
                _logger.debug('record : %s', x)
            bill_date = x[0]
            bill_update = x[1]
            bill_id = x[2]
            bill_status = x[3]
            payment_date = x[4]
            bill_amount = x[5]
            bill_balance = x[6]
            payment_method_code = x[7]
            payment_amount = x[8]
            pricelist_id = x[9]
            pricelist_name = x[10]
            cash_bank_account_code = x[11]
            receivable_code = x[12]
            payable_code = x[13]
            
            # preliminary checks
            if not receivable_code or not payable_code:
                raise orm.except_orm(
                    _('Configuration Error!'),
                    _("Some Open Hospital accounts not set %s")
                    %(pricelist_name))
            
            if payment_date and not cash_bank_account_code:
                raise orm.except_orm(
                    _('Configuration Error!'),
                    _("An Open Hospital payment method account not set %s")
                    %(payment_method_code))
            
            # in any case debit cash or bank
            self.check_account_in_list(cr, uid, cash_bank_account_code, cash_and_banks_lines, 'daily collection', name, move_id)
            cash_and_banks_lines[cash_bank_account_code]['debit'] += payment_amount
            if bill_status == 'C':
                if abs(payment_amount) < abs(bill_amount):
                    if _debug:
                        _logger.debug('bill ID: %s abs(payment_amount) < abs(bill_amount)', bill_id)
                    if payment_date < bill_update:
                        # at the time of this payment the bill was open, so I have to treat it like so:
                        # credit payable
                        self.check_account_in_list(cr, uid, payable_code, payable_lines, 'payables', name, move_id)
                        payable_lines[payable_code]['credit'] += payment_amount
                    else:
                        # at the time of this payment the bill has been closed
                        # two cases (even both): 1. other payments before closing or 2. any discount.
                        # For case 1, sum previous payment and I reduce payable by that amount 
                        # For case 2, it is handled later on 
                        # debit payable | credit income (later on)
                        debit_amount = bill_amount - payment_amount
                        cursor.execute("""
                        
                            SELECT SUM(BLP_AMOUNT) 
                            FROM BILLPAYMENTS
                            WHERE BLP_ID_BILL = %s
                            AND DATE(BLP_DATE) < %s
                        
                        """, (bill_id, date))
                        result = cursor.fetchall()
                        for payment in result:
                            previous_payment_sum = payment[0]
                            if previous_payment_sum > 0:
                                debit_amount = previous_payment_sum
                                if _debug:
                                    _logger.debug('previous payments : %s', debit_amount)
                            
                        self.check_account_in_list(cr, uid, payable_code, payable_lines, 'payables', name, move_id)
                        payable_lines[payable_code]['debit'] += debit_amount
                        
            elif bill_status == 'O':
                # an advance payment from customer:
                # credit payable
                self.check_account_in_list(cr, uid, payable_code, payable_lines, 'payables', name, move_id)
                payable_lines[payable_code]['credit'] += payment_amount
                
            elif bill_status == 'L':
                # pay later bill, should not have payments before settling by companies
                # but in case, it means the patient paid a little and the receivable from companies
                # must be reduced:
                # credit receivable
                self.check_account_in_list(cr, uid, receivable_code, receivable_lines, 'receivables', name, move_id)
                receivable_lines[receivable_code]['credit'] += payment_amount
            else:
                # status different from 'C', 'O' and 'L'
                ignored_bills.append(bill_id)
            
            if bill_amount == 0:
                # bill to be checked
                warning_bills.append(bill_id)  
            
        cursor.execute("""
        
            SELECT DATE(BLL_DATE), DATE(BLL_UPDATE), BLL_ID, BLL_STATUS, NULL AS BLI_DATE, BLL_AMOUNT, BLL_BALANCE, BLI_ITEM_DESC, BLI_ITEM_AMOUNT * BLI_QTY AS AMOUNT, LST_ID, LST_NAME, 
                IF(BLL_ADM_ID IS NULL, PRT_INCOME_OPD_CODE, PRT_INCOME_IPD_CODE) AS ACCOUNT_CODE, LST_RECEIVABLE_CODE, LST_PAYABLE_CODE

            FROM BILLITEMS
                LEFT JOIN PRICETYPE ON BLI_PRT_ID = PRT_ID
                LEFT JOIN BILLS ON BLI_ID_BILL = BLL_ID
                LEFT JOIN PRICELISTS ON BLL_ID_LST = LST_ID

            WHERE BLL_STATUS NOT IN ('D', 'X')
            AND DATE(BLL_UPDATE) = %s
            
        """, (date,))
        
        records = cursor.fetchall()
        if _debug:
            _logger.debug('records found : %s', len(records))
        
        for x in records:
            if _debug:
                _logger.debug('record : %s', x)
            bill_date = x[0]
            bill_update = x[1]
            bill_id = x[2]
            bill_status = x[3]
            billitem_date = x[4] # non esiste ancora
            bill_amount = x[5]
            bill_balance = x[6]
            billitem_desc = x[7]
            billitem_amount = x[8]
            pricelist_id = x[9]
            pricelist_name = x[10]
            billitem_account_code = x[11] 
            receivable_code = x[12]
            payable_code = x[13]
            
            # preliminary checks
            if not receivable_code or not payable_code:
                raise orm.except_orm(
                    _('Configuration Error!'),
                    _("Some Open Hospital accounts not set %s")
                    %(pricelist_name))
            
            if not billitem_account_code:
                raise orm.except_orm(
                    _('Configuration Error!'),
                    _("Some Open Hospital price types accounts not set %s")
                    %(billitem_desc))
                
            # we credit incomes only for closed and deferred bills
            # closed bills
            if bill_status == 'C':
                if billitem_amount > 0:
                    # normal case
                    self.check_account_in_list(cr, uid, billitem_account_code, income_lines, 'incomes', name, move_id)
                    income_lines[billitem_account_code]['credit'] += billitem_amount
                elif billitem_amount < 0:
                    # two cases: refund or exemption full or partial
                    if bill_amount < 0:
                        # refund case, we reduce the income:
                        # debit income
                        self.check_account_in_list(cr, uid, billitem_account_code, income_lines, 'incomes', name, move_id)
                        income_lines[billitem_account_code]['debit'] += -billitem_amount
                    elif bill_amount > 0:
                        # exempt case:
                        # debit expense
                        self.check_account_in_list(cr, uid, billitem_account_code, exempted_lines, 'exempted', name, move_id)
                        exempted_lines[billitem_account_code]['debit'] += -billitem_amount
                    else:
                        # abnormal case
                        warning_bills.append(bill_id)
                else:
                    # billitem_amount == 0
                    # we record separately
                    self.check_account_in_list(cr, uid, billitem_account_code, exempted_lines, '', '', move_id)
                    description = exempted_lines[billitem_account_code]['name']
                    if billitem_desc not in description:
                        if description == ' ': # previous description and name empty
                            description = billitem_desc
                        else:
                            description += ',' + billitem_desc
                    exempted_lines[billitem_account_code]['name'] = description
                
            # deferred bills       
            elif bill_status == 'L':
                # pay later bill, we debit receivable that will be settled by companies
                # and credit incomes:
                # debit receivable, credit incomes
                if billitem_amount > 0:
                    self.check_account_in_list(cr, uid, billitem_account_code, income_lines, 'incomes', name, move_id)
                    income_lines[billitem_account_code]['credit'] += billitem_amount
                    
                    self.check_account_in_list(cr, uid, receivable_code, receivable_lines, 'receivables', name, move_id)
                    receivable_lines[receivable_code]['debit'] += billitem_amount
                
                elif billitem_amount < 0:
                    # refund case, we reduce the income and also the receivable from companies
                    # debit income, credit receivable
                    self.check_account_in_list(cr, uid, billitem_account_code, income_lines, 'incomes', name, move_id)
                    income_lines[billitem_account_code]['debit'] += -billitem_amount
                    
                    self.check_account_in_list(cr, uid, receivable_code, receivable_lines, 'receivables', name, move_id)
                    receivable_lines[receivable_code]['credit'] += -billitem_amount
                else:
                    # billitem_amount == 0 when PayLater
                    # abnormal case
                    warning_bills.append(bill_id)
            
            else:
                # the bill is open and so ignored
                ignored_bills.append(bill_id)
               
        if _debug: 
            _logger.debug('cash_and_banks_lines : %s', cash_and_banks_lines)
            _logger.debug('receivable_lines : %s', receivable_lines)
            _logger.debug('payable_lines : %s', payable_lines)
            
            _logger.debug('income_lines : %s', income_lines)
            _logger.debug('exempted_lines : %s', exempted_lines)
            
        _logger.debug('warning_bills : %s', warning_bills)
        _logger.debug('ignored_bills : %s', ignored_bills)
        
        period_obj = self.pool.get('account.period')
        period_ids = period_obj.find(cr, uid, date)
        period_id = period_ids and period_ids[0] or False
        if not period_id:
            raise orm.except_orm(
                    _('Configuration Error!'),
                    _("OpenERP period not defined for date: %s")
                    %(date))
        
        # normalize values (only debit or credit for each line)
        for line in exempted_lines:
            vals = self.balance_debit_credit(exempted_lines[line])
            move_line_pool.create(cr, uid, vals, context=context, check=False)
        for line in payable_lines:
            vals = self.balance_debit_credit(payable_lines[line])
            move_line_pool.create(cr, uid, vals, context=context, check=False)
        for line in receivable_lines:
            vals = self.balance_debit_credit(receivable_lines[line])
            move_line_pool.create(cr, uid, vals, context=context, check=False)
        for line in cash_and_banks_lines:
            vals = self.balance_debit_credit(cash_and_banks_lines[line])
            move_line_pool.create(cr, uid, vals, context=context, check=False)
        for line in income_lines:
            vals = self.balance_debit_credit(income_lines[line])
            move_line_pool.create(cr, uid, vals, context=context, check=False) 
            
        moves_created.append(move_id)
        
        if len(moves_created) > 0:
            mod_obj = self.pool.get('ir.model.data')
            act_obj = self.pool.get('ir.actions.act_window')
            
            result = mod_obj.get_object_reference(cr, uid, 'account', 'action_move_line_form')
            id = result and result[1] or False
            
            integration_id = integration_obj.search(cr, uid, [('date','=',date)])
            integration_obj.write(cr, uid, integration_id, {
                    'status' : 'ok',
                },context)
            
            result = act_obj.read(cr, uid, [id], context=context)[0]
            result['domain'] = str([('id','in',moves_created)])
            return result
        else:
            return {
                'name': 'Generate Entries from Open Hospital data',
                'type': 'ir.actions.act_window',
                'res_model': 'isf.openhospital.integration.config.settings.message',
                'view_mode': 'form',
                'view_type': 'form',
                'target': 'new',
                'context' : {'message': 'No data to import!'},
            }
    
    
    def check_account_in_list(self, cr, uid, code, list, description, name, move_id):
        if code not in list:
            account_pool = self.pool.get('account.account')
            account_id = account_pool.search(cr, uid, [('code','=',code)])[0]
            list[code] = self.get_move_line('', description + ' ' + name, move_id, account_id)
            
    def balance_debit_credit(self, vals):
        debit = vals['debit']
        credit = vals['credit']
        if debit - credit > 0:
            vals['debit'] = debit - credit
            vals['credit'] = 0
        else:
            vals['credit'] = credit - debit
            vals['debit'] = 0
        if _debug:
            _logger.debug('vals : %s', vals)
        return vals
        
    def get_move_line(self, ref, name, move_id, account_id):
        move_line = {
            # 'analytic_account_id': False,
            # 'tax_code_id': False, 
            # 'tax_amount': 0,
            'ref' : ref,
            'name': name,
            # 'currency_id': currency_id,
            'credit': 0.0,
            'debit': 0.0,
            # 'date_maturity' : False,
            # 'amount_currency': 0.0,
            # 'partner_id': False,
            'move_id': move_id,
            'account_id': account_id,
            'state' : 'valid'
        }
        return move_line
            
import_data_and_generate_entries()