#import MySQLdb
#import mysql.connector

import sys
if 'linux' in sys.platform:
    import mysql.connector as mysql_connector
else:
    import MySQLdb as mysql_connector
 
# import time
# import datetime
# from dateutil.relativedelta import relativedelta
# from operator import itemgetter
# from os.path import join as opj

# from openerp.tools import DEFAULT_SERVER_DATE_FORMAT as DF
from openerp.tools.translate import _
from openerp.osv import fields, osv, orm
# from openerp import tools
import logging

_logger = logging.getLogger(__name__)
_debug=False

class isf_openhospital_integration_config_settings(osv.osv_memory):
    _name = 'isf.openhospital.integration.config.settings'
    _inherit = 'res.config.settings'
    
    _columns = {
        'openhospital_server' : fields.char('Server', size=256, required=False),
        'openhospital_port' : fields.char('Port', size=5, required=False),
        'openhospital_database' : fields.char('Database', size=256, required=False),
        'openhospital_user' : fields.char('User', size=256, required=False),
        'openhospital_password' : fields.char('Password', size=256, required=False),
        }
    
    # Get Methods
    def get_default_openhospital_server(self, cr, uid, ids, context=None):
        openhospital_server = self.pool.get('ir.values').get_default(cr, uid, 'isf.openhospital.integration','openhospital_server')
        return {'openhospital_server' : openhospital_server }
    
    def get_default_openhospital_port(self, cr, uid, ids, context=None):
        openhospital_port = self.pool.get('ir.values').get_default(cr, uid, 'isf.openhospital.integration','openhospital_port')
        return {'openhospital_port' : openhospital_port }
    
    def get_default_openhospital_database(self, cr, uid, ids, context=None):
        openhospital_database = self.pool.get('ir.values').get_default(cr, uid, 'isf.openhospital.integration','openhospital_database')
        return {'openhospital_database' : openhospital_database }
    
    def get_default_openhospital_user(self, cr, uid, ids, context=None):
        openhospital_user = self.pool.get('ir.values').get_default(cr, uid, 'isf.openhospital.integration','openhospital_user')
        return {'openhospital_user' : openhospital_user }
    
    def get_default_openhospital_password(self, cr, uid, ids, context=None):
        openhospital_password = self.pool.get('ir.values').get_default(cr, uid, 'isf.openhospital.integration','openhospital_password')
        return {'openhospital_password' : openhospital_password }
    
    # Set Methods
    def set_openhospital_server(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.openhospital.integration', 'openhospital_server', config.openhospital_server)
        
    def set_openhospital_port(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.openhospital.integration', 'openhospital_port',config.openhospital_port)
    
    def set_openhospital_database(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.openhospital.integration', 'openhospital_database',config.openhospital_database)
        
    def set_openhospital_user(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.openhospital.integration', 'openhospital_user',config.openhospital_user)
        
    def set_openhospital_password(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        ir_values = self.pool.get('ir.values')
        ir_values.set_default(cr, uid, 'isf.openhospital.integration', 'openhospital_password',config.openhospital_password)
    
    def test_connection(self, cr, uid, ids, context=None):
        config = self.browse(cr, uid, ids[0], context)
        _logger.debug('Test connection...')
        _logger.debug('Context: %s ', context)
        
        try:
            # db = MySQLdb.connect(host=config.openhospital_server, 
            #                      port=int(config.openhospital_port), 
            #                      db=config.openhospital_database,
            #                      user=config.openhospital_user, 
            #                      passwd=config.openhospital_password)
            db = mysql_connector.connect(
                host=config.openhospital_server,
                port=int(config.openhospital_port), 
                db=config.openhospital_database,
                user=config.openhospital_user, 
                passwd=config.openhospital_password
            )
            
        except Exception, ex:
            raise orm.except_orm(
                _('Connection Error!'),
                _("Exception : %s")
                % (ex))
        
        cursor = db.cursor()
            
        # execute SQL select statement
        cursor.execute("SELECT 'OK' as TEST;")
        
        # get and display one row at a time.
        for x in cursor.fetchall():
            if x[0] == 'OK':
                model, view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'isf_openhospital_integration', 'view_isf_openhospital_integration_config_settings_connection_ok')
                return {
                    'name': 'Connection OK',
                    'type': 'ir.actions.act_window',
                    'res_model': 'isf.openhospital.integration.config.settings.message',
                    'view_mode': 'form',
                    'view_type': 'form',
                    'view_id': view_id,
                    'target': 'new',
                    #'context': {'message': 'Connection successful!'} # default_message
                }
            
            else:
                raise orm.except_orm(
                    _('Connection Error!'),
                    _("Exception : %s")
                    % (x[0]))
        
        return True
        
        
class isf_openhospital_integration_config_settings_message(osv.osv_memory):

    _name = "isf.openhospital.integration.config.settings.message"
    _description = "Open Hospital Integration Message"
    
    def _get_default_message(self, cr, uid, context=None):
        if not context:
            context = {}
        if 'message' not in context:
            context.update({'message' : 'Connection successful!'})
        return context['message']

    _columns = {
       'message': fields.text('message', size=512),
    }
    
    _defaults = {
        'message': _get_default_message,
    }
    
    def generate_entries(self, cr, uid, ids, context):
        _logger.debug('Generate entries for context[date]: %s', context['date'])
        _logger.debug('==> existing move_id: %s', context['move_id'])
        move_id = context['move_id']
        date = context['date']
        
        if move_id:
            _logger.debug('==> delete previous entries for date: %s', date)
            move_pool = self.pool.get('account.move')
            move_pool.unlink(cr, uid, [move_id])
            
        integration_obj = self.pool.get('isf.openhospital.integration.import.data')
        return integration_obj.generate_entries(cr, uid, date, context)
    
