# -*- coding: utf-8 -*-

{
	'name': 'ISF Trial Balance Report',
	'version': '1.0',
	'category': 'Tools',
	'description': """

ISF Account Financial Report 
==================================

Modify the OE Trial Balance Report reducing the font size

""",
	'author': 'Alessandro Domanico (alessandro.domanico@informaticisenzafrontiere.org)',
	'website': 'www.informaticisenzafrontiere.org',
	'license': 'AGPL-3',
	'depends': ['account_accountant'],
	'data': [],
	'demo': [],
	'installable' : True,
}

