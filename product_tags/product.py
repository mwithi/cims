# -*- coding: utf-8 -*-
#################################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013 Julius Network Solutions SARL <contact@julius.fr>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################
from openerp.osv import fields, orm
from openerp.tools.translate import _

import logging
_logger = logging.getLogger(__name__)
_debug = False

class product_tag(orm.Model):
    _name = "product.tag"
    _columns = {
        'name': fields.char('Tag Name', required=True, size=64),
    }

class product_product(orm.Model):
    _inherit = "product.product"
    
    def _concat_tags(self, cr, uid, ids, field_name, arg, context):
        if context is None:
            context = {}
            
        if _debug:
            _logger.debug('==> in _concat_tags method')
        
        datas = self.browse(cr, uid, ids)
        res = {}
        for data in datas:
            tags = []
            for tag in data.tag_ids:
                tags.append(tag.name)
            tags.sort()
            res[data.id] = ', '.join(tags)
            if res[data.id] == '':
                 res[data.id] = '<Undefined>'
            if _debug:
                _logger.debug('==> res[data.id] : %s', res[data.id])
        
        return res
    
    def _remove_tag(self, cr, uid, ids, context=None):
        all_ids = self.pool.get('product.product').search(cr, uid, [])
        return all_ids
    
    _columns = {
        'tag_ids' : fields.many2many('product.tag', 'product_product_tag_rel', 'product_id', 'tag_id', 'Active Ingredients'),
        'tags' : fields.function(_concat_tags, type='char', string='Tags', 
            store = {'product.product': (lambda self, cr, uid, ids, c={}: ids, ['tag_ids'], 10),
                     'product.tag': (_remove_tag, ['name'], 10)})
    }
    
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
