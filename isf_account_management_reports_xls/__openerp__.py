# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#
#    Copyright (c) 2020 Informatici Senza Frontiere ONLUS. All rights reserved.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    'name': 'ISF Management Reports Excel',
    'version': '0.0.1',
    'license': 'AGPL-3',
    'author': 'Alessandro Domanico <alessandro.domanico@informaticisenzafrontiere.org>',
    'category': 'Generic Modules/Accounting',
    'description': """ 

    This module adds XLS export to the following Management Reports:
        - Management Balance Sheet
        - Management Income Statement 
        - Management Income Statement Monthly
    """,
    'depends': ['report_xls', 'isf_account_management_reports'],
    'images': [
        'static/src/img/icon.png',],
    'demo_xml': [],
    'init_xml': [],
    'update_xml' : [
        'wizard/management_reports_wizard_view.xml',   
    ],
    'active': False,
    'installable': True,
}
