# -*- encoding: utf-8 -*-

from openerp.osv import orm
import logging
_logger = logging.getLogger(__name__)

class management_report_wizard(orm.TransientModel):
    _inherit = 'management.reports.webkit'
       
    def xls_export(self, cr, uid, ids, context=None):
        return self.check_report(cr, uid, ids, context=context)

    def _print_report(self, cr, uid, ids, data, context=None):
        context = context or {}
        if context.get('xls_export'):
            # we update form with display account value
            data = self.pre_print_report(cr, uid, ids, data, context=context)
            
            fields_to_read = ['management_report_type']
            vals = self.read(cr, uid, ids, fields_to_read, context=context)[0]
            data['form'].update(vals)
            
            return {'type': 'ir.actions.report.xml',
                    'report_name': 'account.management_report_xls',
                    'datas': data}
        else:
            return super(management_report_wizard, self)._print_report(cr, uid, ids, data, context=context)

