from openerp.osv import fields, osv
from tools.translate import _
import logging
import datetime

import logging
_logger = logging.getLogger(__name__)
_debug=False

class isf_lib_hr(osv.osv_memory):
    _name = 'isf.lib.hr'
    
    def get_contract(self, cr, uid, employee_id):
        contract_obj = self.pool.get('hr.contract')
        contract_ids = contract_obj.search(cr, uid, [('employee_id','=',employee_id)])
        if _debug:
            _logger.debug('Employee[%s] Ids[%s]',employee_id, contract_ids)
        
        contract = contract_obj.browse(cr, uid, contract_ids)[0] or None
        
        return contract
        
isf_lib_hr()