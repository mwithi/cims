from openerp.osv import fields, osv
from tools.translate import _
import logging
import datetime
import time

_logger = logging.getLogger(__name__)
_debug=False

class isf_lib_utils(osv.osv_memory):
    _name = 'isf.lib.utils'
    
    def get_company_currency_id(self, cr, uid, context=None):
        users = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        company_id = users.company_id.id
        currency_id = users.company_id.currency_id
		
        return currency_id.id
    
    def get_company_currency(self, cr, uid, context=None):
        users = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        company_id = users.company_id.id
        currency_id = users.company_id.currency_id
        
        return currency_id
        
    def get_other_currency(self, cr, uid, currency_id, context=None):
        res_currency = self.pool.get('res.currency')
        currency_ids = res_currency.search(cr, uid, [('id','!=', currency_id)], limit=1)
        currency_obj = res_currency.browse(cr, uid, currency_ids)
		
        for currency in currency_obj:
            return currency.id
            
    def get_account_by_id(self, cr, uid, account_id):
        if account_id == False:
            return account_id
            
        account_obj = self.pool.get('account.account')
        account_ids = account_obj.search(cr, uid, [('id','=',account_id)])
        if _debug:
            _logger.debug('Accound Ids : Id[%s] %s', account_id,account_ids)
        account = account_obj.browse(cr, uid, account_ids)[0] or None
        
        return account
        
    def get_product_by_id(self, cr, uid, product_id):
        product_obj = self.pool.get('product.product')
        product_ids = product_obj.search(cr, uid, [('id','=',product_id)])
        product = product_obj.browse(cr, uid, product_ids)[0]
        
        return product
        
    def get_product_by_name(self, cr, uid, name):
        product_obj = self.pool.get('product.product')
        product_ids = product_obj.search(cr, uid, [('name','=',name)])
        product = product_obj.browse(cr, uid, product_ids)[0]
        
        return product
        
    #
    # Return the available quantity for the product_id in the specified location_id 
    #   
    def _get_product_qty(self, cr, uid, product_id, location_id):
        stock_move_obj = self.pool.get('stock.move')
        stock_move_in_ids = stock_move_obj.search(cr, uid, [('product_id','=',product_id),('location_dest_id','=',location_id),('state','=','done')])
        stock_move_out_ids = stock_move_obj.search(cr, uid, [('product_id','=',product_id),('location_id','=',location_id),('location_dest_id','!=',location_id),('state','=','done')])
        
        product_qty_in = 0.0
        product_qty_out = 0.0
        
        # Product IN
        for line in stock_move_obj.browse(cr, uid, stock_move_in_ids):
            product_qty_in = product_qty_in + line.product_qty
            
        # Product OUT
        for line in stock_move_obj.browse(cr, uid, stock_move_out_ids):
            product_qty_out = product_qty_out + line.product_qty
            
        return (product_qty_in - product_qty_out)
        
    #
    # Return the available quantity for the product_id in the specified location_id with lot_id
    #
    def _get_product_qty_with_lot_id(self, cr, uid, product_id, lot_id, location_id):
        stock_move_obj = self.pool.get('stock.move')
        stock_move_in_ids = stock_move_obj.search(cr, uid, [('product_id','=',product_id),('prodlot_id','=',lot_id),('location_dest_id','=',location_id),('state','=','done')])
        stock_move_out_ids = stock_move_obj.search(cr, uid, [('product_id','=',product_id),('prodlot_id','=',lot_id),('location_id','=',location_id),('state','=','done'),('location_dest_id','!=',location_id)])
        
        product_qty_in = 0.0
        product_qty_out = 0.0
        
        # Product IN
        for line in stock_move_obj.browse(cr, uid, stock_move_in_ids):
            product_qty_in = product_qty_in + line.product_qty
            
        # Product OUT
        for line in stock_move_obj.browse(cr, uid, stock_move_out_ids):
            product_qty_out = product_qty_out + line.product_qty
            
        return (product_qty_in - product_qty_out)
    
        
    #
    # Generare a stock_move from location_id to location_dest_id for product_id per quantity qty in date 
    #
    def _generate_stock_move(self, cr, uid, name, ref, location_id, location_dest_id, product_id, qty,  date,move_type,lot_id=False,origin=False,uom_id = False,context=None):
        if context is None:
            context = {}
            
        if _debug:
            _logger.debug('origin : %s', origin)
            _logger.debug('name : %s', name)
            _logger.debug('ref : %s', ref)
            
            
        date = date + time.strftime(" %H:%M:%S")
        stock_obj = self.pool.get('stock.move')

        if not uom_id:
            product_uom_id = product_id.uom_id.id
            factor = product_id.uom_id.factor
        else:
            product_uom_id = uom_id.id
            factor = uom_id.factor

        move_id = stock_obj.create(cr, uid, {
            'name': name,
            'picking_id': ref, #cannot be set because is a many2one with 'stock.picking'
            'origin': origin, #cannot be set because is related to picking_id in 'stock.picking'
            'product_uom': product_uom_id,
            'product_uos': product_uom_id,
            'picking_id': False,
            'prodlot_id' : lot_id,
            'product_id': product_id.id,
            'product_uos_qty': qty,
            'product_qty': qty,
            'tracking_id': False,
            'state': 'draft',
            'location_id': location_id,
            'location_dest_id': location_dest_id,
            'type' : move_type, #cannot be set because is related to picking_id in 'stock.picking'
            'date' : date,
            'date_expected' : date,
            'note' : origin,
        }, context=context)
        
        stock_ids = []
        stock_ids.append(move_id)
        stock_obj.write(cr, uid, stock_ids, {'state' : 'done'}, context=context)
        
        return move_id
        
    def get_period(self, cr, uid, date_start, date_stop):
        period_obj = self.pool.get('account.period')
        period_ids = period_obj.search(cr, uid, [('date_start','>=',date_start),('date_stop','<=',date_stop)])
        period = period_obj.browse(cr, uid, period_ids)[0] or None
        
        return period
        
    def _get_period_date(self, cr, uid, period_id):
        period_obj = self.pool.get('account.period')
        period_ids = period_obj.search(cr, uid, [('id','=',period_id)])
        period = period_obj.browse(cr, uid, period_ids)[0] or None
        
        result = {}
        if period:
            result.update({
                'date_start' : period.date_start,
                'date_stop': period.date_stop
            })
        return result
        
     
    #
    # Return a list dictionary with lot_id and product_qty to manage the request
    # The method return False if are unable to find one or more lot to cover the product qty
    #
    def get_lot_id(self, cr, uid, product_id, product_qty, stock_location_id,date,block_on_negative=False):
        stock_prod_lot_obj = self.pool.get('stock.production.lot')
        now = datetime.datetime.now()
        
        if not date:
            return False
        
        if _debug:
            _logger.debug('DATE : %s',date)
            _logger.debug('LOCATION : %s',stock_location_id)
            
        str_time = date+" 00:00:01"
        stock_prod_lot_ids = stock_prod_lot_obj.search(cr, uid, [('product_id','=',product_id),'|',('life_date','>=',date),('life_date','=',False)], order='life_date,date')
        result = []
        remain_qty = product_qty
        ctx = {}
        ctx.update({
            'location_id' : stock_location_id,
        })
        for lot in stock_prod_lot_obj.browse(cr, uid, stock_prod_lot_ids,context=ctx):
            qty = lot.stock_available
            
            if _debug:
                _logger.debug('Stock [%s] Available[%s] Remain[%s]',lot.id,lot.stock_available,remain_qty)
            
            if qty > 0.0 and remain_qty > 0.0:
                assigned_qty = 0.0
                if qty >= remain_qty:
                    assigned_qty = remain_qty
                    remain_qty = 0.0
                else:
                    assigned_qty = qty
                    remain_qty -= assigned_qty
                
                lot = {
                    'id' : lot.id,
                    'qty' : assigned_qty,
                    'cost_price' : lot.cost_price,
                }
                
                result.append(lot)
                
            #
            # If the module permit to use negative, the negative sell is registerd
            # on the last stock_lot.
            #
            #if not block_on_negative and remain_qty > 0.0 and len(result) > 0:
            #    lot = result[len(result) - 1]
            #    lot_qty = lot['qty']
            #    result[len(result) - 1].update({
            #        'qty' : lot_qty + remain_qty,
            #    })
                
        if remain_qty > 0.0 and block_on_negative:
            return False
            
        if remain_qty > 0.0:
            return False
            
        if _debug:
            _logger.debug('RESULT : %s', result)
            
        return result
    
    #
    # Create a lot_id with expiry_date
    #
    def create_lot_id_with_expiry_date(self, cr, uid, product_id, expiry_date, creation_date=None):
        stock_lot_obj = self.pool.get('stock.production.lot')
        product_obj = self.pool.get('product.product')
        product_ids = product_obj.search(cr, uid, [('id','=',product_id.id)])
        product = product_obj.browse(cr, uid, product_ids)[0] or False
            
        lot_id = False
        if product:
            if creation_date is None:
                creation_date = time.strftime("%Y-%m-%d %H:%M:%S")
            #else:
            #    creation_date = creation_date + time.strftime(" %H:%M:%S")
        
            if expiry_date:    
                lot_id = stock_lot_obj.create(cr, uid, {
                    'product_id' : product.id,
                    'life_date' : expiry_date,
                    'ref' : expiry_date[:10],
                    'date' : creation_date,
                }, context=None)
            else:
                lot_id = stock_lot_obj.create(cr, uid, {
                    'product_id' : product.id,
                    'life_date' : creation_date,
                    'ref' : creation_date[:10],
                    'date' : creation_date,
                }, context=None)
        
        if _debug:
            _logger.debug('CREATE : %s', lot_id)
        
        return lot_id
            
    def create_lot_id(self, cr, uid, product, date, document_number,num_order):
        stock_lot_obj = self.pool.get('stock.production.lot')
        ref = product.name+'_'+date + '_' + document_number
        
        lot_id = stock_lot_obj.create(cr, uid, {
            'product_id' : product.id,
            'ref' : ref,
            'prefix' : num_order,
        }, context=None)
        
        if _debug:
            _logger.debug('CREATE : %s', lot_id)
        
        return lot_id
        
    def get_lot_id_by_order(self, cr, uid, product_id, order_id):
        stock_lot_obj = self.pool.get('stock.production.lot')
        stock_lot_ids = stock_lot_obj.search(cr, uid, [('product_id','=',product_id),('prefix','=',order_id)])
        if(stock_lot_ids):
            stock_lot = stock_lot_obj.browse(cr, uid, stock_lot_ids)[0]
            return stock_lot.id
        
        return False

    def get_lot_by_order(self, cr, uid, product_id, order_id):
        stock_lot_obj = self.pool.get('stock.production.lot')
        stock_lot_ids = stock_lot_obj.search(cr, uid, [('product_id','=',product_id),('prefix','=',order_id)])
        if(stock_lot_ids):
            stock_lot = stock_lot_obj.browse(cr, uid, stock_lot_ids)[0]
            return stock_lot
        
        return False
            
        
    
    def get_cost_price_from_history(self, cr, uid, product, lot_id):
        cost_history_obj = self.pool.get('isf.product.history.purchase')
        
        if lot_id is not None:
            if _debug:
                _logger.debug('Find for cost price, lot_id : %s', lot_id)
        
            cost_history_ids = cost_history_obj.search(cr, uid, [('product','=',product.id),('lot_id','=',lot_id)])
        
            # If I found the exactly cost price from lot_id, return it
            if len(cost_history_ids) > 0:
                cost_history = cost_history_obj.browse(cr, uid, cost_history_ids)[0]
                return cost_history.price
        
        # If I don't found the price, calculate a average cost price from history
        cost_sum = 0
        number_of_prices = 0
        cost_price = 0
        cost_history_ids = cost_history_obj.search(cr, uid, [('product','=',product.id)],order='write_date')
        for cost_history in cost_history_obj.browse(cr, uid, cost_history_ids):
            if cost_history.price > 0:
                cost_sum += cost_history.price
                number_of_prices += 1
        
        if number_of_prices > 0:
            cost_price = cost_sum / number_of_prices
            
        if cost_price > 0:
            return cost_price
            
        # Return the sell price if is not possible to found a cost price
        return product.list_price
        
    def set_cost_price(self, cr, uid, product_id, lot_id, price):
        #cost_history_obj = self.pool.get('isf.product.history.purchase')
        #res_id = cost_history_obj.create(cr, uid, {
        #    'product' : product_id,
        #    'lot_id' : lot_id,
        #    'price' : price,
        #})
        lot_obj = self.pool.get('stock.production.lot')
        lot_obj.write(cr, uid, lot_id, {'cost_price' : price}, context=None),
        
        return True
        

    def get_cost_price(self, cr, uid, product_id, lot_id, ratio):
        inv_obj = self.pool.get('account.invoice')
        stock_lot_obj = self.pool.get('stock.production.lot')
        stock_lot_ids = stock_lot_obj.search(cr, uid, [('id','=',lot_id)])
        stock_lot = stock_lot_obj.browse(cr, uid, stock_lot_ids)[0]
        
        inv_ids = inv_obj.search(cr, uid, [('origin','=', stock_lot.prefix)])
        
        if len(inv_ids) > 0:
            inv = inv_obj.browse(cr, uid, inv_ids)[0]
            if inv:
                for line in inv.invoice_line:
                    if line.product_id.id == product_id:
                        return line.price_unit / ratio
        
        return False        
        
    
    def find_lot_id_by_expiry_date(self, cr, uid, product_id, expiry_date):
        stock_lot_obj = self.pool.get('stock.production.lot')
        stock_lot_ids = stock_lot_obj.search(cr, uid, [('product_id','=',product_id),('ref','=',expiry_date)],order='date,life_date')
        for stock in stock_lot_obj.browse(cr, uid, stock_lot_ids):
            return stock.id
        return False
    
    def find_lot_id(self, cr, uid, product_id, qty, location_id):
        stock_lot_obj = self.pool.get('stock.production.lot')
        ctx = {'location_id' : location_id}
        stock_lot_ids = stock_lot_obj.search(cr, uid, [('product_id','=',product_id)],order='life_date,date',context=ctx)
        result = []
        
        if _debug:
            _logger.debug('lot ids : %s', stock_lot_ids)
        remain_qty = qty
        for stock_lot in stock_lot_obj.browse(cr, uid, stock_lot_ids,context=ctx):
            creation_date = stock_lot.date[:-9]
            available = stock_lot.stock_available
            
            if _debug:
                _logger.debug('Stock id : %s , available : %f remain : %f', stock_lot.id, available, remain_qty)
            
            if available > 0.0:
                if available >= remain_qty:
                    stock_qty = remain_qty
                else:
                    stock_qty = available
            
                res = {
                    'lot_id' : stock_lot.id,
                    'qty' : stock_qty,
                    'cost_price' : stock_lot.cost_price
                }
                result.append(res)
                remain_qty = remain_qty - stock_qty
                
                
                if remain_qty == 0:
                    break
                
        if _debug:
            _logger.debug('Remain qty : %d', remain_qty)
        if remain_qty == 0:
            return result
            
        return False
    
    def set_expiry_date(self, cr, uid, lot_id, expiry_date):
        stock_lot_obj = self.pool.get('stock.production.lot')
        stock_lot_ids = stock_lot_obj.search(cr, uid, [('id','=',lot_id)])
        
        if len(stock_lot_ids) > 0:
            ref = expiry_date[:10]
            stock_lot_obj.write(cr, uid, stock_lot_ids, {'ref' : ref})
            
        return True
        
    def generate_account_entry(self, cr, uid, journal_id, date, ref, product, debit_account_id, credit_account_id, debit_aa_id, credit_aa_id, amount,context=None):
        if context is None:
            context = {}
            
        move_obj = self.pool.get('account.move')
        move_line_obj = self.pool.get('account.move.line')
        move = move_obj.account_move_prepare(cr, uid, journal_id, date, ref=ref, context=context)	
        move_id = move_obj.create(cr, uid, move, context=context)
        
        move_line_credit = {
            'analytic_account_id': credit_aa_id, 
            'tax_code_id': False, 
            'tax_amount': 0,
            'ref' : ref,
            'name': product.name or '/',
            'currency_id': False,
            'credit': amount,
            'debit': 0.0,
            'date_maturity' : False,
            'amount_currency': False,
            'partner_id': False,
            'move_id': move_id,
            'account_id': credit_account_id,
            'state' : 'valid',
        }
    
        result = move_line_obj.create(cr, uid, move_line_credit,context=context,check=False)
        
        move_line_debit = {
            'analytic_account_id': debit_aa_id, 
            'tax_code_id': False, 
            'tax_amount': 0,
            'ref' : ref,
            'name': product.name or '/',
            'currency_id': False,
            'credit': 0.0,
            'debit': amount,
            'date_maturity' : False,
            'amount_currency': False,
            'partner_id': False,
            'move_id': move_id,
            'account_id': debit_account_id,
            'state' : 'valid',
        }
    
        result = move_line_obj.create(cr, uid, move_line_debit,context=context,check=False)
        
    def _generate_cost_of_items_moves(self, cr, uid, move_line_pool,name, product_name, cost, move_cost_id, dbt_account_id, crd_account_id, analytic_account_id, 
        stock_analytic_account_id, currency_id=False,amount_currency=False, context=None):
        move_line_cost1 = {
                'analytic_account_id': analytic_account_id, 
                'tax_code_id': False, 
                'tax_amount': 0,
                'ref' : name,
                'name': product_name or '/',
                'currency_id': currency_id,
                'credit': 0.0,
                'debit': cost,
                'date_maturity' : False,
                'amount_currency': amount_currency,
                'partner_id': False,
                'move_id': move_cost_id,
                'account_id': dbt_account_id,
                'state' : 'valid',
            }
        
        move_line_cost2 = {
                'analytic_account_id': False, 
                'tax_code_id': False, 
                'tax_amount': 0,
                'ref' : name,
                'name': product_name or '/',
                'currency_id': currency_id,
                'credit': cost,
                'debit': 0.0,
                'date_maturity' : False,
                'amount_currency': -1 * amount_currency,
                'partner_id': False,
                'move_id': move_cost_id,
                'account_id': crd_account_id,
                'state' : 'valid',
            }
            
        if _debug:
            _logger.debug('Move 1 : %s Move 2 : %s',move_line_cost1,move_line_cost2)
    
        res1 = move_line_pool.create(cr, uid, move_line_cost1,context=context,check=False)
        res2 = move_line_pool.create(cr, uid, move_line_cost2,context=context,check=False)
        
        return res1 & res2
        
isf_lib_utils()