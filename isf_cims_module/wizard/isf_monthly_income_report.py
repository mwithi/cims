from openerp.osv import orm, fields, osv
from openerp import netsvc

import datetime

class isf_monthly_income_wizard(osv.osv_memory):
    _name = "isf.monthly.income.wizard"
    _description = "Monthly Income Report Wizard"
    _columns = {
        'period_id' : fields.many2one('account.period','Period',required=True),
#         'income_account_ids' : fields.many2many('account.account', 'isf_mi_income_rel', string="Incomes Accounts", select=True),
        'entry_type' : fields.selection([('all','All Entries'),('posted','Only Posted')],'Target Moves'),
        'analytic_account_ids': fields.many2many('account.analytic.account', string="Analytic Accounts", select=True),
        'print_date' : fields.date('Print Date'),
#         'cash_bank_account_ids' : fields.many2many('account.account', 'isf_mi_cash_bank_account_rel', string="Cash/Bank Account", select=True),
    }
    
    _defaults = {
        'entry_type' : 'posted',
        'print_date' : fields.date.context_today,
    }
    
isf_monthly_income_wizard()
