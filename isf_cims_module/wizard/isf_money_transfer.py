from openerp.osv import fields, osv
from tools.translate import _
import logging
import datetime

_logger = logging.getLogger(__name__)
_debug=False

class isf_money_transfer(osv.osv_memory):
    _name = 'isf.money.transfer'	
    
    def _get_help(self, cr, uid, context=None):
        if context is None:
            context = {}
            
        help = self.pool.get('ir.values').get_default(cr, uid, 'isf.money.transfer','help')
        return help
    
    
    def get_gain_account(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
	
        if _debug:
                _logger.debug('Search for Gain Account')
                
        company_pool = self.pool.get('res.company')
        company_ids = company_pool.search(cr, uid, [], limit=1)
        company_obj = company_pool.browse(cr, uid, company_ids, context=context)
        
        for company in company_obj:
            if _debug:
                _logger.debug('Gain id : %d',company.income_currency_exchange_account_id.id)
        
            return company.income_currency_exchange_account_id.id;
    
    def get_loss_account(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
	
        if _debug:
                _logger.debug('Search for Loss Account')
                
        company_pool = self.pool.get('res.company')
        company_ids = company_pool.search(cr, uid, [], limit=1)
        company_obj = company_pool.browse(cr, uid, company_ids, context=context)
        
        for company in company_obj:
            if _debug:
                _logger.debug('Loss id : %d',company.expense_currency_exchange_account_id.id)
        
            return company.expense_currency_exchange_account_id.id;
    
    
        
    def _get_default_journal_id(self, cr, uid, context=None):
        if context is None:
            context = {}
	
        journal_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.money.transfer','journal_id')
        return journal_id
    
	
    _columns = {
        'name' : fields.char('Description', size=64, required=True),
        'date' : fields.date('Date',select=True,required=True),
        'amount' : fields.float('Amount',digits=(16,2),required=True),
        'currency' : fields.many2one('res.currency','Currency',required=True),
        'currency_view' : fields.many2one('res.currency','Currency'),
        'account_src' : fields.many2one('account.account','Source',required=True),
        'account_dst' : fields.many2one('account.account','Destination',required=True),
        'dst_amount' : fields.float('Destination amount', digits=(16,2),required=True),
        'ref' : fields.char('Reference', size=64, required=True),
        'dst_currency' : fields.many2one('res.currency','Currency',required=False),
        'dst_currency_view' : fields.many2one('res.currency','Currency'),
        # Hidden fields
        'journal_id' : fields.many2one('account.journal','Journal'),
        'period_id': fields.many2one('account.period', 'Period'),
        'current_rate' : fields.float('Current rate',digits=(16,2)),
        'change_adjust' : fields.float('Difference',digits=(16,2)),
        'current_rate_view' : fields.float('Transaction Rate',digits=(16,2)),
        'change_adjust_view' : fields.float('Exchange Difference',digits=(16,2)),
        'dst_amount_view' : fields.boolean('View Amount Dst'),
        'help' : fields.text('Help', size=512),
        'system_amount_src' : fields.float('System Amount Src',digits=(16,2)),
        'system_amount_dst' : fields.float('System Amount Dst',digits=(16,2)),
        'gain_or_loss_desc' : fields.char('Gain or Loss', size=16),
        'rate_at_date' : fields.float('Rate At Date', digits=(16,2)),
    }
	
    _defaults = {
        'date' : fields.date.context_today,
        'journal_id' : _get_default_journal_id,
        'current_rate' : 1.0,
        'change_adjust': 0.0,
        'dst_amount_view' : False,
        'help' : _get_help,
        'name' : False,
        'account_src' : False,
        'account_dst' : False,
    }
    
    def _check_amount(self, cr, uid, ids, context=None):
        obj = self.browse(cr, uid, ids[0], context=context)
            
        if obj.amount <= 0.0 :
            return False
        return True
    
    def _check_amount_dst(self, cr, uid, ids, context=None):
        obj = self.browse(cr, uid, ids[0], context=context)
            
        if obj.dst_amount <= 0.0 :
            return False
        return True
        
    def _check_account(self, cr, uid, ids, context=None):
        obj = self.browse(cr, uid, ids[0], context=context)
        
        if obj.account_src and obj.account_dst:
            if obj.account_src == obj.account_dst:
                return False
        return True

    _constraints = [
         (_check_amount, 'Source Amount must be positive ( > 0.0)',['amount']),
         (_check_amount_dst, 'Destination Amount must be positive ( > 0.0)',['dst_amount']),
         (_check_account, 'Source and Destination account must be different',['account_src']),
         (_check_account, 'Source and Destination account must be different',['account_dst']),
    ]
	
    def _get_company_currency_id(self, cr, uid, context=None):
        users = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        company_id = users.company_id.id
        currency_id = users.company_id.currency_id
		
        return currency_id.id
	
    def save_and_new(self, cr, uid, ids, context=None):
        if _debug:
            _logger.debug('Call Save and New method')
        ir_model_pool = self.pool.get('ir.model')
        
        for record in self.browse(cr, uid, ids, context=context):
            if _debug:
                _logger.debug('Model id : %s', record.id)
                
        self._transfer(cr, uid, ids, context=None)
                           
        return {
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'view_type': 'form',
            #'res_id': record.id,
            'res_model': self._name,
            'target': 'new',
            # save original model in context, because selecting the list of available
            # templates requires a model in context
            'context': {
                'default_model': 'isf.money.transfer',
                'ref' : False,
            },
    }
    
    def _transfer(self, cr, uid, ids, context = None):
        if context is None:
            context = {}
        data = self.browse(cr, uid, ids, context=context)[0]
        
        if _debug:
            _logger.debug('CONTEXT (MT) : %s',context)

        journal_id = self._get_default_journal_id(cr, uid, context=context)
        actual_period_id = self._get_actual_period_id(cr, uid, context=context)

        move_line_pool = self.pool.get('account.move.line')		
        move_pool = self.pool.get('account.move')
        currency_pool = self.pool.get('res.currency')
		
        move = move_pool.account_move_prepare(cr, uid, journal_id, data.date, ref=data.ref, context=context)	
	
        if _debug:
            _logger.debug('Move : %s',move)
    
        move_id = move_pool.create(cr, uid, move, context=context)
		
        company_curr_id = self._get_company_currency_id(cr, uid, context=context)
        amount_currency_credit = False
        amount_currency_debit = False
        currency_id = False
        #credit = data.amount
        #debit = data.amount
        credit = currency_pool.compute(cr, uid, data.currency.id, company_curr_id, data.amount, context=context)
        debit = currency_pool.compute(cr, uid, data.currency.id, company_curr_id, data.amount, context=context)
        isGain = False
    
        if data.currency.id != data.dst_currency.id:
            # Source
            if company_curr_id == data.currency.id:
                currency_id = data.dst_currency.id
                amount_currency_credit = 0.0
                amount_currency_debit = data.dst_amount
                #credit = currency_pool.compute(cr, uid, data.dst_currency.id, company_curr_id, data.dst_amount, context=context)
                credit = data.amount
                debit = currency_pool.compute(cr, uid, data.dst_currency.id, company_curr_id, data.dst_amount, context=context)
            else:
            #Destinantion
                currency_id = data.currency.id
                amount_currency_credit = -1 * data.amount
                amount_currency_debit = 0.0
                debit = data.dst_amount
                credit = currency_pool.compute(cr, uid, data.currency.id, company_curr_id, data.amount, context=context)
                
            isGain = (debit > credit)
        elif data.currency.id == data.dst_currency.id and data.currency.id != company_curr_id and data.dst_currency.id != company_curr_id:
            amount_currency_credit = -1 * data.amount
            amount_currency_debit = data.amount
            currency_id = data.currency.id
            
        debit_currency_id = currency_id
        credit_currency_id = currency_id
        if amount_currency_credit == 0.0:
            credit_currency_id = False
            
        if  amount_currency_debit == 0.0:
            debit_currency_id = False
            
        move_line = {
            'analytic_account_id': False, 
            'tax_code_id': False, 
            'tax_amount': 0,
            'ref' : data.ref,
            'name': data.name or '/',
            'currency_id': credit_currency_id,
            'credit': credit,
            'debit': 0.0,
            'date_maturity' : False,
            'amount_currency': amount_currency_credit,
            'partner_id': False,
            'move_id': move_id,
            'account_id': data.account_src.id,
            'state' : 'valid'
        }
		
        if _debug:
            _logger.debug('move_line : %s',move_line)
		
        result = move_line_pool.create(cr, uid, move_line,context=context,check=False)
		
        if _debug:
            _logger.debug('Result : %s', result)
			
        move_line = {
            'analytic_account_id': False, 
            'tax_code_id': False, 
            'tax_amount': 0,
            'ref' : data.ref,
            'name': data.name or '/',
            'currency_id': debit_currency_id,
            'credit': 0.0,
            'debit': debit,
            'date_maturity' : False,
            'amount_currency': amount_currency_debit,
            'partner_id': False,
            'move_id': move_id,
            'account_id': data.account_dst.id,
            'state' : 'valid'
        }
		
        result = move_line_pool.create(cr, uid, move_line,context=context,check=False)
		
        if _debug:
            _logger.debug('Result : %s', result)
            _logger.debug('Current rate : %f',data.current_rate)
            _logger.debug('Adjust : %f',data.change_adjust)
            
        # Manage the difference between system rate and user rate
        if data.change_adjust != 0.0:
            if _debug:
                _logger.debug('Manage EXCHANGE difference')
        
            account = False
            credit = 0.0
            debit = 0.0
            amount_currency = False
            
            if isGain:
                account = self.get_gain_account(cr, uid, ids, context=context)
                credit = data.change_adjust
                if credit < 0.0:
                    credit = -1 * credit
            else:
                account = self.get_loss_account(cr, uid, ids, context=context)
                debit = data.change_adjust
                if debit < 0.0:
                    debit = -1 * debit 
                
            move_line = {
                'analytic_account_id': False, 
                'tax_code_id': False, 
                'tax_amount': 0,
                'ref' : data.ref,
                'name': data.name or '/',
                'currency_id': False,
                'credit': credit,
                'debit': debit,
                'date_maturity' : False,
                'amount_currency': False,
                'partner_id': False,
                'move_id': move_id,
                'account_id': account,
                'state' : 'valid'
            }
		
            result = move_line_pool.create(cr, uid, move_line,context=context,check=False)
        
        
			
    def make_transfer(self, cr, uid, ids, context=None):
        self._transfer(cr, uid, ids, context=None)
	
        return {'type': 'ir.actions.act_window_close'}
	
	
    def _get_actual_period_id(self, cr, uid, context=None):
        if context is None:
            context = {}
			
        period_pool = self.pool.get('account.period')
        period_ids = period_pool.search(cr, uid, [])
        period_obj = period_pool.browse(cr, uid, period_ids, context=context)
		
        now = datetime.datetime.now()
        now_str = str(now.year) + "-" + str(now.month) + "-" + str(now.day)
		
        period_id = None
        for period in period_obj:
            if _debug:
                if now_str <= period.date_stop and now_str >= period.date_start:
                    period_id = period.id
                    _logger.debug('Now : %s Start : %s , Stop : %s',now_str, period.date_start, period.date_stop)
				
        return period_id
		         
    def onchange_amount(self, cr, uid, ids, amount, amount_dst, context=None):
        if context is None:
            context = {}
            
        src_currency_id = context.get('currency_id')
        dst_currency_id = context.get('dst_currency_id')   
        account_src_id = context.get('account_src_id')
        account_dst_id = context.get('account_dst_id') 
        currency_pool = self.pool.get('res.currency')
        
        currency_ids = currency_pool.search(cr, uid, [('id','=',src_currency_id)])
        currency_obj = currency_pool.browse(cr, uid, currency_ids)
    
        for c in currency_obj:
            c_obj_src = c
            break
            
        currency_ids = currency_pool.search(cr, uid, [('id','=',dst_currency_id)])
        currency_obj = currency_pool.browse(cr, uid, currency_ids)
    
        for c in currency_obj:
            c_obj_dst = c
            break
            
            
            
        current_rate = 1.0
        if amount and amount_dst:
            current_rate = float(amount) / float(amount_dst)
            _logger.debug('Amount : %f, Amount_dst : %f Rate : %.6f', amount, amount_dst, current_rate)
                        
            
        result = {'value':{} }
        result['value'].update({
            'current_rate' : current_rate,
            'current_rate_view' : current_rate,
            'change_adjust' : 0.0,
            'change_adjust_view' : 0.0,
            'system_amount_src' : 0.0,
            'system_amount_dst' : 0.0,
        })
        if _debug:
            _logger.debug('CONTEXT : %s', context)
            
        if src_currency_id and dst_currency_id and account_src_id and account_dst_id:
            if _debug:
                _logger.debug('--- ALL FIELDS ARE OK ---')    
            
            # If currency (soource and destination) are different, I have to calculare a gain or loss exchange
            # All operations are performed in company currency
            if src_currency_id != dst_currency_id:    
                if amount and amount_dst:
                    company_currency_id = self._get_company_currency_id(cr, uid, context=context)
                    
                    rate_at_date = 0.0
                    if src_currency_id == company_currency_id:
                        current_rate = float(amount) / float(amount_dst)
                    else:
                        current_rate = float(amount_dst) / float(amount)
                
                    src_amount_in_company_currency =  currency_pool.compute(cr, uid, src_currency_id, company_currency_id , float(amount), context=context)   
                    dst_amount_in_company_currency =  currency_pool.compute(cr, uid, dst_currency_id, company_currency_id , float(amount_dst), context=context)        
                
                    if _debug:
                        _logger.debug('SRC : %f DST : %f',src_amount_in_company_currency,dst_amount_in_company_currency)
                
                    gain_or_loss = '(Gain)'
                    
                    difference = float(src_amount_in_company_currency) - float(dst_amount_in_company_currency)
                    if difference > 0:
                        gain_or_loss = '(Loss)'
                    
                    
                    if _debug:
                        _logger.debug('Difference : %f Rate : %f', difference, current_rate)
                
                    result['value'].update({
                        'current_rate' : current_rate,
                        'change_adjust' : difference,
                        'current_rate_view' : current_rate,
                        'change_adjust_view' : difference,
                        'system_amount_src' : src_amount_in_company_currency,
                        'system_amount_dst' : dst_amount_in_company_currency,
                        'gain_or_loss_desc' : gain_or_loss,
                        'rate_at_date' : rate_at_date,
                    })
            else:
                result['value'].update({
                    'amount' : amount,
                    'dst_amount' : amount,
                    'change_adjust' : 0.0,
                    'change_adjust_view' : 0.0,
                    'system_amount_src' : 0.0,
                    'system_amount_dst' : 0.0,
                })
            
        return result
        
            
    def _onchange_account(self, cr, uid, ids, account_src_id, account_dst_id, context=None):
        account_pool = self.pool.get('account.account')
        account_src_ids = account_pool.search(cr, uid, [('id','=',account_src_id)])
        account_src_obj = account_pool.browse(cr, uid, account_src_ids)
        
        account_dst_ids = account_pool.search(cr, uid, [('id','=',account_dst_id)])
        account_dst_obj = account_pool.browse(cr, uid, account_dst_ids)
        
        currency_dst_id = None
        currency_src_id = None
        for account_src in account_src_obj:
            currency_src_id = account_src.currency_id
            
        for account_dst in account_dst_obj:
            currency_dst_id = account_dst.currency_id
        
        if account_src_id:
            if not currency_src_id:
                cur_id = self._get_company_currency_id(cr, uid, context=context)
                currency_pool = self.pool.get('res.currency')
                currency_ids = currency_pool.search(cr, uid, [('id','=',cur_id)])
                currency_obj = currency_pool.browse(cr, uid, currency_ids)
            
                for currency in currency_obj:
                    currency_src_id = currency
                    
        if account_dst_id:
            if not currency_dst_id:
                cur_id = self._get_company_currency_id(cr, uid, context=context)
                currency_pool = self.pool.get('res.currency')
                currency_ids = currency_pool.search(cr, uid, [('id','=',cur_id)])
                currency_obj = currency_pool.browse(cr, uid, currency_ids)
            
                for currency in currency_obj:
                    currency_dst_id = currency
                    
        dst_amount_view = False
        if currency_src_id and currency_dst_id:
            if currency_src_id != currency_dst_id:
                dst_amount_view = True
        
        return currency_src_id, dst_amount_view
	
    def onchange_account_src(self, cr, uid, ids,account_src_id,account_dst_id, context=None):
        result = {'value':{} }
		
        currency_id, dst_amount_view = self._onchange_account(cr, uid, ids, account_src_id, account_dst_id,context=context)
        
        if currency_id:
            result['value'].update({
                'amount' : 0.0,
                'currency' : currency_id.id,
                'currency_view' : currency_id.id,
                'dst_amount_view' : dst_amount_view,
                #'dst_amount_view' : True,
            })
		
        return result
    	
    def onchange_account_dst(self, cr, uid, ids,account_src_id,account_dst_id,context=None):
        result = {'value':{} }
        
        currency_id,dst_amount_view = self._onchange_account(cr, uid, ids, account_src_id,account_dst_id,context=context)
		
        if currency_id:
            result['value'].update({
                'dst_amount' : 0.0,
                'dst_currency' : currency_id.id,
                'dst_currency_view' : currency_id.id,
                'dst_amount_view' : dst_amount_view,
            })
		
        return result
        
    def _get_analytic_journal_by_name(self, cr, uid, name):
        analytic_journal_pool = self.pool.get('account.analytic.journal')
        analytic_journal_ids = analytic_journal_pool.search(cr, uid, [('name','=',name)])
        return analytic_journal_ids[0] if len(analytic_journal_ids) else None
    
    def _create_default_journal(self, cr, uid, ids=None, context=None):
        if context is None:
            context = {}
            
        journal_pool = self.pool.get('account.journal')
        journal_ids = journal_pool.search(cr, uid, [('code','=','MTJ')])
        journal_obj = journal_pool.browse(cr, uid, journal_ids, context=context)
        
        Found = False
        for journal in journal_obj:
            if _debug:
                _logger.debug('Found : %d,%s',journal.id, journal.code)
            Found = True
        
        if Found:  
            _logger.debug('Default journal found') 
        else:
            _logger.debug('Default journal not found : create')
            
            seq_pool = self.pool.get('ir.sequence')
            seq_ids = seq_pool.search(cr, uid, [('name','=','Money Transfer Sequence')])
            seq_obj = seq_pool.browse(cr, uid, seq_ids)
            sequence_id = False
            for seq in seq_obj:
                if _debug:
                    _logger.debug('Sequence : %d,%s',seq.id, seq.name)
                sequence_id = seq.id
                break
                
            if not sequence_id:
                seq_vals = {
                    'name' : 'Money Transfer Sequence',
                    'prefix' : 'MT/%(year)s/',
                    'padding' : 4,
                    'implementation' : 'no_gap',
                }
                
                sequence_id = seq_pool.create(cr,uid, seq_vals, context=context) 
            
            company_currency_id = self._get_company_currency_id(cr, uid, context=context)
            
            journal = {
                'name' : 'Money Transfer Journal',
                'code' : 'MTJ',
                'type' : 'general',
                'sequence_id' : sequence_id,
                'update_posted' : True,
                'currency' : company_currency_id,
                'analytic_journal_id' : self._get_analytic_journal_by_name(cr, uid, 'General')
            }
            
            journal_pool.create(cr, uid, journal, context=context)
            
        return True
	
isf_money_transfer()