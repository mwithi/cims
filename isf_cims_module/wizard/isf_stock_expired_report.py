from openerp.osv import orm, fields, osv
from openerp import netsvc

import datetime

class isf_stock_expired_wizard(osv.TransientModel):
    _name = "isf.stock.expired.wizard"
    _description = "Stock Expired"
    _columns = {
        'date' : fields.date('Date',required=True),
        'location_id' : fields.many2one('stock.location',string="Stock Location", required=True),
    }
    
    _defaults = {
        'date' : fields.date.context_today,
    }
    
isf_stock_expired_wizard()