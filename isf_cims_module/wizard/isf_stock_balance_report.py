from openerp.osv import orm, fields, osv
from openerp import netsvc

import datetime

class isf_stock_balance_report(osv.osv_memory):
    _name = "isf.stock.balance.report"
    _description = "Stock Balance Report"
    _columns = {
        'date' : fields.date('Date',required=True),
        'location_id' : fields.many2one('stock.location',string="Stock Location", required=True),
    }
    
    _defaults = {
        'date' : fields.date.context_today,
    }
    
isf_stock_balance_report()
