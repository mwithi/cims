from openerp.osv import orm, fields, osv
from openerp import netsvc

import datetime

class isf_stock_ledger_report(osv.TransientModel):
    _name = "isf.stock.ledger.report"
    _description = "Stock Ledger Report"
    _columns = {
        'product_id' : fields.many2one('product.product','Product'),
        'store_id' : fields.many2one('stock.location','Store'),
        'date_start' : fields.date('Date Start',required=True),
        'date_end' : fields.date('Date End',required=True),
    }
    
    _defaults = {
        'date_start' : fields.date.context_today,
        'date_end' : fields.date.context_today,
    }
