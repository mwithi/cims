from openerp.osv import orm, fields, osv
from openerp import netsvc

import datetime

class isf_store_reorder_report(osv.osv_memory):
    _name = "isf.store.reorder.report"
    _description = "Store Reorder Report"
    _columns = {
        'date' : fields.date('Date',required=True),
    }
    
    _defaults = {
        'date' : fields.date.context_today,
    }
    
isf_store_reorder_report()
