import time

from openerp.osv import osv, fields

class isf_account_analytic_cost_ledger(osv.osv_memory):
    _inherit = 'account.analytic.cost.ledger'
    _description = 'Account Analytic Cost Ledger'

    def _get_report_type(self, cr, uid,  context=None):
        return (
           ('summary', 'Summary Report'),
           ('detailed', 'Detailed Report'))
        

    _columns = {
        'report_type': fields.selection(_get_report_type, 'Report Type', required=True),
    }
    
    _defaults = {
        'report_type' : 'summary',
    }

isf_account_analytic_cost_ledger()