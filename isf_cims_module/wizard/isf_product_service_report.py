from openerp.osv import orm, fields, osv
from openerp import netsvc

import datetime

class isf_product_service_report(osv.osv_memory):
    _name = "isf.product.service.report"
    _description = "Products And Services Report"

    def _get_stores(self, cr, uid, context=None):
        result = []

        result.append(('all','ALL'))
        shop_o = self.pool.get('isf.shop')
        
        # Shops
        shop_ids = shop_o.search(cr, uid, [],order='name')
        for shop in shop_o.browse(cr, uid, shop_ids):
            result.append((shop.name,shop.name))        

        # Pharmacy
        pharm_o = self.pool.get('isf.pharmacy.pos')
        if pharm_o is not None:
            result.append(('PHARMACY','PHARMACY'))
        result.sort()
        return result

    _columns = {
        'type': fields.selection([('product','Product'),('service','Service'),('all','All')],'Type'),
        'shop_id': fields.selection(_get_stores,'Location'),
    }

    _defaults = {
        'type' : 'all',
        'shop_id' : 'all',
    }
    
    
isf_product_service_report()
