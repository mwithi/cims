from openerp.osv import fields, osv
from tools.translate import _
import logging
import datetime
import re

_logger = logging.getLogger(__name__)
_debug=False

class isf_purchase_without_invoice(osv.osv_memory):
    _name = 'isf.purchase.without.invoice'
    
    def _get_help(self, cr, uid, context=None):
        if context is None:
            context = {}
            
        help = self.pool.get('ir.values').get_default(cr, uid, 'isf.purchase.without.invoice','help')
        return help
            
    def _get_default_journal_id(self, cr, uid, context=None):
        if context is None:
            context = {}
	
        journal_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.purchase.without.invoice','journal_id')
        return journal_id
    
    def _get_cash_bank_accounts(self, cr, uid, context=None):
        if context is None:
            context = {}
            
        result = []
        ir_values = self.pool.get('ir.values')
        account_list = ir_values.get_default(cr, uid, 'isf.purchase.without.invoice', 'cash_bank_account_ids')
        account_obj = self.pool.get('account.account')
		
        if account_list is not None:
            for account_id in account_list:
                account_ids = account_obj.search(cr, uid, [('id','=',account_id)], limit=1)
                for account in account_obj.browse(cr, uid, account_ids, context=context):
                    result.append((account.id, account.code+" "+account.name))
                
        return result
    
    def _get_expense_accounts(self, cr, uid, context=None):
        if context is None:
            context = {}
            
        result = []
        ir_values = self.pool.get('ir.values')
        account_list = ir_values.get_default(cr, uid, 'isf.purchase.without.invoice', 'expense_account_ids')
        account_obj = self.pool.get('account.account')
		
        if account_list is not None:
            for account_id in account_list:
                account_ids = account_obj.search(cr, uid, [('id','=',account_id)], limit=1)
                for account in account_obj.browse(cr, uid, account_ids, context=context):
                    result.append((account.id, account.code+" "+account.name))
                
        return result
    
    def _get_analytic_accounts(self, cr, uid, context=None):
        if context is None:
            context = {}
            
        result = []
        
        ir_values = self.pool.get('ir.values')
        account_list = ir_values.get_default(cr, uid, 'isf.purchase.without.invoice', 'analytic_account_ids')
        account_obj = self.pool.get('account.analytic.account')
		
        if account_list is not None:
            for account_id in account_list:
                account_ids = account_obj.search(cr, uid, [('id','=',account_id)], limit=1)
                for account in account_obj.browse(cr, uid, account_ids, context=context):
                    result.append((account.id, account.name_get()[0][1]))
                    if _debug:
                        _logger.debug('==> account.name_get() : %s', account.name_get())
                        _logger.debug('==> append : %s', (account.id, account.name_get()[0][1]))
                    
        result_sorted = sorted(result, key = lambda x: (x[1], len(x[1])))
            
        return result_sorted
    
    def _check_analytic_accounts_display(self, cr, uid, context=None):
        if context is None:
            context = {}
        
        Found = True
        ir_values = self.pool.get('ir.values')
        account_list = ir_values.get_default(cr, uid, 'isf.purchase.without.invoice', 'analytic_account_ids')
        
        if account_list:
            if len(account_list) == 0:
                Found = False
        
        return Found
    
        
    _columns = {
        'journal_id' : fields.many2one('account.journal','Journal', required=True),
        'ref' : fields.char('Reference', size=64,required=True),
        'name' : fields.char('Description', size=64, required=True),
        'date' : fields.date('Date', required=True),
        'currency_view' : fields.many2one('res.currency', 'Currency'),
        'currency' : fields.many2one('res.currency', 'Currency', required=True),
        'currency_amount' : fields.float('Currency Amount',digits=(12,4)),
        'company_amount' : fields.float('Company Amount',digits=(12,4)),
        'cash_account' : fields.selection(_get_cash_bank_accounts,'Cash\Bank account', required=True),
        'expense_account' : fields.selection(_get_expense_accounts,'Expense account', required=True),
        'analytic_account' : fields.selection(_get_analytic_accounts,'Analytic account', required=True),
        'analytic_account_display' : fields.boolean('Analytic Account View'),
        'help' : fields.text('Help', size=512),
    }
    
    _defaults = {
        'journal_id' : _get_default_journal_id,
        'date' : fields.date.context_today,
        'analytic_account_display' : _check_analytic_accounts_display,
        'help' : _get_help,
    }
    
    def _check_amount(self, cr, uid, ids, context=None):
        obj = self.browse(cr, uid, ids[0], context=context)
            
        if obj.currency_amount <= 0.0 :
            return False
        return True

    _constraints = [
         (_check_amount, 'Amount must be positive ( > 0.0)',['amount']),
    ]
    
    def _get_actual_period_id(self, cr, uid, context=None):
        if context is None:
            context = {}
			
        period_pool = self.pool.get('account.period')
        period_ids = period_pool.search(cr, uid, [])
        period_obj = period_pool.browse(cr, uid, period_ids, context=context)
		
        now = datetime.datetime.now()
        now_str = str(now.year) + "-" + str(now.month) + "-" + str(now.day)
		
        period_id = None
        for period in period_obj:
            if _debug:
                if now_str <= period.date_stop and now_str >= period.date_start:
                    period_id = period.id
                    if _debug:
                        _logger.debug('Now : %s Start : %s , Stop : %s',now_str, period.date_start, period.date_stop)
				
        return period_id

    def _get_company_currency_id(self, cr, uid, context=None):
        users = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        company_id = users.company_id.id
        currency_id = users.company_id.currency_id
		
        return currency_id.id
	
    def _get_analytic_journal_by_name(self, cr, uid, name):
        analytic_journal_pool = self.pool.get('account.analytic.journal')
        analytic_journal_ids = analytic_journal_pool.search(cr, uid, [('name','=',name)])
        return analytic_journal_ids[0] if len(analytic_journal_ids) else None
    
    def save_and_new(self, cr, uid, ids, context=None):
        self._save(cr, uid, ids, context=context)
        
        return {
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'view_type': 'form',
            #'res_id': record.id,
            'res_model': self._name,
            'target': 'new',
            # save original model in context, because selecting the list of available
            # templates requires a model in context
            'context': {
                'default_model': 'isf.purchase.without.invoice',
                'ref' : False,
            },
        }
        
    def _save(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        data = self.browse(cr, uid, ids, context=context)[0]

        journal_id = self._get_default_journal_id(cr, uid, context=context)
        actual_period_id = self._get_actual_period_id(cr, uid, context=context)

        move_line_pool = self.pool.get('account.move.line')		
        move_pool = self.pool.get('account.move')
		
        move = move_pool.account_move_prepare(cr, uid, journal_id, data.date, ref=data.ref, context=context)	
	
        if _debug:
            _logger.debug('Context : %s', context)
            _logger.debug('Amount_currency : %f', data.currency_amount)
            _logger.debug('account_move_prepare : %s', move)

        move_id = move_pool.create(cr, uid, move, context=context)
		
		
        if _debug:
            _logger.debug('move_id : %s', move_id)

		
        company_currency_id = self._get_company_currency_id(cr, uid, context=context)
        currency_id = False
        amount_currency = False
        amount_currency_credit = False
        amount_currency_debit = False
        amount = data.company_amount
		
        
        if data.currency.id != company_currency_id:
            currency_pool = self.pool.get('res.currency')
            if _debug:
                _logger.debug('Setting second currency value')
            amount = currency_pool.compute(cr, uid, data.currency.id, company_currency_id, data.currency_amount, context=context)
            amount_currency_credit = -1 * data.currency_amount
            amount_currency_debit = data.currency_amount
            amount_currency = data.currency_amount
            currency_id = data.currency.id
				
        move_line = {
            'analytic_account_id': data.analytic_account or False, 
            'tax_code_id': False, 
            'tax_amount': 0,
            'ref' : data.ref,
            'name': data.name or '/',
            'currency_id': currency_id,
            'credit': 0.0,
            'debit': amount,
            'date_maturity' : False,
            'amount_currency': amount_currency_debit,
            'partner_id': False,
            'move_id': move_id,
            'account_id': int(data.expense_account),
            'state' : 'valid'
        }
		
        if _debug:
            _logger.debug('move_line : %s',move_line)
		
        result = move_line_pool.create(cr, uid, move_line,context=context,check=False)
		
        if _debug:
            _logger.debug('Result : %s', result)
			
        move_line = {
            'analytic_account_id': False, 
            'tax_code_id': False, 
            'tax_amount': 0,
            'ref' : data.ref,
            'name': data.name or '/',
            'currency_id': currency_id,
            'credit': amount,
            'debit': 0.0,
            'date_maturity' : False,
            'amount_currency': amount_currency_credit,
            'partner_id': False,
            'move_id': move_id,
            'account_id': int(data.cash_account),
            'state' : 'valid'
        }
	
        result = move_line_pool.create(cr, uid, move_line,context=context,check=False)
		
        if _debug:
            _logger.debug('Result : %s', result)
    
    def purchase_items(self, cr, uid, ids, context=None):
        self._save(cr, uid, ids, context=context)
		
        return {'type': 'ir.actions.act_window_close'}
            
    def onchange_amount(self, cr, uid, ids,  currency,currency_amount,context=None):
        if context is None:
            context = {}
        
        result = {'value':{} }
        currency_pool = self.pool.get('res.currency')
        company_currency = self._get_company_currency_id(cr, uid, context=context)
        company_amount = currency_pool.compute(cr, uid, currency, company_currency, currency_amount, context=context)
        
        
        result['value'].update({
            'company_amount' : company_amount,
        })
        
        return result
        
    def onchange_currency(self, cr, uid, ids, currency, context=None):
        result = {'value':{} }
        result['value'].update({
            'currency_amount' : 0.0,
            'company_amount' : 0.0,
        })
        
        return result
        
        
    def _create_default_journal(self, cr, uid, ids=None, context=None):
        if context is None:
            context = {}
            
        journal_pool = self.pool.get('account.journal')
        journal_ids = journal_pool.search(cr, uid, [('code','=','NIPJ')])
        journal_obj = journal_pool.browse(cr, uid, journal_ids, context=context)
        
        Found = False
        for journal in journal_obj:
            if _debug:
                _logger.debug('Found : %d,%s',journal.id, journal.code)
            Found = True
        
        if Found:  
            _logger.debug('Default journal found') 
        else:
            _logger.debug('Default journal not found : create')
            
            seq_pool = self.pool.get('ir.sequence')
            seq_ids = seq_pool.search(cr, uid, [('name','=','Purchase Without Invoice Sequence')])
            seq_obj = seq_pool.browse(cr, uid, seq_ids)
            sequence_id = False
            for seq in seq_obj:
                if _debug:
                    _logger.debug('Sequence : %d,%s',seq.id, seq.name)
                sequence_id = seq.id
                break
                
            if not sequence_id:
                seq_vals = {
                    'name' : 'Purchase Without Invoice Sequence',
                    'prefix' : 'NIP/%(year)s/',
                    'padding' : 4,
                    'implementation' : 'no_gap',
                }
                
                sequence_id = seq_pool.create(cr,uid, seq_vals, context=context) 
            
            if _debug:
                    _logger.debug('Analytic Journal found: %s', self._get_analytic_journal_by_name(cr, uid, 'Purchases'))
            journal = {
                'name' : 'Purchase Without Invoice Journal',
                'code' : 'NIPJ',
                'type' : 'purchase',
                'sequence_id' : sequence_id,
                'update_posted' : True,
                'analytic_journal_id' : self._get_analytic_journal_by_name(cr, uid, 'Purchases')
            }
            
            journal_pool.create(cr, uid, journal, context=context)
            
        return True
    
    def onchange_cash_account(self, cr, uid, ids, account_id, context=None):
        if context is None:
            context = {}
    
        result = {'value':{} }
        account_pool = self.pool.get('account.account')
        account_ids = account_pool.search(cr, uid, [('id','=',account_id)],limit=1)
        account_obj = account_pool.browse(cr, uid, account_ids, context=context)
        
        for account in account_obj:
            currency_id = account.currency_id.id
            
            if not currency_id:
                currency_id = self._get_company_currency_id(cr, uid, context=context)
                
            result['value'].update({
                'currency' : currency_id,
                'currency_view' : currency_id                    
            })
        return result

    

isf_purchase_without_invoice()