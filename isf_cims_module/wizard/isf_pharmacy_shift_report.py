from openerp.osv import orm, fields, osv
from openerp import netsvc

import datetime

class isf_pharmacy_shift_report(osv.osv_memory):
    _name = "isf.pharmacy.shift.report"
    _description = "Pharmacy Shift Report"
    _columns = {
        'date' : fields.date('Date', required=True),
        'shift_a_start' : fields.integer('Shift A Start',required=True),
        'shift_a_end' : fields.integer('Shift A End',required=True),
        'shift_b_start' : fields.integer('Shift B Start',required=True),
        'shift_b_end' : fields.integer('Shift B End',required=True),
        'shift_c_start' : fields.integer('Shift C Start',required=True),
        'shift_c_end' : fields.integer('Shift C End',required=True),
    }
    
    
isf_pharmacy_shift_report()
