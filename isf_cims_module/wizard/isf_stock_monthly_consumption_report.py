from openerp.osv import orm, fields, osv
from openerp import netsvc

import datetime

class isf_stock_monthly_consumption_report(osv.TransientModel):
    _name = "isf.stock.monthly.consumption.report"
    _description = "Monthly Consumption Report"
    
    _columns = {
        'location_id' : fields.many2one('stock.location','Location'),
        'date_start' : fields.date('Date Start',required=True),
        'date_end' : fields.date('Date End',required=True),
        'report_type' : fields.selection([('department','Per Department'),('month','Per Month')], 'Report Type'),
        'group_criteria' : fields.selection([('product','Product'),('category','Category'),('tag','Active Ingredient')], 'Group By'),
        'show_data' : fields.selection([('qty','Quantity'),('value','Value')], 'Show'),
    }
    
    _defaults = {
        'date_start' : fields.date.context_today,
        'date_end' : fields.date.context_today,
        'report_type' : 'department',
        'group_criteria' : 'product',
        'show_data' : 'value',
    }
