# -*- coding: utf-8 -*-
import time
from pprint import pprint as pp

from openerp.report import report_sxw
import logging
import locale
import datetime


_logger = logging.getLogger(__name__)
_debug=False

class isf_stock_move_report_parser(report_sxw.rml_parse):
    _name = 'report.isf.stock.move.webkit'

    def __init__(self, cr, uid, name, context=None):
        if _debug:
            _logger.debug('==> INIT <==')
    
        super(isf_stock_move_report_parser, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
            'pp': pp,
            'lines': self.lines,
            'headers' : self.headers,
            'tableheaders' : self.tableheaders,
        })
        self.context = context
        self.result_acc = []
        self.headers_acc = []
        self.table_headers_acc = []
    
    
    
    
    def headers(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.stock.move.report')
        date_s_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['date_start'])
        date_e_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['date_end'])
        location_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['location_id'])
        date_start = date_s_obj[0]['date_start']
        date_end = date_e_obj[0]['date_end']
        
        if _debug:
            _logger.debug('Table header')
        
        location = 'None'
        if location_obj:
            if location_obj[0]['location_id']:
                location = location_obj[0]['location_id'][1]
        
        
        res = {
            'date_start' : date_start,
            'date_end' : date_end,
            'location' : location,
        }
        self.headers_acc.append(res)
        
        if _debug:
            _logger.debug('Table header : %s',res)
        
        
        return self.headers_acc
        
    def types(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.stock.move.report')
        
        report_type_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['report_type'])
        report_type = report_type_obj[0]['report_type']
        
        if _debug:
            _logger.debug('TYPE : %s', report_type)
        
        res = {
            'type' : report_type,
        }
        
        self.result_type.append(res)
        return self.result_type
    
    def _get_qty_before_date(self, cr, uid, product_id, location_id, date):
        move_obj = self.pool.get('stock.move')
        
        Date = datetime.datetime.strptime(date, "%Y-%m-%d")
        
        date_before = Date + datetime.timedelta(days=-1)
        date = date_before.strftime("%Y-%m-%d")
        
        move_in_ids = move_obj.search(cr, uid, [('product_id','=',product_id),('location_dest_id','=',location_id),('date_expected','<',date),('state','=','done')])
        move_out_ids = move_obj.search(cr, uid, [('product_id','=',product_id),('location_id','=',location_id),('date_expected','<',date),('state','=','done')])
    
        in_qty = 0
        for move in move_obj.browse(cr, uid, move_in_ids):
            factor = move.product_uom.factor
            in_qty += move.product_qty / factor
            
        out_qty = 0
        for move in move_obj.browse(cr, uid, move_out_ids):
            factor = move.product_uom.factor
            out_qty += move.product_qty / factor
        
        return in_qty - out_qty
     
    def _get_qty_at_date(self, cr, uid, product_id, location_id, date):
        move_obj = self.pool.get('stock.move')
        loc_obj = self.pool.get('stock.location')
        
        #date = date + ' 23:59:59'
        move_in_ids = move_obj.search(cr, uid, [('product_id','=',product_id),('location_dest_id','=',location_id),('state','=','done'),('date_expected','<=',date)])
        move_out_ids = move_obj.search(cr, uid, [('product_id','=',product_id),('location_id','=',location_id),('state','=','done'),('date_expected','<=',date)])
    
        in_qty = 0
        for move in move_obj.browse(cr, uid, move_in_ids):
            factor = move.product_uom.factor
            in_qty += move.product_qty / factor
        if _debug:
            if product_id == 740:
                _logger.debug('IN QTY : %f', in_qty)
            
            
        out_qty = 0
        for move in move_obj.browse(cr, uid, move_out_ids):
            factor = move.product_uom.factor
            out_qty += move.product_qty / factor
        if _debug:
            if product_id == 740:
                _logger.debug('OUT QTY : %f', out_qty)
        
        return in_qty - out_qty
        
    def _get_qty_for_stock_at_date(self, cr, uid, stock_id, location_id, date):
        move_obj = self.pool.get('stock.move')
        
        #date = date
        move_in_ids = move_obj.search(cr, uid, [('prodlot_id','=',stock_id),('location_dest_id','=',location_id),('state','=','done'),('date_expected','<=',date)])
        move_out_ids = move_obj.search(cr, uid, [('prodlot_id','=',stock_id),('location_id','=',location_id),('state','=','done'),('date_expected','<=',date)])
    
        in_qty = 0
        for move in move_obj.browse(cr, uid, move_in_ids):
            factor = move.product_uom.factor
            in_qty += move.product_qty / factor
            
        out_qty = 0
        for move in move_obj.browse(cr, uid, move_out_ids):
            factor = move.product_uom.factor
            out_qty += move.product_qty / factor
        
        return in_qty - out_qty
        
    def _get_received_qty(self, cr, uid, product_id, location_id, date_start,date_end):
        move_obj = self.pool.get('stock.move')
        move_in_ids = move_obj.search(cr, uid, [('product_id','=',product_id),('state','=','done'),('location_dest_id','=',location_id),('state','=','done'),('date_expected','>=',date_start),('date_expected','<=',date_end)])
    
        in_qty = 0
        for move in move_obj.browse(cr, uid, move_in_ids):
            factor = move.product_uom.factor
            in_qty += move.product_qty / factor
    
        return in_qty
        
    def _get_used_qty(self, cr, uid, product_id, location_id, date_start,date_end):
        move_obj = self.pool.get('stock.move')
        
        move_out_ids = move_obj.search(cr, uid, [('product_id','=',product_id),('state','=','done'),('location_id','=',location_id),('date_expected','>=',date_start),('date_expected','<=',date_end)])
    
        out_qty = 0
        for move in move_obj.browse(cr, uid, move_out_ids):
            factor = move.product_uom.factor
            out_qty += move.product_qty / factor
           
        return out_qty
        
    def _lines_stock_move(self, cr, uid, date_start, date_end):
        ctx = self.context.copy()
        isf_lib = self.pool.get('isf.lib.utils')
        product_obj = self.pool.get('product.product')
        product_ids = product_obj.search(cr, uid, [('active','=',True),('type','=','product')])
        
        obj_report = self.pool.get('isf.stock.move.report')
        report_data = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['location_id'])
        location_id = report_data[0]['location_id'][0]
        
        
        result = {}
        for product in product_obj.browse(cr, uid, product_ids):
            open_qty = int(self._get_qty_before_date(cr, uid, product.id, location_id, date_start))
            close_qty = int(self._get_qty_at_date(cr, uid, product.id, location_id, date_end))
            rcv_qty = int(self._get_received_qty(cr, uid, product.id, location_id, date_start,date_end))
            used_qty = int(self._get_used_qty(cr, uid, product.id, location_id, date_start,date_end))
            
            if open_qty != 0 or close_qty != 0 or rcv_qty != 0 or used_qty != 0:
                p_name = product.name_template
                p_code = product.default_code
                rec = result.get(p_name)
                if rec is None:
                    rec = []
                    rec.append({'code' : p_code, 'name': p_name, 'open':open_qty,'close':close_qty,'received':rcv_qty,'used':used_qty })
                    result.update({p_name:rec})
                else:
                    rec.append({'code' : p_code, 'name': p_name, 'open':open_qty,'close':close_qty,'received':rcv_qty,'used':used_qty })
                    result.update({p_name:rec})
                
                     
        list_keys = result.keys()
        list_keys.sort()  
        for date_key in list_keys:
            rec_list = result.get(date_key)
            for rec in rec_list:  
                self.result_acc.append(rec)
            
        return self.result_acc
        
    def tableheaders(self):

        location_obj = self.pool.get('stock.location')
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.stock.move.report')
        location_obj = self.pool.get('stock.location')
        location_ids = location_obj.search(self.cr, self.uid, [('available_in_selection','=',True),('usage','=','internal')])
        
        
        for location in location_obj.browse(self.cr, self.uid, location_ids):
            res = {
                'location' : location.name,
            }
        
            self.table_headers_acc.append(res)
        
        
        return self.table_headers_acc
        
    def _lines_store_wise(self, cr, uid, date_start, date_end, location_ids):
        product_obj = self.pool.get('product.product')
        product_ids = product_obj.search(cr, uid, [('active','=',True),('type','=','product')])
        isf_lib = self.pool.get('isf.lib.utils')
        lot_obj = self.pool.get('stock.production.lot')
        
        result = {}
        
        for product in product_obj.browse(cr, uid, product_ids):
            first_account = '1'
            total = 0
            
            for location_id in location_ids:
                qty = int(self._get_qty_at_date(cr, uid, product.id, location_id, date_start))
                    
                total += qty
                res = {
                    'code' : product.default_code,
                    'product' : product.name_template,
                    'first_account' : first_account,
                    'is_total' : '0',
                    'qty' : qty,
                    }
                self.result_acc.append(res)
                first_account = '0'
                
            # Totals per line
            res = {
                'product' : product.name_template,
                'first_account' : '0',
                'is_total' : '1',
                'qty' : total,
                }
            self.result_acc.append(res)
        
        return self.result_acc
    
    def lines(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.stock.move.report')
        
        report_type_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['report_type'])
        date_s_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['date_start'])
        date_start = date_s_obj[0]['date_start']
        date_e_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['date_end'])
        date_end = date_e_obj[0]['date_end']
       
        return self._lines_stock_move(self.cr, self.uid, date_start, date_end)
            
    

report_sxw.report_sxw('report.isf.stock.move.webkit', 'isf.stock.move.report', 'addons/isf_cims_module/report/stock_move.mako', parser=isf_stock_move_report_parser)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
