# -*- coding: utf-8 -*-
import time
from pprint import pprint as pp

from openerp.report import report_sxw
import logging
import locale


_logger = logging.getLogger(__name__)
_debug=False

class isf_stock_transaction_report_parser(report_sxw.rml_parse):
    _name = 'report.isf.stock.transaction.webkit'

    def __init__(self, cr, uid, name, context=None):
    
        super(isf_stock_transaction_report_parser, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
            'pp': pp,
            'lines': self.lines,
            'headers' : self.headers,
        })
        self.context = context
        self.result_acc = []
        self.headers_acc = []

    """"
    def set_context(self, objects, data, ids, report_type=None):
        for obj in objects:
            print obj, obj.id
        new_ids = ids
        return super(isf_bs_report_parser, self).set_context(objects, data, new_ids, report_type=report_type)
    """
    #def _add_header(self, node, header=1):
    #    if header == 0:
    #        self.rml_header = ""
    #    return True
    
    def _store_name(self, cr, uid, code):
        store_id = int(code[1:].strip())
        store_type = code[0]

        if store_type == 'A':
            return 'ALL'
        elif store_type == 'S':
            pos_o = self.pool.get('isf.shop')
            pos_ids = pos_o.search(cr, uid, [('id','=',store_id)])
            pos = pos_o.browse(cr, uid, pos_ids)[0]
            return pos.name

        return 'PHARMACY'
    
    
    def headers(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.stock.transaction.report')
        date_s_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['date_start'])
        date_e_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['date_end'])
        date_start = date_s_obj[0]['date_start']
        date_end = date_e_obj[0]['date_end']
        store_o = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['store_id'])
        store = store_o[0]['store_id']
        
        store_name = self._store_name(self.cr, self.uid, store)
        res = { 
            'date_start' : date_start,
            'date_end' : date_end,
            'store' : store_name,
        }
        self.headers_acc.append(res)
        
        return self.headers_acc
    
    def _get_company_currency_id(self, cr, uid, context=None):
        users = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        company_id = users.company_id.id
        currency_id = users.company_id.currency_id
		
        return currency_id

    def _get_product_type(self, product_type):
        if product_type == 'product':
            return 'Product'
        if product_type == 'service':
            return 'Service'
    
    def _manage_pharmacy_pos(self, cr, uid, date_start, date_end,context=None):
        result = {}
        pharm_o = self.pool.get('isf.pharmacy.pos')
        #pharm_ids = pharm_o.search(cr, uid, [('payment_date','>=',date_start),('payment_date','<=',date_end),('state','=','close')],order='date,type')
        pharm_ids = pharm_o.search(cr, uid, [('date','>=',date_start),('date','<=',date_end),('state','in',['close','credit'])],order='date,type')

        acc_o = self.pool.get('account.account')
        currency_pool = self.pool.get('res.currency')
        first_sold = first_foc = first_internal = True
        total_sales = total_foc = total_internal = 0
        total_sales_qty = total_foc_qty = total_internal_qty = 0
        base_curr = self._get_company_currency_id(cr, uid)
        print_header = print_sold_header = print_foc_header = print_used_header = True
        rec_idx = 0

        for pharm in pharm_o.browse(cr, uid, pharm_ids):
            acc_ids = acc_o.search(cr, uid,[('code','=',pharm.payment_method)])
            acc = acc_o.browse(cr, uid, acc_ids)[0]
            
            curr_amount = 0
            currency = base_curr.name
            amount = pharm.total_amount
            if acc.currency_id:
                currency = acc.currency_id.name
                curr_amount = pharm.total_amount
                amount = currency_pool.compute(cr, uid, acc.currency_id.id, base_curr.id, pharm.total_amount, context=context)
                
            if pharm.type in ['paid','credit']:
                t_type = 'SOLD'
                print_header = print_sold_header
                if print_sold_header:
                    print_sold_header = False
            if pharm.type == 'foc':
                t_type = 'FOC'
                print_header = print_foc_header
                if print_foc_header:
                    print_foc_header = False
            if pharm.type == 'internal':
                t_type = 'USED'
                print_header = print_used_header
                if print_used_header:
                    print_used_header = False
            
            ref = ''
            if pharm.account_move_1:
                ref = pharm.account_move_1.name
            if pharm.invoice_id:
                ref = pharm.invoice_id.name
            
            
            rec = {
                'product_type' : 'Product',
                'print_header' : print_header,
                'type' : t_type,
                'currency' : currency,
                'date' : pharm.date,
                'transaction' : pharm.sequence_id,
                'ref' : ref,
                'amount' : amount,
                'curr_amount' : curr_amount,
                'is_total' : False,
                'store' : 'PHARMACY',
            }
            print_header = False
            
            result.update({rec_idx:rec})
            rec_idx += 1

        return result
    
    def _manage_generic_pos(self, cr, uid, shop_id, date_start, date_end,context=None):
        result = {}
        pos_o = self.pool.get('isf.pos')
        acc_o = self.pool.get('account.account')
        currency_pool = self.pool.get('res.currency')
        pos_ids = pos_o.search(cr, uid, [('date','>=',date_start),('date','<=',date_end),('state','=','confirm'),('shop_id','=',shop_id)],order='date,shop_id')
        
        print_sold_header = print_foc_header = print_used_header = True
        print_header = True
        base_curr = self._get_company_currency_id(cr, uid)
        total_sales = total_foc = total_internal = 0
        rec_idx = 0
        for pos in pos_o.browse(cr, uid, pos_ids):
            currency = base_curr.name
            curr_amount = 0
            amount = pos.total_amount
            acc_ids = acc_o.search(cr, uid,[('code','=',pos.payment_id)])
            if acc_ids:
                acc = acc_o.browse(cr, uid, acc_ids)[0]
                if acc.currency_id:
                    currency = acc.currency_id.name
                    curr_amount = currency_pool.compute(cr, uid, acc.currency_id.id, base_curr.id, pos.total_amount, context=None)
            
            if pos.transaction_type_product in ['sale','credit']:
                t_type = 'SOLD'
                print_header = print_sold_header
                if print_sold_header:
                    print_sold_header = False
            if pos.transaction_type_product == 'foc':
                t_type = 'FOC'
                print_header = print_foc_header
                if print_foc_header:
                    print_foc_header = False
            if pos.transaction_type_product == 'internal':
                t_type = 'USED'
                print_header = print_used_header
                if print_used_header:
                    print_used_header = False
                if pos.stock_journal_items_id:
                    ref = pos.stock_journal_items_id.name
            
            if pos.journal_items_id:
                ref = pos.journal_items_id.name
            if pos.invoice_id:
                ref = pos.invoice_id.name
            
            if not ref:
                ref = 'N.A.'
            rec = {
                'print_header' : print_header,
                'type' : t_type,
                'product_type' : self._get_product_type(pos.product_type_sel),
                'currency' : currency,
                'date' : pos.date,
                'transaction' : pos.sequence_id,
                'ref' : ref or '',
                'amount' : amount,
                'curr_amount' : curr_amount, 
                'is_total' : False,
                'store' : pos.shop_id.name,
            }
            
            result.update({rec_idx:rec})
            rec_idx += 1
        return result
    
    def _generate_report(self, cr, uid, norm_rec):
        list_keys = norm_rec.keys()
        list_keys.sort()
        base_curr = self._get_company_currency_id(cr, uid)
        
        list_p_foc = []
        list_p_sold = []
        list_p_used = []
        list_p_foc_curr = []
        list_p_sold_curr = []
        list_p_used_curr = []
        list_s_foc = []
        list_s_sold = []
        list_s_used = []
        list_s_foc_curr = []
        list_s_sold_curr = []
        list_s_used_curr = []
        
        total_sold = total_foc = total_used = 0
        total_sold_curr = total_foc_curr = total_used_curr = 0
        total_p_sold = total_p_foc = total_p_used = 0
        total_p_sold_curr = total_p_foc_curr = total_p_used_curr = 0
        total_s_sold = total_s_foc = total_s_used = 0
        total_s_sold_curr = total_s_foc_curr = total_s_used_curr = 0
        for key in list_keys:
            rec = norm_rec.get(key)
            if rec['type'] == 'SOLD' and rec['product_type'] == 'Product':
                if rec['currency'] == base_curr.name:
                    list_p_sold.append(rec)
                    total_p_sold += rec['amount']
                else:
                    list_p_sold_curr.append(rec)
                    total_p_sold_curr += rec['curr_amount']

            if rec['type'] == 'SOLD' and rec['product_type'] == 'Service':
                if rec['currency'] == base_curr.name:
                    list_s_sold.append(rec)
                    total_s_sold += rec['amount']
                else:
                    list_s_sold_curr.append(rec)
                    total_s_sold_curr += rec['curr_amount']
                    
            if rec['type'] == 'FOC' and rec['product_type'] == 'Product':
                if rec['currency'] == base_curr.name:
                    list_p_foc.append(rec)
                    total_p_foc += rec['amount']
                else:
                    list_p_foc_curr.append(rec)
                    total_p_foc_curr += rec['curr_amount']

            if rec['type'] == 'FOC' and rec['product_type'] == 'Service':
                if rec['currency'] == base_curr.name:
                    list_s_foc.append(rec)
                    total_s_foc += rec['amount']
                else:
                    list_s_foc_curr.append(rec)
                    total_s_foc_curr += rec['curr_amount']

            if rec['type'] == 'USED' and rec['product_type'] == 'Product':
                if rec['currency'] == base_curr.name:
                    list_p_used.append(rec)
                    total_p_used += rec['amount']
                else:
                    list_p_used_curr.append(rec)
                    total_p_used_curr += rec['curr_amount']

            if rec['type'] == 'USED' and rec['product_type'] == 'Service':
                if rec['currency'] == base_curr.name:
                    list_s_used.append(rec)
                    total_s_used += rec['amount']
                else:
                    list_s_used_curr.append(rec)
                    total_s_used_curr += rec['curr_amount']

            # Update rec to set amount in locale format
            amount_locale = locale.format("%20.0f",rec['amount'], grouping=True)
            curr_amount_locale = locale.format("%20.0f",rec['curr_amount'], grouping=True)
            rec.update({
                'amount' : amount_locale,
                'curr_amount' : curr_amount_locale,
                })
        
        
        ################### SOLD ###################
        # Products 

        if list_p_sold:
            total_p_sold_local = locale.format("%20.0f",total_p_sold, grouping=True)
            rec = {
                'is_total' : True,
                'amount' : total_p_sold_local,
                'curr_amount' : 0,
            }
            list_p_sold.append(rec)
        
        if list_p_sold_curr:
            list_p_sold_curr_local = locale.format("%20.0f",list_p_sold_curr, grouping=True)
            rec = {
                'is_total' : True,
                'amount' : 0,
                'curr_amount' : list_p_sold_curr_local,
            }
            list_p_sold_curr.append(rec)

        # Services
        if list_s_sold:
            total_s_sold_local = locale.format("%20.0f",total_s_sold, grouping=True)
            rec = {
                'is_total' : True,
                'amount' : total_s_sold_local,
                'curr_amount' : 0,
            }
            list_s_sold.append(rec)
        
        if list_s_sold_curr:
            total_s_sold_curr_local = locale.format("%20.0f",total_s_sold_curr, grouping=True)
            rec = {
                'is_total' : True,
                'amount' : 0, 
                'curr_amount' : total_s_sold_curr_local,
            }
            list_s_sold_curr.append(rec)
        
        ################### FOC ###################
        # Products
        if list_p_foc:
            total_p_foc_local = locale.format("%20.0f",total_p_foc, grouping=True)
            rec = {
                'is_total' : True,
                'amount' : total_p_foc_local,
                'curr_amount' : 0,
            }
            list_p_foc.append(rec)
        
        if list_p_foc_curr:
            total_p_foc_curr_local = locale.format("%20.0f",total_p_foc_curr, grouping=True)
            rec = {
                'is_total' : True,
                'amount' : 0,
                'curr_amount' : total_p_foc_curr_local,
            }
            list_p_foc_curr.append(rec)
        
        # Services
        if list_s_foc:
            total_s_foc_local = locale.format("%20.0f",total_s_foc, grouping=True)
            rec = {
                'is_total' : True,
                'amount' : total_s_foc_local,
                'curr_amount' : 0,
            }
            list_s_foc.append(rec)
        
        if list_s_foc_curr:
            total_s_foc_curr_local = locale.format("%20.0f",total_s_foc_curr, grouping=True)
            rec = {
                'is_total' : True,
                'amount' : 0,
                'curr_amount' : total_s_foc_curr_local,
            }
            list_s_foc_curr.append(rec)

        ################### USED ###################
        # Products 
        if list_p_used:
            total_p_used_local = locale.format("%20.0f",total_p_used, grouping=True)
            rec = {
                'is_total' : True,
                'amount' : total_p_used_local,
                'curr_amount' : 0,
            }
            list_p_used.append(rec)
        
        if list_p_used_curr:
            total_p_used_curr_local = locale.format("%20.0f",total_p_used_curr, grouping=True)
            rec = {
                'is_total' : True,
                'amount' : 0,
                'curr_amount' : total_p_used_curr_local,
            }
            list_p_used_curr.append(rec)

        # Services
        if list_s_used:
            total_s_used_local = locale.format("%20.0f",total_s_used, grouping=True)
            rec = {
                'is_total' : True,
                'amount' : total_s_used_local,
                'curr_amount' : 0,
            }
            list_s_used.append(rec)
        
        if list_s_used_curr:
            total_s_used_curr_local = locale.format("%20.0f",total_s_used_curr, grouping=True)
            rec = {
                'is_total' : True,
                'amount' : 0,
                'curr_amount' : total_s_used_curr_local,
            }
            list_s_used_curr.append(rec)

        for rec in list_p_sold:
            self.result_acc.append(rec)
        for rec in list_p_sold_curr:
            self.result_acc.append(rec)
        for rec in list_s_sold:
            self.result_acc.append(rec)
        for rec in list_s_sold_curr:
            self.result_acc.append(rec)
        for rec in list_p_foc:
            self.result_acc.append(rec)
        for rec in list_p_foc_curr:
            self.result_acc.append(rec)
        for rec in list_s_foc:
            self.result_acc.append(rec)
        for rec in list_s_foc_curr:
            self.result_acc.append(rec)
        for rec in list_p_used:
            self.result_acc.append(rec)
        for rec in list_p_used_curr:
            self.result_acc.append(rec)
        for rec in list_s_used:
            self.result_acc.append(rec)
        for rec in list_s_used_curr:
            self.result_acc.append(rec)
            
        return self.result_acc   
    
    def _get_all_stores(self, cr, uid, context=None):
        result = []
        
        # Shops
        shop_o = self.pool.get('isf.shop')
        shop_ids = shop_o.search(cr, uid, [])
        for shop in shop_o.browse(cr, uid, shop_ids):
            result.append('S'+str(shop.id))        

        # Pharmacy
        pharm_o = self.pool.get('isf.pharmacy.pos')
        if pharm_o is not None:
            result.append('L00')
        return result

    def lines(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.stock.transaction.report')
        
        date_s_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['date_start'])
        date_start = date_s_obj[0]['date_start']
        date_e_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['date_end'])
        date_end = date_e_obj[0]['date_end']
        store_o = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['store_id'])
        store = store_o[0]['store_id']
        
        store_type = store[0]
        store_id = store[1:].strip()

        norm_rec = {}
        if store_type == 'A':
            stores_ids = self._get_all_stores(self.cr, self.uid)
            for store in stores_ids:
                store_type = store[0]
                store_id = store[1:].strip()
                if store_type == 'S':
                    res = self._manage_generic_pos(self.cr, self.uid, int(store_id), date_start, date_end,context=ctx)
                elif store_type == 'L':
                    res = self._manage_pharmacy_pos(self.cr, self.uid, date_start, date_end,context=ctx)

                for r,v in res.iteritems():
                    norm_rec.update({r:v})

        elif store_type == 'S':
            norm_rec = self._manage_generic_pos(self.cr, self.uid, int(store_id), date_start, date_end,context=ctx)
        elif store_type == 'L':
            norm_rec = self._manage_pharmacy_pos(self.cr, self.uid, date_start, date_end,context=ctx)
        
        return self._generate_report(self.cr, self.uid, norm_rec)
        

report_sxw.report_sxw('report.isf.stock.transaction.webkit', 'isf.stock.transaction.report', 'addons/isf_cims_module/report/transaction.mako', parser=isf_stock_transaction_report_parser)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
