# -*- coding: utf-8 -*-
import time
from pprint import pprint as pp

from openerp.report import report_sxw
import logging
import locale
import datetime


_logger = logging.getLogger(__name__)
_debug=False

class isf_pharmacy_credit_report_parser(report_sxw.rml_parse):
    _name = 'report.isf.pharmacy.credit.webkit'

    def __init__(self, cr, uid, name, context=None):
        super(isf_pharmacy_credit_report_parser, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
            'pp': pp,
            'lines': self.lines,
            'headers' : self.headers,
        })
        self.context = context
        self.result_acc = []
        self.headers_acc = []
    
    
    def headers(self, ids=None, done=None):
        ctx = self.context.copy()
        isf_lib = self.pool.get('isf.lib.utils')
        obj_report = self.pool.get('isf.pharmacy.credit.wizard')
        cust_data = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['customer_id'])
        date_start_date = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['date_start'])
        date_stop_data = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['date_stop'])
        date_start = date_start_date[0]['date_start']
        date_stop = date_stop_data[0]['date_stop']
        customer = cust_data[0]['customer_id']
        currency = isf_lib.get_company_currency(self.cr, self.uid)
        
        res = {
            'date_start' : date_start,
            'date_stop' : date_stop,
            'customer' : customer[1],
            'currency' : currency.name,
        }
        self.headers_acc.append(res)
        
        
        return self.headers_acc
        
    def lines(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.pharmacy.credit.wizard')
        cust_data = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['customer_id'])
        date_start_date = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['date_start'])
        date_stop_data = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['date_stop'])
        date_start = date_start_date[0]['date_start']
        date_stop = date_stop_data[0]['date_stop']
        customer_id = cust_data[0]['customer_id'][0]
        
        pharm_o = self.pool.get('isf.pharmacy.pos')
        pharm_ids = pharm_o.search(self.cr, self.uid, [('date','>=',date_start),('date','<=',date_stop),('type','=','credit'),('state','=','credit'),('patient_id','=',customer_id)])
        total_amount = 0
        total_discount = 0
        for pharm in pharm_o.browse(self.cr, self.uid, pharm_ids):
            total_amount += pharm.total_amount
            total_discount += pharm.total_discount
            res = {
                'is_total' : '0',
                'date' : pharm.date,
                'desc' : pharm.description or '',
                'tr_id' : pharm.sequence_id or 'N.A.',
                'invoice' : pharm.invoice_id.number,
                'total_amount' : locale.format("%20d",pharm.total_amount, grouping=True),
                'total_discount' : locale.format("%20d",pharm.total_discount, grouping=True),
            }
            self.result_acc.append(res)
        
        res = {
            'is_total' : '1',
            'total_amount' : locale.format("%20d",total_amount, grouping=True),
            'total_discount' : locale.format("%20d",total_discount, grouping=True),
        }
        self.result_acc.append(res)
        return self.result_acc
    

report_sxw.report_sxw('report.isf.pharmacy.credit.webkit', 'isf.pharmacy.credit.wizard', 'addons/isf_cims_module/report/pharm_credit.mako', parser=isf_pharmacy_credit_report_parser)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
