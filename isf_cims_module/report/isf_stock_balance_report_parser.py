# -*- coding: utf-8 -*-
import time
from pprint import pprint as pp

from openerp.report import report_sxw
import logging
import locale
import datetime


_logger = logging.getLogger(__name__)
_debug = False

class isf_stock_balance_report_parser(report_sxw.rml_parse):
    _name = 'report.isf.stock.balance.webkit'

    def __init__(self, cr, uid, name, context=None):
        if _debug:
            _logger.debug('==> INIT <==')
    
        super(isf_stock_balance_report_parser, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
            'pp': pp,
            'lines': self.lines,
            'headers' : self.headers,
            'tableheaders' : self.tableheaders,
        })
        self.context = context
        self.result_acc = []
        self.headers_acc = []
        self.table_headers_acc = []
    
    
    
    def headers(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.stock.balance.report')
        date_s_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['date'])
        location_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['location_id'])
        date = date_s_obj[0]['date']
        
        location = 'None'
        if location_obj:
            if location_obj[0]['location_id']:
                location = location_obj[0]['location_id'][1]
        
        
        res = {
            'date' : date,
            'location' : location,
        }
        self.headers_acc.append(res)
        
        if _debug:
            _logger.debug('Table header : %s',res)
        
        
        return self.headers_acc
    
     
    def _get_qty_at_date(self, cr, uid, product_id, location_id, date):
        move_obj = self.pool.get('stock.move')
        loc_obj = self.pool.get('stock.location')
        
        date = date + ' 23:59:59'
        move_in_ids = move_obj.search(cr, uid, [('product_id','=',product_id),('location_dest_id','=',location_id),('state','=','done'),('date_expected','<=',date)])
        move_out_ids = move_obj.search(cr, uid, [('product_id','=',product_id),('location_id','=',location_id),('state','=','done'),('date_expected','<=',date)])
    
        in_qty = 0
        for move in move_obj.browse(cr, uid, move_in_ids):
            factor = move.product_uom.factor
            in_qty += move.product_qty / factor
        if _debug:
            if product_id == 740:
                _logger.debug('IN QTY : %f', in_qty)
            
            
        out_qty = 0
        for move in move_obj.browse(cr, uid, move_out_ids):
            factor = move.product_uom.factor
            out_qty += move.product_qty / factor
        if _debug:
            if product_id == 740:
                _logger.debug('OUT QTY : %f', out_qty)
        
        return in_qty - out_qty
        
    def _get_qty_for_stock_at_date(self, cr, uid, stock_id, location_id, date):
        move_obj = self.pool.get('stock.move')
        
        date = date + ' 23:59:59'
        move_in_ids = move_obj.search(cr, uid, [('prodlot_id','=',stock_id),('location_dest_id','=',location_id),('state','=','done'),('date_expected','<=',date)])
        move_out_ids = move_obj.search(cr, uid, [('prodlot_id','=',stock_id),('location_id','=',location_id),('state','=','done'),('date_expected','<=',date)])
    
        in_qty = 0
        for move in move_obj.browse(cr, uid, move_in_ids):
            factor = move.product_uom.factor
            in_qty += move.product_qty / factor
            
        out_qty = 0
        for move in move_obj.browse(cr, uid, move_out_ids):
            factor = move.product_uom.factor
            out_qty += move.product_qty / factor
        
        return in_qty - out_qty
        
    def _lines_stock_balance(self, cr, uid, date):
        ctx = self.context.copy()
        stock_obj = self.pool.get('stock.production.lot')
        isf_lib = self.pool.get('isf.lib.utils')
        product_obj = self.pool.get('product.product')
        product_ids = product_obj.search(cr, uid, [('active','=',True),('type','=','product')])
        
        obj_report = self.pool.get('isf.stock.balance.report')
        report_data = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['location_id'])
        location_id = report_data[0]['location_id'][0]
        
        result = {}
        stock_ids = stock_obj.search(cr, uid, [])
        # First : looking for drugs with lot registered
        now = datetime.datetime.now()
        for stock in stock_obj.browse(self.cr, self.uid, stock_ids):
            if _debug:
                _logger.debug('stock : %s',stock.life_date)
            date_expiry = stock.ref
            if stock.life_date:
                date_expiry = datetime.datetime.strptime(str(stock.life_date)[:10], '%Y-%m-%d')
                
            qty = int(self._get_qty_for_stock_at_date(cr, uid, stock.id, location_id, date))
            if qty != 0:
                expired = '0'
                if date_expiry:
                    if date_expiry < now:
                        expired = '1'
                p_name = stock.product_id.name_template
                p_code = stock.product_id.default_code
                rec = result.get(p_name)
                if rec is None:
                    rec = []
                    rec.append({'code': stock.name, 'p_code' : p_code, 'name': p_name, 'qty':qty, 'expiry_date':str(stock.life_date)[:10],'expired' : expired})
                    result.update({p_name:rec})
                else:
                    rec.append({'code': stock.name, 'p_code' : p_code, 'name': p_name, 'qty':qty, 'expiry_date':str(stock.life_date)[:10],'expired' : expired})
                    result.update({p_name:rec})
                     
        list_keys = result.keys()
        list_keys.sort()  
        for date_key in list_keys:
            rec_list = result.get(date_key)
            for rec in rec_list:
                if rec['qty'] > 0:  
                    self.result_acc.append(rec)
            
        return self.result_acc
        
    def tableheaders(self):

        location_obj = self.pool.get('stock.location')
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.stock.report')
        location_obj = self.pool.get('stock.location')
        location_ids = location_obj.search(self.cr, self.uid, [('available_in_selection','=',True),('usage','=','internal')])
        
        
        for location in location_obj.browse(self.cr, self.uid, location_ids):
            res = {
                'location' : location.name,
            }
        
            self.table_headers_acc.append(res)
        
        
        return self.table_headers_acc
        
    
    def lines(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.stock.balance.report')
        
        report_type_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['report_type'])
        date_s_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['date'])
        date = date_s_obj[0]['date']
        
        return self._lines_stock_balance(self.cr, self.uid, date)
    

report_sxw.report_sxw('report.isf.stock.balance.webkit', 'isf.stock.balance.report', 'addons/isf_cims_module/report/stock_balance.mako', parser=isf_stock_balance_report_parser)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
