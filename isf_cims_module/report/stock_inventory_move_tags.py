import time
from openerp.report import report_sxw
import logging

_logger = logging.getLogger(__name__)
_debug = False

class stock_inventory_move_tags(report_sxw.rml_parse):
    _name = 'stock.inventory.move.tags'
    
    _sorted_tag = []
    
    def __init__(self, cr, uid, name, context):
        super(stock_inventory_move_tags, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
             'time': time,
             'total_price':self._price_total,
             'qty_total':self._qty_total,
             'lines':self._lines,
        })
    
    def _unique(self, seq):
        seen = set()
        seen_add = seen.add
        return [x for x in seq if not (x in seen or seen_add(x))]
    
    def _getKey(self, tag):
        return tag.name
        
    def _price_total(self, objects):
        total = 0.0
        for obj in objects:
            total += obj.price
        return {'price':total}
        
    def _qty_total(self, objects):
        total = 0.0
        for obj in objects:
            factor = obj.product_uom.factor
            total += obj.product_qty / factor
        return {'quantity':total}
    
    def _lines(self, objects, tags=None):
        if _debug:
            _logger.debug('==> called tags : %s', tags)
        if not tags:
            product_obj = self.pool.get('product.product')
            product_ids = product_obj.search(self.cr, self.uid, [])
            products = product_obj.browse(self.cr, self.uid, product_ids)
            tags = [d['tags'] for d in products]
            #tags = self._unique(tags)
            tags = sorted(set(tags))
            if _debug:
                _logger.debug('==> used tags : %s', tags)
        
        lines = []
        line = {
                'type' : '',
                'title' : '',
                'value' : '',
        }
        
        for tag in tags:
            obj_list = []
            for obj in objects:
                if obj.product_id.tags == tag:
                    obj_list.append(obj)
            
            if len(obj_list) > 0:
                line = {'type' : 'total', 'title' : tag, 'value' : self._price_total(obj_list)}
                lines.append(line)
            
#         if _debug:
#             _logger.debug('==> lines : %s', lines)
        return lines
        
        
    
report_sxw.report_sxw(
    'report.isf.stock.inventory.move.tags',
    'stock.inventory',
    'addons/isf_cims_module/report/stock_inventory_move_tags.rml',
    parser=stock_inventory_move_tags,
    header='internal'
)
