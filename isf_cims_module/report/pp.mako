<html>
<head>
    <style type="text/css">
        ${css}
table {
    border-spacing: 0px;
    page-break-inside: auto
}

table tr {
        page-break-inside: avoid; 
        page-break-after: auto;
}
        
@media print {
   thead {display: table-header-group;}
}
        
td {
    font-size: normal;
}

table { 
    border-collapse: collapse; 
}


    </style>
</head>
<body>
    <%page expression_filter="entity"/>
     <h1><b><i>Transfer Items</i></b></h1>
    <%
    """
    print "IN REPORT!"
    print objects, dir(objects)
    print "*************** LOCALS:"
    pp(locals())
    print headers()
    print lines()
    """
    %>
    
    
   
        
        <table style="width: 100%" border="0">
            %for head in headers():
                <tr>
                    <td><b>ID : </b></td><td>${head['sequence_id']}</td>
                </tr>
                <tr>
                    <td><b>Description : </b></td><td>${head['desc']}</td>
                </tr>
                <tr>
                    <td><b>Document Number : </b></td><td>${head['ref']}</td>
                </tr>
                <tr>
                    <td><b>Date : </b></td><td>${head['date']}</td>
                </tr>
                <tr>
                    <td><b>Source : </b></td><td>${head['source']}</td>
                </tr>
                <tr>
                    <td><b>Destination : </b></td><td>${head['dest']}</td>
                </tr>
                
            %endfor
        </table>
        <br/>
        <table style="width: 100%" border="0">
            <tr class="table_header" style="border-bottom: 0.5px solid black">
                <td style="width: 10%">Lot</td>
                <td style="width: 10%">Expiry Date</td>
                <td style="width: 50%">Product</td>
                <td style="width: 10%">UoM</td>
                <td style="width: 10%" align="right">Required</td>
                <td style="width: 10%" align="right">Issued</td>
            </tr>
            %for line in lines():
                %if line['lot_id'] != 'total':
                    <tr class="row" style="background-color:#F2F2F2">
                        <td>${line['lot_id']}</td>
                        <td>${line['expiry_date']}</td>
                        <td>${line['product']}</td>
                        <td>${line['uom']}</td>
                        <td align="right">${line['required']}</td>
                        <td align="right">${line['issued']}</td>
                    </tr>
                %endif
            %endfor
        </table>
        

</body>
</html>
