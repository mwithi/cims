# -*- coding: utf-8 -*-
import time
from pprint import pprint as pp

from openerp.report import report_sxw
import logging
import locale
import datetime


_logger = logging.getLogger(__name__)
_debug=False

class isf_store_wise_report_parser(report_sxw.rml_parse):
    _name = 'report.isf.store.wise.webkit'

    def __init__(self, cr, uid, name, context=None):
        if _debug:
            _logger.debug('==> INIT <==')
    
        super(isf_store_wise_report_parser, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
            'pp': pp,
            'lines': self.lines,
            'headers' : self.headers,
            'tableheaders' : self.tableheaders,
        })
        self.context = context
        self.result_acc = []
        self.headers_acc = []
        self.table_headers_acc = []
    
    
    
    def headers(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.store.wise.report')
        date_s_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['date'])
        date = date_s_obj[0]['date']
        
        res = {
            'date' : date,
        }
        self.headers_acc.append(res)
        
        if _debug:
            _logger.debug('Table header : %s',res)
        
        
        return self.headers_acc
    
     
    def _get_qty_at_date(self, cr, uid, product_id, location_id, date):
        move_obj = self.pool.get('stock.move')
        loc_obj = self.pool.get('stock.location')
        
        date = date + ' 23:59:59'
        move_in_ids = move_obj.search(cr, uid, [('product_id','=',product_id),('location_dest_id','=',location_id),('state','=','done'),('date_expected','<=',date)])
        move_out_ids = move_obj.search(cr, uid, [('product_id','=',product_id),('location_id','=',location_id),('state','=','done'),('date_expected','<=',date)])
    
        in_qty = 0
        for move in move_obj.browse(cr, uid, move_in_ids):
            factor = move.product_uom.factor
            in_qty += move.product_qty / factor
            
        out_qty = 0
        for move in move_obj.browse(cr, uid, move_out_ids):
            factor = move.product_uom.factor
            out_qty += move.product_qty / factor
        
        return in_qty - out_qty
        
    def _get_qty_for_stock_at_date(self, cr, uid, stock_id, location_id, date):
        move_obj = self.pool.get('stock.move')
        
        date = date + ' 23:59:59'
        move_in_ids = move_obj.search(cr, uid, [('prodlot_id','=',stock_id),('location_dest_id','=',location_id),('state','=','done'),('date_expected','<=',date)])
        move_out_ids = move_obj.search(cr, uid, [('prodlot_id','=',stock_id),('location_id','=',location_id),('state','=','done'),('date_expected','<=',date)])
    
        in_qty = 0
        for move in move_obj.browse(cr, uid, move_in_ids):
            factor = move.product_uom.factor
            in_qty += move.product_qty / factor
            
        out_qty = 0
        for move in move_obj.browse(cr, uid, move_out_ids):
            factor = move.product_uom.factor
            out_qty += move.product_qty / factor
        
        return in_qty - out_qty
        
    def _lines_store_wise(self, cr, uid, location_ids, date):
        product_obj = self.pool.get('product.product')
        product_ids = product_obj.search(cr, uid, [('active','=',True),('type','=','product')])
        isf_lib = self.pool.get('isf.lib.utils')
        lot_obj = self.pool.get('stock.production.lot')
        
        result = {}
        
        for product in product_obj.browse(cr, uid, product_ids):
            first_account = '1'
            total = 0
            min_qty = product.min_qty
            
            tmp_list = []
            for location_id in location_ids:
                qty = int(self._get_qty_at_date(cr, uid, product.id, location_id, date))
                    
                total += qty
                warning_level = qty < product.min_qty
                out_of_stock = qty == 0
                res = {
                    'code' : product.default_code,
                    'product' : product.name_template,
                    'first_account' : first_account,
                    'is_total' : '0',
                    'qty' : qty,
                    'warning_level' : warning_level,
                    'out_of_stock' : out_of_stock,
                    }
                tmp_list.append(res)
                first_account = '0'
            
            warning_level = total < min_qty
            out_of_stock = total == 0
            # Totals per line
            res = {
                'product' : product.name_template,
                'first_account' : '0',
                'is_total' : '1',
                'qty' : total,
                'warning_level' : warning_level,
                'out_of_stock' : out_of_stock,
                }
            
            oos = {
                'product' : product.name_template,
                'first_account' : '0',
                'is_total' : '2',
                'qty' : total,
                'warning_level' : warning_level,
                'out_of_stock' : out_of_stock,
                }
                
            if total != 0 or min_qty > 0:
                for r in tmp_list:
                    self.result_acc.append(r)
                self.result_acc.append(res)
                self.result_acc.append(oos)
            
        return self.result_acc
        
    def tableheaders(self):

        location_obj = self.pool.get('stock.location')
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.store.wise.report')
        location_obj = self.pool.get('stock.location')
        location_ids = location_obj.search(self.cr, self.uid, [('available_in_selection','=',True),('usage','=','internal')])
        
        
        for location in location_obj.browse(self.cr, self.uid, location_ids):
            res = {
                'location' : location.name,
            }
        
            self.table_headers_acc.append(res)
        
        return self.table_headers_acc
        
    
    def lines(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.store.wise.report')
        
        report_type_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['report_type'])
        date_s_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['date'])
        date = date_s_obj[0]['date']
        loc_obj = self.pool.get('stock.location')
        loc_ids = loc_obj.search(self.cr, self.uid, [('available_in_selection','=',True),('usage','=','internal')])
        
        return self._lines_store_wise(self.cr, self.uid, loc_ids, date)
    

report_sxw.report_sxw('report.isf.store.wise.webkit', 'isf.store.wise.report', 'addons/isf_cims_module/report/store_wise.mako', parser=isf_store_wise_report_parser)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
