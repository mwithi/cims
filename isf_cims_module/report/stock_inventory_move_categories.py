import time
from openerp.report import report_sxw
import logging

_logger = logging.getLogger(__name__)
_debug = False

class stock_inventory_move_categories(report_sxw.rml_parse):
    _name = 'stock.inventory.move.categories'
    
    _sorted_category = []
    
    def __init__(self, cr, uid, name, context):
        super(stock_inventory_move_categories, self).__init__(cr, uid, name, context=context)
        
        cat_obj = self.pool.get('product.category')
        cat_ids = cat_obj.search(cr, uid, [])
        categories = cat_obj.browse(cr, uid, cat_ids)
        self._sorted_category = sorted(categories, key=self._getKey)
        if _debug:
            _logger.debug('cat_ids : %s', cat_ids)
            _logger.debug('categories : %s', categories)
            _logger.debug('_sorted_category : %s', self._sorted_category)
        
        self.localcontext.update({
             'time': time,
             'total_price':self._price_total,
             'qty_total':self._qty_total,
             'lines':self._lines,
        })
        
        
    def _price_total(self, objects):
        total = 0.0
        for obj in objects:
            total += obj.price
        return {'price':total}
        
    def _qty_total(self, objects):
        total = 0.0
        for obj in objects:
            factor = obj.product_uom.factor
            total += obj.product_qty / factor
        return {'quantity':total}
    
    def _getKey(self, category):
        return category.parent_left
    
    def _lines(self, objects, categories=None):
        cat_obj = self.pool.get('product.category')
        if _debug:
            _logger.debug('==> called categories : %s', categories)
        if not categories:
            categories = cat_obj.browse(self.cr, self.uid, [1])
            if _debug:
                _logger.debug('==> used categories : %s', categories)
        
        lines = []
        line = {
                'type' : '',
                'title' : '',
                'value' : '',
        }
        
        for category in categories:
            childs = cat_obj.search(self.cr, self.uid, [('parent_id','=',category.id)])
            all_childs = cat_obj.search(self.cr, self.uid, [('parent_left','>',category.parent_left),('parent_left','<',category.parent_right)])

            if _debug:
                _logger.debug('==> category %s childs (%s): %s', category.id, len(childs), childs)
            child_categories = cat_obj.browse(self.cr, self.uid, childs)
            all_child_categories = cat_obj.browse(self.cr, self.uid, all_childs)
            if _debug:
                _logger.debug('==> category %s sub childs (%s): %s', category.id, len(all_child_categories), all_child_categories)
            all_child_categories.extend(child_categories)
            if _debug:
                _logger.debug('==> category %s childs and sub childs (%s): %s', category.id, len(all_child_categories), all_child_categories)
            
            obj_list = []
            for obj in objects:
                if obj.product_id.categ_id in all_child_categories or obj.product_id.categ_id == category:
                    obj_list.append(obj)
            
            if len(obj_list) > 0:
                if _debug:
                    _logger.debug('==> found %s products in this category, its childs and sub childs', len(obj_list))
                line = {'type' : 'total', 'title' : category.complete_name, 'value' : self._price_total(obj_list)}
                lines.append(line)
            
            if len(childs) > 0:
                lines2 = self._lines(obj_list, child_categories)
                lines.extend(lines2)
                
#             else:
#                 lines2 = []
#                 obj_list = []
#                 for obj in objects:
#                     if obj.product_id.categ_id == category:
#                         obj_list.append(obj)
#                         line = {'type' : 'product', 'title': obj.product_id.name, 'value' : obj}
#                         lines2.append(line)
#                 
#                 if len(obj_list) > 0:
#                     _logger.debug('==> category %s no childs, processed %s products', category.id, len(obj_list))
# #                     line = {'type' : 'category', 'value' : category.complete_name}
# #                     lines.append(line)
# #                     lines.extend(lines2)
# #                     line = {'type' : 'total', 'value' : self._price_total(obj_list)}
# #                     lines.append(line)
#                     line = {'type' : 'total', 'title' : category.complete_name, 'value' : self._price_total(obj_list)}
#                     lines.append(line)
                    
        if _debug:
            _logger.debug('==> lines : %s', lines)
        return lines
        
        
    
report_sxw.report_sxw(
    'report.isf.stock.inventory.move.categories',
    'stock.inventory',
    'addons/isf_cims_module/report/stock_inventory_move_categories.rml',
    parser=stock_inventory_move_categories,
    header='internal'
)
