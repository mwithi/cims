# -*- coding: utf-8 -*-
import time
from pprint import pprint as pp

from openerp.report import report_sxw
import logging
import locale



_logger = logging.getLogger(__name__)
_debug=False

class isf_product_service_report_parser(report_sxw.rml_parse):
    _name = 'report.isf.product.service.webkit'

    def __init__(self, cr, uid, name, context=None):
        if _debug:
            _logger.debug('==> INIT <==')
    
        super(isf_product_service_report_parser, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
            'pp': pp,
            'lines': self.lines,
        })
        self.context = context
        self.result_acc = []

    
    def _get_product_report_object(self, ids):
        pr_obj = self.pool.get('isf.product.report')
        
        pr = bs_obj.browse(self.cr, self.uid, ids)
        
        return pr

    def _get_pharmacy_product_category_id(self, cr, uid):
        ir_values = self.pool.get('ir.values')
        return ir_values.get_default(cr, uid, 'isf.pharmacy.pos', 'product_category_id')
        
    def _get_product_categories(self, cr, uid, shop_id,product_type):
        res = []
        if shop_id in ('PHARMACY','all'):
            res.append(self._get_pharmacy_product_category_id(cr, uid))
        if shop_id != 'PHARMACY':
            shop_obj = self.pool.get('isf.shop')
            if shop_id == 'all':
                shop_ids = shop_obj.search(cr, uid, [])
            else:
                shop_ids = shop_obj.search(cr, uid, [('name','=',shop_id)])
                if _debug:
                    _logger.debug('SHOPS : %s',shop_ids)
            for shop in shop_obj.browse(cr, uid, shop_ids):
                if product_type in ('product','all') and shop.product_type in ('product','both'):
                    res.append(shop.product_category_id.id)
                if product_type in ('service','all') and shop.product_type in ('service','both'):
                    res.append(shop.service_category_id.id)

        return res

    def _get_store_from_category(self, cr, uid, categ_id):
        res = []
        pos_obj = self.pool.get('isf.shop')
        pos_ids = pos_obj.search(cr, uid, ['|',('product_category_id','=',categ_id),('service_category_id','=',categ_id)])
        if len(pos_ids) > 0:
            return pos_obj.browse(cr, uid, pos_ids)[0].name
        return 'PHARMACY'

    def _get_product_type(self, product_type):
        if product_type == 'service':
            return 'Service'
        return 'Product'

    def lines(self, ids=None, done=None):
        ctx = self.context.copy()
        isf_lib = self.pool.get('isf.lib.utils')
        obj_report = self.pool.get('isf.product.service.report')
        store_o = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['shop_id'])
        store = store_o[0]['shop_id']
        type_o = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['type'])
        product_type = type_o[0]['type']
        currency = isf_lib.get_company_currency(self.cr, self.uid)

        product_obj = self.pool.get('product.product')
        category_obj = self.pool.get('product.category')

        if _debug:
            _logger.debug('SHOP : %s TYPE : %s',store,product_type)

        category_ids = self._get_product_categories(self.cr, self.uid, store, product_type)

        if _debug:
            _logger.debug('Categories : %s',category_ids)
        
        product_ids = product_obj.search(self.cr, self.uid, [('categ_id','child_of',category_ids)],order='categ_id,type,name asc')

        print_header = '0'
        categ_id = 0
        p_type = -1
        for product in product_obj.browse(self.cr, self.uid, product_ids):
            if categ_id != product.categ_id.id or p_type != product.type:
                print_header = '1'

            product_type = self._get_product_type(product.type)
            res = {
                'code' : product.default_code,
                'name' : product.name,
                'sell_price' : locale.format("%10d",product.list_price, grouping=True),
                'type' : product.type,
                'store' : self._get_store_from_category(self.cr, self.uid, product.categ_id.id),
                'print_header' : print_header,
                'type' : product_type,
                'category' : product.categ_id.name,
                'currency' : currency.name,
            }

            categ_id = product.categ_id.id
            p_type = product.type
            print_header = '0'
            
            self.result_acc.append(res)
            
        return self.result_acc

report_sxw.report_sxw('report.isf.product.service.webkit', 'isf.product.service.report', 'addons/isf_cims_module/report/product_service_report.mako', parser=isf_product_service_report_parser)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
