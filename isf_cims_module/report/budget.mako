<html>
<head>
    <style type="text/css">
        ${css}
    </style>
</head>
<body>
    <%page expression_filter="entity"/>
    <h2><b><i><center>Budget Comparison Income/Expense Statement<br>Divided by Analytic Account<center></i></b></h2>
    <%
    """
    print "IN REPORT!"
    print objects, dir(objects)
    print "*************** LOCALS:"
    pp(locals())
    print date_lines()
    print lines()
    """
    %>

    <table style="width: 100%" border="0">
        <tr class="table_header">
            <td colspan="5"><center>
                %for date in datelines():
                    Begin Period : ${date['begin_date']} End Period : ${date['end_date']}
                %endfor
                </center>
            </td>
        </tr>
    </table>

    <table style="width: 100%" border="0">
        <tr class="table_header">
            <td> Analytic Account </td>
            <td> Planned Amount </td>
            <td> Effective Amount </td>
            <td> Remaining Balance</td>
            <td> RB </td> 
        </tr>
        %for account in lines():
            %if account['analytic'] == '1':
            <tr class="row">
                <td colspan="5">
                    <table class="tr_bottom_line"></table>
                </td>
            </tr>
            <tr>
                <td class="label" colpan="5"><i>${account['analytic_account']}</i></td>
            </tr>
            <tr class="row">
                <td colspan="5">
                    <table class="tr_bottom_line"></table>
                </td>
            </tr>
            %endif
            
            %if account['is_total'] == '0' and account['analytic'] == '0':
                %if account['first_for_account'] == '1' and account['is_income'] == '1':
                    <tr class="row" style="border-top: 0.5px solid blue;background-color:#F2F2F2">
                    <td class="label"><b>Income ${account['analytic_account']}</b></td>
                    <td class="currency">${account['planned_amount']}</td>
                    <td class="currency">${account['effective_amount']}</td>
                    %if account['isColored'] == '0':
                        <td class="currency">${account['balance']} </td>
                        <td class="currency">${account['percentage_on_totals']} &#37;</td>
                    %endif
                    %if account['isColored'] == '1':
                        <td class="currency" style="color:#00F500">${account['balance']} </td>
                        <td class="currency" style="color:#00F500">${account['percentage_on_totals']} &#37;</td>
                    %endif
                    </tr>
                %endif
                %if account['first_for_account'] == '1' and account['is_income'] == '0':
                    <tr class="row" style="border-top: 0.5px solid blue;background-color:#F2F2F2">
                    <td class="label"><b>Expense ${account['analytic_account']}</b></td>
                    <td class="currency">${account['planned_amount']}</td>
                    <td class="currency">${account['effective_amount']}</td>
                    %if account['isColored'] == '0':
                        <td class="currency">${account['balance']} </td>
                        <td class="currency">${account['percentage_on_totals']} &#37;</td>
                    %endif
                    %if account['isColored'] == '1':
                        <td class="currency" style="color:#FE0000">${account['balance']} </td>
                        <td class="currency" style="color:#FE0000">${account['percentage_on_totals']} &#37;</td>
                    %endif
                    </tr>
                %endif
                
                <tr class="row" style="border-top: 0.5px solid blue;background-color:#F2F2F2">
                <td class="label">${account['account']}</td>
                <td/>
                <td class="currency">${account['amount']} </td>
                <td/>
                <td class="currency">${account['percentage']} &#37;</td>
                </tr>
            %endif
        %endfor         
    </table>
</body>
</html>
