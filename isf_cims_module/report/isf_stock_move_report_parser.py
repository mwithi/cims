# -*- coding: utf-8 -*-
import time
from pprint import pprint as pp

from openerp.report import report_sxw
import logging
import locale
import datetime

_logger = logging.getLogger(__name__)
_debug=False

class isf_stock_move_internal_report_parser(report_sxw.rml_parse):
    _name = 'report.isf.stock.move.internal.webkit'

    def __init__(self, cr, uid, name, context=None):
        if _debug:
            _logger.debug('==> INIT <==')
    
        super(isf_stock_move_internal_report_parser, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
            'pp': pp,
            'lines': self.lines,
            'headers' : self.headers,
        })
        self.context = context
        self.result_acc = []
        self.headers_acc = []
    
    
    def lines(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.stock.moves')
        
        active_id = ctx.get('active_id')
        moves_ids = obj_report.search(self.cr, self.uid, [('id','=',active_id)])
        moves = obj_report.browse(self.cr, self.uid, moves_ids)[0]
        
        total_price = 0.0
        for line in moves.line_ids:
            factor = line.uom_id.factor
            name = line.product_id.name
            code = line.product_id.default_code
            if not code:
                code = str(line.product_id.id)
            
            res = {
                'lot_id' : line.stock_id.name,
                'expiry_date' : line.stock_id.ref[:7],
                'uom' : line.uom_id.name,
                'product' : "["+code+"] " + name,
                'required' : line.required  / factor,
                'issued' : line.quantity / factor ,
            }
            self.result_acc.append(res)
        
        return self.result_acc
        
    def headers(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.stock.moves')
        
        active_id = ctx.get('active_id')
        moves_ids = obj_report.search(self.cr, self.uid, [('id','=',active_id)])
        moves = obj_report.browse(self.cr, self.uid, moves_ids)[0]
        
        
        res = {
            'date' : moves.date,
            'ref' : moves.ref,
            'source' : moves.location_source_id.name,
            'dest' : moves.location_dest_id.name,
            'desc' : moves.name,
            'sequence_id' : moves.sequence_id,
        }
        self.headers_acc.append(res)
        
        if _debug:
            _logger.debug('Table header : %s',res)
        
        
        return self.headers_acc
            
    

report_sxw.report_sxw('report.isf.stock.move.internal.webkit', 'isf.stock.moves', 'addons/isf_cims_module/report/pp.mako', parser=isf_stock_move_internal_report_parser)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
