# -*- coding: utf-8 -*-
import time
from pprint import pprint as pp

from openerp.report import report_sxw
import logging
import locale


_logger = logging.getLogger(__name__)
_debug=False

class isf_stock_sale_summary_report_parser(report_sxw.rml_parse):
    _name = 'report.isf.stock.sale.summary.webkit'

    def __init__(self, cr, uid, name, context=None):
    
        super(isf_stock_sale_summary_report_parser, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'time': time,
            'pp': pp,
            'lines': self.lines,
            'headers' : self.headers,
        })
        self.context = context
        self.result_acc = []
        self.headers_acc = []

    """"
    def set_context(self, objects, data, ids, report_type=None):
        for obj in objects:
            print obj, obj.id
        new_ids = ids
        return super(isf_bs_report_parser, self).set_context(objects, data, new_ids, report_type=report_type)
    """
    #def _add_header(self, node, header=1):
    #    if header == 0:
    #        self.rml_header = ""
    #    return True
    
    
    
    def _store_name(self, cr, uid, code):
        store_id = int(code[1:].strip())
        store_type = code[0]

        if store_type == 'S':
            pos_o = self.pool.get('isf.shop')
            pos_ids = pos_o.search(cr, uid, [('id','=',store_id)])
            pos = pos_o.browse(cr, uid, pos_ids)[0]
            return pos.name
        elif store_type == 'A':
            return 'ALL'

        return 'PHARMACY'

    def headers(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.stock.sale.summary.report')
        date_s_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['date_start'])
        date_e_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['date_end'])
        date_start = date_s_obj[0]['date_start']
        date_end = date_e_obj[0]['date_end']
        store_o = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['store_id'])
        store = store_o[0]['store_id']
        
        if _debug:
            _logger.debug('==> Store Headers : %s', store)
        
        store_name = self._store_name(self.cr, self.uid, store)
        res = { 
            'date_start' : date_start,
            'date_end' : date_end,
            'store' : store_name,
        }
        self.headers_acc.append(res)
        
        return self.headers_acc
    
    def _manage_generic_pos(self, cr, uid, shop_id, date_start, date_end):
        result_p = {}
        result_s = {}
        resdata = []
        pos_o = self.pool.get('isf.pos')
        pos_ids = pos_o.search(cr, uid, [('date','>=',date_start),('date','<=',date_end),('state','=','confirm'),('product_type_sel','=','product'),('shop_id','=',int(shop_id))])
        
        total_sales = total_foc = total_internal = 0
        total_sales_qty = total_foc_qty = total_internal_qty = 0

        for pos in pos_o.browse(cr, uid, pos_ids):
            for line in pos.product_ids:
                rec = result_p.get(line.product_id.name_template)
                if rec is None:
                    rec = {'product': line.product_id.name_template, 'code':line.product_id.default_code, 
                        'sales_qty' : 0, 'foc_qty' : 0, 'internal_qty' : 0, 'total_qty' : 0,'type' : 'Product',
                        'sales' : 0, 'foc': 0, 'internal' : 0,'is_total' : False,'is_header':False,'store':pos.shop_id.name}
                    result_p.update({line.product_id.name_template:rec})
                if pos.transaction_type_product in ['sale','credit']:
                    rec['sales'] += (line.quantity * line.unit_price)
                    total_sales += (line.quantity * line.unit_price)
                    total_sales_qty += line.quantity
                    rec['sales_qty'] += line.quantity
                    rec['total_qty'] = rec['sales_qty'] + rec['foc_qty'] + rec['internal_qty']
                if pos.transaction_type_product == 'foc':
                    rec['foc'] += (line.quantity * line.unit_price)
                    total_foc += (line.quantity * line.unit_price)
                    total_foc_qty += line.quantity
                    rec['foc_qty'] += line.quantity
                    rec['total_qty'] = rec['sales_qty'] + rec['foc_qty'] + rec['internal_qty']
                if pos.transaction_type_product == 'internal':
                    rec['internal'] += (line.quantity * line.unit_price)
                    total_internal += (line.quantity * line.unit_price)
                    total_internal_qty += line.quantity
                    rec['internal_qty'] += line.quantity
                    rec['total_qty'] = rec['sales_qty'] + rec['foc_qty'] + rec['internal_qty']
        
        total_sales_s = total_foc_s = total_internal_s = 0
        total_sales_qty_s = total_foc_qty_s = total_internal_qty_s = 0
        pos_ids = pos_o.search(cr, uid, [('date','>=',date_start),('date','<=',date_end),('state','=','confirm'),('product_type_sel','=','service'),('shop_id','=',int(shop_id))])
        for pos in pos_o.browse(cr, uid, pos_ids):
            for line in pos.service_ids:
                rec = result_s.get(line.service_id.name_template)
                if rec is None:
                    rec = {'product': line.service_id.name_template, 'code':line.service_id.default_code or '', 
                        'sales_qty' : 0, 'foc_qty' : 0, 'internal_qty' : 0, 'total_qty' : 0, 'type' : 'Service',
                        'sales' : 0, 'foc': 0, 'internal' : 0,'is_total' : False,'is_header':False,'store':pos.shop_id.name}
                    result_s.update({line.service_id.name_template:rec})
                if pos.transaction_type_service in ['sale','credit']:
                    rec['sales'] += (line.quantity * line.unit_price)
                    total_sales_s += (line.quantity * line.unit_price)
                    total_sales_qty_s += line.quantity
                    rec['sales_qty'] += line.quantity
                    rec['total_qty'] = rec['sales_qty'] + rec['foc_qty'] + rec['internal_qty']
                if pos.transaction_type_service == 'foc':
                    rec['foc'] += (line.quantity * line.unit_price)
                    total_foc_s += (line.quantity * line.unit_price)
                    total_foc_qty_s += line.quantity
                    rec['foc_qty'] += line.quantity
                    rec['total_qty'] = rec['sales_qty'] + rec['foc_qty'] + rec['internal_qty']
                if pos.transaction_type_service == 'internal':
                    rec['internal'] += (line.quantity * line.unit_price)
                    total_internal_s += (line.quantity * line.unit_price)
                    total_internal_qty_s += line.quantity
                    rec['internal_qty'] += line.quantity
                    rec['total_qty'] = rec['sales_qty'] + rec['foc_qty'] + rec['internal_qty']
        
        # Manage Product  
        first = True   
        list_key = result_p.keys()
        list_key.sort()
        for key in list_key:
            rec = result_p.get(key)
            rec.update({
                'sales' : locale.format("%20d",rec['sales'], grouping=True),
                'foc' : locale.format("%20d",rec['foc'], grouping=True),
                'internal' : locale.format("%20d",rec['internal'], grouping=True),
                'sales_qty' : rec['sales_qty'],
                'foc_qty' : rec['foc_qty'],
                'internal_qty' : rec['internal_qty'],
                'total_qty' : rec['total_qty'],
                'is_header' : first,
                'store' : rec['store'],
            })
            first = False
            resdata.append(rec)
        if len(resdata) > 0:
            rec = {'product': False,'sales' : locale.format("%20d",total_sales, grouping=True), 
                'foc': locale.format("%20d",total_foc, grouping=True), 
                'internal' : locale.format("%20d",total_internal, grouping=True),
                'sales_qty' : total_sales_qty,
                'foc_qty' : total_foc_qty,
                'internal_qty' : total_internal_qty,
                'total_qty' : total_sales_qty + total_foc_qty + total_internal_qty,
                'is_total' : True,
                'is_header' : False,
                'type' : False,
            }
            resdata.append(rec)

        # Manage Service  
        first = True   
        list_key = result_s.keys()
        list_key.sort()
        for key in list_key:
            rec = result_s.get(key)
            rec.update({
                'sales' : locale.format("%20d",rec['sales'], grouping=True),
                'foc' : locale.format("%20d",rec['foc'], grouping=True),
                'internal' : locale.format("%20d",rec['internal'], grouping=True),
                'sales_qty' : rec['sales_qty'],
                'foc_qty' : rec['foc_qty'],
                'internal_qty' : rec['internal_qty'],
                'total_qty' : rec['total_qty'],
                'is_header' : first,
            })
            first = False
            resdata.append(rec)
        if len(resdata) > 0:
            rec = {'product': False,'sales' : locale.format("%20d",total_sales_s, grouping=True), 
                'foc': locale.format("%20d",total_foc_s, grouping=True), 
                'internal' : locale.format("%20d",total_internal, grouping=True),
                'sales_qty' : total_sales_qty_s,
                'foc_qty' : total_foc_qty_s,
                'internal_qty' : total_internal_qty_s,
                'total_qty' : total_sales_qty_s + total_foc_qty_s + total_internal_qty_s,
                'is_total' : True,
                'is_header' : False,
                'type' : False,
                }

            resdata.append(rec)
        return resdata
       
        
    def _manage_pharmacy_pos(self, cr, uid, date_start, date_end):
        result = {}
        resdata = []
        pharm_o = self.pool.get('isf.pharmacy.pos')
        pharm_ids = pharm_o.search(cr, uid, [('payment_date','>=',date_start),('payment_date','<=',date_end),('state','=','close')])
        
        total_sales = total_foc = total_internal = 0
        total_sales_qty = total_foc_qty = total_internal_qty = 0
        for pharm in pharm_o.browse(cr, uid, pharm_ids):
            for line in pharm.product_line_ids:
                rec = result.get(line.product.name_template)
                if rec is None:
                    rec = {'product': line.product.name_template, 'code':line.product.default_code, 
                        'sales_qty' : 0, 'foc_qty' : 0, 'internal_qty' : 0, 'total_qty' : 0, 'type' : 'Product',
                        'sales' : 0, 'foc': 0, 'internal' : 0,'is_total' : False,'is_header':False}
                    result.update({line.product.name_template:rec})
                if pharm.type in ['paid','credit']:
                    rec['sales'] += (line.amount * line.price)
                    total_sales += (line.amount * line.price)
                    total_sales_qty += line.amount
                    rec['sales_qty'] += line.amount
                    rec['total_qty'] = rec['sales_qty'] + rec['foc_qty'] + rec['internal_qty']
                if pharm.type == 'foc':
                    rec['foc'] += (line.amount * line.price)
                    total_foc += (line.amount * line.price)
                    total_foc_qty += line.amount
                    rec['foc_qty'] += line.amount
                    rec['total_qty'] = rec['sales_qty'] + rec['foc_qty'] + rec['internal_qty']
                if pharm.type == 'internal':
                    rec['internal'] += (line.amount * line.price)
                    total_internal += (line.amount * line.price)
                    total_internal_qty += line.amount
                    rec['internal_qty'] += line.amount
                    rec['total_qty'] = rec['sales_qty'] + rec['foc_qty'] + rec['internal_qty']
                
        first = True    
        list_key = result.keys()
        list_key.sort()
        for key in list_key:
            rec = result.get(key)
            rec.update({
                'sales' : locale.format("%20d",rec['sales'], grouping=True),
                'foc' : locale.format("%20d",rec['foc'], grouping=True),
                'internal' : locale.format("%20d",rec['internal'], grouping=True),
                'sales_qty' : rec['sales_qty'],
                'foc_qty' : rec['foc_qty'],
                'internal_qty' : rec['internal_qty'],
                'total_qty' : rec['total_qty'],
                'is_header' : first,
                'store' : 'PHARMACY',
            })
            first = False
            resdata.append(rec)
        if len(resdata) > 0:
            rec = {'product': False,'sales' : locale.format("%20d",total_sales, grouping=True), 
                'foc': locale.format("%20d",total_foc, grouping=True), 
                'internal' : locale.format("%20d",total_internal, grouping=True),
                'sales_qty' : total_sales_qty,
                'foc_qty' : total_foc_qty,
                'internal_qty' : total_internal_qty,
                'total_qty' : total_sales_qty + total_foc_qty + total_internal_qty,
                'is_total' : True,
                'is_header' : False,
                'type' : False,
            }
            resdata.append(rec)
        return resdata

    def _get_all_stores(self, cr, uid, context=None):
        result = []
        
        # Shops
        shop_o = self.pool.get('isf.shop')
        shop_ids = shop_o.search(cr, uid, [])
        for shop in shop_o.browse(cr, uid, shop_ids):
            result.append('S'+str(shop.id))        

        # Pharmacy
        pharm_o = self.pool.get('isf.pharmacy.pos')
        if pharm_o is not None:
            result.append('L00')
        return result
    
    def lines(self, ids=None, done=None):
        ctx = self.context.copy()
        obj_report = self.pool.get('isf.stock.sale.summary.report')
        
        date_s_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['date_start'])
        date_start = date_s_obj[0]['date_start']
        date_e_obj = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['date_end'])
        date_end = date_e_obj[0]['date_end']
        store_o = obj_report.read(self.cr, self.uid, ctx['active_ids'], ['store_id'])
        store = store_o[0]['store_id']
        
        store_type = store[0]
        store_id = store[1:].strip()

        if store_type == 'A':
            stores_ids = self._get_all_stores(self.cr, self.uid)
            if _debug:
                _logger.debug('STORE IDS : %s',stores_ids)
            for store in stores_ids:
                store_type = store[0]
                store_id = store[1:].strip()
                if store_type == 'S':
                    res = self._manage_generic_pos(self.cr, self.uid, store_id, date_start, date_end)
                elif store_type == 'L':
                    res = self._manage_pharmacy_pos(self.cr, self.uid, date_start, date_end)

                for r in res:
                    self.result_acc.append(r)
        elif store_type == 'S':
            return self._manage_generic_pos(self.cr, self.uid, store_id, date_start, date_end)
        elif store_type == 'L':
            return self._manage_pharmacy_pos(self.cr, self.uid, date_start, date_end)
        
        return self.result_acc
        
        

report_sxw.report_sxw('report.isf.stock.sale.summary.webkit', 'isf.stock.sale.summary.report', 'addons/isf_cims_module/report/sale_summary.mako', parser=isf_stock_sale_summary_report_parser)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
