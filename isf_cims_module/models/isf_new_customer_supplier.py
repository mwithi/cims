import datetime
from lxml import etree
import math
import pytz
import re
import warnings
import logging

import openerp
from openerp import SUPERUSER_ID
from openerp import pooler, tools
from openerp.osv import osv, fields
from openerp.tools.translate import _
from openerp.tools.yaml_import import is_comment

_logger = logging.getLogger(__name__)
_debug=False

class format_address(object):
    def fields_view_get_address(self, cr, uid, arch, context={}):
        user_obj = self.pool.get('res.users')
        fmt = user_obj.browse(cr, SUPERUSER_ID, uid, context).company_id.country_id
        fmt = fmt and fmt.address_format
        layouts = {
            '%(city)s %(state_code)s\n%(zip)s': """
                <div class="address_format">
                    <field name="city" placeholder="City" style="width: 50%%"/>
                    <field name="state_id" class="oe_no_button" placeholder="State" style="width: 47%%" options='{"no_open": true}'/>
                    <br/>
                    <field name="zip" placeholder="ZIP"/>
                </div>
            """,
            '%(zip)s %(city)s': """
                <div class="address_format">
                    <field name="zip" placeholder="ZIP" style="width: 40%%"/>
                    <field name="city" placeholder="City" style="width: 57%%"/>
                    <br/>
                    <field name="state_id" class="oe_no_button" placeholder="State" options='{"no_open": true}'/>
                </div>
            """,
            '%(city)s\n%(state_name)s\n%(zip)s': """
                <div class="address_format">
                    <field name="city" placeholder="City"/>
                    <field name="state_id" class="oe_no_button" placeholder="State" options='{"no_open": true}'/>
                    <field name="zip" placeholder="ZIP"/>
                </div>
            """
        }
        for k,v in layouts.items():
            if fmt and (k in fmt):
                doc = etree.fromstring(arch)
                for node in doc.xpath("//div[@class='address_format']"):
                    tree = etree.fromstring(v)
                    node.getparent().replace(node, tree)
                arch = etree.tostring(doc)
                break
        return arch

class customer_supplier(osv.TransientModel, format_address):
    _name = "isf.new.customer.supplier.view"
    
    def view_init(self, cr, uid, fields_list, context=None):
        if context is None:
            context = {}
        res = super(customer_supplier, self).view_init(cr, uid, fields_list, context=context)
        property_account_receivable_parent_customer_ids = self.pool.get('ir.values').get_default(cr, uid, 'isf.cims.config','property_account_receivable_parent_customer_ids')
        if not property_account_receivable_parent_customer_ids :
            raise osv.except_osv(_('Warning'), _('You need to set Settings -> CIMS config -> General Setting'))
        return res
    
    
    def _get_customer(self, cr, uid, context=None):
        if context is None:
            context = {}

        if context.get('customer'):
            return context.get('customer')
        
    def _get_supplier(self, cr, uid, context=None):
        if context is None:
            context = {}

        if context.get('supplier'):
            return context.get('supplier')
        
    def _default_category(self, cr, uid, context=None):
        if context is None:
            context = {}
        if context.get('category_id'):
            return [context['category_id']]
        return False
    
    def _get_receivable_parent_accounts(self, cr, uid, context=None):
        if context is None:
            context = {}
            
        result = []
        ir_values = self.pool.get('ir.values')
        account_obj = self.pool.get('account.account')
        if context.get('customer'):
            account_list = ir_values.get_default(cr, uid, 'isf.cims.config', 'property_account_receivable_parent_customer_ids')
        else:
            account_list = ir_values.get_default(cr, uid, 'isf.cims.config', 'property_account_receivable_parent_supplier_ids')
        if _debug:
            _logger.debug(account_list)
        if account_list is not None:
            for account_id in account_list:
                account_ids = account_obj.search(cr, uid, [('id','=',account_id)], limit=1)
                for account in account_obj.browse(cr, uid, account_ids, context=context):
                    result.append((account.code, account.code+" "+account.name))
                
        return result
    
    def _get_payable_parent_accounts(self, cr, uid, context=None):
        if context is None:
            context = {}
            
        result = []
        ir_values = self.pool.get('ir.values')
        account_obj = self.pool.get('account.account')
        if context.get('customer'):
            account_list = ir_values.get_default(cr, uid, 'isf.cims.config', 'property_account_payable_parent_customer_ids')
        else:
            account_list = ir_values.get_default(cr, uid, 'isf.cims.config', 'property_account_payable_parent_supplier_ids')
        
        if account_list is not None:
            for account_id in account_list:
                account_ids = account_obj.search(cr, uid, [('id','=',account_id)], limit=1)
                for account in account_obj.browse(cr, uid, account_ids, context=context):
                    result.append((account.code, account.code+" "+account.name))
                
        return result
    
    _order = "name"
    _columns = {
        'name': fields.char('Name', size=128, required=True, select=True),
        'street': fields.char('Street', size=128),
        'zip': fields.char('Zip', change_default=True, size=24),
        'city': fields.char('City', size=128),
        'state_id': fields.many2one("res.country.state", 'State'),
        'parent_id': fields.many2one('res.partner', 'Related Company'),
        'website': fields.char('Website', size=64, help="Website of Partner or Company"),
        'country_id': fields.many2one('res.country', 'Country'),
        'email': fields.char('Email', size=240),
        'phone': fields.char('Phone', size=64),
        'fax': fields.char('Fax', size=64),
        'mobile': fields.char('Mobile', size=64),
        'is_company': fields.boolean('Is a Company', help="Check if the contact is a company, otherwise it is a person"),
        'comment': fields.text('Notes'),
        'customer': fields.boolean('Customer', help="Check this box if this contact is a customer."),
        'supplier': fields.boolean('Supplier', help="Check this box if this contact is a supplier. If it's not checked, purchase people will not see it when encoding a purchase order."),
        
        'title': fields.many2one('res.partner.title', 'Title'),
        'category_id': fields.many2many('res.partner.category', id1='partner_id', id2='category_id', string='Tags'),
        'use_parent_address': fields.boolean('Use Company Address', help="Select this if you want to set company's address information  for this contact"),
        'type': fields.selection([('default', 'Default'), ('invoice', 'Invoice'),
                                   ('delivery', 'Shipping'), ('contact', 'Contact'),
                                   ('other', 'Other')], 'Address Type',
            help="Used to select automatically the right address according to the context in sales and purchases documents."),
        'color': fields.integer('Color Index'),
        'property_account_payable': fields.property(
            'account.account',
            type='many2one',
            relation='account.account',
            string="Account Payable",
            view_load=True,
            domain="[('type', '=', 'payable')]",
            help="This account will be used instead of the default one as the payable account for the current partner",
            ),
        'property_account_receivable': fields.property(
            'account.account',
            type='many2one',
            relation='account.account',
            string="Account Receivable",
            view_load=True,
            domain="[('type', '=', 'receivable')]",
            help="This account will be used instead of the default one as the receivable account for the current partner",
            ),
        'name_account_receivable': fields.char('Name', size=256, select=True),
        'code_account_receivable': fields.char('Code', size=64, select=1),
        'name_account_payable': fields.char('Name', size=256, select=True),
        'code_account_payable': fields.char('Code', size=64, select=1),
        'is_new_receivable_account':fields.boolean('New Receivable account?'),
        'is_new_payable_account':fields.boolean('New Payable account?'),
        'property_account_receivable_parent': fields.selection(_get_receivable_parent_accounts,'Receivable Parent Account',required=False,store=False,select=True),
        'property_account_payable_parent': fields.selection(_get_payable_parent_accounts,'Payable Parent Account',required=False,store=False,select=True),
            }
    
     
    _defaults = {
        
        
        'customer' : _get_customer,
        'supplier' : _get_supplier,
        'category_id': _default_category,
        'color': 0,
        'is_company': False,
        'type': 'contact', # type 'default' is wildcard and thus inappropriate
        'use_parent_address': False,
    
    }
    
    def on_change_name(self, cr, uid, ids, name):
        return {'value': {'name_account_payable':name,'name_account_receivable':name, }}
     
    def onchange_type(self, cr, uid, ids, is_company, context=None):
        value = {}
        value['title'] = False
        if is_company:
            domain = {'title': [('domain', '=', 'partner')]}
        else:
            domain = {'title': [('domain', '=', 'contact')]}
        return {'value': value, 'domain': domain}
    
    def on_change_flag(self, cr, uid, ids, name, active, context=None):
        if context is None:
            context = {}
            
        supplier = context.get('supplier')
        if supplier:
            partner = _('Supplier')
        else:
            partner = _('Customer')
        
        if active and not name :  
            raise osv.except_osv(_('Warning'), _('Please enter the name'))
        
        return {}

    def on_change_parent_receivable(self, cr, uid, ids, name, property_account_receivable_parent_code, context):
        data = self.browse(cr, uid, ids)
        supplier = context.get('supplier')
        customer = context.get('customer')
        
        if property_account_receivable_parent_code:
            parent_account_receivable_id = self.pool.get('account.account').search(cr,uid,[('code','=',property_account_receivable_parent_code)])[0]
        
        account_type_receivable_customer_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.cims.config','account_type_receivable_customer_id')
        account_type_receivable_supplier_id= self.pool.get('ir.values').get_default(cr, uid, 'isf.cims.config','account_type_receivable_supplier_id')
        
        if property_account_receivable_parent_code and parent_account_receivable_id :
            
            if supplier and account_type_receivable_supplier_id :  
               
                maxcodereceivable = None
                
                cr.execute('''
                    SELECT code FROM account_account WHERE type LIKE 'receivable' AND parent_id ='''+str(parent_account_receivable_id) + ''' AND type = 'receivable' ORDER BY code DESC limit 1''')
                if _debug:
                    _logger.debug('''
                        SELECT code FROM account_account WHERE type LIKE 'receivable' AND parent_id ='''+str(parent_account_receivable_id) + ''' AND type = 'receivable' ORDER BY code DESC limit 1''')
            
                res = cr.fetchall()
                list_res = []
                
                for row in res:
                    list_res.append(str(row[0]))
                      
                    cleanlist =  [f for f in list_res if 'copy' not in f]
                    resultint = map(int, cleanlist)                   
                    maxcodereceivable=max(resultint)+1
    
                return {'value':{'code_account_receivable':maxcodereceivable, 'name_account_receivable':name }}
                    
            if customer and account_type_receivable_customer_id :
                
                maxcodereceivable =None
    
                cr.execute('''
                    SELECT code FROM account_account WHERE type LIKE 'receivable' AND parent_id ='''+str(parent_account_receivable_id) + ''' AND type = 'receivable' ORDER BY code DESC limit 1''')
                if _debug:
                    _logger.debug('''
                        SELECT code FROM account_account WHERE type LIKE 'receivable' AND parent_id ='''+str(parent_account_receivable_id) + ''' AND type = 'receivable' ORDER BY code DESC limit 1''')
    
                res = cr.fetchall()
                list_res = []
                    
                for row in res:
                    list_res.append(str(row[0]))
                      
                    cleanlist =  [f for f in list_res if 'copy' not in f]
                    resultint = map(int, cleanlist)
                    maxcode=max(resultint)
                    maxcodereceivable = maxcode+1
                    
                return {'value':{'code_account_receivable':maxcodereceivable, 'name_account_receivable':name }}
        
        return {}
                
        
    def on_change_parent_payable(self, cr, uid, ids, name, property_account_payable_parent_code, context):
        data = self.browse(cr, uid, ids)
        supplier = context.get('supplier')
        customer = context.get('customer')
        
        if property_account_payable_parent_code:
            parent_account_payable_id = self.pool.get('account.account').search(cr,uid,[('code','=',property_account_payable_parent_code)])[0]
        
        account_type_payable_customer_id = self.pool.get('ir.values').get_default(cr, uid, 'isf.cims.config','account_type_payable_customer_id')
        account_type_payable_supplier_id= self.pool.get('ir.values').get_default(cr, uid, 'isf.cims.config','account_type_payable_supplier_id')
        
        if property_account_payable_parent_code and parent_account_payable_id : 
            
            if supplier and account_type_payable_supplier_id :
                
                maxcodepayable = None
                
                cr.execute('''
                    SELECT code FROM account_account WHERE type LIKE 'payable' AND parent_id ='''+ str(parent_account_payable_id) + ''' AND type = 'payable' ORDER BY code DESC limit 1''')
                if _debug:
                    _logger.debug('''
                        SELECT code FROM account_account WHERE type LIKE 'payable' AND parent_id ='''+ str(parent_account_payable_id) + ''' AND type = 'payable' ORDER BY code DESC limit 1''')
              
                respayable = cr.fetchall()
                list_res_payable = []
                
                for row in respayable:
                    list_res_payable.append(str(row[0]))
                      
                    cleanlistpayable =  [f for f in list_res_payable if 'copy' not in f]
                    resultintpayable = map(int, cleanlistpayable)
                    maxcodepayable=max(resultintpayable)+1
                
                return {'value':{'code_account_payable':maxcodepayable, 'name_account_payable':name }}
                
            if customer and account_type_payable_customer_id :
                
                maxcodepayable =None
                    
                cr.execute('''
                    SELECT code FROM account_account WHERE type LIKE 'payable' AND parent_id ='''+ str(parent_account_payable_id) + ''' AND type = 'payable' ORDER BY code DESC limit 1''')
                if _debug:
                    _logger.debug('''
                        SELECT code FROM account_account WHERE type LIKE 'payable' AND parent_id ='''+ str(parent_account_payable_id) + ''' AND type = 'payable' ORDER BY code DESC limit 1''')
               
                respayable = cr.fetchall()
                list_res_payable = []
                    
                for row in respayable:
                    list_res_payable.append(str(row[0]))
                      
                    cleanlistpayable =  [f for f in list_res_payable if 'copy' not in f]
                    resultintpayable = map(int, cleanlistpayable)
                    maxcodepayable=max(resultintpayable)+1
                    
                return {'value':{'code_account_payable':maxcodepayable, 'name_account_payable':name }}
        
        return {}
        
    def add_partners(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids)[0]
        part_pool = self.pool.get('res.partner')
        account_pool = self.pool.get('account.account')
        if data.supplier:
            user_type_rec=account_type_receivable_supplier_id= self.pool.get('ir.values').get_default(cr, uid, 'isf.cims.config','account_type_receivable_supplier_id')
            user_type_pay=account_type_payable_supplier_id= self.pool.get('ir.values').get_default(cr, uid, 'isf.cims.config','account_type_payable_supplier_id')
            
        else:
            user_type_rec=account_type_receivable_customer_id= self.pool.get('ir.values').get_default(cr, uid, 'isf.cims.config','account_type_receivable_customer_id')
            user_type_pay=account_type_payable_customer_id= self.pool.get('ir.values').get_default(cr, uid, 'isf.cims.config','account_type_payable_customer_id')
         
        if data.is_new_receivable_account:
            parent_account_receivable_id = self.pool.get('account.account').search(cr,uid,[('code','=',data.property_account_receivable_parent)])[0]
            valsRec ={
                'name':data.name_account_receivable,
                'code':data.code_account_receivable,
                'user_type':user_type_rec,
                'type': 'receivable',
                'parent_id':parent_account_receivable_id,
            }
            rec_id = account_pool.create(cr, uid, valsRec, context=context)
        else:   
            rec_id =data.property_account_receivable
            
        if data.is_new_payable_account:
            parent_account_payable_id = self.pool.get('account.account').search(cr,uid,[('code','=',data.property_account_payable_parent)])[0]  
            valsPay ={
                'name':data.name_account_payable,
                'code':data.code_account_payable,
                'user_type':user_type_pay,
                'type': 'payable',
                'parent_id':parent_account_payable_id,
            }
            pay_id = account_pool.create(cr, uid, valsPay, context=context)
        else:
            pay_id=data.property_account_payable
            
        vals = {
                'name': data.name,
                'street': data.street,
                'zip':data.zip,
                'city': data.city,
                'state_id': data.state_id,
                'website': data.website,
                'country_id': data.country_id,
                'email': data.email,
                'phone': data.phone,
                'fax': data.fax,
                'mobile': data.mobile,
                'is_company': data.is_company,
                'comment':data.comment,
                'customer': data.customer,
                'supplier': data.supplier,
                'title': data.title,
                'parent_id': data.parent_id,
                'has_image': False,
                'category_id': data.category_id,
                'use_parent_address': data.use_parent_address,
                'type': data.type,
                'color': data.color,
                'property_account_payable':pay_id,
                'property_account_receivable':rec_id,
            }
        
        part_id = part_pool.create(cr, uid, vals, context=context)
        
        if _debug:
            _logger.debug('==> new prod_id : %s', part_id)
        
        return True