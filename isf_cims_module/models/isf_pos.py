from openerp.tools import DEFAULT_SERVER_DATE_FORMAT as DF
from openerp.tools.translate import _
from openerp.osv import fields, osv
from openerp import tools
from openerp import netsvc
import logging
import datetime
import openerp
from lxml import etree

_logger = logging.getLogger(__name__)
_debug=False

#####################################################################################################################################################################
########################################################################### ISF SHOP ################################################################################
#####################################################################################################################################################################

class isf_shop(osv.Model):
    _name = 'isf.shop'
    
    _columns = {
        'name' : fields.char('Name',required=True),
        'service_analytic_id' : fields.many2one('account.analytic.account','Service Analytic Account'),
        'product_analytic_id' : fields.many2one('account.analytic.account','Product Analytic Account'),
        'product_type' : fields.selection([('product','Product'),('service','Service'),('both','Product/Service')],'Product Type'),
        'location_id' : fields.many2one('stock.location','Location'),
        'service_category_id' : fields.many2one('product.category','Service Category',required=False),
        'product_category_id' : fields.many2one('product.category','Product Category',required=False),
        'income_account_id' : fields.many2one('account.account','Income Account',required=True),
        'account_foc_id' : fields.many2one('account.account','FoC Account',required=False),
        'account_int_id' : fields.many2one('account.account','Internal Use Account',required=False),
        'account_sold_id' : fields.many2one('account.account','Cost of Item Sold Account',required=False),
        'account_expired_id' : fields.many2one('account.account','Cost of Item Expired Account',required=False),
        'account_broken_id' : fields.many2one('account.account','Cost of Item Broken\Loss Account',required=False),
        'account_asset_id' : fields.many2one('account.account','Asset Account'),
        'journal_id' : fields.many2one('account.journal','Journal',required=True),
        'stock_journal_id' : fields.many2one('account.journal','Stock Journal'),
        'account_discount_id' : fields.many2one('account.account','Discount Account',required=True),
    }


#####################################################################################################################################################################
########################################################################### ISF SERVICE #############################################################################
#####################################################################################################################################################################

class isf_pos_service_line_select(osv.TransientModel):
    _name = 'isf.pos.service.line.select'
    
    _columns = {
        'service_ids' : fields.many2many('product.product',string='Services',required=False,domain=[('type','=','service')]),
    }
    
    def import_service(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids)[0]
        line_ids = data['service_ids']
        lines_obj = self.pool.get('product.product')
        active_id = context.get('active_id')
        
        
        line_obj = self.pool.get('isf.pos.service.line')
        
        for service in line_ids:
            line_id = line_obj.create(cr, uid, {
                'service_id' : service.id,
                'unit_price' : service.list_price,
                'quantity' : 1,
                'pos_id' : active_id,
                'total_price' : service.list_price,
            }, context=context)
                
        return True
    
class isf_pos_service_line(osv.Model):
    _name = 'isf.pos.service.line'
    
    _columns = {
        'service_id' : fields.many2one('product.product','Service'),
        'quantity' : fields.integer('Quantity'),
        'unit_price' : fields.float('Unit Price'),
        'total_price' : fields.float('Total'),
        'pos_id' : fields.many2one('isf.pos',string="POS Id",select=True),
    }
    
    _defaults = {
        'quantity' : 1,
    }
    
    def onchange_service(self, cr, uid, ids, quantity, unit_price,context=None):
        result = {'value' : {}}
        
        result['value'].update({
            'total_price' : unit_price * quantity,
        })
        
        self.write(cr, uid, ids, {'total_price' : unit_price * quantity}, context=context)
        
        return result;
 
#####################################################################################################################################################################
########################################################################### ISF PRODUCT #############################################################################
#####################################################################################################################################################################   

class isf_pos_product_batch(osv.TransientModel):
    _name = 'isf.pos.product.batch'
    
    _columns = {
        'stock_id' : fields.many2one('stock.production.lot','Batch'),
        'product_id' : fields.related('stock_id','product_id',type="many2one",relation="product.product",string="Product",store=False),
        'product_name' : fields.related('product_id','name_template',readonly=True,type="char",relation="product.product",string="Product Name",store=False),
        'qty' : fields.float('Qty'),
        'pos_id': fields.integer('PoS'),
        'pos_line_id': fields.integer('PoS'),
    }

class isf_pos_product_line_select(osv.TransientModel):
    _name = 'isf.pos.product.line.select'
    
    _columns = {
        'batch_product_ids' : fields.one2many('isf.pos.product.batch','pos_line_id','Batches',required=False),
        'pos_id' : fields.integer('PoS ID'),
    }
    
    def _pre_load_batch(self, cr, uid,context=None):
        lot_o = self.pool.get('stock.production.lot')
        lot_ids = lot_o.search(cr, uid, [('stock_available','>',0.0)], context=context)
        batch_o = self.pool.get('isf.pos.product.batch')
        pos_id = context.get('statement_id')
        
        if _debug:
            _logger.debug('POS ID : %s', pos_id)
        
        for lot in lot_o.browse(cr, uid, lot_ids, context=context):
            if lot.stock_available > 0:
                res = batch_o.search(cr, uid, [('stock_id','=',lot.id),('pos_line_id','=',pos_id)])
                if len(res) == 0:
                    batch_o.create(cr, uid, {
                        'stock_id' : lot.id,
                        'product_id' : lot.product_id.id,
                        'qty': lot.stock_available,
                        'pos_line_id' : pos_id,
                    })
                    
    def default_get(self, cr, uid, fields, context=None):
        res = super(isf_pos_product_line_select, self).default_get(cr, uid, fields, context=context)
        statement_id = context.get('active_id')
        data_o = self.pool.get('isf.pos')
        data_ids = data_o.search(cr, uid, [('id','=',statement_id)])
        data = data_o.browse(cr, uid, data_ids)[0]
        res['location_id'] = data.location_id.id
        res['pos_id'] = statement_id
        
        ctx = context.copy()
        ctx.update({
            'location_id' : data.location_id.id,
            'statement_id' : data.id,
        })
    
    
        stock_o = self.pool.get('isf.pos.product.batch')
        stock_ids = stock_o.search(cr, uid, [('pos_line_id','=',data.id)])
        
        #for stock in stock_o.browse(cr, uid, stock_ids):
        #    stock_o.unlink(cr, uid, stock.id, context=context)
        
        self._pre_load_batch(cr, uid, context=ctx)
    
        return res
    
    def import_product(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids)[0]
        line_ids = data['batch_product_ids']
        lines_obj = self.pool.get('product.product')
        active_id = context.get('active_id')
        line_obj = self.pool.get('isf.pos.product.line')
        
        now = datetime.datetime.now()
        stock_o = self.pool.get('stock.production.lot')
        
        for line in line_ids:
                
            expired = False
            exp = datetime.datetime.strptime(line.stock_id.ref, "%Y-%m-%d")
            if exp < now:
                expired = True
            line_id = line_obj.create(cr, uid, {
                'stock_id' : line.stock_id.id,
                'expiry_date' : line.stock_id.ref,
                'product_id' : line.stock_id.product_id.id,
                'uom_id' : line.stock_id.product_id.uom_id.id,
                'unit_price' : line.stock_id.product_id.list_price,
                'stock_available' : line.qty,
                'quantity' : 1,
                'pos_id' : active_id,
                'total_price' : line.stock_id.product_id.list_price,
                'expired' : expired,
            }, context=context)
                
        return True

class isf_pos_product_line(osv.Model):
    _name = 'isf.pos.product.line'
    
    _columns = {
        'stock_id' : fields.many2one('stock.production.lot','Lot Id'),
        'expiry_date' : fields.char('Expiry Date'),
        'product_id' : fields.many2one('product.product','Product'),
        'stock_available' : fields.integer('Available'),
        'quantity' : fields.integer('Quantity'),
        'uom_id' : fields.many2one('product.uom','UoM'),
        'unit_price' : fields.float('Unit Price'),
        'total_price' : fields.float('Total'),
        'pos_id' : fields.many2one('isf.pos',string="POS Id",select=True),
        'expired' : fields.boolean('Expired'),
    }
    
    _defaults = {
        'quantity' : 1,
        'expired' : False,
    }
    
    def onchange_product(self, cr, uid, ids, stock_available, quantity, product_id, unit_price,context=None):
        result = {'value' : {}}
        
        if quantity < 0:
            quantity = 1
            
        if quantity > stock_available:
            quantity = stock_available
        
        prod_obj = self.pool.get('product.product')
        prod_ids = prod_obj.search(cr, uid, [('id','=',product_id)])
        if prod_ids:
            prod = prod_obj.browse(cr, uid, prod_ids)[0]
            result['value'].update({
                'quantity' : quantity,
                'total_price' : unit_price * quantity,
                'uom_id' : prod.uom_id.id,
            })
        
        self.write(cr, uid, ids, {'total_price' : unit_price * quantity, 'uom_id' : result['value']['uom_id']}, context=context)
        
        return result;
    
#####################################################################################################################################################################
########################################################################### ISF POS #################################################################################
#####################################################################################################################################################################

class isf_pos(osv.Model):
    _name = 'isf.pos'
    
    def _calculate_discount(self, cr, uid, ids, field_name, arg, context=None):
        total_amount = 0.0
        res = {}
        discount = 0.0
        
        for data in self.browse(cr, uid, ids, context=context):
            total_amount = 0.0
            if data.product_type_sel == 'service':
                for line in data.service_ids:
                     total_amount = total_amount + line.unit_price * line.quantity
                if data.discount_price != 0.0:
                    discount = total_amount - data.discount_price
            elif data.product_type_sel == 'product':
                for line in data.product_ids:
                     total_amount = total_amount + line.unit_price * line.quantity
                if data.discount_price != 0.0:
                    discount = total_amount - data.discount_price
                    
            res[data.id] = discount
        return res
     
    def _calculate_total(self, cr, uid, ids, field_name, arg, context=None):
        total_amount = 0.0
        res = {}
        discount = 0.0
        total_amount = 0.0
        
        
        for data in self.browse(cr, uid, ids, context=context):
            if data.product_type_sel == 'service':
                if data.transaction_type_service != 'foc':
                    service_obj = self.pool.get('isf.pos.service.line')
                    service_ids = service_obj.search(cr, uid, [('pos_id','=',data.id)])
                    for line in service_obj.browse(cr, uid, service_ids):
                         total_amount = total_amount + line.unit_price * line.quantity
           
            elif data.product_type_sel == 'product':
                if data.transaction_type_product != 'foc':
                    product_obj = self.pool.get('isf.pos.product.line')
                    product_ids = product_obj.search(cr, uid, [('pos_id','=',data.id)])
                    for line in product_obj.browse(cr, uid, product_ids):
                        total_amount = total_amount + line.unit_price * line.quantity
                       
            res[data.id] = total_amount - data.discount_price
        return res
        
    def _calculate_free(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        
        for data in self.browse(cr, uid, ids, context=context):
            total_amount = 0.0
            
            if data.product_type_sel == 'service':
                if data.transaction_type_service == 'foc':
                    for line in data.service_ids:
                        total_amount = total_amount + line.unit_price * line.quantity
            elif data.product_type_sel == 'product':
                if data.transaction_type_product == 'foc':
                    for line in data.product_ids:
                        total_amount = total_amount + line.unit_price * line.quantity
            res[data.id] = total_amount
            
        return res
        
    def _get_payment_methods(self, cr, uid, context=None):
        if context is None:
            context = {}
            
        result = []
        ir_values = self.pool.get('ir.values')
        account_list = ir_values.get_default(cr, uid, 'isf.pos.config', 'cash_bank_account_ids')
        account_obj = self.pool.get('account.account')
		
        if account_list is not None:
            for account_id in account_list:
                account_ids = account_obj.search(cr, uid, [('id','=',account_id)], limit=1)
                for account in account_obj.browse(cr, uid, account_ids, context=context):
                    result.append((account.code, account.code+" "+account.name))
                
        return result
    
    def _get_default_payments_method(self, cr, uid, context=None):
        account_obj = self.pool.get('account.account')
        payment_ids = self.pool.get('ir.values').get_default(cr, uid, 'isf.pos.config','cash_bank_account_ids')
        result = []
        preferred =  self.pool.get('ir.values').get_default(cr, uid, 'isf.pos.config', 'preferred_method_id')
        if preferred and preferred != '':
            account_ids = account_obj.search(cr, uid,[('id','=',preferred)])
            account = account_obj.browse(cr, uid, account_ids)[0]
            return account.code
            
        return False
            
    def _get_transaction_type_service(self, cr, iud, context=None):
        return (('sale','Sales'),('foc','FoC'),('internal','Internal'),('credit','Credit'))
        
    def _get_transaction_type_product(self, cr, iud, context=None):
        return (('sale','Sales'),('foc','FoC'),('internal','Internal'),('credit','Credit'),('expired','Expired'),('loss','Broken\Loss'))
  
    def _get_patient_id(self, cr, uid, ids, name, args, context=None):
        result = {}
        patient_obj = self.pool.get('isf.oh.patient')
        for obj in self.browse(cr, uid, ids, context=context):
            patient_id = patient_obj.search(cr, uid, [('partner_id', '=', obj.partner_id.id)])
            if patient_id:
                result[obj.id] = patient_id[0]
        return result
  
    _columns = {
        'state' : fields.selection([('draft','Draft'),('credit','Credit'),('confirm','Confirm')],'State',readonly=True),
        'name' : fields.char('Document Number',size=32,required=True),
        'date' : fields.date('Date',required=True),
        'desc' : fields.char('Description',required=False),
        'partner_id' : fields.many2one('res.partner','Customer'),
        'patient_id': fields.function(
            _get_patient_id,
            type='many2one',
            relation="isf.oh.patient",
            string='Patient',
        ),
        #'location_id' : fields.related('shop_id','location_id',rel="stock.location",type="many2one",string="Location",store=False),
        'location_id' : fields.many2one('stock.location','Location'),
        'shop_id' : fields.many2one('isf.shop','Location',required=True,store=True),
        'pharmacy' : fields.boolean('Pharmacy'),
        'category_id' : fields.many2one('product.category','Category Id'),
        'transaction_type_service' : fields.selection(_get_transaction_type_service,'Type',required=True),
        'transaction_type_product' : fields.selection(_get_transaction_type_product,'Type',required=True),
        'product_type' : fields.char("Product Type"),
        'product_type_sel' : fields.selection([('service','Service'),('product','Product')],'Product Type'),
        'product_ids' : fields.one2many('isf.pos.product.line','pos_id','Product'),
        'service_ids' : fields.one2many('isf.pos.service.line','pos_id','Service'),
        'payment_id' : fields.selection(_get_payment_methods,'Payment Type',required=False),
        'journal_items_id' : fields.many2one('account.move'),
        'stock_journal_items_id' : fields.many2one('account.move'),
        'invoice_id' : fields.many2one('account.invoice'),
        'sequence_id': fields.char('PoS ID', size=32,required=False),
        #totals fields
        'discount_price' : fields.float('Discount Price', required=False),
        'total_amount' : fields.function(_calculate_total, type="float", string="Total Amount",store=True),
        'total_discount' : fields.function(_calculate_discount,type="float",string="Discount",store=True),
        'total_free' : fields.function(_calculate_free, type="float", string="Free of Charge",store=True),
    }
    
    _sql_constraints = [('gr_number_name_unique', 'unique(name)', '#GR Number already exists')]
    
    _defaults = {
        'pharmacy' : False,
        'product_type' : False,
        'product_type_sel' : 'service',
        'transaction_type_service' : 'sale',
        'transaction_type_product' : 'sale',
        'state' : 'draft',
        'discount_price' : 0.0,
        'payment_id' : _get_default_payments_method,
        'date' : fields.date.context_today,
    }

    def create(self, cr, uid, vals, context=None):
        obj_sequence = self.pool.get('ir.sequence')
        obj_period = self.pool.get('account.period')
        seq_ids = obj_sequence.search(cr, uid, [('name','=','POS')])
        if len(seq_ids) == 0:
            self.create_sequence(cr, uid, vals)
            seq_ids = obj_sequence.search(cr, uid, [('name','=','POS')])
        
        period_ids = obj_period.find(cr, uid, vals['date'], context=context)
        period = obj_period.browse(cr, uid, period_ids)[0]
        ctx = context.copy()
        ctx.update({
            'fiscalyear_id': period.fiscalyear_id.id,
        })
        new_seq = obj_sequence.next_by_id(cr, uid, seq_ids[0],ctx)
        
        vals['sequence_id'] = new_seq
        return super(isf_pos, self).create(cr, uid, vals, context)
    
    def create_sequence(self, cr, uid, vals, context=None):
        """ Create new no_gap entry sequence for every new Joural
        """
        # in account.journal code is actually the prefix of the sequence
        # whereas ir.sequence code is a key to lookup global sequences.
        prefix = '1'
        
        seq = {
            'name': 'POS',
            'implementation':'no_gap',
            'prefix': "POS/%(year)s/",
            'padding': 4,
            'number_increment': 1
        }
        return self.pool.get('ir.sequence').create(cr, uid, seq)

    
    def confirm_service(self, cr, uid, ids, wizard, context=None):
        if context is None:
            context= {}
                
        if not wizard.service_ids:
            raise openerp.exceptions.Warning(_('Add one or more services to the receipt'))
        
        move_line_pool = self.pool.get('account.move.line')
        move_pool = self.pool.get('account.move')
        isf_lib = self.pool.get('isf.lib.utils')
        
        account_obj = self.pool.get('account.account')
        account_ids = account_obj.search(cr, uid, [('code','=', wizard.payment_id)])
        account = account_obj.browse(cr, uid, account_ids)[0]
        comp_curr_id = isf_lib.get_company_currency_id(cr, uid)
        amount_currency = False
        amount_second_currency_credit = False
        amount_second_currency_debit = False
        currency = False
        discount_amount_currency = False
        
        move = move_pool.account_move_prepare(cr, uid, wizard.shop_id.journal_id.id, wizard.date, ref=wizard.name, context=context)	
        move_id = move_pool.create(cr, uid, move, context=context)
        
        self.write(cr, uid, ids, {'journal_items_id' : move_id }, context=context)
        
        total = 0.0
        discounted = False
        for line in wizard.service_ids:
            total += line.unit_price * line.quantity
            
        if wizard.discount_price > 0.0:
            discounted = True
        
        if comp_curr_id != account.currency_id.id and account.currency_id.id != False:
            currency_pool = self.pool.get('res.currency')
            amount_currency = currency_pool.compute(cr, uid, account.currency_id.id, comp_curr_id, total - wizard.discount_price, context=context)
            currency = account.currency_id.id
            discount_amount_currency = currency_pool.compute(cr, uid, account.currency_id.id, comp_curr_id, wizard.discount_price, context=context)

            credit = amount_currency
            debit = amount_currency
            amount_second_currency_credit = total
            amount_second_currency_debit = total

            if discounted:
                amount_second_currency_debit = total - wizard.discount_price
                debit = discount_amount_currency
                discount = discount_amount_currency - credit
        else:
            credit = total
            debit = total
            discount = wizard.discount_price

            if discounted:
                debit -= discount
            
        move_line_credit = {
            'analytic_account_id': wizard.shop_id.service_analytic_id.id, 
            'tax_code_id': False, 
            'tax_amount': 0,
            'ref' : wizard.name,
            'name': 'Income from '+wizard.shop_id.name or '/',
            'currency_id': currency,
            'credit': credit,
            'debit': 0.0,
            'date_maturity' : False,
            'amount_currency': -1 * amount_second_currency_credit,
            'partner_id': False,
            'move_id': move_id,
            'account_id': wizard.shop_id.income_account_id.id,
            'state' : 'valid',
        }
        
        result = move_line_pool.create(cr, uid, move_line_credit,context=context,check=False)
        
          
        move_line_debit = {
                'analytic_account_id': False, 
                'tax_code_id': False, 
                'tax_amount': 0,
                'ref' : wizard.name,
                'name': 'Payment' or '/',
                'currency_id': currency,
                'credit': 0.0,
                'debit': debit,
                'date_maturity' : False,
                'amount_currency': amount_second_currency_debit,
                'partner_id': False,
                'move_id': move_id,
                'account_id': account.id,
                'state' : 'valid',
            }
            
        result = move_line_pool.create(cr, uid, move_line_debit,context=context,check=False)
        
        if discounted:
            move_line_debit_discount = {
                'analytic_account_id': wizard.shop_id.service_analytic_id.id, 
                'tax_code_id': False, 
                'tax_amount': 0,
                'ref' : wizard.name,
                'name': 'Discount' or '/',
                'currency_id': currency,
                'credit': 0.0,
                'debit': discount,
                'date_maturity' : False,
                'amount_currency': discount_amount_currency,
                'partner_id': False,
                'move_id': move_id,
                'account_id': wizard.shop_id.account_discount_id.id,
                'state' : 'valid',
            }
            
            result = move_line_pool.create(cr, uid, move_line_debit_discount,context=context,check=False)
        
        
        return True
    
    def register_cost_of_item(self, cr, uid, ids, wizard, context=None):
        if context is None:
            context= {}
            
        move_line_pool = self.pool.get('account.move.line')
        move_pool = self.pool.get('account.move')
        isf_lib = self.pool.get('isf.lib.utils')
        
        cost_acc_id = False
            
        if wizard.transaction_type_product == 'sale':
            cost_acc_id = wizard.shop_id.account_sold_id.id
        elif wizard.transaction_type_product == 'internal':
            cost_acc_id = wizard.shop_id.account_int_id.id
        elif wizard.transaction_type_product == 'foc':
            cost_acc_id = wizard.shop_id.account_foc_id.id
        elif wizard.transaction_type_product == 'expired':
            cost_acc_id = wizard.shop_id.account_expired_id.id
        elif wizard.transaction_type_product == 'loss':
            cost_acc_id = wizard.shop_id.account_broken_id.id
        
        if wizard.transaction_type_product == 'sale':
            account_obj = self.pool.get('account.account')
            account_ids = account_obj.search(cr, uid, [('code','=', wizard.payment_id)])
            account = account_obj.browse(cr, uid, account_ids)[0]
        comp_curr_id = isf_lib.get_company_currency_id(cr, uid)
    
        move = move_pool.account_move_prepare(cr, uid, wizard.shop_id.stock_journal_id.id, wizard.date, ref=wizard.name, context=context)	
        move_id = move_pool.create(cr, uid, move, context=context)
        
        self.write(cr, uid, ids, {'stock_journal_items_id' : move_id }, context=context)
        
        
        total_cost_price = 0.0
        for line in wizard.product_ids:
            cost_price = line.stock_id.cost_price
            if cost_price == 0:
                cost_price = line.product_id.list_price
            total_cost_price += cost_price * line.quantity
        
        move_line_credit = {
            'analytic_account_id': False, 
            'tax_code_id': False, 
            'tax_amount': 0,
            'ref' : wizard.name,
            'name': 'Cost of Item '+wizard.shop_id.name or '/',
            'currency_id': False,
            'credit': total_cost_price,
            'debit': 0.0,
            'date_maturity' : False,
            'amount_currency': False,
            'partner_id': False,
            'move_id': move_id,
            'account_id': wizard.shop_id.account_asset_id.id,
            'state' : 'valid',
        }
    
        result = move_line_pool.create(cr, uid, move_line_credit,context=context,check=False)
    
      
        move_line_debit = {
                'analytic_account_id': wizard.shop_id.product_analytic_id.id, 
                'tax_code_id': False, 
                'tax_amount': 0,
                'ref' : wizard.name,
                'name': 'Cost of Item' or '/',
                'currency_id': False,
                'credit': 0.0,
                'debit': total_cost_price,
                'date_maturity' : False,
                'amount_currency': False,
                'partner_id': False,
                'move_id': move_id,
                'account_id': cost_acc_id,
                'state' : 'valid',
            }
        
        result = move_line_pool.create(cr, uid, move_line_debit,context=context,check=False)
        
        return True
     
    def confirm_product(self, cr, uid, ids, wizard, context=None):
        if context is None:
            context= {}

        if not wizard.product_ids:
            raise openerp.exceptions.Warning(_('Add one or more products to the receipt'))
            
        move_line_pool = self.pool.get('account.move.line')
        move_pool = self.pool.get('account.move')
        isf_lib = self.pool.get('isf.lib.utils')
        
        account_obj = self.pool.get('account.account')
        account_ids = account_obj.search(cr, uid, [('code','=', wizard.payment_id)])
        account = account_obj.browse(cr, uid, account_ids)[0]
        comp_curr_id = isf_lib.get_company_currency_id(cr, uid)
        amount_currency = False
        currency = False
        discount_currency = False
        total_amount_currency = False
        
        move = move_pool.account_move_prepare(cr, uid, wizard.shop_id.journal_id.id, wizard.date, ref=wizard.name, context=context)	
        move_id = move_pool.create(cr, uid, move, context=context)
        
        self.write(cr, uid, ids, {'journal_items_id' : move_id }, context=context)
        
        total = 0.0
        
        for line in wizard.product_ids:
            total += line.unit_price * line.quantity
            if _debug:
                _logger.debug('Total : %s',total)
    
        real_cash = total - wizard.discount_price
        discounted = wizard.discount_price
        
        if _debug:
            _logger.debug('Currency : %s',account.currency_id.id)
        
        if comp_curr_id != account.currency_id.id and account.currency_id.id != False:
            currency_pool = self.pool.get('res.currency')
            amount_currency = currency_pool.compute(cr, uid, account.currency_id.id, comp_curr_id, real_cash, context=context)
            total_amount_currency = currency_pool.compute(cr, uid, account.currency_id.id, comp_curr_id, total, context=context)
            currency = account.currency_id.id
            discount_currency = currency_pool.compute(cr, uid, account.currency_id.id, comp_curr_id, discounted, context=context)
            
        move_line_credit = {
            'analytic_account_id': wizard.shop_id.product_analytic_id.id, 
            'tax_code_id': False, 
            'tax_amount': 0,
            'ref' : wizard.name,
            'name': 'Income from '+wizard.shop_id.name or '/',
            'currency_id': currency,
            'credit': total,
            'debit': 0.0,
            'date_maturity' : False,
            'amount_currency': -1 * total_amount_currency,
            'partner_id': False,
            'move_id': move_id,
            'account_id': wizard.shop_id.income_account_id.id,
            'state' : 'valid',
        }
        
        result = move_line_pool.create(cr, uid, move_line_credit,context=context,check=False)
        
          
        move_line_debit = {
                'analytic_account_id': False, 
                'tax_code_id': False, 
                'tax_amount': 0,
                'ref' : wizard.name,
                'name': 'Payment' or '/',
                'currency_id': currency,
                'credit': 0.0,
                'debit': real_cash,
                'date_maturity' : False,
                'amount_currency': amount_currency,
                'partner_id': False,
                'move_id': move_id,
                'account_id': account.id,
                'state' : 'valid',
            }
            
        result = move_line_pool.create(cr, uid, move_line_debit,context=context,check=False)
        
        # Register discount
        if wizard.discount_price > 0.0:
            move_line_discount = {
                'analytic_account_id': False, 
                'tax_code_id': False, 
                'tax_amount': 0,
                'ref' : wizard.name,
                'name': 'Payment' or '/',
                'currency_id': currency,
                'credit': 0.0,
                'debit': discounted,
                'date_maturity' : False,
                'amount_currency': discount_currency,
                'partner_id': False,
                'move_id': move_id,
                'account_id': wizard.shop_id.account_discount_id.id,
                'state' : 'valid',
            }
            
            result = move_line_pool.create(cr, uid, move_line_discount,context=context,check=False)
        
        return True
        
    def _get_dest_location_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.pos.config','dest_location_id')
    
    def _get_enable_stock(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.pos.config','enable_stock')
        
    def create_stock_move(self, cr, uid, ids, wizard,context=None):
        if context is None:
            context = {}
            
        isf_lib = self.pool.get('isf.lib.utils')
        dest_location_id = self._get_dest_location_id(cr, uid)
        for line in wizard.product_ids:
            isf_lib._generate_stock_move(cr, uid, wizard.name, wizard.shop_id.location_id.id, dest_location_id, line.product_id, line.quantity,
                wizard.date,'out',line.stock_id.id, wizard.sequence_id,line.uom_id,context=context)
        
        
        return True
        
    def confirm(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
    
        wizard = self.browse(cr, uid, ids)[0]
        stock_enabled = self._get_enable_stock(cr, uid)
       
        # Manage Services
        if wizard.product_type_sel == 'service':    
            if wizard.transaction_type_service == 'sale':    
                self.confirm_service(cr, uid, ids, wizard, context=context)
                self.write(cr, uid, ids, {'state':'confirm'}, context=context)
            elif wizard.transaction_type_service == 'credit':
                self.manage_credit(cr, uid, ids, wizard, context=context)
                self.write(cr, uid, ids, {'state':'credit'}, context=context)
        # Manage Products
        elif wizard.product_type_sel == 'product':
            if wizard.transaction_type_product == 'sale': 
                self.confirm_product(cr, uid, ids, wizard, context=context)
                if stock_enabled:
                    self.register_cost_of_item(cr, uid, ids, wizard, context=context)
                self.create_stock_move(cr, uid, ids, wizard, context=context)
                self.write(cr, uid, ids, {'state':'confirm'}, context=context)
            elif wizard.transaction_type_product in ['foc','internal','loss','expired']:
                if stock_enabled:
                    self.register_cost_of_item(cr, uid, ids, wizard, context=context)
                self.create_stock_move(cr, uid, ids, wizard, context=context)
                self.write(cr, uid, ids, {'state':'confirm'}, context=context)
            elif wizard.transaction_type_product == 'credit':
                self.manage_credit(cr, uid, ids, wizard, context=context)
                self.write(cr, uid, ids, {'state':'credit'}, context=context)

        return True

    def _get_product_discount_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.pos.config','product_discount_id')

    def _get_account_discount_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.pos.config','account_discount_id')

        
    def manage_credit(self, cr, uid, ids, wizard, context=None):
        inv_obj = self.pool.get('account.invoice')
        inv_line_obj = self.pool.get('account.invoice.line')
        product_discount_id = self._get_product_discount_id(cr, uid)
        discount_account_id = self._get_account_discount_id(cr, uid)
        
        if _debug:
            _logger.debug('==> POS : manage credit')

        total_paid = 0.0

        vals = {
            'type' : 'out_invoice',
            'partner_id' : wizard.partner_id.id,
            'account_id': wizard.partner_id.property_account_receivable.id,
            'date_due' : wizard.date,
        }
        inv_id = inv_obj.create(cr, uid, vals, context=context)

        if _debug:
            _logger.debug('VALS : %s',vals)
            _logger.debug('product_type : %s',wizard.product_type)

        aa_id = False
        
        if wizard.product_type_sel == 'product':
            aa_id = wizard.shop_id.product_analytic_id.id
            for line in wizard.product_ids:
                line_vals = {
                    'name' : line.product_id.name_template,
                    'invoice_id' : inv_id,
                    'product_id': line.product_id.id,
                    'account_id' : line.product_id.property_account_income.id,
                    'quantity' : line.quantity,
                    'price_unit' : line.unit_price,
                    'account_analytic_id' : wizard.shop_id.product_analytic_id.id,
                }
                line_id = inv_line_obj.create(cr, uid, line_vals, context=context)
                total_paid += line.quantity * line.unit_price
        elif wizard.product_type_sel == 'service':
            aa_id = wizard.shop_id.service_analytic_id.id
            for line in wizard.service_ids:
                line_vals = {
                    'name' : line.service_id.name_template,
                    'invoice_id' : inv_id,
                    'product_id': line.service_id.id,
                    'account_id' : line.service_id.property_account_income.id,
                    'quantity' : line.quantity,
                    'price_unit' : line.unit_price,
                    'account_analytic_id' : wizard.shop_id.service_analytic_id.id,
                }
                line_id = inv_line_obj.create(cr, uid, line_vals, context=context)
                total_paid += line.quantity * line.unit_price

        if wizard.discount_price > 0 :
            line_vals = {
                'name' : 'Discount',
                'invoice_id' : inv_id,
                'product_id': product_discount_id,
                'account_id' : discount_account_id,
                'quantity' : 1.0,
                'price_unit' : -1.0 * (total_paid - wizard.discount_price),
                'account_analytic_id' : aa_id,
            }
            line_id = inv_line_obj.create(cr, uid, line_vals, context=context)
        
        self.write(cr, uid, ids, {'invoice_id' : inv_id}, context=context)
        
        wf_service = netsvc.LocalService("workflow")
        wf_service.trg_validate(uid, 'account.invoice', inv_id, 'invoice_open', cr)

        
        return True   

    def pay(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids)[0]
        inv_obj = self.pool.get('account.invoice')
        inv = inv_obj.browse(cr, uid, data.invoice_id.id, context=context)
        
        dummy, view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'account_voucher', 'view_vendor_receipt_dialog_form')
        return {
            'name':_("Pay Invoice"),
            'view_mode': 'form',
            'view_id': view_id,
            'view_type': 'form',
            'res_model': 'account.voucher',
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'new',
            'domain': '[]',
            'context': {
                'payment_expected_currency': inv.currency_id.id,
                'default_partner_id': self.pool.get('res.partner')._find_accounting_partner(inv.partner_id).id,
                'default_amount': inv.type in ('out_refund', 'in_refund') and -inv.residual or inv.residual,
                'default_reference': inv.name,
                'close_after_process': True,
                'invoice_type': inv.type,
                'invoice_id': inv.id,
                'default_type': inv.type in ('out_invoice','out_refund') and 'receipt' or 'payment',
                'type': inv.type in ('out_invoice','out_refund') and 'receipt' or 'payment'
            }
        }     
        
    def set_to_draft(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
            
        wizard = self.browse(cr, uid, ids)[0]
        move_obj = self.pool.get('account.move')
        
        move_ids = []
        if wizard.journal_items_id:
            move_ids.append(wizard.journal_items_id.id)
            
        if wizard.stock_journal_items_id:
            move_ids.append(wizard.stock_journal_items_id.id)
            
        if wizard.invoice_id:
            self._cancel_invoice(cr, uid, ids,context=context)
        
        move_obj.button_cancel(cr, uid, move_ids)
        move_obj.unlink( cr, uid, move_ids, context=None, check=False)
        
        if wizard.product_type_sel == 'product':
            stock_obj = self.pool.get('stock.move')
            stock_ids = stock_obj.search(cr, uid, [('name','=',wizard.name)])
            stock_obj.write(cr, uid, stock_ids, {'state':'cancel'}, context=context)
        
        self.write(cr, uid, ids, {'state':'draft'}, context=context)
        
        return True
    
    def onchange_product_type(self, cr, uid, ids, product_type, context=None):
        result = {'value' : {}}
        
        sel = 'service'
        if product_type == 'product':
            sel = 'product'
        
        result['value'].update({
            'product_type_sel' : sel,
        })
        
        return result
        
    def onchange_shop(self, cr, uid, ids, shop_id, context=None):
        shop_obj = self.pool.get('isf.shop')
        shop_ids = shop_obj.search(cr, uid, [('id','=',shop_id)])
        shop = shop_obj.browse(cr, uid, shop_ids)[0]
        
        res = {'value':{}}
        
        category_id = False
        if shop.product_type == 'product':
            category_id = shop.product_category_id.id
        elif shop.product_type in ['service','both']:
            category_id = shop.service_category_id.id
        
        res['value'].update({
            'product_type' : shop.product_type,
            'category_id' : category_id,
            'location_id' : shop.location_id.id,
        })
        
        return res
        
    def unlink(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
            
        
        pharmacy_pos = self.read(cr, uid, ids, ['state'], context=context)
        unlink_ids = []

        for t in pharmacy_pos:
            if t['state'] not in ('draft'):
                raise openerp.exceptions.Warning(_('You cannot delete an entry which is not draft. '))
            else:
                unlink_ids.append(t['id'])

        osv.osv.unlink(self, cr, uid, unlink_ids, context=context)
        return True
        
    def button_reset_total(self, cr, uid, ids, context=None):
        data = self.browse(cr, uid, ids, context=context)[0]
        total_amount = 0.0
        total_free = 0.0
        total_discount = 0.0
        for line in data.service_ids:
            total_amount = total_amount + line.unit_price * line.quantity 
        
        self.write(cr, uid, ids, {'total_amount': total_amount}, context=context)
        self.write(cr, uid, ids, {'total_free': total_free}, context=context)
        self.write(cr, uid, ids, {'total_discount': total_discount}, context=context)
        
    def _cancel_invoice(self, cr, uid, ids,context=None):
        data = self.browse(cr, uid, ids)[0]
        inv_obj = self.pool.get('account.invoice')
        inv_obj.action_cancel(cr, uid, [data.invoice_id.id], context=context)
        return True
    
    def onchange_transaction_type_service(self, cr, uid, ids, transaction_type_service, context=None):
        result = {'value': {}}
        if transaction_type_service:
            result['value'].update({
                'transaction_type_service' : transaction_type_service,
                'transaction_type_product' : transaction_type_service,
            })
        return result
        
    def onchange_transaction_type_product(self, cr, uid, ids, transaction_type_product, context=None):
        result = {'value' :{}}
        if transaction_type_product != 'expired' or transaction_type_product != 'loss':
            result['value'].update({
                'transaction_type_service' : transaction_type_product,
            })
        return result
