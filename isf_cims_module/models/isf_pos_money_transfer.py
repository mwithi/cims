from openerp.tools import DEFAULT_SERVER_DATE_FORMAT as DF
from openerp.tools.translate import _
from openerp.osv import fields, osv
from openerp import tools
import logging
import datetime
import openerp
from lxml import etree

_logger = logging.getLogger(__name__)
_debug=False

class isf_pos_money_transfer(osv.Model):
    _name = 'isf.pos.money.transfer'
    
    _rec_name = 'date'
    
    def _get_payment_methods(self, cr, uid, context=None):
        if context is None:
            context = {}
            
        result = []
        ir_values = self.pool.get('ir.values')
        account_list = ir_values.get_default(cr, uid, 'isf.pos.config', 'cash_bank_account_ids')
        account_obj = self.pool.get('account.account')
		
        if account_list is not None:
            for account_id in account_list:
                account_ids = account_obj.search(cr, uid, [('id','=',account_id)], limit=1)
                for account in account_obj.browse(cr, uid, account_ids, context=context):
                    result.append((account.code, account.code+" "+account.name))
                
        return result
        
    def _get_accounts(self, cr, uid, context=None):
        if context is None:
            context = {}
            
        result = []
        ir_values = self.pool.get('ir.values')
        account_list = ir_values.get_default(cr, uid, 'isf.pos.config', 'central_account_ids')
        account_obj = self.pool.get('account.account')
		
        if account_list is not None:
            for account_id in account_list:
                account_ids = account_obj.search(cr, uid, [('id','=',account_id)], limit=1)
                for account in account_obj.browse(cr, uid, account_ids, context=context):
                    result.append((account.code, account.code+" "+account.name))
                
        return result
    
    _columns = {
        'state' : fields.selection([('draft','Draft'),('done','Done')],'state',readonly=True),
        'shop_id' : fields.many2one('isf.shop','Shop',required=True),
        'date' : fields.date('Date'),
        'note' : fields.text('Note'),
        'registered_money' : fields.float('Registered'),
        'collected_money' : fields.float('Collected'),
        'sequence_id' : fields.char('ID',size=32),
        'move_id' : fields.many2one('account.move','Move Id'),
        'collected_account_ids' : fields.selection(_get_payment_methods,'Source Account',required=True,store=True,select=True),
        'central_account_ids' : fields.selection(_get_accounts,'Dest. Account',required=True,store=True,select=True),
        'central_currency_id' : fields.many2one('res.currency','Currency'),
        'collect_currency_id' : fields.many2one('res.currency','Currency'),
    }
    
    _defaults = {
        'state' : 'draft',
        'date' : fields.date.context_today,
        'central_currency_id' : False,
        'collect_currency_id' : False,
    }
    
    def _get_company_currency_id(self, cr, uid, context=None):
        users = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        company_id = users.company_id.id
        currency_id = users.company_id.currency_id
		
        return currency_id.id
    
    def create(self, cr, uid, vals, context=None):
        obj_sequence = self.pool.get('ir.sequence')
        seq_ids = obj_sequence.search(cr, uid, [('name','=','PoS Cash Transfer')])
        new_seq = obj_sequence.next_by_id(cr, uid, seq_ids,context)
        vals['sequence_id'] = new_seq
        res_id = super(isf_pos_money_transfer, self).create(cr, uid, vals, context=context)
        return res_id
        
    
    def onchange_shop_date(self, cr, uid, ids, shop_id,date, account_id,context=None):
        result = {'value' : {}}
        
        currency_id = False
        if account_id:
            account_o = self.pool.get('account.account')
            account_ids = account_o.search(cr, uid, [('code','=',account_id)])
            account = account_o.browse(cr, uid, account_ids)[0]
            currency_id = self._get_company_currency_id(cr, uid)
            if account.currency_id:
                currency_id = account.currency_id.id
        
        pos_o = self.pool.get('isf.pos')
        pos_ids = pos_o.search(cr, uid, [('shop_id','=',shop_id),('date','=',date),('state','=','confirm'),('payment_id','=',account_id)])
        total = 0
        for pos in pos_o.browse(cr, uid, pos_ids):
            total += pos.total_amount
            
        result =  {'value' : {'registered_money' : total,'collect_currency_id':currency_id}}
        
        self.write(cr, uid, ids, {'registered_money' : total}, context=context)
        self.write(cr, uid, ids, {'collect_currency_id' : currency_id}, context=context)
        return result
        
    def onchange_central_account_ids(self, cr, uid, ids, account_id, context=None):
        result = {'value' : {}}
        
        account_o = self.pool.get('account.account')
        account_ids = account_o.search(cr, uid, [('code','=',account_id)])
        account = account_o.browse(cr, uid, account_ids)[0]
        currency_id = self._get_company_currency_id(cr, uid)
        if account.currency_id:
            currency_id = account.currency_id.id
        
        result['value'].update({
            'central_currency_id' : currency_id,
        })
        
        self.write(cr, uid, ids, {'central_currency_id' : currency_id}, context=context)
        
        return result
    
    def confirm(self, cr, uid, ids, context=None):
        move_id = self._create_move(cr, uid, ids, context=context)
        if move_id:
            self.write(cr, uid, ids, {'move_id' : move_id },context=context)
            return True
        return False
        
    def _get_central_account_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.pos.config','central_account_id')
        
    def _get_unexpected_income_account_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.pos.config','unexpected_income_account_id')
    
    def _get_unexpected_expense_account_id(self, cr, uid):
        return self.pool.get('ir.values').get_default(cr, uid, 'isf.pos.config','unexpected_expense_account_id')
    
    def _create_move(self, cr, uid, ids, context=None):
        wizard = self.browse(cr, uid, ids)[0]
       
        if wizard.central_currency_id.id != wizard.collect_currency_id.id:
            raise openerp.exceptions.Warning(_('You must select two accounts with the same currency'))
            return False
       
        # Register transaction
        account_o = self.pool.get('account.account')
        move_line_pool = self.pool.get('account.move.line')		
        move_pool = self.pool.get('account.move')
        credit_account_code = wizard.collected_account_ids
        debit_account_code = wizard.central_account_ids
        
        account_ids = account_o.search(cr, uid, [('code','=',credit_account_code)])
        credit_account_id = account_o.browse(cr, uid, account_ids)[0]
        account_ids = account_o.search(cr, uid, [('code','=',debit_account_code)])
        debit_account_id = account_o.browse(cr, uid, account_ids)[0]
        
        aa_id = False
        if wizard.shop_id.product_type == 'product':
            aa_id = wizard.shop_id.product_analytic_id.id
        elif wizard.shop_id.product_type == 'service':
            aa_id = wizard.shop_id.service_analytic_id.id
        elif wizard.shop_id.product_type == 'both':
            aa_id = wizard.shop_id.service_analytic_id.id
        
        journal_id = wizard.shop_id.journal_id.id
        move = move_pool.account_move_prepare(cr, uid, journal_id, wizard.date, ref=wizard.sequence_id, context=context)	
        move_id = move_pool.create(cr, uid, move, context=context)
        move_pool.write(cr, uid, [move_id], {'narration' : wizard.note}, context=context)
        
        move_line = {
												'analytic_account_id': False, 
												'tax_code_id': False, 
												'tax_amount': 0,
												'ref' : wizard.sequence_id,
												'name': 'PoS Cash Transfer '+wizard.shop_id.name,
												'currency_id': False,
												'credit': wizard.collected_money,
												'debit': 0.0,
												'date_maturity' : False,
												'amount_currency': False,
												'partner_id': False,
												'move_id': move_id,
												'account_id': credit_account_id.id,
												'state' : 'valid'
        }
        result = move_line_pool.create(cr, uid, move_line,context=context,check=False)
	
        move_line = {
												'analytic_account_id': aa_id, 
												'tax_code_id': False, 
												'tax_amount': 0,
												'ref' : wizard.sequence_id,
												'name': 'PoS Cash Transfer '+wizard.shop_id.name,
												'currency_id': False,
												'credit': 0.0,
												'debit': wizard.registered_money,
												'date_maturity' : False,
												'amount_currency': False,
												'partner_id': False,
												'move_id': move_id,
												'account_id': debit_account_id.id,
												'state' : 'valid'
								}
		
        result = move_line_pool.create(cr, uid, move_line,context=context,check=False)
	
        diff_money =  wizard.registered_money - wizard.collected_money
        if _debug:
            _logger.debug('DIFF MONEY : %s', diff_money)
        
        if diff_money != 0:
            if diff_money > 0:
                account_id = self._get_unexpected_income_account_id(cr, uid)
                op_desc = 'Unexpected Income'
                credit = diff_money
                debit = 0.0
            elif diff_money < 0:
                account_id = self._get_unexpected_expense_account_id(cr, uid)
                op_desc = 'Unexpected Expense'
                credit = 0.0
                debit = -diff_money 
            
            if _debug:
                _logger.debug('Account[%s] Debit[%s] Credit[%s] Op[%s]',account_id,debit,credit,op_desc)
			
            move_line = {
														'analytic_account_id': aa_id, 
														'tax_code_id': False, 
														'tax_amount': 0,
														'ref' : wizard.sequence_id,
														'name': op_desc,
														'currency_id': False,
														'credit': credit,
														'debit': debit,
														'date_maturity' : False,
														'amount_currency': False,
														'partner_id': False,
														'move_id': move_id,
														'account_id': account_id,
														'state' : 'valid'
												}
		
            result = move_line_pool.create(cr, uid, move_line,context=context,check=False)
    
        self.write(cr, uid, ids, {'state':'done'}, context=context)
        return move_id
	
    def set_to_draft(self, cr, uid, ids, context=None):
        wizard = self.browse(cr, uid, ids)[0]
        move_ids = []
        if wizard.move_id:
            move_obj = self.pool.get('account.move')
            move_obj.button_cancel(cr, uid, [wizard.move_id.id])
            move_obj.unlink( cr, uid, [wizard.move_id.id], context=None, check=False)
        self.write(cr, uid, ids, {'state':'draft'}, context=context)
        return True
        
    def unlink(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
            
        
        wizard = self.read(cr, uid, ids, ['state'], context=context)
        unlink_ids = []

        for t in wizard:
            if t['state'] not in ('draft'):
                raise openerp.exceptions.Warning(_('You cannot delete a data entry statement which is not draft. '))
            else:
                unlink_ids.append(t['id'])

        osv.osv.unlink(self, cr, uid, unlink_ids, context=context)
        return True