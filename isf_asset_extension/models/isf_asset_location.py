from openerp.osv import fields, orm, osv
import logging
import time
from openerp import tools

_logger = logging.getLogger(__name__)
_debug = False

class isf_asset_location(orm.Model):
    _name = 'isf.asset.location'
    _description = 'Asset Location'
    _order = 'name'
    
    def _get_count_items(self, cr, uid, ids, name, args, context=None):
        res = {}
        asset_pool = self.pool.get('account.asset.asset')
        for location in self.browse(cr, uid, ids, context):
            count = asset_pool.search(cr, uid, [('location_id', '=', location.id)])
            res[location.id] = len(count)
        return res
    
    _columns = {
        'parent_location_id' : fields.many2one('isf.asset.location','Parent Location'),
        'name' : fields.char('Name', required=True),
        'count' : fields.function(_get_count_items, method=True, string="Assets count", type='integer',store=True)
    }
    
    _sql_constraints = [(
        'name', 'unique(name)', 'Location already exists')]
    
    def _insertUpdateLocations(self, cr, uid, ids=None, context=None):
        context = context or {}
        module_update_obj = self.pool.get('isf.module.update')
        updated = True
        if not module_update_obj.is_updated(cr, uid, __name__, _debug, context):
            if _debug:
                _logger.debug('==> Updating...')
            locations = cr.execute("SELECT DISTINCT(location) FROM account_asset_asset")
            locations = cr.fetchall()
            for location in locations:
                if _debug:
                    _logger.debug('==> location : %s', location[0])
                if location[0]:
                    id = 0
                    try:
                        cr.execute("INSERT INTO isf_asset_location(create_uid, create_date, write_date, write_uid, name) VALUES (1, (now() at time zone 'UTC'), (now() at time zone 'UTC'), 1, %s) RETURNING id", (location))
                        id = cr.fetchone()[0]
                        if _debug:
                            _logger.debug('==> location_id : %s', id)
                        cr.execute("UPDATE account_asset_asset SET location_id = %s WHERE location = %s", (id, location[0]))
                    except Exception, ex:
                        _logger.exception('==> handled Exception : %s', ex)
                        updated = False
            cr.commit()
            if updated:
                module_update_obj.module_updated(cr, uid, __name__, _debug, context)
        else:
            if _debug:
                _logger.debug('==> Already updated!')
        return True
    
isf_asset_location()