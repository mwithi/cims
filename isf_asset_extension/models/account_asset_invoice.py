# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#
#    Copyright (C) 2010-2012 OpenERP s.a. (<http://openerp.com>).
#    Copyright (c) 2014 Noviat nv/sa (www.noviat.com). All rights reserved.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program. If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, orm
import logging
_logger = logging.getLogger(__name__)


class account_invoice(orm.Model):
    _inherit = 'account.invoice'

    def action_number(self, cr, uid, ids, context=None):
        super(account_invoice, self).action_number(cr, uid, ids, context)
        asset_obj = self.pool.get('account.asset.asset')
        asset_line_obj = self.pool.get('account.asset.depreciation.line')
        for inv in self.browse(cr, uid, ids):
            move = inv.move_id
            assets = [aml.asset_id for aml in
                      filter(lambda x: x.asset_id, move.line_id)]
            ctx = {'create_asset_from_move_line': True}
            for asset in assets:
                asset_obj.write(
                    cr, uid, [asset.id],
                    {'code': '/'}, context=ctx)
                asset_line_name = asset_obj._get_depreciation_entry_name(
                    cr, uid, asset, 0)
                asset_line_obj.write(
                    cr, uid, [asset.depreciation_line_ids[0].id],
                    {'name': asset_line_name},
                    context={'allow_asset_line_update': True})
        return True