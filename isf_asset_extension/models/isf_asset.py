from openerp.osv import fields, orm, osv
from openerp import SUPERUSER_ID
import logging
import time
from openerp import tools
from openerp.tools.translate import _

_logger = logging.getLogger(__name__)
_debug = False

class isf_account_asset_category(orm.Model):
    _inherit = 'account.asset.category'

    _columns = {
        'code': fields.char('Prefix', size=5, required=True, help="The prefix used for the entry sequence."),
        'sequence_id': fields.many2one('ir.sequence', 'Entry Sequence', help="This field contains the information related to the numbering of the asset entries of this categoru.", required=True),
    }

    def create_sequence(self, cr, uid, vals, context=None):
        """ Create new no_gap entry sequence for every new Asset Category
        """
        # in account.asset.category code is actually the prefix of the sequence
        # whereas ir.sequence code is a key to lookup global sequences.
        prefix = vals['code'].upper()

        seq = {
            'name': vals['name'],
            'implementation':'no_gap',
            'prefix': prefix,
            'padding': 4,
            'number_increment': 1
        }
        if 'company_id' in vals:
            seq['company_id'] = vals['company_id']
        return self.pool.get('ir.sequence').create(cr, uid, seq)


    def create(self, cr, uid, vals, context=None):
        if vals.get('method_time') != 'year' and not vals.get('prorata'):
            vals['prorata'] = True
        if not 'sequence_id' in vals or not vals['sequence_id']:
            # if we have the right to create an Asset Category, we should be able to
            # create it's sequence.
            vals.update({'sequence_id': self.create_sequence(cr, SUPERUSER_ID, vals, context)})
        categ_id = super(isf_account_asset_category, self).create(
            cr, uid, vals, context=context)
        acc_obj = self.pool.get('account.account')
        acc_id = vals.get('account_asset_id')
        if acc_id:
            account = acc_obj.browse(cr, uid, acc_id)
            if not account.asset_category_id:
                acc_obj.write(
                    cr, uid, [acc_id], {'asset_category_id': categ_id})
        return categ_id
    

class isf_asset(orm.Model):
    _inherit = 'account.asset.asset'
    
    _columns = {
        'code': fields.char(
            'Reference', size=32, readonly=True,
            states={'draft': [('readonly', True)]}), # made readonly, it will be populated by the category's sequence
        'old_code': fields.char(
            'Old Reference', size=32, readonly=True,
            states={'draft': [('readonly', False)]}),
        'condition': fields.selection([
            ('fair', 'Fair, Still Useful'),
            ('good', 'Good, Running'),
            ('useful', 'Good, Useful'),
            ('verygood', 'Very Good, Almost New'),
            ('new', 'New, the Asset is new'),
            ('grounded', 'Poor, Grounded'),
            ('poor', 'Poor, Running'),
            ('replace', 'Poor, Needs Replacement'),
            ('repairable', 'Poor, Repairable'),
            ('verypoor', 'Very Poor, To scrap'),
            ], 'State of Use',
            help="Goods Condition"),
        'location': fields.related(
            'location_id', 'name',
            string='Location Name',
            type='char',
            relation='isf.asset.location',
            store=True, readonly=True,),
        'location_id': fields.many2one('isf.asset.location','Location ID',ondelete='restrict'),
        'serial_number': fields.char('Serial Number', size=64),
        'state': fields.selection([
            ('draft', 'Draft'),
            ('instock', 'In Stock'),
            ('open', 'Running'),
            ('close', 'Close'),
            ('removed', 'Removed'),
            ], 'Status', required=True,
            help="When an asset is created, the status is 'Draft'.\n"
                 "If the asset is confirmed, the status goes in 'In Stock' "
                 "If the asset is used, the status goes in 'Running'"
                 "and the depreciation lines can be posted "
                 "to the accounting.\n"
                 "If the last depreciation line is posted, "
                 "the asset goes into the 'Close' status.\n"
                 "When the removal entries are generated, "
                 "the asset goes into the 'Removed' status."),
        #TODO: Add Historical Value field
#         'historical_value': fields.float(
#             'Historical Value', required=True, readonly=True,
#             states={'draft': [('readonly', False)]},
#             help="The Asset Historical Value."),
        #TODO: Rename Purchase Value
#         'purchase_value': fields.float(
#             'Starting Value', required=True, readonly=True,
#             states={'draft': [('readonly', False)]},
#             help="\nThe Asset Value is calculated as follows:"
#                  "\nPurchase Value - Salvage Value."),
    }

    
    _defaults = {
        'code': '/',
    }
    
    # override from orm.py to pass _defaults
    def copy(self, cr, uid, id, default=None, context=None):

        default = {} if default is None else default.copy()
        context = {} if context is None else context.copy()
        default.update({
            'code':'/',
        })
        context.update({
            'copy':True
        })
        return super(isf_asset, self).copy(cr, uid, id, default, context)
    
    def use(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        currency_obj = self.pool.get('res.currency')
        for asset in self.browse(cr, uid, ids, context=context):
            if not asset.location_id:
                raise osv.except_osv(_('Error!'), _('Please specify a location for the asset.'))
            if asset.type == 'normal' and currency_obj.is_zero(
                    cr, uid, asset.company_id.currency_id,
                    asset.value_residual):
                asset.write({'state': 'close'}, context=context)
            else:
                asset.write({'state': 'open'}, context=context)
        return True
    
    def validate(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        currency_obj = self.pool.get('res.currency')
        obj_sequence = self.pool.get('ir.sequence')
        for asset in self.browse(cr, uid, ids, context=context):
            if asset.type == 'normal':
                if currency_obj.is_zero(cr, uid, asset.company_id.currency_id, 
                    asset.value_residual):
                    asset.write({'state': 'close'}, context=context)
                else:
                    asset.write({'state': 'instock'}, context=context)
            if asset.type == 'view':
                asset.write({'state': 'open'}, context=context)
            
            if asset.type == 'normal':
                if asset.code == '/':     
                    new_name = False
                    category = asset.category_id
                    
                    if category.sequence_id:
                        new_name = obj_sequence.next_by_id(cr, uid, category.sequence_id.id)
                    else:
                        raise osv.except_osv(_('Error!'), _('Please define a sequence on the Asset Category.'))
                    
                    if new_name:
                        asset.write({'code':new_name}, context=context)
            if asset.type == 'view':
                if asset.code == '/': 
                    asset.write({'code': ''}, context=context)
                    
        return True
    
    def onchange_purchase_salvage_value(
            self, cr, uid, ids, purchase_value,
            salvage_value, date_start, date_purchase, context=None):
        if not context:
            context = {}
        val = {}
#         if date_purchase and not date_start:
#             date_start = date_purchase
#             val['date_start'] = date_start
        purchase_value = purchase_value or 0.0
        salvage_value = salvage_value or 0.0
        if purchase_value or salvage_value:
            val['asset_value'] = purchase_value - salvage_value
        if ids:
            aadl_obj = self.pool.get('account.asset.depreciation.line')
            dl_create_ids = aadl_obj.search(
                cr, uid, [('type', '=', 'create'), ('asset_id', 'in', ids)])
            dl_depreciate_ids = aadl_obj.search(
                cr, uid, [('type', '=', 'depreciate'), ('move_id','!=',False), ('asset_id', 'in', ids)])
            if dl_depreciate_ids:
                aadl_obj.write(
                    cr, uid, dl_create_ids,
                    {'amount': val['asset_value'], 'line_date': date_start})
            else:
                aadl_obj.write(
                    cr, uid, dl_create_ids,
                    {'amount': val['asset_value'], 'line_date': date_start},
                    context={'allow_asset_line_update': True})
        return {'value': val}

class isf_asset_multiple_validate(osv.osv_memory):
    _name = 'isf.assets.multiple.validate'
    
    def multiple_validate(self, cr, uid, ids, context=None):
        asset_obj = self.pool.get('account.asset.asset')
        active_ids = context['active_ids']
        
        asset_obj.validate(cr, uid, active_ids, context=context)
            
        return True
    
class isf_asset_multiple_compute(osv.osv_memory):
    _name = 'isf.assets.multiple.compute'
    
    def multiple_compute(self, cr, uid, ids, context=None):
        asset_obj = self.pool.get('account.asset.asset')
        active_ids = context['active_ids']
        
        assets = asset_obj.browse(cr, uid, active_ids)
        for asset in assets:
            if asset.type != 'view':
                asset_obj.compute_depreciation_board(cr, uid, [asset.id], context=context)
            
        return True
    
class isf_asset_multiple_revaluate(osv.osv_memory):
    _name = 'isf.assets.multiple.revaluate'
    
    _columns = {
        'percentage' : fields.float('Percentage', digits=(3,2)),
        'date_revaluation': fields.date('Date', required=True),
    }
    
    _defaults = {
        'percentage' : 100.0,
    }
    
    def _check_percentage_value(self, cr, uid, ids, context=None):
        for revaluation in self.browse(cr, uid, ids, context=context):
            if revaluation.percentage < 0 or revaluation.percentage == 100:
                return False
        return True
    
    _constraints = [(
        _check_percentage_value,
        "The percentage cannot be 100 or negative!",
        ['percentage']
    )]
    
    def multiple_revaluate(self, cr, uid, ids, context=None):
        asset_obj = self.pool.get('account.asset.asset')
        asset_revaluation_obj = self.pool.get('account.asset.revaluation.wizard')
        active_ids = context['active_ids']
        wiz_data = self.browse(cr, uid, ids[0], context=context)
        percentage = wiz_data.percentage
        
        if _debug:
            _logger.debug('==> active_ids : %s', active_ids)
            _logger.debug('==> percentage : %s', percentage)
            _logger.debug('==> date_revaluation : %s', wiz_data.date_revaluation)
        
        for asset in asset_obj.browse(cr, uid, active_ids):
            if asset.type == 'view':
                context['active_ids'] = [d['id'] for d in asset.child_ids]
                if _debug:
                    _logger.debug('==> asset.child_ids : %s', context['active_ids'])
                self.multiple_revaluate(cr, uid, ids, context)
            else:
                if _debug:
                    _logger.debug('==> asset.id : %s', asset.id)
                    _logger.debug('==> asset.name : %s', asset.name)
                    _logger.debug('==> asset.purchase_value : %s', asset.purchase_value)
                    _logger.debug('==> asset.asset_value : %s', asset.asset_value)
                    _logger.debug('==> asset.value_residual : %s', asset.value_residual)
                
                context['active_id'] = asset.id
                context['early_removal'] = True
                vals = {
                    'date_revaluation': wiz_data.date_revaluation,
                    'revaluated_value': asset.asset_value * percentage / 100.0,
                    'note': 'test',
                }
                asset_revaluation_id = asset_revaluation_obj.create(cr, uid, vals, context)
                asset_revaluation_obj.revaluate(cr, uid, [asset_revaluation_id], context)
        
        #asset_obj.validate(cr, uid, active_ids, context=context)
            
        return True
