# -*- encoding: utf-8 -*-

{
    'name': ' ISF Asset Extension',
    'version': '0.9',
    'depends': ['isf_cims_module',
                'account_asset_management',
                'account_asset_register'],
    'author': 'Alessandro Domanico <alessandro.domanico@informaticisenzafrontiere.org>',
    'description': """
Hospital Asset Extension
====================================

The module adds these fields:

* Reference Code (automatic upon validation, as defined on the Asset Category)
* Old Reference
* State of Use
* Serial Number
* Location
* Status "In Stock" between "Draft" and "Running"

these features:

* allows changes on asset start date when in 'draft' state and if first depreciation has not been posted yet

these procedures:

* multiple assets computation
* multiple assets validation
* multiple assets revaluation

these reports:

* Fixed Assets Revision Book

these menus:

* CIMS \ Operations \ Assets
* CIMS \ Reports \ 107 - Assets Register
* CIMS \ Reports \ 904 - Fixed Assets Revision Book
    """,
    'license': 'AGPL-3',
    'category': 'Localization',
    'data': ['view/account_asset_view.xml',
             'view/asset_register_view.xml',
             'view/isf_asset_validate_selected_view.xml',
             'view/isf_asset_compute_selected_view.xml',
             'view/isf_asset_revaluate_selected_view.xml',
             'view/isf_asset_location_view.xml',
             'wizard/isf_asset_revision_book_view.xml',
             'security/ir.model.access.csv',
             'data.xml',
            ],
    'demo': [],
    'installable': True,
    'auto_install': False
}
