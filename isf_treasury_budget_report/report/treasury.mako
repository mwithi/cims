<html>
<head>
    <style type="text/css">
        ${css}
		table {
		    border-spacing: 0px;
		    page-break-inside: auto
		}
		
		table tr {
                page-break-inside: avoid; 
                page-break-after: auto;
  		}
		
		table, td, th {
		    border: 1px dotted black;
		}
		
		.none {
			border: 0px;
		}
		
		.section {
			border: 0px;
			font-size: large;
		}
		
		.title {
			border: 0px;
			font-size: medium;
		}
		
		.title_children {
			border: 0px;
			font-size: small;
		}
		
		.subtitle {
			border: 0px;
			font-size: small;
		}
		
		.total {
			border: 2px solid black;
		}
		
		.empty {
			border: 0px;
			border-left: 1px dotted black;
			border-right: 1px dotted black;
		}
		
		.emptyfy {
			border: 0px;
			border-left: 2px solid black;
			border-right: 2px solid black;
		}
		
		.endfy {
			border: 0px;
			border-left: 2px solid black;
			border-right: 2px solid black;
			border-bottom: 2px solid black;
		}
		
		.fy {
			border-left: 2px solid black;
			border-right: 2px solid black;
		}
		
		.posted {
			color: black;
		}
		
		.unposted {
			color: blue;
		}
		
    </style>
</head>
<body>
    <%page expression_filter="entity"/>
    <h1><b><i>Treasury Budget / Cash Flows Monthly Report</i></b></h1>
    <%
    """
    print "IN REPORT!"
    print objects, dir(objects)
    print "*************** LOCALS:"
    pp(locals())
    print date_lines()
    print lines()
    """
    %>
    
    <%def name="indent(level, title)">
	    ${"-" * level + " " * level * 4}${title}
	</%def>
    
    <table style="width: 100%" border="0">
        <tr class="table_header">
            <td colspan="3" height="30px">
                %for date in datelines():
                    Fiscal Year: ${date['fiscal_year']} (Values: ${date['divisor']} ${date['currency']})
                    %if date['target'] == 'all':
                    	<br />
                    	Unposted in blue
                    %endif
                %endfor
            </td>
        </tr>
    </table>

    <table style="width: 100%; font-size: 12px; border: 1px dotted;">
        <tr>
            <th> Ledger Position </th>
            <th> Account </th>
            
            %for month in months_names():
            	<th> ${month.decode('utf-8', errors='replace')} </th>
            %endfor
            
            <th class="total"> FY </th>
            <th> % </th>
        </tr>
        %for line in lines():
        	%if line['type'] == 'section':
			    <tr class="section">
			    
        	%elif line['type'] == 'title':
        		<tr class="title">  
        		
        	%elif line['type'] == 'total':
				<tr class="total">    
			
			%elif line['type'] == 'calc':
				<tr>
				
			%elif line['type'] == 'end':
				<tr>
				
    		%else:
   				<tr>

        	%endif
	        	%if line['type'] == 'section':
	        		<td colspan="14" class="section" height="50px"><strong>${line['title']}</strong></td>
	        		<td class="emptyfy"></td>
	        		
	        	%elif line['type'] == 'title':
	        		%if line['level'] == 0:
	        			<td colspan="14" class="title" height="40px"><strong>${indent(line['level'],line['title'])}</strong></td>
	        		%else:
	        			<td colspan="14" class="title_children" height="30px">${indent(line['level'],line['title'])}</td>
	        		%endif
	        		<td class="emptyfy"></td>
	        	
	        	%elif line['type'] == 'subtitle':
	        		<td class="empty"></td>
	        		<td colspan="13" class="subtitle"><strong>${line['title']}</strong></td>
	        		<td class="emptyfy"></td>
	        		
	        	%elif line['type'] == 'total':
	        		<td class="none" width="5%"></strong></td>
	        		<td width="25%"><strong>${line['title']}</strong></td>
	        	
	        	%elif line['type'] == 'calc':
	        		<td class="none" width="5%"></strong></td>
	        		<td width="25%">${line['title']}</td>
	        		
	        	%elif line['type'] == 'end':
	        		<td class="none" width="5%"></strong></td>
	        		<td width="25%">${line['title']}</td>
	        		
	        	%else:
	        		<td width="5%">${indent(line['level'],line['position'])}</td>
	        		<td width="25%">${line['title']}</td>
	        	%endif
        		%if line['type'] == 'value':
        			%if line['1_posted']:
	        			<td width="5%" align="right" class="posted">${formatLang(line['1'])}</td>
	        		%else:
	        			<td width="5%" align="right" class="unposted">${formatLang(line['1'])}</td>
	  				%endif
	  				%if line['2_posted']:
	        			<td width="5%" align="right" class="posted">${formatLang(line['2'])}</td>
	        		%else:
	        			<td width="5%" align="right" class="unposted">${formatLang(line['2'])}</td>
	  				%endif
	  				%if line['3_posted']:
	        			<td width="5%" align="right" class="posted">${formatLang(line['3'])}</td>
	        		%else:
	        			<td width="5%" align="right" class="unposted">${formatLang(line['3'])}</td>
	  				%endif
	  				%if line['4_posted']:
	        			<td width="5%" align="right" class="posted">${formatLang(line['4'])}</td>
	        		%else:
	        			<td width="5%" align="right" class="unposted">${formatLang(line['4'])}</td>
	  				%endif
	  				%if line['5_posted']:
	        			<td width="5%" align="right" class="posted">${formatLang(line['5'])}</td>
	        		%else:
	        			<td width="5%" align="right" class="unposted">${formatLang(line['5'])}</td>
	  				%endif
	  				%if line['6_posted']:
	        			<td width="5%" align="right" class="posted">${formatLang(line['6'])}</td>
	        		%else:
	        			<td width="5%" align="right" class="unposted">${formatLang(line['6'])}</td>
	  				%endif
	  				%if line['7_posted']:
	        			<td width="5%" align="right" class="posted">${formatLang(line['7'])}</td>
	        		%else:
	        			<td width="5%" align="right" class="unposted">${formatLang(line['7'])}</td>
	  				%endif
	  				%if line['8_posted']:
	        			<td width="5%" align="right" class="posted">${formatLang(line['8'])}</td>
	        		%else:
	        			<td width="5%" align="right" class="unposted">${formatLang(line['8'])}</td>
	  				%endif
	  				%if line['9_posted']:
	        			<td width="5%" align="right" class="posted">${formatLang(line['9'])}</td>
	        		%else:
	        			<td width="5%" align="right" class="unposted">${formatLang(line['9'])}</td>
	  				%endif
	  				%if line['10_posted']:
	        			<td width="5%" align="right" class="posted">${formatLang(line['10'])}</td>
	        		%else:
	        			<td width="5%" align="right" class="unposted">${formatLang(line['10'])}</td>
	  				%endif
	  				%if line['11_posted']:
	        			<td width="5%" align="right" class="posted">${formatLang(line['11'])}</td>
	        		%else:
	        			<td width="5%" align="right" class="unposted">${formatLang(line['11'])}</td>
	  				%endif
	  				%if line['12_posted']:
	        			<td width="5%" align="right" class="posted">${formatLang(line['12'])}</td>
	        		%else:
	        			<td width="5%" align="right" class="unposted">${formatLang(line['12'])}</td>
	  				%endif
	        		<td width="6%" align="right" class="fy">${formatLang(line['13'])}</td>
	        		<td width="4%" align="right" >${formatLang(line['14'])} &#37;</td>
	        	%elif line['type'] == 'total':
	        		<td width="5%" align="right"><strong>${formatLang(line['1'])}</strong></td>
	        		<td width="5%" align="right"><strong>${formatLang(line['2'])}</strong></td>
	        		<td width="5%" align="right"><strong>${formatLang(line['3'])}</strong></td>
	        		<td width="5%" align="right"><strong>${formatLang(line['4'])}</strong></td>
	        		<td width="5%" align="right"><strong>${formatLang(line['5'])}</strong></td>
	        		<td width="5%" align="right"><strong>${formatLang(line['6'])}</strong></td>
	        		<td width="5%" align="right"><strong>${formatLang(line['7'])}</strong></td>
	        		<td width="5%" align="right"><strong>${formatLang(line['8'])}</strong></td>
	        		<td width="5%" align="right"><strong>${formatLang(line['9'])}</strong></td>
	        		<td width="5%" align="right"><strong>${formatLang(line['10'])}</strong></td>
	        		<td width="5%" align="right"><strong>${formatLang(line['11'])}</strong></td>
	        		<td width="5%" align="right"><strong>${formatLang(line['12'])}</strong></td>
	        		<td width="6%" align="right" class="fy"><strong>${formatLang(line['13'])}</strong></td>
	        		<td width="4%" align="right"><strong>${formatLang(line['14'])}  &#37;</strong></td>
	        	%elif line['type'] == 'calc':
	        		<td width="5%" align="right">${formatLang(line['1'])}</td>
	        		<td width="5%" align="right">${formatLang(line['2'])}</td>
	        		<td width="5%" align="right">${formatLang(line['3'])}</td>
	        		<td width="5%" align="right">${formatLang(line['4'])}</td>
	        		<td width="5%" align="right">${formatLang(line['5'])}</td>
	        		<td width="5%" align="right">${formatLang(line['6'])}</td>
	        		<td width="5%" align="right">${formatLang(line['7'])}</td>
	        		<td width="5%" align="right">${formatLang(line['8'])}</td>
	        		<td width="5%" align="right">${formatLang(line['9'])}</td>
	        		<td width="5%" align="right">${formatLang(line['10'])}</td>
	        		<td width="5%" align="right">${formatLang(line['11'])}</td>
	        		<td width="5%" align="right">${formatLang(line['12'])}</td>
	        		<td width="6%" align="right" class="fy">${formatLang(line['13'])}</td>
	        		<td width="4%" align="right">${formatLang(line['14'])}</td>
	        	%elif line['type'] == 'end':
	        		<td width="5%" align="right">${formatLang(line['1'])}</td>
	        		<td width="5%" align="right">${formatLang(line['2'])}</td>
	        		<td width="5%" align="right">${formatLang(line['3'])}</td>
	        		<td width="5%" align="right">${formatLang(line['4'])}</td>
	        		<td width="5%" align="right">${formatLang(line['5'])}</td>
	        		<td width="5%" align="right">${formatLang(line['6'])}</td>
	        		<td width="5%" align="right">${formatLang(line['7'])}</td>
	        		<td width="5%" align="right">${formatLang(line['8'])}</td>
	        		<td width="5%" align="right">${formatLang(line['9'])}</td>
	        		<td width="5%" align="right">${formatLang(line['10'])}</td>
	        		<td width="5%" align="right">${formatLang(line['11'])}</td>
	        		<td width="5%" align="right">${formatLang(line['12'])}</td>
	        		<td width="6%" align="right" class="endfy">${formatLang(line['13'])}</td>
	        		<td width="4%" align="right">${formatLang(line['14'])}</td>
	        	%else:
	        		<!-- td colspan="14"></td -->
	        	%endif
        	</tr>
        %endfor
    </table>
    %for date in datelines():
        %if date['warning']:
        	<br />
        	%if len(excluded()) > 0:
	        <table width="50%">
		            <tr class="total">
		        		<td>WARNING!!! Some Ledger Positions are not included, please inform System Administrator</td>
		            </tr> 
		            %for account in excluded():
		            <tr>
		            	<td>${account['account_code']} - ${account['ref']} - ${account['note']}</td>
		            </tr>
		            %endfor
		    </table>
		    %endif
		    <br />
		    %if len(duplicated()) > 0:
		    <table width="50%">
		        
			        <tr class="total">
		        		<td>WARNING!!! Some Ledger Positions are duplicated, please inform System Administrator</td>
		            </tr> 
		            %for account in duplicated():
		            <tr>
		            	<td>${account['account_code']} - ${account['ref']} - ${account['note']}</td>
		            </tr>
		            %endfor
	        </table>
	        %endif
    	%endif
   	%endfor
</body>
</html>
